package com.ruoyi.tableConfig.api;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.tableConfig.api.domain.*;
import com.ruoyi.tableConfig.api.factory.RemoteRoleTableFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(contextId = "remoteRoleTableService", value = ServiceNameConstants.SYSTEM_TABLECONFIG, fallbackFactory = RemoteRoleTableFallbackFactory.class)
public interface RemoteRoleTableService {
    /**
     * 获取表格权限业务表详细信息
     *
     * @param roleTableId 角色表格id
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTable/{roleTableId}")
    public R<SysRoleTable> getRoleTableInfo(@PathVariable("roleTableId") Long roleTableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取表格权限业务表详细信息
     *
     * @param roleId 角色id
     * @param source 请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTable/getRoleTableByRoleId/{roleId}")
    public R<List<SysRoleTable>> getRoleTableInfoByRoleId(@PathVariable("roleId") Long roleId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取表格权限业务表详细信息
     *
     * @param tableId 表格id
     * @param source  请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTable/getRoleTableByTableId/{tableId}")
    public R<List<SysRoleTable>> getRoleTableInfoByTableId(@PathVariable("tableId") Long tableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取表格权限业务表详细信息
     *
     * @param menuId 菜单id
     * @param source 请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTable/getRoleTableByMenuId/{menuId}")
    public R<SysRoleTable> getRoleTableInfoByMenuId(@PathVariable("menuId") Long menuId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取表格权限业务表详细信息
     *
     * @param sysRoleTable
     * @param source       请求来源
     * @return
     */
    @PostMapping("/remoteRole/roleTable/getRoleTable")
    public R<SysRoleTable> getRoleTableInfo(@RequestBody SysRoleTable sysRoleTable, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 新增角色表格权限业务表
     *
     * @param sysRoleTable
     * @param source
     * @return
     */
    @PostMapping("/remoteRole/roleTable")
    public R<SysRoleTable> roleTableAdd(@RequestBody SysRoleTable sysRoleTable, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 修改角色表格权限业务表
     *
     * @param sysRoleTable
     * @param source
     * @return
     */
    @PutMapping("/remoteRole/roleTable")
    public R<SysRoleTable> roleTableEdit(@RequestBody SysRoleTable sysRoleTable, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 删除角色表格权限业务表
     *
     * @param roleTableIds
     * @param source
     * @return
     */
    @DeleteMapping("/remoteRole/roleTable/{roleTableIds}")
    public R<Integer> roleTableRemoves(@PathVariable(value = "roleTableIds", required = true) Long[] roleTableIds, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);


    /**
     * 获取表格权限列配置详细信息
     *
     * @param roleColumnId 表格权限列id
     * @param source       请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableColumn/{roleColumnId}")
    public R<SysRoleTableColumn> getColumnInfoByRoleColumnId(@PathVariable("roleColumnId") Long roleColumnId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取表格权限列配置详细信息
     *
     * @param columnId    表格列id
     * @param roleTableId 角色表格id
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableColumn/{roleTableId}/{columnId}")
    public R<SysRoleTableColumn> getColumnInfoByColumnId(@PathVariable("columnId") Long columnId, @PathVariable("roleTableId") Long roleTableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取表格权限列配置详细信息
     *
     * @param roleTableId 角色表格id
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableColumn/getColumnByTableId/{roleTableId}")
    public R<List<SysRoleTableColumn>> getColumnInfoByRoleTableId(@PathVariable("roleTableId") Long roleTableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);


    /**
     * 新增表格权限列配置
     *
     * @param sysRoleTableColumn
     * @param source
     * @return
     */
    @PostMapping("/remoteRole/roleTableColumn")
    public R<SysRoleTableColumn> columnAdd(@RequestBody SysRoleTableColumn sysRoleTableColumn, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 修改表格权限列配置
     *
     * @param sysRoleTableColumn
     * @param source
     * @return
     */
    @PutMapping("/remoteRole/roleTableColumn")
    public R<SysRoleTableColumn> columnEdit(@RequestBody SysRoleTableColumn sysRoleTableColumn, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);


    /**
     * 获取角色可编辑配置项详细信息
     *
     * @param roleEditConfigId 角色可编辑配置项id
     * @param source           请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleEditConfig/{roleEditConfigId}")
    public R<SysRoleTableEditConfig> getEditConfigInfo(@PathVariable("roleEditConfigId") Long roleEditConfigId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取角色可编辑配置项详细信息
     *
     * @param roleTableId 角色表格id
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleEditConfig/getEditConfigByTableId/{roleTableId}")
    public R<SysRoleTableEditConfig> getEditConfigInfoByTableId(@PathVariable("roleTableId") Long roleTableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 新增角色可编辑配置项
     *
     * @param sysRoleTableEditConfig
     * @param source
     * @return
     */
    @PostMapping("/remoteRole/roleEditConfig")
    public R<SysRoleTableEditConfig> editConfigAdd(@RequestBody SysRoleTableEditConfig sysRoleTableEditConfig, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 修改角色可编辑配置项
     *
     * @param sysRoleTableEditConfig
     * @param source
     * @return
     */
    @PutMapping("/remoteRole/roleEditConfig")
    public R<SysRoleTableEditConfig> editConfigEdit(@RequestBody SysRoleTableEditConfig sysRoleTableEditConfig, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);


    /**
     * 获取角色校验规则配置项详细信息
     *
     * @param roleEditRulesId 角色校验规则配置id
     * @param source          请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableRules/{roleEditRulesId}")
    public R<SysRoleTableEditRules> getEditRulesInfo(@PathVariable("roleEditRulesId") Long roleEditRulesId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取角色校验规则配置项详细信息
     *
     * @param roleTableId 角色表格id
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableRules/getEditRulesByTableId/{roleTableId}")
    public R<List<SysRoleTableEditRules>> getEditRulesInfoByRoleTableId(@PathVariable("roleTableId") Long roleTableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 新增角色校验规则配置项
     *
     * @param sysRoleTableEditRules
     * @param source
     * @return
     */
    @PostMapping("/remoteRole/roleTableRules")
    public R<SysRoleTableEditRules> roleRuleAdd(@RequestBody SysRoleTableEditRules sysRoleTableEditRules, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 修改角色校验规则配置项
     *
     * @param sysRoleTableEditRules
     * @param source
     * @return
     */
    @PutMapping("/remoteRole/roleTableRules")
    public R<SysRoleTableEditRules> roleRuleEdit(@RequestBody SysRoleTableEditRules sysRoleTableEditRules, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取角色功能表查询表详细信息
     *
     * @param roleFormId 角色表单id
     * @param source     请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableForm/{roleFormId}")
    public R<SysRoleTableForm> getRoleFormInfo(@PathVariable("roleFormId") Long roleFormId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取角色功能表查询表详细信息
     *
     * @param roleTableId 角色表单id
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableForm/getRoleFormByTableId/{roleTableId}")
    public R<SysRoleTableForm> getRoleFormInfoByRoleTableId(@PathVariable("roleTableId") Long roleTableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 新增角色功能表查询表
     *
     * @param sysRoleTableForm
     * @param source
     * @return
     */
    @PostMapping("/remoteRole/roleTableForm")
    public R<SysRoleTableForm> roleTableFormAdd(@RequestBody SysRoleTableForm sysRoleTableForm, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 修改角色功能表查询表
     *
     * @param sysRoleTableForm
     * @param source
     * @return
     */
    @PutMapping("/remoteRole/roleTableForm")
    public R<SysRoleTableForm> roleTableFormEdit(@RequestBody SysRoleTableForm sysRoleTableForm, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);


    /**
     * 获取角色表单配置项列表详细信息
     *
     * @param roleItemId 角色表单配置项列表id
     * @param source     请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableFormItem/{roleItemId}")
    public R<SysRoleTableFormItem> getRoleItemInfo(@PathVariable("roleItemId") Long roleItemId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取角色表单配置项列表详细信息
     *
     * @param roleFormId 角色表单id
     * @param source     请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableFormItem/getRoleItemByFormId/{roleFormId}")
    public R<List<SysRoleTableFormItem>> getRoleItemInfoByFormId(@PathVariable("roleFormId") Long roleFormId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 新增角色表单配置项列表
     *
     * @param sysRoleTableFormItem
     * @param source
     * @return
     */
    @PostMapping("/remoteRole/roleTableFormItem")
    public R<SysRoleTableFormItem> roleFormItemAdd(@RequestBody SysRoleTableFormItem sysRoleTableFormItem, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 修改角色表单配置项列表
     *
     * @param sysRoleTableFormItem
     * @param source
     * @return
     */
    @PutMapping("/remoteRole/roleTableFormItem")
    public R<SysRoleTableFormItem> roleFormItemEdit(@RequestBody SysRoleTableFormItem sysRoleTableFormItem, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取角色功能表分页配置详细信息
     *
     * @param rolePagerId 角色分页id
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTablePager/{rolePagerId}")
    public R<SysRoleTablePager> getRolePagerInfo(@PathVariable("rolePagerId") Long rolePagerId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取角色功能表分页配置详细信息
     *
     * @param roleTableId 角色表格id
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTablePager/getRolePagerByTableId/{roleTableId}")
    public R<SysRoleTablePager> getRolePagerInfoByTableId(@PathVariable("roleTableId") Long roleTableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 新增角色功能表分页配置
     *
     * @param sysRoleTablePager
     * @param source
     * @return
     */
    @PostMapping("/remoteRole/roleTablePager")
    public R<SysRoleTablePager> rolePageAdd(@RequestBody SysRoleTablePager sysRoleTablePager, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 修改角色功能表分页配置
     *
     * @param sysRoleTablePager
     * @param source
     * @return
     */
    @PutMapping("/remoteRole/roleTablePager")
    public R<SysRoleTablePager> rolePageEdit(@RequestBody SysRoleTablePager sysRoleTablePager, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取角色功能表数据代理详细信息
     *
     * @param roleProxyId 角色代理id
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableProxy/{roleProxyId}")
    public R<SysRoleTableProxy> getRoleProxyInfo(@PathVariable("roleProxyId") Long roleProxyId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取角色功能表数据代理详细信息
     *
     * @param roleTableId 角色表格id
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableProxy/getRoleProxyByTableId/{roleTableId}")
    public R<SysRoleTableProxy> getRoleProxyInfoByTableId(@PathVariable("roleTableId") Long roleTableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 新增角色功能表数据代理
     *
     * @param sysRoleTableProxy
     * @param source
     * @return
     */
    @PostMapping("/remoteRole/roleTableProxy")
    public R<SysRoleTableProxy> roleProxyAdd(@RequestBody SysRoleTableProxy sysRoleTableProxy, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 修改角色功能表数据代理
     *
     * @param sysRoleTableProxy
     * @param source
     * @return
     */
    @PutMapping("/remoteRole/roleTableProxy")
    public R<SysRoleTableProxy> roleProxyEdit(@RequestBody SysRoleTableProxy sysRoleTableProxy, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取角色工具栏配置详细信息
     *
     * @param roleToolbarConfigId 角色工具栏配置id
     * @param source              请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableToolbarConfig/{roleToolbarConfigId}")
    public R<SysRoleTableToolbarConfig> getRoleToolBarConfigInfo(@PathVariable("roleToolbarConfigId") Long roleToolbarConfigId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取角色工具栏配置详细信息
     *
     * @param roleTableId 角色表格id
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteRole/roleTableToolbarConfig/{roleTableId}")
    public R<SysRoleTableToolbarConfig> getRoleToolBarConfigInfoByTableId(@PathVariable("roleTableId") Long roleTableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 新增角色工具栏配置
     *
     * @param sysRoleTableToolbarConfig
     * @param source
     * @return
     */
    @PostMapping("/remoteRole/roleTableToolbarConfig")
    public R<SysRoleTableToolbarConfig> roleToolBarAdd(@RequestBody SysRoleTableToolbarConfig sysRoleTableToolbarConfig, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 修改角色工具栏配置
     *
     * @param sysRoleTableToolbarConfig
     * @param source
     * @return
     */
    @PutMapping("/remoteRole/roleTableToolbarConfig")
    public R<SysRoleTableToolbarConfig> roleToolBarEdit(@RequestBody SysRoleTableToolbarConfig sysRoleTableToolbarConfig, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
