package com.ruoyi.tableConfig.api.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * 功能表数据代理对象 sys_table_proxy
 *
 * @author zly
 * @date 2023-04-25
 */
@Data
@TableName("sys_table_proxy")
public class SysTableProxy extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 代理表id
     */
    @ExcelProperty(value = "代理表id")
    @TableId(value = "proxy_id", type = IdType.AUTO)
    private Long proxyId;

    /**
     * 表格id
     */
    @ExcelProperty(value = "表格id")
    private Long tableId;

    /**
     * 是否启用
     */
    @ExcelProperty(value = "是否启用")
    private Boolean enabled;

    /**
     * 是否自动加载查询数据
     */
    @ExcelProperty(value = "是否自动加载查询数据")
    private Boolean autoLoad;

    /**
     * 额外的请求参数改变时是否重新请求数据
     */
    @ExcelProperty(value = "额外的请求参数改变时是否重新请求数据")
    private Boolean paramsChangeLoad;

    /**
     * 是否显示内置的消息提示（可以设为
     */
    @ExcelProperty(value = "是否显示内置的消息提示")
    private Boolean message;

    /**
     * 存在 type=index 列时有效，是否代理动态序号（根据分页动态变化
     */
    @ExcelProperty(value = "存在 type=index 列时有效，是否代理动态序号（根据分页动态变化")
    private Boolean seq;

    /**
     * 是否代理排序
     */
    @ExcelProperty(value = "是否代理排序")
    private Boolean sort;

    /**
     * 是否代理筛选
     */
    @ExcelProperty(value = "是否代理筛选")
    private Boolean filter;

    /**
     * 是否代理表单
     */
    @ExcelProperty(value = "是否代理表单")
    private Boolean form;

    /**
     * 获取的属性配置
     */
    @ExcelProperty(value = "获取的属性配置")
    private String props;

    /**
     * 查询配置
     */
    @ExcelProperty(value = "查询配置")
    @TableField(value = "querybase")
    private String query;

    /**
     * 全量查询配置
     */
    @ExcelProperty(value = "全量查询配置")
    private String queryAll;

    /**
     * 删除配置
     */
    @ExcelProperty(value = "删除配置")
    @TableField(value = "deletebase")
    private String delete;

    /**
     * 插入配置
     */
    @ExcelProperty(value = "插入配置")
    @TableField(value = "insertbase")
    private String insert;

    /**
     * 更新配置
     */
    @ExcelProperty(value = "更新配置")
    @TableField(value = "updatebase")
    private String update;

    /**
     * 保存
     */
    @ExcelProperty(value = "保存")
    private String save;

}
