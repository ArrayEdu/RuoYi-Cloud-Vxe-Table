package com.ruoyi.tableConfig.api.factory;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.tableConfig.api.RemoteUserTableService;
import com.ruoyi.tableConfig.api.domain.SysUserTableColumn;
import com.ruoyi.tableConfig.api.domain.SysUserTableFormItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户表格服务降级处理
 *
 * @author zly
 */
@Component
public class RemoteUserTableFallbackFactory implements FallbackFactory<RemoteUserTableService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteTableFallbackFactory.class);

    @Override
    public RemoteUserTableService create(Throwable throwable) {
        log.error("用户表格服务调用失败:{}", throwable.getMessage());
        return new RemoteUserTableService() {

            @Override
            public R<List<SysUserTableColumn>> getUserColumnInfo(SysUserTableColumn sysUserTableColumn, String source) {
                return R.fail("获取用户表格列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysUserTableColumn> getColumnInfoByColumnId(Long userColumnId, String source) {
                return R.fail("获取用户表格列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysUserTableColumn>> getColumnInfoByRoleTableId(Long roleTableId, String source) {
                return R.fail("通过角色表格id获取用户表格列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysUserTableColumn>> getColumnInfoByUserId(Long userId, String source) {
                return R.fail("通过用户id获取用户表格列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<Integer> userTableColumnAdd(SysUserTableColumn sysUserTableColumn, String source) {
                return R.fail("新增用户表格列配置失败:" + throwable.getMessage());
            }

            @Override
            public R<Integer> userTableColumnEdit(SysUserTableColumn sysUserTableColumn, String source) {
                return R.fail("修改用户表格列配置失败:" + throwable.getMessage());
            }

            @Override
            public R<Boolean> userTableColumnRemoves(Long[] userColumnIds, String source) {
                return R.fail("删除用户表格列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<Integer> userTableColumnRemoveByUserId(Long userId, String source) {
                return R.fail("通过用户id删除用户表格列配置失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysUserTableFormItem>> getFormItemInfo(SysUserTableFormItem sysUserTableFormItem, String source) {
                return R.fail("获取用户表单列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysUserTableFormItem> getFormItemInfoByUserItemId(Long userItemId, String source) {
                return R.fail("获取用户表单列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysUserTableFormItem>> getFormItemInfoByRoleFormId(Long roleFormId, String source) {
                return R.fail("通过角色表单id获取用户表单列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysUserTableFormItem>> getFormItemInfoByUserId(Long userId, String source) {
                return R.fail("通过用户id获取用户表单列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<Integer> userTableFormItemAdd(SysUserTableFormItem sysUserTableFormItem, String source) {
                return R.fail("新增用户表单列配置失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysUserTableFormItem>> userTableFormItemEdit(SysUserTableFormItem sysUserTableFormItem, String source) {
                return R.fail("修改用户表单列配置失败:" + throwable.getMessage());
            }

            @Override
            public R<Boolean> userTableFormItemRemoves(Long[] userItemIds, String source) {
                return R.fail("删除用户表单列配置失败:" + throwable.getMessage());
            }

            @Override
            public R<Integer> userTableFormItemRemoveByUserId(Long userId, String source) {
                return R.fail("通过用户id删除用户表单列配置失败:" + throwable.getMessage());
            }

            @Override
            public R asyncUserTable(Boolean update, String source) {
                return R.fail("同步用户表格数据失败:" + throwable.getMessage());
            }
        };
    }
}