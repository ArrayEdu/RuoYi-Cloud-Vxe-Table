package com.ruoyi.tableConfig.api;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.tableConfig.api.domain.*;
import com.ruoyi.tableConfig.api.factory.RemoteTableFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(contextId = "remoteTableService", value = ServiceNameConstants.SYSTEM_TABLECONFIG, fallbackFactory = RemoteTableFallbackFactory.class)
public interface RemoteTableService {

    /**
     * 获取功能表格详细信息
     *
     * @param tableId 表格id
     * @param source  请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTable/{tableId}")
    public R<SysTable> getTableInfo(@PathVariable("tableId") Long tableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过菜单id获取表格数据
     *
     * @param menuId 菜单id
     * @param source 请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTable/getTableByMenuId/{menuId}")
    public R<List<SysTable>> getTableInfoByMenuId(@PathVariable("menuId") Long menuId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取功能表格列配置详细信息
     *
     * @param columnId 列id
     * @param source   请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableColumn/{columnId}")
    public R<SysTableColumn> getColumnInfo(@PathVariable("columnId") Long columnId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过表格id获取功能表格列配置详细信息
     *
     * @param tableId 表格id
     * @param source  请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableColumn/getColumnByTableId/{tableId}")
    public R<List<SysTableColumn>> getColumnInfoByTableId(@PathVariable("tableId") Long tableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取可编辑配置项详细信息
     *
     * @param editConfigId 可编辑配置项id
     * @param source       请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableEditConfig/{editConfigId}")
    public R<SysTableEditConfig> getEditConfigInfo(@PathVariable("editConfigId") Long editConfigId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过表格id获取可编辑配置项详细信息
     *
     * @param tableId 表格id
     * @param source  请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableEditConfig/getEditConfigByTableId/{tableId}")
    public R<SysTableEditConfig> getEditConfigInfoByTableId(@PathVariable("tableId") Long tableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取校验规则配置项详细信息
     *
     * @param editRulesId 校验规则配置id
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableRules/{editRulesId}")
    public R<SysTableEditRules> getEditRulesInfo(@PathVariable("editRulesId") Long editRulesId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过表格id获取校验规则配置项详细信息
     *
     * @param tableId 表格id
     * @param source  请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableRules/getEditRulesByTableId/{tableId}")
    public R<List<SysTableEditRules>> getEditRulesInfoByTableId(@PathVariable("tableId") Long tableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取功能表查询详细信息
     *
     * @param formId 表单id
     * @param source 请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableForm/{formId}")
    public R<SysTableForm> getFormInfo(@PathVariable("formId") Long formId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过表格id获取功能表查询详细信息
     *
     * @param tableId 表格id
     * @param source  请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableForm/getFormByTableId/{tableId}")
    public R<SysTableForm> getFormInfoByTableId(@PathVariable("tableId") Long tableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取表单配置项列详细信息
     *
     * @param itemId 表单配置项列id
     * @param source 请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableFormItem/{itemId}")
    public R<SysTableFormItem> getFormItemInfo(@PathVariable("itemId") Long itemId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过表单id获取表单配置项列详细信息
     *
     * @param formId 表单id
     * @param source 请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableFormItem/getFormItemByFormId/{formId}")
    public R<List<SysTableFormItem>> getFormItemInfoByFormId(@PathVariable("formId") Long formId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取功能表分页配置详细信息
     *
     * @param pagerId 功能表分页配置id
     * @param source  请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTablePager/{pagerId}")
    public R<SysTablePager> getPageInfo(@PathVariable("pagerId") Long pagerId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过表格id获取功能表分页配置详细信息
     *
     * @param tableId 表格id
     * @param source  请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTablePager/getPagerByTableId/{tableId}")
    public R<SysTablePager> getPageInfoByTableId(@PathVariable("tableId") Long tableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取功能表数据代理详细信息
     *
     * @param proxyId 功能表数据代理id
     * @param source  请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableProxy/{proxyId}")
    public R<SysTableProxy> getProxyInfo(@PathVariable("proxyId") Long proxyId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过表格id获取功能表数据代理详细信息
     *
     * @param tableId 表格id
     * @param source  请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableProxy/getProxyByTableId/{tableId}")
    public R<SysTableProxy> getProxyInfoByTableId(@PathVariable("tableId") Long tableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取工具栏配置详细信息
     *
     * @param toolbarConfigId 工具栏配置id
     * @param source          请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableToolbarConfig/{toolbarConfigId}")
    public R<SysTableToolbarConfig> getToolBarConfigInfo(@PathVariable("toolbarConfigId") Long toolbarConfigId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过表格id获取工具栏配置详细信息
     *
     * @param tableId 表格id
     * @param source  请求来源
     * @return
     */
    @GetMapping("/remoteTable/menuTableToolbarConfig/getToolBarByTableId/{tableId}")
    public R<SysTableToolbarConfig> getToolBarConfigInfoByTableId(@PathVariable("tableId") Long tableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
