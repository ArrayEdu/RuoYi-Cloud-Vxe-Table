package com.ruoyi.tableConfig.api.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * 表单配置项列对象 sys_table_form_item
 *
 * @author zly
 * @date 2023-04-25
 */
@Data
@TableName("sys_table_form_item")
public class SysTableFormItem extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 表单项id
     */
    @TableId(value = "item_id", type = IdType.AUTO)
    private Long itemId;

    /**
     * 表单id
     */
    @ExcelProperty(value = "表单id")
    private Long formId;

    /**
     * 字段名
     */
    @ExcelProperty(value = "字段名")
    private String field;

    /**
     * 标题
     */
    @ExcelProperty(value = "标题")
    private String title;

    /**
     * 栅格占据的列数（共
     */
    @ExcelProperty(value = "栅格占据的列数")
    private Integer span;

    /**
     * 内容对齐方式
     */
    @ExcelProperty(value = "内容对齐方式")
    private String align;

    /**
     * 标题对齐方式
     */
    @ExcelProperty(value = "标题对齐方式")
    private String titleAlign;

    /**
     * 标题宽度
     */
    @ExcelProperty(value = "标题宽度")
    private Integer titleWidth;

    /**
     * 是否显示标题冒号
     */
    @ExcelProperty(value = "是否显示标题冒号")
    private Boolean titleColon;

    /**
     * 是否显示必填字段的红色星号
     */
    @ExcelProperty(value = "是否显示必填字段的红色星号")
    private Boolean titleAsterisk;

    /**
     * 标题内容过长时显示为省略号
     */
    @ExcelProperty(value = "标题内容过长时显示为省略号")
    private String titleOverflow;

    /**
     * 是否显示标题
     */
    @ExcelProperty(value = "是否显示标题")
    private Boolean showTitle;

    /**
     * 给表单项附加
     */
    @ExcelProperty(value = "给表单项附加")
    private String className;

    /**
     * 给表单项内容附加
     */
    @ExcelProperty(value = "给表单项内容附加")
    private String contentClassName;

    /**
     * 给表单项内容附加样式
     */
    @ExcelProperty(value = "给表单项内容附加样式")
    private String contentStyle;

    /**
     * 默认是否显示
     */
    @ExcelProperty(value = "默认是否显示")
    private Boolean visible;

    /**
     * 该方法的返回值用来决定该项是否显示
     */
    @ExcelProperty(value = "该方法的返回值用来决定该项是否显示")
    private String visibleMethod;

    /**
     * 默认收起
     */
    @ExcelProperty(value = "默认收起")
    private Boolean folding;

    /**
     * 折叠节点
     */
    @ExcelProperty(value = "折叠节点")
    private Boolean collapseNode;

    /**
     * 重置时的默认值
     */
    @ExcelProperty(value = "重置时的默认值")
    private String resetValue;

    /**
     * 项渲染器配置项
     */
    @ExcelProperty(value = "项渲染器配置项")
    private String itemRender;

    /**
     * 项集合
     */
    @ExcelProperty(value = "项集合")
    private String children;

    /**
     * 插槽
     */
    @ExcelProperty(value = "插槽")
    private String slots;

    /**
     * 排序
     */
    @ExcelProperty(value = "排序")
    private Integer sortNo;

    /**
     * 字典类型
     */
    @ExcelProperty(value = "字典类型")
    private String dictType;

    /**
     * 是否显示删除按钮
     */
    @ExcelProperty(value = "是否显示删除按钮")
    private Boolean formFilter;

    /**
     * 是否为查询操作项
     */
    @ExcelProperty(value = "是否为查询操作项")
    private Boolean searchHandle;

    /**
     * 查询排序
     */
    @ExcelProperty(value = "查询排序")
    private Integer searchNo;

    /**
     * 是否是查询项
     */
    @ExcelProperty(value = "是否是查询项")
    private Boolean searchVisible;

    /**
     * 是否为固定查询
     */
    @ExcelProperty(value = "是否为固定查询")
    private Boolean searchFixed;


}
