package com.ruoyi.tableConfig.api.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * 校验规则配置项对象 sys_table_edit_rules
 *
 * @author zly
 * @date 2023-04-25
 */
@Data
@TableName("sys_table_edit_rules")
public class SysTableEditRules extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 校验规则配置id
     */
    @TableId(value = "edit_rules_id", type = IdType.AUTO)
    private Long editRulesId;

    /**
     * 表id
     */
    @ExcelProperty(value = "表id")
    private Long tableId;

    /**
     * 是否必填
     */
    @ExcelProperty(value = "是否必填")
    private Boolean required;

    /**
     * 校验值最小长度
     */
    @ExcelProperty(value = "校验值最小长度")
    private Long min;

    /**
     * 校验值最大长度
     */
    @ExcelProperty(value = "校验值最大长度")
    private Long max;

    /**
     * 数据校验的类型
     */
    @ExcelProperty(value = "数据校验的类型")
    private String type;

    /**
     * 正则校验
     */
    @ExcelProperty(value = "正则校验")
    private String pattern;

    /**
     * 自定义校验方法
     */
    @ExcelProperty(value = "自定义校验方法")
    private String validator;

    /**
     * 校验提示内容
     */
    @ExcelProperty(value = "校验提示内容")
    private String message;

}
