package com.ruoyi.tableConfig.api.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 角色功能表查询表对象 sys_role_table_form
 *
 * @author zly
 * @date 2023-06-06
 */
@Data
@TableName("sys_role_table_form")
public class SysRoleTableForm extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 查询表id
     */
    @TableId(value = "role_form_id", type = IdType.AUTO)
    private Long roleFormId;
    /**
     * 菜单查询表id
     */
    @ExcelProperty(value = "菜单查询表id")
    private Long formId;

    /**
     * 表格id
     */
    @ExcelProperty(value = "表格id")
    private Long roleTableId;

    /**
     * 所有项的栅格占据的列数（共
     */
    @ExcelProperty(value = "所有项的栅格占据的列数")
    private Integer span;

    /**
     * 所有项的内容对齐方式
     */
    @ExcelProperty(value = "所有项的内容对齐方式")
    private String align;

    /**
     * 尺寸
     */
    @ExcelProperty(value = "尺寸")
    private String size;

    /**
     * 所有项的标题对齐方式
     */
    @ExcelProperty(value = "所有项的标题对齐方式")
    private String titleAlign;

    /**
     * 所有项的标题宽度
     */
    @ExcelProperty(value = "所有项的标题宽度")
    private String titleWidth;

    /**
     * 是否显示标题冒号
     */
    @ExcelProperty(value = "是否显示标题冒号")
    private Boolean titleColon;

    /**
     * 是否显示必填字段的红色星号
     */
    @ExcelProperty(value = "是否显示必填字段的红色星号")
    private Boolean titleAsterisk;

    /**
     * 所有设置标题内容过长时显示为省略号
     */
    @ExcelProperty(value = "所有设置标题内容过长时显示为省略号")
    private String titleOverflow;

    /**
     * 给表单附加
     */
    @ExcelProperty(value = "给表单附加")
    private String className;

    /**
     * v-model
     */
    @ExcelProperty(value = "v-model")
    private Boolean collapseStatus;

    /**
     * 是否使用自定义布局
     */
    @ExcelProperty(value = "是否使用自定义布局")
    private Boolean customLayout;

    /**
     * 是否禁用默认的回车提交方式禁用后配合
     */
    @ExcelProperty(value = "是否禁用默认的回车提交方式禁用后配合")
    private Boolean preventSubmit;

    /**
     * 是否启用
     */
    @ExcelProperty(value = "是否启用")
    private Boolean enabled;

    /**
     * 校验规则配置项
     */
    @ExcelProperty(value = "校验规则配置项")
    private String rules;

    /**
     * 检验配置项
     */
    @ExcelProperty(value = "检验配置项")
    private String validConfig;

    /**
     * 表单列
     */
    @TableField(exist = false)
    private List<SysUserTableFormItem> items;

    @TableField(exist = false)
    private List<SysUserTableFormItem> filterItems;

}
