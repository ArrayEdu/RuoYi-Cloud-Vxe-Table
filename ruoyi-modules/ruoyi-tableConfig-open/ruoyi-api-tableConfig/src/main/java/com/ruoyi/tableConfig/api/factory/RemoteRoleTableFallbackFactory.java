package com.ruoyi.tableConfig.api.factory;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.tableConfig.api.RemoteRoleTableService;
import com.ruoyi.tableConfig.api.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色表格服务降级处理
 *
 * @author zly
 */
@Component
public class RemoteRoleTableFallbackFactory implements FallbackFactory<RemoteRoleTableService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteRoleTableFallbackFactory.class);

    @Override
    public RemoteRoleTableService create(Throwable throwable) {
        log.error("角色表格服务调用失败:{}", throwable.getMessage());
        return new RemoteRoleTableService() {
            @Override
            public R<SysRoleTable> getRoleTableInfo(Long roleTableId, String source) {
                return R.fail("获取表格权限业务表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysRoleTable>> getRoleTableInfoByRoleId(Long roleId, String source) {
                return R.fail("获取表格权限业务表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysRoleTable>> getRoleTableInfoByTableId(Long tableId, String source) {
                return R.fail("获取表格权限业务表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTable> getRoleTableInfoByMenuId(Long menuId, String source) {
                return R.fail("获取表格权限业务表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTable> getRoleTableInfo(SysRoleTable sysRoleTable, String source) {
                return R.fail("获取表格权限业务表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTable> roleTableAdd(SysRoleTable sysRoleTable, String source) {
                return R.fail("获取表格权限业务表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTable> roleTableEdit(SysRoleTable sysRoleTable, String source) {
                return R.fail("获取表格权限业务表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<Integer> roleTableRemoves(Long[] roleTableIds, String source) {
                return R.fail("删除表格权限业务表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableColumn> getColumnInfoByRoleColumnId(Long roleColumnId, String source) {
                return R.fail("获取表格权限列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableColumn> getColumnInfoByColumnId(Long columnId, Long roleTableId, String source) {
                return R.fail("获取表格权限列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysRoleTableColumn>> getColumnInfoByRoleTableId(Long roleTableId, String source) {
                return R.fail("获取表格权限列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableColumn> columnAdd(SysRoleTableColumn sysRoleTableColumn, String source) {
                return R.fail("获取表格权限列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableColumn> columnEdit(SysRoleTableColumn sysRoleTableColumn, String source) {
                return R.fail("获取表格权限列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableEditConfig> getEditConfigInfo(Long roleEditConfigId, String source) {
                return R.fail("获取角色可编辑配置项详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableEditConfig> getEditConfigInfoByTableId(Long roleTableId, String source) {
                return R.fail("获取角色可编辑配置项详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableEditConfig> editConfigAdd(SysRoleTableEditConfig sysRoleTableEditConfig, String source) {
                return R.fail("获取角色可编辑配置项详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableEditConfig> editConfigEdit(SysRoleTableEditConfig sysRoleTableEditConfig, String source) {
                return R.fail("获取角色可编辑配置项详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableEditRules> getEditRulesInfo(Long roleEditRulesId, String source) {
                return R.fail("获取角色校验规则配置项详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysRoleTableEditRules>> getEditRulesInfoByRoleTableId(Long roleTableId, String source) {
                return R.fail("获取角色校验规则配置项详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableEditRules> roleRuleAdd(SysRoleTableEditRules sysRoleTableEditRules, String source) {
                return R.fail("获取角色校验规则配置项详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableEditRules> roleRuleEdit(SysRoleTableEditRules sysRoleTableEditRules, String source) {
                return R.fail("获取角色校验规则配置项详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableForm> getRoleFormInfo(Long roleFormId, String source) {
                return R.fail("获取角色功能表查询表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableForm> getRoleFormInfoByRoleTableId(Long roleTableId, String source) {
                return R.fail("获取角色功能表查询表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableForm> roleTableFormAdd(SysRoleTableForm sysRoleTableForm, String source) {
                return R.fail("获取角色功能表查询表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableForm> roleTableFormEdit(SysRoleTableForm sysRoleTableForm, String source) {
                return R.fail("获取角色功能表查询表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableFormItem> getRoleItemInfo(Long roleItemId, String source) {
                return R.fail("获取角色表单配置项列表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysRoleTableFormItem>> getRoleItemInfoByFormId(Long roleFormId, String source) {
                return R.fail("获取角色表单配置项列表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableFormItem> roleFormItemAdd(SysRoleTableFormItem sysRoleTableFormItem, String source) {
                return R.fail("获取角色表单配置项列表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableFormItem> roleFormItemEdit(SysRoleTableFormItem sysRoleTableFormItem, String source) {
                return R.fail("获取角色表单配置项列表详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTablePager> getRolePagerInfo(Long rolePagerId, String source) {
                return R.fail("获取角色功能表分页配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTablePager> getRolePagerInfoByTableId(Long roleTableId, String source) {
                return R.fail("获取角色功能表分页配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTablePager> rolePageAdd(SysRoleTablePager sysRoleTablePager, String source) {
                return R.fail("获取角色功能表分页配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTablePager> rolePageEdit(SysRoleTablePager sysRoleTablePager, String source) {
                return R.fail("获取角色功能表分页配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableProxy> getRoleProxyInfo(Long roleProxyId, String source) {
                return R.fail("获取角色功能表数据代理详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableProxy> getRoleProxyInfoByTableId(Long roleTableId, String source) {
                return R.fail("获取角色功能表数据代理详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableProxy> roleProxyAdd(SysRoleTableProxy sysRoleTableProxy, String source) {
                return R.fail("获取角色功能表数据代理详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableProxy> roleProxyEdit(SysRoleTableProxy sysRoleTableProxy, String source) {
                return R.fail("获取角色功能表数据代理详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableToolbarConfig> getRoleToolBarConfigInfo(Long roleToolbarConfigId, String source) {
                return R.fail("获取角色工具栏配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableToolbarConfig> getRoleToolBarConfigInfoByTableId(Long roleTableId, String source) {
                return R.fail("获取角色工具栏配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableToolbarConfig> roleToolBarAdd(SysRoleTableToolbarConfig sysRoleTableToolbarConfig, String source) {
                return R.fail("获取角色工具栏配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRoleTableToolbarConfig> roleToolBarEdit(SysRoleTableToolbarConfig sysRoleTableToolbarConfig, String source) {
                return R.fail("获取角色工具栏配置详细信息失败:" + throwable.getMessage());
            }
        };
    }
}
