package com.ruoyi.tableConfig.api;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.tableConfig.api.domain.SysUserTableColumn;
import com.ruoyi.tableConfig.api.domain.SysUserTableFormItem;
import com.ruoyi.tableConfig.api.factory.RemoteUserTableFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(contextId = "remoteUserTableService", value = ServiceNameConstants.SYSTEM_TABLECONFIG, fallbackFactory = RemoteUserTableFallbackFactory.class)
public interface RemoteUserTableService {

    /**
     * 获取用户表格列配置详细信息
     *
     * @param sysUserTableColumn
     * @param source             请求来源
     * @return
     */
    @PostMapping("/remoteUser/userTableColumn/getUserColumnInfo")
    public R<List<SysUserTableColumn>> getUserColumnInfo(@RequestBody SysUserTableColumn sysUserTableColumn, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取用户表格列配置详细信息
     *
     * @param userColumnId
     * @param source       请求来源
     * @return
     */
    @GetMapping("/remoteUser/userTableColumn/getInfoByColumnId/{userColumnId}")
    public R<SysUserTableColumn> getColumnInfoByColumnId(@PathVariable("userColumnId") Long userColumnId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过角色表格id获取用户表格列配置详细信息
     *
     * @param roleTableId
     * @param source      请求来源
     * @return
     */
    @GetMapping("/remoteUser/userTableColumn/getInfoByRoleTableId/{roleTableId}")
    public R<List<SysUserTableColumn>> getColumnInfoByRoleTableId(@PathVariable("roleTableId") Long roleTableId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过用户id获取用户表格列配置详细信息
     *
     * @param userId
     * @param source 请求来源
     * @return
     */
    @GetMapping("/remoteUser/userTableColumn/getInfoByUserId/{userId}")
    public R<List<SysUserTableColumn>> getColumnInfoByUserId(@PathVariable("userId") Long userId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 新增用户表格列配置
     *
     * @param sysUserTableColumn
     * @param source             请求来源
     * @return
     */
    @PostMapping("/remoteUser/userTableColumn/add")
    public R<Integer> userTableColumnAdd(@RequestBody SysUserTableColumn sysUserTableColumn, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 修改用户表格列配置
     *
     * @param sysUserTableColumn
     * @param source             请求来源
     * @return
     */
    @PutMapping("/remoteUser/userTableColumn/edit")
    public R<Integer> userTableColumnEdit(@RequestBody SysUserTableColumn sysUserTableColumn, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 删除用户表格列配置详细信息
     *
     * @param userColumnIds
     * @param source        请求来源
     * @return
     */
    @DeleteMapping("/remoteUser/userTableColumn/deleteUserColumnId/{userColumnIds}")
    public R<Boolean> userTableColumnRemoves(@PathVariable(value = "userColumnIds", required = true) Long[] userColumnIds, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过用户id删除用户表格列配置
     *
     * @param userId
     * @param source 请求来源
     * @return
     */
    @DeleteMapping("/remoteUser/userTableColumn/deleteSysUserTableColumnByUserId/{userId}")
    public R<Integer> userTableColumnRemoveByUserId(@PathVariable(value = "userId", required = true) Long userId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取用户表单列配置详细信息
     *
     * @param sysUserTableFormItem
     * @param source               请求来源
     * @return
     */
    @PostMapping("/remoteUser/userTableFormItem/getInfo")
    public R<List<SysUserTableFormItem>> getFormItemInfo(@RequestBody SysUserTableFormItem sysUserTableFormItem, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取用户表单列配置详细信息
     *
     * @param userItemId
     * @param source     请求来源
     * @return
     */
    @GetMapping("/remoteUser/userTableFormItem/getInfoByColumnId/{userItemId}")
    public R<SysUserTableFormItem> getFormItemInfoByUserItemId(@PathVariable("userItemId") Long userItemId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过角色表单id获取用户表单列配置详细信息
     *
     * @param roleFormId
     * @param source     请求来源
     * @return
     */
    @GetMapping("/remoteUser/userTableFormItem/getInfoByRoleTableId/{roleFormId}")
    public R<List<SysUserTableFormItem>> getFormItemInfoByRoleFormId(@PathVariable("roleFormId") Long roleFormId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过用户id获取用户表单列配置详细信息
     *
     * @param userId
     * @param source 请求来源
     * @return
     */
    @GetMapping("/remoteUser/userTableFormItem/getInfoByUserId/{userId}")
    public R<List<SysUserTableFormItem>> getFormItemInfoByUserId(@PathVariable("userId") Long userId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 新增用户表单列配置
     *
     * @param sysUserTableFormItem
     * @param source               请求来源
     * @return
     */
    @PostMapping("/remoteUser/userTableFormItem/add")
    public R<Integer> userTableFormItemAdd(@RequestBody SysUserTableFormItem sysUserTableFormItem, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 修改用户表单列配置
     *
     * @param sysUserTableFormItem
     * @param source               请求来源
     * @return
     */
    @PutMapping("/remoteUser/userTableFormItem/edit")
    public R<List<SysUserTableFormItem>> userTableFormItemEdit(@RequestBody SysUserTableFormItem sysUserTableFormItem, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 删除用户表单列配置
     *
     * @param userItemIds
     * @param source      请求来源
     * @return
     */
    @DeleteMapping("/remoteUser/userTableFormItem/deleteUserItemId/{userItemIds}")
    public R<Boolean> userTableFormItemRemoves(@PathVariable(value = "userItemIds", required = true) Long[] userItemIds, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过用户id删除用户表单列配置
     *
     * @param userId
     * @param source 请求来源
     * @return
     */
    @DeleteMapping("/remoteUser/userTableFormItem/deleteSysUserTableFormItemByUserId/{userId}")
    public R<Integer> userTableFormItemRemoveByUserId(@PathVariable(value = "userId", required = true) Long userId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 从角色表格中同步到用户
     *
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping("/remoteUser/asyncUserTable/{update}")
    public R asyncUserTable(@PathVariable("update") Boolean update, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
