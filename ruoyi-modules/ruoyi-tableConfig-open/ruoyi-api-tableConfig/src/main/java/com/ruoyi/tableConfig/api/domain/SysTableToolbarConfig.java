package com.ruoyi.tableConfig.api.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * 工具栏配置对象 sys_table_toolbar_config
 *
 * @author zly
 * @date 2023-05-15
 */
@Data
@TableName("sys_table_toolbar_config")
public class SysTableToolbarConfig extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 工具栏配置id
     */
    private Long toolbarConfigId;

    /**
     * 表格id
     */
    @ExcelProperty(value = "表格id")
    private Long tableId;

    /**
     * 尺寸(medium,
     */
    @ExcelProperty(value = "尺寸")
    private String extSize;

    /**
     * 是否加载中
     */
    @ExcelProperty(value = "是否加载中")
    private Boolean loading;

    /**
     * 配套的样式
     */
    @ExcelProperty(value = "配套的样式")
    private Boolean perfect;

    /**
     * 给工具栏
     */
    @ExcelProperty(value = "给工具栏")
    private String className;

    /**
     * 导入按钮配置（需要设置
     */
    @ExcelProperty(value = "导入按钮配置")
    private String extImport;

    /**
     * 导出按钮配置（需要设置
     */
    @ExcelProperty(value = "导出按钮配置")
    private String ExtExport;

    /**
     * 打印按钮配置
     */
    @ExcelProperty(value = "打印按钮配置")
    private String print;

    /**
     * 刷新按钮配置
     */
    @ExcelProperty(value = "刷新按钮配置")
    private String refresh;

    /**
     * 自定义列配置
     */
    @ExcelProperty(value = "自定义列配置")
    private String custom;

    /**
     * 左侧按钮列表
     */
    @ExcelProperty(value = "左侧按钮列表")
    private String buttons;

    /**
     * 右侧工具列表
     */
    @ExcelProperty(value = "右侧工具列表")
    private String tools;

    /**
     * 是否启用
     */
    @ExcelProperty(value = "是否启用")
    private Boolean enabled;

    /**
     * 自定义的打印配置
     */
    @ExcelProperty(value = "自定义的打印配置")
    private String printDesign;

    /**
     * 打印时查询数据接口
     */
    @ExcelProperty(value = "打印时查询数据接口")
    private String printDataQueryPort;

    /**
     * 查询打印数据前处理函数
     */
    @ExcelProperty(value = "查询打印数据前处理函数")
    private String printDataQueryBefore;

    /**
     * 查询打印数据后处理函数
     */
    @ExcelProperty(value = "查询打印数据后处理函数")
    private String printDataQueryAfter;

    /**
     * 是否允许最大化显示
     */
    @ExcelProperty(value = "是否允许最大化显示")
    private String zoom;

    /**
     * 插槽
     */
    @ExcelProperty(value = "插槽")
    private String slots;
}
