package com.ruoyi.tableConfig.api.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * 角色功能表分页配置对象 sys_role_table_pager
 *
 * @author zly
 * @date 2023-06-06
 */
@Data
@TableName("sys_role_table_pager")
public class SysRoleTablePager extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 分页配置id
     */
    @TableId(value = "role_pager_id", type = IdType.AUTO)
    private Long rolePagerId;
    /**
     * 表格分页配置id
     */
    @ExcelProperty(value = "表格分页配置id")
    private Long pagerId;

    /**
     * 表格id
     */
    @ExcelProperty(value = "表格id")
    private Long roleTableId;

    /**
     * 自定义布局
     */
    @ExcelProperty(value = "自定义布局")
    private String layouts;

    /**
     * 当前页
     */
    @ExcelProperty(value = "当前页")
    private Long currentPage;

    /**
     * 每页大小
     */
    @ExcelProperty(value = "每页大小")
    private Long pageSize;

    /**
     * 显示页码按钮的数量
     */
    @ExcelProperty(value = "显示页码按钮的数量")
    private Long pagerCount;

    /**
     * 每页大小选项列表
     */
    @ExcelProperty(value = "每页大小选项列表")
    private String pageSizes;

    /**
     * 对齐方式
     */
    @ExcelProperty(value = "对齐方式")
    private String align;

    /**
     * 带边框
     */
    @ExcelProperty(value = "带边框")
    private Boolean border;

    /**
     * 带背景颜色
     */
    @ExcelProperty(value = "带背景颜色")
    private Boolean background;

    /**
     * 配套的样式
     */
    @ExcelProperty(value = "配套的样式")
    private Boolean perfect;

    /**
     * 给分页附加
     */
    @ExcelProperty(value = "给分页附加")
    private String className;

    /**
     * 当只有一页时自动隐藏
     */
    @ExcelProperty(value = "当只有一页时自动隐藏")
    private Boolean autoHidden;

    /**
     * 自定义上一页图标
     */
    @ExcelProperty(value = "自定义上一页图标")
    private String iconPrevPage;

    /**
     * 自定义向上跳页图标
     */
    @ExcelProperty(value = "自定义向上跳页图标")
    private String iconJumpPrev;

    /**
     * 自定义向下跳页图标
     */
    @ExcelProperty(value = "自定义向下跳页图标")
    private String iconJumpNext;

    /**
     * 自定义下一页图标
     */
    @ExcelProperty(value = "自定义下一页图标")
    private String iconnextPage;

    /**
     * 自定义跳页显示图标
     */
    @ExcelProperty(value = "自定义跳页显示图标")
    private String iconJumpMore;

    /**
     * 是否启用
     */
    @ExcelProperty(value = "是否启用")
    private Boolean enabled;

    /**
     * 插槽
     */
    @ExcelProperty(value = "插槽")
    private String slots;

}
