package com.ruoyi.tableConfig.api.factory;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.tableConfig.api.RemoteTableService;
import com.ruoyi.tableConfig.api.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 菜单表格服务降级处理
 *
 * @author zly
 */
@Component
public class RemoteTableFallbackFactory implements FallbackFactory<RemoteTableService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteTableFallbackFactory.class);

    @Override
    public RemoteTableService create(Throwable throwable) {
        log.error("菜单表格服务调用失败:{}", throwable.getMessage());
        return new RemoteTableService() {
            @Override
            public R<SysTable> getTableInfo(Long tableId, String source) {
                return R.fail("获取功能表格详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysTable>> getTableInfoByMenuId(Long menuId, String source) {
                return R.fail("获取功能表格详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTableColumn> getColumnInfo(Long columnId, String source) {
                return R.fail("获取功能表格列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysTableColumn>> getColumnInfoByTableId(Long tableId, String source) {
                return R.fail("获取功能表格列配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTableEditConfig> getEditConfigInfo(Long editConfigId, String source) {
                return R.fail("获取可编辑配置项详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTableEditConfig> getEditConfigInfoByTableId(Long tableId, String source) {
                return R.fail("获取可编辑配置项详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTableEditRules> getEditRulesInfo(Long editRulesId, String source) {
                return R.fail("获取校验规则配置项详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysTableEditRules>> getEditRulesInfoByTableId(Long tableId, String source) {
                return R.fail("获取校验规则配置项详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTableForm> getFormInfo(Long formId, String source) {
                return R.fail("获取功能表查询详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTableForm> getFormInfoByTableId(Long tableId, String source) {
                return R.fail("获取功能表查询详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTableFormItem> getFormItemInfo(Long itemId, String source) {
                return R.fail("获取表单配置项列详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysTableFormItem>> getFormItemInfoByFormId(Long formId, String source) {
                return R.fail("获取表单配置项列详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTablePager> getPageInfo(Long pagerId, String source) {
                return R.fail("获取功能表分页配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTablePager> getPageInfoByTableId(Long tableId, String source) {
                return R.fail("获取功能表分页配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTableProxy> getProxyInfo(Long proxyId, String source) {
                return R.fail("获取功能表数据代理详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTableProxy> getProxyInfoByTableId(Long tableId, String source) {
                return R.fail("获取功能表数据代理详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTableToolbarConfig> getToolBarConfigInfo(Long toolbarConfigId, String source) {
                return R.fail("获取工具栏配置详细信息失败:" + throwable.getMessage());
            }

            @Override
            public R<SysTableToolbarConfig> getToolBarConfigInfoByTableId(Long tableId, String source) {
                return R.fail("获取工具栏配置详细信息失败:" + throwable.getMessage());
            }
        };
    }
}
