package com.ruoyi.tableConfig.api.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 表格权限业务表对象 sys_role_table
 *
 * @author zly
 * @date 2023-06-06
 */
@Data
@TableName("sys_role_table")
public class SysRoleTable extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 表id
     */
    @ExcelProperty(value = "表id")
    @TableId(value = "role_table_id", type = IdType.AUTO)
    private Long roleTableId;

    /**
     * 表名称
     */
    @ExcelProperty(value = "表名称")
    private String tableName;

    /**
     * 表描述
     */
    @ExcelProperty(value = "表描述")
    private String tableComment;

    /**
     * 功能id
     */
    @ExcelProperty(value = "功能id")
    private Long menuId;

    /**
     * 角色id
     */
    @ExcelProperty(value = "角色id")
    private Long roleId;

    /**
     * 表格id
     */
    @ExcelProperty(value = "表格id")
    private Long tableId;

    /**
     * 唯一标识
     */
    @ExcelProperty(value = "唯一标识")
    private String id;

    /**
     * 表格的高度；支持铺满父容器或者固定高度，如果设置
     */
    @ExcelProperty(value = "表格的高度；支持铺满父容器或者固定高度，如果设置")
    private String height;

    /**
     * 表格主键字段
     */
    @ExcelProperty(value = "表格主键字段")
    private String uniqueId;

    /**
     * 表格行id字段
     */
    @ExcelProperty(value = "表格行id字段")
    private String rowId;

    /**
     * 表格的最大高度
     */
    @ExcelProperty(value = "表格的最大高度")
    private Integer maxHeight;

    /**
     * 自动监听父元素的变化去重新计算表格（对于父元素可能存在动态变化、显示隐藏的容器中、列宽异常等场景中的可能会用到）
     */
    @ExcelProperty(value = "自动监听父元素的变化去重新计算表格")
    private Boolean autoResize;

    /**
     * 自动跟随某个属性的变化去重新计算表格，和手动调用
     */
    @ExcelProperty(value = "自动跟随某个属性的变化去重新计算表格，和手动调用")
    private Boolean syncResize;

    /**
     * 是否带有斑马纹（需要注意的是，在可编辑表格场景下，临时插入的数据不会有斑马纹样式）
     */
    @ExcelProperty(value = "是否带有斑马纹")
    private Boolean stripe;

    /**
     * 是否带有边框
     */
    @ExcelProperty(value = "是否带有边框")
    private Boolean border;

    /**
     * 是否为圆角边框
     */
    @ExcelProperty(value = "是否为圆角边框")
    private Boolean round;

    /**
     * 表格的尺寸
     */
    @ExcelProperty(value = "表格的尺寸")
    private String size;

    /**
     * 所有的列对齐方式
     */
    @ExcelProperty(value = "所有的列对齐方式")
    private String align;

    /**
     * 所有的表头列的对齐方式
     */
    @ExcelProperty(value = "所有的表头列的对齐方式")
    private String headerAlign;

    /**
     * 所有的表尾列的对齐方式
     */
    @ExcelProperty(value = "所有的表尾列的对齐方式")
    private String footerAlign;

    /**
     * 是否显示表头
     */
    @ExcelProperty(value = "是否显示表头")
    private Boolean showHeader;

    /**
     * 给行附加
     */
    @ExcelProperty(value = "给行附加")
    private String rowClassName;

    /**
     * 给单元格附加
     */
    @ExcelProperty(value = "给单元格附加")
    private String cellClassName;

    /**
     * 给表头的行附加
     */
    @ExcelProperty(value = "给表头的行附加")
    private String headerRowClassName;

    /**
     * 给表头的单元格附加
     */
    @ExcelProperty(value = "给表头的单元格附加")
    private String headerCellClassName;

    /**
     * 给表尾的行附加
     */
    @ExcelProperty(value = "给表尾的行附加")
    private String footerRowClassName;

    /**
     * 给表尾的单元格附加
     */
    @ExcelProperty(value = "给表尾的单元格附加")
    private String footerCellClassName;

    /**
     * 是否显示表尾
     */
    @ExcelProperty(value = "是否显示表尾")
    private Boolean showFooter;

    /**
     * 表尾的数据获取方法，返回一个二维数组
     */
    @ExcelProperty(value = "表尾的数据获取方法，返回一个二维数组")
    private String footerMethod;

    /**
     * 设置所有内容过长时显示为省略号（如果是固定列建议设置该值，提升渲染速度）
     */
    @ExcelProperty(value = "设置所有内容过长时显示为省略号")
    private String showOverflow;

    /**
     * 设置表头所有内容过长时显示为省略号
     */
    @ExcelProperty(value = "设置表头所有内容过长时显示为省略号")
    private String showHeaderOverflow;

    /**
     * 设置表尾所有内容过长时显示为省略号
     */
    @ExcelProperty(value = "设置表尾所有内容过长时显示为省略号")
    private String showFooterOverflow;

    /**
     * 保持原始值的状态，被某些功能所依赖，比如编辑状态、还原数据等（开启后影响性能，具体取决于数据量）
     */
    @ExcelProperty(value = "保持原始值的状态，被某些功能所依赖，比如编辑状态、还原数据等")
    private Boolean keepSource;

    /**
     * 空数据时显示的内容
     */
    @ExcelProperty(value = "空数据时显示的内容")
    private String emptyText;

    /**
     * 自定义参数（可以用来存放一些自定义的数据）
     */
    @ExcelProperty(value = "自定义参数")
    private String extParams;

    /**
     * 额外配置（json）
     */
    @ExcelProperty(value = "额外配置")
    private String extraConfig;

    /**
     * 操作配置
     */
    @ExcelProperty(value = "操作配置")
    private String handConfig;

    @TableField(exist = false)
    private SysRoleTableForm formConfig;
    @TableField(exist = false)
    private SysRoleTablePager pagerConfig;
    @TableField(exist = false)
    private SysRoleTableProxy proxyConfig;
    @TableField(exist = false)
    private List<SysUserTableColumn> columns;
    @TableField(exist = false)
    private List<SysRoleTableEditRules> editRules;
    @TableField(exist = false)
    private SysRoleTableEditConfig editConfig;
    @TableField(exist = false)
    private SysRoleTableToolbarConfig toolbarConfig;
}
