package com.ruoyi.tableConfig.api.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * 角色可编辑配置项对象 sys_role_table_edit_config
 *
 * @author zly
 * @date 2023-06-06
 */
@Data
@TableName("sys_role_table_edit_config")
public class SysRoleTableEditConfig extends BaseEntity {
    private static final long serialVersionUID = 1L;


    /**
     * 可编辑配置项id
     */
    @TableId(value = "role_edit_config_id", type = IdType.AUTO)
    @ExcelProperty(value = "可编辑配置项id")
    private Long roleEditConfigId;
    /**
     * 菜单可编辑配置项id
     */
    @ExcelProperty(value = "菜单可编辑配置项id")
    private Long editConfigId;

    /**
     * 表id
     */
    @ExcelProperty(value = "表id")
    private Long roleTableId;

    /**
     * 触发方式
     */
    @ExcelProperty(value = "触发方式")
    private String triggerbase;

    /**
     * 是否启用
     */
    @ExcelProperty(value = "是否启用")
    private Boolean enabled;

    /**
     * 编辑模式
     */
    @ExcelProperty(value = "编辑模式")
    private String mode;

    /**
     * 是否显示列头编辑图标
     */
    @ExcelProperty(value = "是否显示列头编辑图标")
    private Boolean showIcon;

    /**
     * 只对 keep-source 开启有效，是否显示单元格新增与修改状态
     */
    @ExcelProperty(value = "只对 keep-source 开启有效，是否显示单元格新增与修改状态")
    private Boolean showStatus;

    /**
     * 只对 keep-source 开启有效，是否显示单元格修改状态
     */
    @ExcelProperty(value = "只对 keep-source 开启有效，是否显示单元格修改状态")
    private Boolean showUpdateStatus;

    /**
     * 只对 keep-source 开启有效，是否显示单元格新增状态
     */
    @ExcelProperty(value = "只对 keep-source 开启有效，是否显示单元格新增状态")
    private Boolean showInsertStatus;

    /**
     * 是否显示必填字段的红色星号
     */
    @ExcelProperty(value = "是否显示必填字段的红色星号")
    private Boolean showAsterisk;

    /**
     * 当点击非编辑列之后是否自动清除单元格的激活状态
     */
    @ExcelProperty(value = "当点击非编辑列之后是否自动清除单元格的激活状态")
    private Boolean autoClear;

    /**
     * 该方法的返回值用来决定该单元格是否允许编辑
     */
    @ExcelProperty(value = "该方法的返回值用来决定该单元格是否允许编辑")
    private String beforeEditMethod;

    /**
     * 自定义可编辑列的状态图标
     */
    @ExcelProperty(value = "自定义可编辑列的状态图标")
    private String icon;


}
