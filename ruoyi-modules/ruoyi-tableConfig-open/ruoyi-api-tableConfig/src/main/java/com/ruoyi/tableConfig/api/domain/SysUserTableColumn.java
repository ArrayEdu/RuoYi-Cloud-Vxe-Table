package com.ruoyi.tableConfig.api.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * 用户表格列配置对象 sys_user_table_column
 *
 * @author zly
 * @date 2023-06-13
 */
@Data
@TableName("sys_user_table_column")
public class SysUserTableColumn extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 用户表格列配置id
     */
    @TableId(value = "user_column_id", type = IdType.AUTO)
    private Long userColumnId;

    /**
     * 编号
     */
    @ExcelProperty(value = "编号")
    private Long columnId;

    /**
     * 角色归属表编号
     */
    @ExcelProperty(value = "角色归属表编号")
    private Long roleTableId;

    /**
     * 用户id
     */
    @ExcelProperty(value = "用户id")
    private Long userId;

    /**
     * 列名称
     */
    @ExcelProperty(value = "列名称")
    private String columnName;

    /**
     * 列描述
     */
    @ExcelProperty(value = "列描述")
    private String columnComment;

    /**
     * 字段
     */
    @ExcelProperty(value = "字段")
    private String field;

    /**
     * 字段名
     */
    @ExcelProperty(value = "字段名")
    private String title;

    /**
     * 是否显示（1是
     */
    @ExcelProperty(value = "是否显示")
    private Boolean isIgnore;

    /**
     * 是否可视（1是
     */
    @ExcelProperty(value = "是否可视")
    private Boolean visible;

    /**
     * 宽度
     */
    @ExcelProperty(value = "宽度")
    private String width;

    /**
     * 最小列宽度会自动将剩余空间按比例分配
     */
    @ExcelProperty(value = "最小列宽度会自动将剩余空间按比例分配")
    private Long minWidth;

    /**
     * 列宽是否可拖拽（1是 0否）
     */
    @ExcelProperty(value = "列宽是否可拖拽（1是 0否）")
    private Boolean resizable;

    /**
     * 列拖拽宽度
     */
    @TableField(exist = false)
    private Long resizeWidth;
    /**
     * 列渲染宽度
     */
    @TableField(exist = false)
    private Long renderWidth;

    /**
     * 列对齐方式：left（左对齐）,
     */
    @ExcelProperty(value = "列对齐方式：left")
    private String align;

    /**
     * 列的类型
     */
    @ExcelProperty(value = "列的类型")
    private String type;

    /**
     * 将列固定在左侧或者右侧（注意：固定列应该放在左右两侧的位置）
     */
    @ExcelProperty(value = "将列固定在左侧或者右侧")
    private String fixed;

    /**
     * 表头列的对齐方式
     */
    @ExcelProperty(value = "表头列的对齐方式")
    private String headerAlign;

    /**
     * 表尾列的对齐方式
     */
    @ExcelProperty(value = "表尾列的对齐方式")
    private String footerAlign;

    /**
     * 当内容过长时显示为省略号
     */
    @ExcelProperty(value = "当内容过长时显示为省略号")
    private String showOverflow;

    /**
     * 当表头内容过长时显示为省略号
     */
    @ExcelProperty(value = "当表头内容过长时显示为省略号")
    private String showHeaderOverflow;

    /**
     * 当表尾内容过长时显示为省略号
     */
    @ExcelProperty(value = "当表尾内容过长时显示为省略号")
    private String showFooterOverflow;

    /**
     * 给单元格附加
     */
    @ExcelProperty(value = "给单元格附加")
    private String className;

    /**
     * 给表头的单元格附加
     */
    @ExcelProperty(value = "给表头的单元格附加")
    private String headerClassName;

    /**
     * 给表尾的单元格附加
     */
    @ExcelProperty(value = "给表尾的单元格附加")
    private String footerClassName;

    /**
     * 格式化显示内容
     */
    @ExcelProperty(value = "格式化显示内容")
    private String formatter;

    /**
     * 是否允许列排序
     */
    @ExcelProperty(value = "是否允许列排序")
    private Boolean sortable;

    /**
     * 只对
     */
    @ExcelProperty(value = "只对")
    private String sortBy;

    /**
     * 排序的字段类型，比如字符串转数值等
     */
    @ExcelProperty(value = "排序的字段类型，比如字符串转数值等")
    private String sortType;

    /**
     * 额外的参数（可以用来存放一些私有参数）
     */
    @ExcelProperty(value = "额外的参数")
    private String extParams;

    /**
     * 只对 tree-config 配置时有效，指定为树节点
     */
    @ExcelProperty(value = "只对 tree-config 配置时有效，指定为树节点")
    private Boolean treeNode;

    /**
     * 插槽
     */
    @ExcelProperty(value = "插槽")
    private String slots;

    /**
     * 配置筛选条件
     */
    @ExcelProperty(value = "配置筛选条件")
    private String filters;

    /**
     * 只对
     */
    @ExcelProperty(value = "只对")
    private Boolean filterMultiple;

    /**
     * 只对
     */
    @ExcelProperty(value = "只对")
    private String filterMethod;

    /**
     * 只对
     */
    @ExcelProperty(value = "只对")
    private String filterResetMethod;

    /**
     * 只对
     */
    @ExcelProperty(value = "只对")
    private String filterRecoverMethod;

    /**
     * 筛选渲染器配置项
     */
    @ExcelProperty(value = "筛选渲染器配置项")
    private String filterRender;

    /**
     * 自定义单元格数据导出方法，返回自定义的值
     */
    @ExcelProperty(value = "自定义单元格数据导出方法，返回自定义的值")
    private String exportMethod;

    /**
     * 自定义表尾单元格数据导出方法，返回自定义的值
     */
    @ExcelProperty(value = "自定义表尾单元格数据导出方法，返回自定义的值")
    private String footerExportMethod;

    /**
     * 标题前缀图标配置项
     */
    @ExcelProperty(value = "标题前缀图标配置项")
    private String titlePrefix;

    /**
     * 只对特定功能有效，单元格值类型
     */
    @ExcelProperty(value = "只对特定功能有效，单元格值类型")
    private String cellType;

    /**
     * 默认的渲染器配置项
     */
    @ExcelProperty(value = "默认的渲染器配置项")
    private String cellRender;

    /**
     * 可编辑渲染器配置项
     */
    @ExcelProperty(value = "可编辑渲染器配置项")
    private String editRender;

    /**
     * 内容渲染配置项
     */
    @ExcelProperty(value = "内容渲染配置项")
    private String contentRender;

    /**
     * 自定义列的唯一主键
     */
    @ExcelProperty(value = "自定义列的唯一主键")
    private String colId;

    /**
     * 排序
     */
    @ExcelProperty(value = "排序")
    private Integer sortNo;

    /**
     * 字典类型
     */
    @ExcelProperty(value = "字典类型")
    private String dictType;

}
