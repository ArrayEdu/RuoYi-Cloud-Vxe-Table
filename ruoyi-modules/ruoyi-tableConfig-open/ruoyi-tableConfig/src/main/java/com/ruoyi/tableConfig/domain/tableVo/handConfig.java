package com.ruoyi.tableConfig.domain.tableVo;

import lombok.Data;

/**
 * 表格操作配置 前端会根据此配置在前端数据代理中动态添加接口
 */
@Data
public class handConfig {
    private boolean save;
    private boolean add;
    private boolean modify;
    private boolean delete;
}
