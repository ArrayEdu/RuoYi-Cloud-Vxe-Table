package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysRoleTableFormItem;

/**
 * 角色表单配置项列表Mapper接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface SysRoleTableFormItemMapper extends BaseMapperPlus<SysRoleTableFormItem> {

    /**
     * 查询角色表单配置项列表
     *
     * @param roleFormId
     * @return 角色表单配置项列表
     */
    public List<SysRoleTableFormItem> selectSysRoleTableFormItemByRoleFormId(Long roleFormId);

    /**
     * 查询角色表单配置项列表
     *
     * @param roleItemId 角色表单配置项列表主键
     * @return 角色表单配置项列表
     */
    public SysRoleTableFormItem selectSysRoleTableFormItemByRoleItemId(Long roleItemId);

    /**
     * 查询角色表单配置项列表
     *
     * @param itemId 角色表单配置项列表
     * @return 角色表单配置项列表
     */
    public SysRoleTableFormItem selectSysRoleTableFormItemByItemId(Long itemId);

    /**
     * 查询角色表单配置项列表列表
     *
     * @param sysRoleTableFormItem 角色表单配置项列表
     * @return 角色表单配置项列表集合
     */
    public List<SysRoleTableFormItem> selectSysRoleTableFormItemList(SysRoleTableFormItem sysRoleTableFormItem);

    /**
     * 新增角色表单配置项列表
     *
     * @param sysRoleTableFormItem 角色表单配置项列表
     * @return 结果
     */
    public int insertSysRoleTableFormItem(SysRoleTableFormItem sysRoleTableFormItem);

    /**
     * 修改角色表单配置项列表
     *
     * @param sysRoleTableFormItem 角色表单配置项列表
     * @return 结果
     */
    public int updateSysRoleTableFormItem(SysRoleTableFormItem sysRoleTableFormItem);

    /**
     * 删除角色表单配置项列表
     *
     * @param roleFormId 角色表单id
     * @return 结果
     */
    public int deleteSysRoleTableFormItemByRoleFormId(Long roleFormId);

    /**
     * 删除角色表单配置项列表
     *
     * @param roleItemId 角色表单配置项列表主键
     * @return 结果
     */
    public int deleteSysRoleTableFormItemByRoleItemId(Long roleItemId);

    /**
     * 批量删除角色表单配置项列表
     *
     * @param roleItemIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysRoleTableFormItemByRoleItemIds(Long[] roleItemIds);
}
