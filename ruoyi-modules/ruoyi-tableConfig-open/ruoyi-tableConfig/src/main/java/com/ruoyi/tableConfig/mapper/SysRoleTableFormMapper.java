package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysRoleTableForm;

/**
 * 角色功能表查询表Mapper接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface SysRoleTableFormMapper extends BaseMapperPlus<SysRoleTableForm> {

    /**
     * 查询角色功能表查询表
     *
     * @param roleTableId
     * @return 角色功能表查询表
     */
    public SysRoleTableForm selectSysRoleTableFormByRoleTableId(Long roleTableId);

    /**
     * 查询角色功能表查询表
     *
     * @param formId 角色功能表查询表主键
     * @return 角色功能表查询表
     */
    public SysRoleTableForm selectSysRoleTableFormByFormId(Long formId);

    /**
     * 查询角色功能表查询表
     *
     * @param roleFormId 角色功能表查询表主键
     * @return 角色功能表查询表
     */
    public SysRoleTableForm selectSysRoleTableFormByRoleFormId(Long roleFormId);

    /**
     * 查询角色功能表查询表列表
     *
     * @param sysRoleTableForm 角色功能表查询表
     * @return 角色功能表查询表集合
     */
    public List<SysRoleTableForm> selectSysRoleTableFormList(SysRoleTableForm sysRoleTableForm);

    /**
     * 新增角色功能表查询表
     *
     * @param sysRoleTableForm 角色功能表查询表
     * @return 结果
     */
    public int insertSysRoleTableForm(SysRoleTableForm sysRoleTableForm);

    /**
     * 修改角色功能表查询表
     *
     * @param sysRoleTableForm 角色功能表查询表
     * @return 结果
     */
    public int updateSysRoleTableForm(SysRoleTableForm sysRoleTableForm);

    /**
     * 删除角色功能表查询表
     *
     * @param roleTableId 角色表格id
     * @return 结果
     */
    public int deleteSysRoleTableFormByRoleTableId(Long roleTableId);

    /**
     * 删除角色功能表查询表
     *
     * @param roleFormId 角色功能表查询表主键
     * @return 结果
     */
    public int deleteSysRoleTableFormByRoleFormId(Long roleFormId);

    /**
     * 批量删除角色功能表查询表
     *
     * @param roleFormIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysRoleTableFormByRoleFormIds(Long[] roleFormIds);
}
