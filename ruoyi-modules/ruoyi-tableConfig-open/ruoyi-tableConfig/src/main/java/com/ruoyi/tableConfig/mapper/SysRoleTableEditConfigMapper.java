package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysRoleTableEditConfig;

/**
 * 角色可编辑配置项Mapper接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface SysRoleTableEditConfigMapper extends BaseMapperPlus<SysRoleTableEditConfig> {


    /**
     * 查询角色可编辑配置项
     *
     * @param roleTableId 菜单可编辑配置项主键
     * @return 角色可编辑配置项
     */
    public SysRoleTableEditConfig selectSysRoleTableEditConfigByRoleTableId(Long roleTableId);

    /**
     * 查询角色可编辑配置项
     *
     * @param roleEditConfigId 角色可编辑配置项主键
     * @return 角色可编辑配置项
     */
    public SysRoleTableEditConfig selectSysRoleTableEditConfigByRoleEditConfigId(Long roleEditConfigId);

    /**
     * 查询角色可编辑配置项
     *
     * @param editConfigId 角色可编辑配置项主键
     * @return 角色可编辑配置项
     */
    public SysRoleTableEditConfig selectSysRoleTableEditConfigByEditConfigId(Long editConfigId);

    /**
     * 查询角色可编辑配置项列表
     *
     * @param sysRoleTableEditConfig 角色可编辑配置项
     * @return 角色可编辑配置项集合
     */
    public List<SysRoleTableEditConfig> selectSysRoleTableEditConfigList(SysRoleTableEditConfig sysRoleTableEditConfig);

    /**
     * 新增角色可编辑配置项
     *
     * @param sysRoleTableEditConfig 角色可编辑配置项
     * @return 结果
     */
    public int insertSysRoleTableEditConfig(SysRoleTableEditConfig sysRoleTableEditConfig);

    /**
     * 修改角色可编辑配置项
     *
     * @param sysRoleTableEditConfig 角色可编辑配置项
     * @return 结果
     */
    public int updateSysRoleTableEditConfig(SysRoleTableEditConfig sysRoleTableEditConfig);

    /**
     * 删除角色可编辑配置项
     *
     * @param roleTableId 角色表格id
     * @return 结果
     */
    public int deleteSysRoleTableEditConfigByRoleTableId(Long roleTableId);

    /**
     * 删除角色可编辑配置项
     *
     * @param editConfigId 角色可编辑配置项主键
     * @return 结果
     */
    public int deleteSysRoleTableEditConfigByRoleEditConfigId(Long editConfigId);

    /**
     * 批量删除角色可编辑配置项
     *
     * @param editConfigIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysRoleTableEditConfigByRoleEditConfigIds(Long[] editConfigIds);
}
