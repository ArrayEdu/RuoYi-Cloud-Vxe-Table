package com.ruoyi.tableConfig.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;

import javax.servlet.http.HttpServletResponse;

/**
 * 表格权限业务表Service接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface ISysRoleTableService extends IServicePlus<SysRoleTable> {

    /**
     * 查询表格权限业务表
     *
     * @param sysRoleTable 表格权限业务表
     * @return 表格权限业务表
     */
    public SysRoleTable selectSysRoleTable(SysRoleTable sysRoleTable);

    /**
     * 查询表格权限业务表
     *
     * @param tableId 菜单表格id
     * @return 表格权限业务表
     */
    public List<SysRoleTable> selectSysRoleTableByTableId(Long tableId);

    /**
     * 查询表格权限业务表
     *
     * @param menuId 菜单id
     * @return 表格权限业务表
     */
    public List<SysRoleTable> selectSysRoleTableByMenuId(Long menuId);

    /**
     * 查询表格权限业务表
     *
     * @param roleId 角色id
     * @return 表格权限业务表
     */
    public List<SysRoleTable> selectSysRoleTableByRoleId(Long roleId);

    /**
     * 查询表格权限业务表
     *
     * @param roleTableId 表格权限业务表主键
     * @return 表格权限业务表
     */
    public SysRoleTable selectSysRoleTableByRoleTableId(Long roleTableId);

    /**
     * 查询表格权限业务表列表
     *
     * @param sysRoleTable 表格权限业务表
     * @return 表格权限业务表集合
     */
    public List<SysRoleTable> selectSysRoleTableList(SysRoleTable sysRoleTable);

    /**
     * 查询表格权限业务表分页列表
     *
     * @param sysRoleTable 表格权限业务表
     * @param pageQuery    分页配置
     * @return
     */
    public TableDataInfo<SysRoleTable> selectSysRoleTablePage(SysRoleTable sysRoleTable, PageQuery pageQuery);

    /**
     * 新增表格权限业务表
     *
     * @param sysRoleTable 表格权限业务表
     * @return 结果
     */
    public int insertSysRoleTable(SysRoleTable sysRoleTable);

    /**
     * 修改表格权限业务表
     *
     * @param sysRoleTable 表格权限业务表
     * @return 结果
     */
    public int updateSysRoleTable(SysRoleTable sysRoleTable);

    /**
     * 批量删除表格权限业务表
     *
     * @param roleTableIds 需要删除的表格权限业务表主键集合
     * @return 结果
     */
    public int removeSysRoleTableByRoleTableIds(Long[] roleTableIds);

    /**
     * 批量删除表格权限业务表
     *
     * @param roleTableIds 需要删除的表格权限业务表主键集合
     * @return 结果
     */
    public int deleteSysRoleTableByRoleTableIds(Long[] roleTableIds);

    /**
     * 删除表格权限业务表信息
     *
     * @param roleTableId 表格权限业务表主键
     * @return 结果
     */
    public int deleteSysRoleTableByRoleTableId(Long roleTableId);

    /**
     * 表格权限业务表查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysRoleTable> buildQueryWrapper(SysRoleTable sysRoleTable);

    /**
     * 同步菜单数据
     */
    public void syncMenu(Long roleId);

    /**
     * 同步数据
     */
    public void synchDb(Long roleTableId, Long tableId, Long roleId, Long menuId);

    /**
     * 强制同步数据
     */
    public void forceSynchDb(Long roleTableId, Long tableId, Long roleId, Long menuId);

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);

    /**
     * 同步用户表格数据
     */
    public void asyncUserTable(Boolean update);
}
