package com.ruoyi.tableConfig.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableColumn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysTableColumnMapper;
import com.ruoyi.tableConfig.service.ISysTableColumnService;

/**
 * 功能表格列配置Service业务层处理
 *
 * @author zly
 * @date 2023-04-25
 */
@Service
public class SysTableColumnServiceImpl extends ServiceImpl<SysTableColumnMapper, SysTableColumn> implements ISysTableColumnService {
    @Autowired
    private SysTableColumnMapper sysTableColumnMapper;


    /**
     * 查询功能表格列配置
     *
     * @param tableId 功能表格id
     * @return 功能表格列配置
     */
    @Override
    public List<SysTableColumn> selectSysTableColumnByTableId(Long tableId) {
        return sysTableColumnMapper.selectSysTableColumnByTableId(tableId);
    }

    /**
     * 查询功能表格列配置
     *
     * @param columnId 功能表格列配置主键
     * @return 功能表格列配置
     */
    @Override
    public SysTableColumn selectSysTableColumnByColumnId(Long columnId) {
        return sysTableColumnMapper.selectSysTableColumnByColumnId(columnId);
    }

    /**
     * 查询功能表格列配置列表
     *
     * @param sysTableColumn 功能表格列配置
     * @return 功能表格列配置
     */
    @Override
    public List<SysTableColumn> selectSysTableColumnList(SysTableColumn sysTableColumn) {
        return sysTableColumnMapper.selectSysTableColumnList(sysTableColumn);
    }

    @Override
    public TableDataInfo<SysTableColumn> selectSysTableColumnPage(SysTableColumn sysTableColumn, PageQuery pageQuery) {
        LambdaQueryWrapper<SysTableColumn> lqw = buildQueryWrapper(sysTableColumn);
        Page<SysTableColumn> page = sysTableColumnMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增功能表格列配置
     *
     * @param sysTableColumn 功能表格列配置
     * @return 结果
     */
    @Override
    public int insertSysTableColumn(SysTableColumn sysTableColumn) {
        sysTableColumn.setCreateTime(DateUtils.getNowDate());
        return sysTableColumnMapper.insertSysTableColumn(sysTableColumn);
    }

    /**
     * 修改功能表格列配置
     *
     * @param sysTableColumn 功能表格列配置
     * @return 结果
     */
    @Override
    public int updateSysTableColumn(SysTableColumn sysTableColumn) {
        sysTableColumn.setUpdateTime(DateUtils.getNowDate());
        return sysTableColumnMapper.updateSysTableColumn(sysTableColumn);
    }

    /**
     * 批量删除功能表格列配置
     *
     * @param columnIds 需要删除的功能表格列配置主键
     * @return 结果
     */
    @Override
    public int deleteSysTableColumnByColumnIds(Long[] columnIds) {
        return sysTableColumnMapper.deleteSysTableColumnByColumnIds(columnIds);
    }

    /**
     * 删除功能表格列配置信息
     *
     * @param columnId 功能表格列配置主键
     * @return 结果
     */
    @Override
    public int deleteSysTableColumnByColumnId(Long columnId) {
        return sysTableColumnMapper.deleteSysTableColumnByColumnId(columnId);
    }

    @Override
    public LambdaQueryWrapper<SysTableColumn> buildQueryWrapper(SysTableColumn sysTableColumn) {
        LambdaQueryWrapper<SysTableColumn> lambdaQueryWrapper = new LambdaQueryWrapper<SysTableColumn>()
                .like(!StringUtils.isNull(sysTableColumn.getColumnComment()), SysTableColumn::getColumnComment, sysTableColumn.getColumnComment())
                .like(!StringUtils.isNull(sysTableColumn.getTitle()), SysTableColumn::getTitle, sysTableColumn.getTitle())
                .like(!StringUtils.isNull(sysTableColumn.getField()), SysTableColumn::getField, sysTableColumn.getField())
                .eq(!StringUtils.isNull(sysTableColumn.getTableId()), SysTableColumn::getTableId, sysTableColumn.getTableId())
                .eq(!StringUtils.isNull(sysTableColumn.getColumnId()), SysTableColumn::getColumnId, sysTableColumn.getColumnId())
                .like(!StringUtils.isNull(sysTableColumn.getColumnName()), SysTableColumn::getColumnName, sysTableColumn.getColumnName())
                .like(!StringUtils.isNull(sysTableColumn.getRemark()), SysTableColumn::getRemark, sysTableColumn.getRemark())
                .like(!StringUtils.isNull(sysTableColumn.getCreateBy()), SysTableColumn::getCreateBy, sysTableColumn.getCreateBy())
                .like(!StringUtils.isNull(sysTableColumn.getUpdateBy()), SysTableColumn::getUpdateBy, sysTableColumn.getUpdateBy())
                .ge(sysTableColumn.getParams().get("beginCreateTime") != null, SysTableColumn::getCreateTime, sysTableColumn.getParams().get("beginCreateTime"))
                .le(sysTableColumn.getParams().get("endCreateTime") != null, SysTableColumn::getCreateTime, sysTableColumn.getParams().get("endCreateTime"))
                .ge(sysTableColumn.getParams().get("beginUpdateTime") != null, SysTableColumn::getUpdateTime, sysTableColumn.getParams().get("beginUpdateTime"))
                .le(sysTableColumn.getParams().get("endUpdateTime") != null, SysTableColumn::getUpdateTime, sysTableColumn.getParams().get("endUpdateTime"))
                .orderByDesc(SysTableColumn::getColumnId);
        return lambdaQueryWrapper;
    }
}
