package com.ruoyi.tableConfig.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTablePager;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色功能表分页配置Service接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface ISysRoleTablePagerService extends IServicePlus<SysRoleTablePager> {
    /**
     * 查询角色功能表分页配置
     *
     * @param roleTableId
     * @return 角色功能表分页配置
     */
    public SysRoleTablePager selectSysRoleTablePagerByRoleTableId(Long roleTableId);

    /**
     * 查询角色功能表分页配置
     *
     * @param pagerId 角色功能表分页配置主键
     * @return 角色功能表分页配置
     */
    public SysRoleTablePager selectSysRoleTablePagerByPagerId(Long pagerId);

    /**
     * 查询角色功能表分页配置
     *
     * @param rolePagerId 角色功能表分页配置主键
     * @return 角色功能表分页配置
     */
    public SysRoleTablePager selectSysRoleTablePagerByRolePagerId(Long rolePagerId);

    /**
     * 查询角色功能表分页配置列表
     *
     * @param sysRoleTablePager 角色功能表分页配置
     * @return 角色功能表分页配置集合
     */
    public List<SysRoleTablePager> selectSysRoleTablePagerList(SysRoleTablePager sysRoleTablePager);

    /**
     * 查询角色功能表分页配置分页列表
     *
     * @param sysRoleTablePager 角色功能表分页配置
     * @param pageQuery         查询配置
     * @return
     */
    public TableDataInfo<SysRoleTablePager> selectSysRoleTablePagerPage(SysRoleTablePager sysRoleTablePager, PageQuery pageQuery);

    /**
     * 新增角色功能表分页配置
     *
     * @param sysRoleTablePager 角色功能表分页配置
     * @return 结果
     */
    public int insertSysRoleTablePager(SysRoleTablePager sysRoleTablePager);

    /**
     * 修改角色功能表分页配置
     *
     * @param sysRoleTablePager 角色功能表分页配置
     * @return 结果
     */
    public int updateSysRoleTablePager(SysRoleTablePager sysRoleTablePager);

    /**
     * 批量删除角色功能表分页配置
     *
     * @param rolePagerIds 需要删除的角色功能表分页配置主键集合
     * @return 结果
     */
    public int deleteSysRoleTablePagerByRolePagerIds(Long[] rolePagerIds);

    /**
     * 删除角色功能表分页配置信息
     *
     * @param rolePagerId 角色功能表分页配置主键
     * @return 结果
     */
    public int deleteSysRoleTablePagerByRolePagerId(Long rolePagerId);

    /**
     * 角色功能表分页配置查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysRoleTablePager> buildQueryWrapper(SysRoleTablePager sysRoleTablePager);

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
