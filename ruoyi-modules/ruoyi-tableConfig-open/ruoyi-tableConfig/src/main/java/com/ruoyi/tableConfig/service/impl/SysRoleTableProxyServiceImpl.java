package com.ruoyi.tableConfig.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysRoleTableProxyMapper;
import com.ruoyi.tableConfig.service.ISysRoleTableProxyService;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色功能表数据代理Service业务层处理
 *
 * @author zly
 * @date 2023-06-06
 */
@Service
public class SysRoleTableProxyServiceImpl extends ServiceImpl<SysRoleTableProxyMapper, SysRoleTableProxy> implements ISysRoleTableProxyService {
    @Autowired
    private SysRoleTableProxyMapper sysRoleTableProxyMapper;

    /**
     * 查询角色功能表数据代理
     *
     * @param roleTableId
     * @return 角色功能表数据代理
     */
    @Override
    public SysRoleTableProxy selectSysRoleTableProxyByRoleTableId(Long roleTableId) {
        return sysRoleTableProxyMapper.selectSysRoleTableProxyByRoleTableId(roleTableId);
    }

    /**
     * 查询角色功能表数据代理
     *
     * @param proxyId 角色功能表数据代理主键
     * @return 角色功能表数据代理
     */
    @Override
    public SysRoleTableProxy selectSysRoleTableProxyByProxyId(Long proxyId) {
        return sysRoleTableProxyMapper.selectSysRoleTableProxyByProxyId(proxyId);
    }

    @Override
    public SysRoleTableProxy selectSysRoleTableProxyByRoleProxyId(Long roleProxyId) {
        return sysRoleTableProxyMapper.selectSysRoleTableProxyByRoleProxyId(roleProxyId);
    }

    /**
     * 查询角色功能表数据代理列表
     *
     * @param sysRoleTableProxy 角色功能表数据代理
     * @return 角色功能表数据代理
     */
    @Override
    public List<SysRoleTableProxy> selectSysRoleTableProxyList(SysRoleTableProxy sysRoleTableProxy) {
        return sysRoleTableProxyMapper.selectSysRoleTableProxyList(sysRoleTableProxy);
    }

    /**
     * 查询角色功能表数据代理分页列表
     *
     * @param sysRoleTableProxy 角色功能表数据代理
     * @param pageQuery         查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysRoleTableProxy> selectSysRoleTableProxyPage(SysRoleTableProxy sysRoleTableProxy, PageQuery pageQuery) {
        LambdaQueryWrapper<SysRoleTableProxy> lqw = buildQueryWrapper(sysRoleTableProxy);
        Page<SysRoleTableProxy> page = sysRoleTableProxyMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增角色功能表数据代理
     *
     * @param sysRoleTableProxy 角色功能表数据代理
     * @return 结果
     */
    @Override
    public int insertSysRoleTableProxy(SysRoleTableProxy sysRoleTableProxy) {
        sysRoleTableProxy.setCreateTime(DateUtils.getNowDate());
        return sysRoleTableProxyMapper.insertSysRoleTableProxy(sysRoleTableProxy);
    }

    /**
     * 修改角色功能表数据代理
     *
     * @param sysRoleTableProxy 角色功能表数据代理
     * @return 结果
     */
    @Override
    public int updateSysRoleTableProxy(SysRoleTableProxy sysRoleTableProxy) {
        sysRoleTableProxy.setUpdateTime(DateUtils.getNowDate());
        return sysRoleTableProxyMapper.updateSysRoleTableProxy(sysRoleTableProxy);
    }

    /**
     * 批量删除角色功能表数据代理
     *
     * @param roleProxyIds 需要删除的角色功能表数据代理主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableProxyByRoleProxyIds(Long[] roleProxyIds) {
        return sysRoleTableProxyMapper.deleteSysRoleTableProxyByRoleProxyIds(roleProxyIds);
    }

    /**
     * 删除角色功能表数据代理信息
     *
     * @param roleProxyId 角色功能表数据代理主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableProxyByRoleProxyId(Long roleProxyId) {
        return sysRoleTableProxyMapper.deleteSysRoleTableProxyByRoleProxyId(roleProxyId);
    }

    /**
     * 角色功能表数据代理查询条件
     *
     * @param sysRoleTableProxy
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysRoleTableProxy> buildQueryWrapper(SysRoleTableProxy sysRoleTableProxy) {
        LambdaQueryWrapper<SysRoleTableProxy> lambdaQueryWrapper = new LambdaQueryWrapper<SysRoleTableProxy>()
                .eq(!StringUtils.isNull(sysRoleTableProxy.getRoleProxyId()), SysRoleTableProxy::getRoleProxyId, sysRoleTableProxy.getRoleProxyId())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getProxyId()), SysRoleTableProxy::getProxyId, sysRoleTableProxy.getProxyId())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getRoleTableId()), SysRoleTableProxy::getRoleTableId, sysRoleTableProxy.getRoleTableId())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getEnabled()), SysRoleTableProxy::getEnabled, sysRoleTableProxy.getEnabled())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getAutoLoad()), SysRoleTableProxy::getAutoLoad, sysRoleTableProxy.getAutoLoad())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getMessage()), SysRoleTableProxy::getMessage, sysRoleTableProxy.getMessage())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getSeq()), SysRoleTableProxy::getSeq, sysRoleTableProxy.getSeq())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getSort()), SysRoleTableProxy::getSort, sysRoleTableProxy.getSort())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getFilter()), SysRoleTableProxy::getFilter, sysRoleTableProxy.getFilter())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getForm()), SysRoleTableProxy::getForm, sysRoleTableProxy.getForm())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getProps()), SysRoleTableProxy::getProps, sysRoleTableProxy.getProps())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getQuery()), SysRoleTableProxy::getQuery, sysRoleTableProxy.getQuery())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getQueryAll()), SysRoleTableProxy::getQueryAll, sysRoleTableProxy.getQueryAll())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getDelete()), SysRoleTableProxy::getDelete, sysRoleTableProxy.getDelete())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getInsert()), SysRoleTableProxy::getInsert, sysRoleTableProxy.getInsert())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getUpdate()), SysRoleTableProxy::getUpdate, sysRoleTableProxy.getUpdate())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getSave()), SysRoleTableProxy::getSave, sysRoleTableProxy.getSave())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getParamsChangeLoad()), SysRoleTableProxy::getParamsChangeLoad, sysRoleTableProxy.getParamsChangeLoad())
                .eq(!StringUtils.isNull(sysRoleTableProxy.getCreateBy()), SysRoleTableProxy::getCreateBy, sysRoleTableProxy.getCreateBy())
                .ge(sysRoleTableProxy.getParams().get("beginCreateTime") != null, SysRoleTableProxy::getCreateTime, sysRoleTableProxy.getParams().get("beginCreateTime"))
                .le(sysRoleTableProxy.getParams().get("endCreateTime") != null, SysRoleTableProxy::getCreateTime, sysRoleTableProxy.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysRoleTableProxy.getUpdateBy()), SysRoleTableProxy::getUpdateBy, sysRoleTableProxy.getUpdateBy())
                .ge(sysRoleTableProxy.getParams().get("beginUpdateTime") != null, SysRoleTableProxy::getUpdateTime, sysRoleTableProxy.getParams().get("beginUpdateTime"))
                .le(sysRoleTableProxy.getParams().get("endUpdateTime") != null, SysRoleTableProxy::getUpdateTime, sysRoleTableProxy.getParams().get("endUpdateTime"))
                .eq(!StringUtils.isNull(sysRoleTableProxy.getRemark()), SysRoleTableProxy::getRemark, sysRoleTableProxy.getRemark())
                .orderByDesc(SysRoleTableProxy::getRemark);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysRoleTableProxy> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysRoleTableProxy> iPage = sysRoleTableProxyMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper<>());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysRoleTableProxyMapper.selectBatchIds(exceel.getIds());
        } else {
            SysRoleTableProxy sysRoleTableProxy = JSON.parseObject(String.valueOf(exceel.getQuery()), SysRoleTableProxy.class);
            list = sysRoleTableProxyMapper.selectSysRoleTableProxyList(sysRoleTableProxy);
        }
        String fileName = exceel.getFilename().equals(null) ? "角色功能表数据代理数据" : exceel.getFilename();
        Set<String> column = Arrays.stream(exceel.getFields().stream().map(m -> m.getField()).collect(Collectors.toList()).stream().toArray(String[]::new)).collect(Collectors.toSet());
        ExcelUtil.exportExcel(list, fileName, SysRoleTableProxy.class, response, column);
    }
}
