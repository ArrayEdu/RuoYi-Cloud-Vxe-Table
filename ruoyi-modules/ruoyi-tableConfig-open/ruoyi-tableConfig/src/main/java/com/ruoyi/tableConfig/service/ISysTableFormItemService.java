package com.ruoyi.tableConfig.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysTableFormItem;

/**
 * 表单配置项列Service接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface ISysTableFormItemService extends IServicePlus<SysTableFormItem> {
    /**
     * 查询表单配置项列
     *
     * @param formId 表单配置id
     * @return 表单配置项列
     */
    public List<SysTableFormItem> selectSysTableFormItemByFormId(Long formId);

    /**
     * 查询表单配置项列
     *
     * @param itemId 表单配置项列主键
     * @return 表单配置项列
     */
    public SysTableFormItem selectSysTableFormItemByItemId(Long itemId);

    /**
     * 查询表单配置项列列表
     *
     * @param sysTableFormItem 表单配置项列
     * @return 表单配置项列集合
     */
    public List<SysTableFormItem> selectSysTableFormItemList(SysTableFormItem sysTableFormItem);

    /**
     * 查询表单配置项列分页列表
     *
     * @param sysTableFormItem 表单配置项列
     * @param pageQuery        查询配置
     * @return
     */
    public TableDataInfo<SysTableFormItem> selectSysTableFormItemPage(SysTableFormItem sysTableFormItem, PageQuery pageQuery);

    /**
     * 新增表单配置项列
     *
     * @param sysTableFormItem 表单配置项列
     * @return 结果
     */
    public int insertSysTableFormItem(SysTableFormItem sysTableFormItem);

    /**
     * 修改表单配置项列
     *
     * @param sysTableFormItem 表单配置项列
     * @return 结果
     */
    public int updateSysTableFormItem(SysTableFormItem sysTableFormItem);

    /**
     * 批量删除表单配置项列
     *
     * @param itemIds 需要删除的表单配置项列主键集合
     * @return 结果
     */
    public int deleteSysTableFormItemByItemIds(Long[] itemIds);

    /**
     * 删除表单配置项列信息
     *
     * @param itemId 表单配置项列主键
     * @return 结果
     */
    public int deleteSysTableFormItemByItemId(Long itemId);

    public LambdaQueryWrapper<SysTableFormItem> buildQueryWrapper(SysTableFormItem sysTableFormItem);
}
