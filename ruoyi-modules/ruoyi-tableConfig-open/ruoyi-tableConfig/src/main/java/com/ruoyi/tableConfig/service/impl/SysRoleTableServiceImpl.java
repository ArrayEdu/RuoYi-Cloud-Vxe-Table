package com.ruoyi.tableConfig.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.RemoteRoleService;
import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.system.api.domain.SysRoleMenu;
import com.ruoyi.system.domain.SysUserRole;
import com.ruoyi.system.mapper.SysRoleMapper;
import com.ruoyi.system.mapper.SysUserRoleMapper;
import com.ruoyi.tableConfig.api.domain.*;
import com.ruoyi.tableConfig.mapper.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.service.ISysRoleTableService;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;

/**
 * 表格权限业务表Service业务层处理
 *
 * @author zly
 * @date 2023-06-06
 */
@Service
public class SysRoleTableServiceImpl extends ServiceImpl<SysRoleTableMapper, SysRoleTable> implements ISysRoleTableService {
    @Autowired
    private SysRoleTableMapper sysRoleTableMapper;
    @Autowired
    private RemoteRoleService remoteRoleService;
    @Autowired
    private SysTableMapper sysTableMapper;

    @Autowired
    private SysTableColumnMapper sysTableColumnMapper;
    @Autowired
    private SysRoleTableColumnMapper sysRoleTableColumnMapper;

    @Autowired
    private SysTableEditConfigMapper sysTableEditConfigMapper;
    @Autowired
    private SysRoleTableEditConfigMapper sysRoleTableEditConfigMapper;

    @Autowired
    private SysTableEditRulesMapper sysTableEditRulesMapper;
    @Autowired
    private SysRoleTableEditRulesMapper sysRoleTableEditRulesMapper;

    @Autowired
    private SysTableFormMapper sysTableFormMapper;
    @Autowired
    private SysRoleTableFormMapper sysRoleTableFormMapper;

    @Autowired
    private SysTableFormItemMapper sysTableFormItemMapper;
    @Autowired
    private SysRoleTableFormItemMapper sysRoleTableFormItemMapper;

    @Autowired
    private SysTablePagerMapper sysTablePagerMapper;
    @Autowired
    private SysRoleTablePagerMapper sysRoleTablePagerMapper;

    @Autowired
    private SysTableProxyMapper sysTableProxyMapper;
    @Autowired
    private SysRoleTableProxyMapper sysRoleTableProxyMapper;

    @Autowired
    private SysTableToolbarConfigMapper sysTableToolbarConfigMapper;
    @Autowired
    private SysRoleTableToolbarConfigMapper sysRoleTableToolbarConfigMapper;

    @Autowired
    private SysUserTableColumnMapper sysUserTableColumnMapper;

    @Autowired
    private SysUserTableFormItemMapper sysUserTableFormItemMapper;

    @Autowired
    private SysRoleMapper roleMapper;

    @Autowired
    private SysUserRoleMapper userRoleMapper;


    /**
     * 同步菜单数据
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void syncMenu(Long roleId) {
        R<List<SysRoleMenu>> sysRoleMenuList = remoteRoleService.getInfoByRoleId(roleId, SecurityConstants.INNER);
        if (sysRoleMenuList.getData().size() > 0) {
            List<SysRoleTable> sysRoleTableList = sysRoleTableMapper.selectSysRoleTableByRoleId(roleId);
            ArrayList<Long> tableIds = new ArrayList<Long>();
            if (sysRoleTableList.size() > 0) {
                for (SysRoleTable sysRoleTable : sysRoleTableList) {
                    tableIds.add(sysRoleTable.getTableId());
                }
            }
            String userName = SecurityUtils.getUsername();
            Long userId = SecurityUtils.getUserId();
            Date date = new Date();
            for (SysRoleMenu sysRoleMenu : sysRoleMenuList.getData()) {
                //查询菜单功能表格配置
                SysTable sysTable = new SysTable();
                sysTable.setMenuId(sysRoleMenu.getMenuId());
                List<SysTable> sysTableList = sysTableMapper.selectSysTableList(sysTable);
                for (SysTable s : sysTableList) {
                    if (!(tableIds.indexOf(s.getTableId()) > -1)) {
                        SysRoleTable sysRoleTable1 = new SysRoleTable();
                        BeanUtils.copyProperties(s, sysRoleTable1);
                        sysRoleTable1.setRoleId(roleId);
                        sysRoleTable1.setCreateBy(userId);
                        sysRoleTable1.setCreateTime(date);
                        sysRoleTable1.setId(sysRoleTable1.getTableName() + "_" + sysRoleTable1.getTableId() + "_" + roleId);
                        sysRoleTableMapper.insertSysRoleTable(sysRoleTable1);
                        Long roleTableId = sysRoleTable1.getRoleTableId();
                        Long tableId = sysRoleTable1.getTableId();
                        List<SysTableColumn> sysTableColumnList = sysTableColumnMapper.selectSysTableColumnByTableId(tableId);
                        for (SysTableColumn sysTableColumn : sysTableColumnList) {
                            SysRoleTableColumn sysRoleTableColumn = new SysRoleTableColumn();
                            BeanUtils.copyProperties(sysTableColumn, sysRoleTableColumn);
                            sysRoleTableColumn.setRoleTableId(roleTableId);
                            sysRoleTableColumn.setCreateBy(userId);
                            sysRoleTableColumn.setCreateTime(date);
                            sysRoleTableColumnMapper.insertSysRoleTableColumn(sysRoleTableColumn);
                        }
                        //可编辑配置项
                        SysTableEditConfig sysTableEditConfig = sysTableEditConfigMapper.selectSysTableEditConfigByTableId(tableId);
                        if (null != sysTableEditConfig) {
                            SysRoleTableEditConfig sysRoleTableEditConfig = new SysRoleTableEditConfig();
                            BeanUtils.copyProperties(sysTableEditConfig, sysRoleTableEditConfig);
                            sysRoleTableEditConfig.setRoleTableId(roleTableId);
                            sysRoleTableEditConfig.setCreateBy(userId);
                            sysRoleTableEditConfig.setCreateTime(date);
                            sysRoleTableEditConfigMapper.insertSysRoleTableEditConfig(sysRoleTableEditConfig);
                        }
                        //校验规则配置项
                        List<SysTableEditRules> sysTableEditRulesList = sysTableEditRulesMapper.selectSysTableEditRulesByTableId(tableId);
                        for (SysTableEditRules sysTableEditRules : sysTableEditRulesList) {
                            SysRoleTableEditRules sysRoleTableEditRules = new SysRoleTableEditRules();
                            BeanUtils.copyProperties(sysTableEditRules, sysRoleTableEditRules);
                            sysRoleTableEditRules.setRoleTableId(roleTableId);
                            sysRoleTableEditRules.setCreateBy(userId);
                            sysRoleTableEditRules.setCreateTime(date);
                            sysRoleTableEditRulesMapper.insertSysRoleTableEditRules(sysRoleTableEditRules);
                        }
                        //查询配置
                        SysTableForm sysTableForm = sysTableFormMapper.selectSysTableFormByTableId(tableId);
                        if (null != sysTableForm) {
                            SysRoleTableForm sysRoleTableForm = new SysRoleTableForm();
                            BeanUtils.copyProperties(sysTableForm, sysRoleTableForm);
                            sysRoleTableForm.setRoleTableId(roleTableId);
                            sysRoleTableForm.setCreateBy(userId);
                            sysRoleTableForm.setCreateTime(date);
                            sysRoleTableFormMapper.insertSysRoleTableForm(sysRoleTableForm);
                            List<SysTableFormItem> sysTableFormItemList = sysTableFormItemMapper.selectSysTableFormItemByFormId(sysTableForm.getFormId());
                            for (SysTableFormItem sysTableFormItem : sysTableFormItemList) {
                                SysRoleTableFormItem sysRoleTableFormItem = new SysRoleTableFormItem();
                                BeanUtils.copyProperties(sysTableFormItem, sysRoleTableFormItem);
                                sysRoleTableFormItem.setRoleFormId(sysRoleTableForm.getRoleFormId());
                                sysRoleTableFormItem.setCreateBy(userId);
                                sysRoleTableFormItem.setCreateTime(date);
                                sysRoleTableFormItemMapper.insertSysRoleTableFormItem(sysRoleTableFormItem);
                            }
                        }
                        //分页配置
                        SysTablePager sysTablePager = sysTablePagerMapper.selectSysTablePagerByTableId(tableId);
                        if (null != sysTablePager) {
                            SysRoleTablePager sysRoleTablePager = new SysRoleTablePager();
                            BeanUtils.copyProperties(sysTablePager, sysRoleTablePager);
                            sysRoleTablePager.setRoleTableId(roleTableId);
                            sysRoleTablePager.setCreateBy(userId);
                            sysRoleTablePager.setCreateTime(date);
                            sysRoleTablePagerMapper.insertSysRoleTablePager(sysRoleTablePager);
                        }
                        //代理配置
                        SysTableProxy sysTableProxy = sysTableProxyMapper.selectSysTableProxyByTableId(tableId);
                        if (null != sysTableProxy) {
                            SysRoleTableProxy sysRoleTableProxy = new SysRoleTableProxy();
                            BeanUtils.copyProperties(sysTableProxy, sysRoleTableProxy);
                            sysRoleTableProxy.setRoleTableId(roleTableId);
                            sysRoleTableProxy.setCreateBy(userId);
                            sysRoleTableProxy.setCreateTime(date);
                            sysRoleTableProxyMapper.insertSysRoleTableProxy(sysRoleTableProxy);
                        }
                        //工具栏配置
                        SysTableToolbarConfig sysTableToolbarConfig = sysTableToolbarConfigMapper.selectSysTableToolbarConfigByTableId(tableId);
                        if (null != sysTableToolbarConfig) {
                            SysRoleTableToolbarConfig sysRoleTableToolbarConfig = new SysRoleTableToolbarConfig();
                            BeanUtils.copyProperties(sysTableToolbarConfig, sysRoleTableToolbarConfig);
                            sysRoleTableToolbarConfig.setRoleTableId(roleTableId);
                            sysRoleTableToolbarConfig.setCreateBy(userId);
                            sysRoleTableToolbarConfig.setCreateTime(date);
                            sysRoleTableToolbarConfigMapper.insertSysRoleTableToolbarConfig(sysRoleTableToolbarConfig);
                        }
                    }
                }
            }
        }
    }

    /**
     * 同步数据
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void synchDb(Long roleTableId, Long tableId, Long roleId, Long menuId) {
        SysTable sysTable = sysTableMapper.selectSysTableByTableId(tableId);
        Long userId = SecurityUtils.getUserId();
        Date date = new Date();
        SysRoleTable sysRoleTable = new SysRoleTable();
        BeanUtils.copyProperties(sysTable, sysRoleTable);
        sysRoleTable.setRoleTableId(roleTableId);
        sysRoleTable.setRoleId(roleId);
        sysRoleTable.setMenuId(menuId);
        sysRoleTable.setUpdateBy(userId);
        sysRoleTable.setUpdateTime(date);
        sysRoleTableMapper.updateSysRoleTable(sysRoleTable);
        List<SysTableColumn> sysTableColumnList = sysTableColumnMapper.selectSysTableColumnByTableId(tableId);
        List<SysRoleTableColumn> sysRoleTableColumnList = sysRoleTableColumnMapper.selectSysRoleTableColumnByRoleTableId(roleTableId);
        ArrayList<Long> columnIds = new ArrayList<Long>();
        if (sysRoleTableColumnList.size() > 0) {
            for (SysRoleTableColumn sysRoleTableColumn : sysRoleTableColumnList) {
                columnIds.add(sysRoleTableColumn.getColumnId());
            }
        }
        for (SysTableColumn sysTableColumn : sysTableColumnList) {
            if (sysRoleTableColumnList.size() > 0) {
                //插入角色表格列中不存在的
                if (columnIds.size() > 0 && !(columnIds.indexOf(sysTableColumn.getColumnId()) > -1)) {
                    SysRoleTableColumn sysRoleTableColumn = new SysRoleTableColumn();
                    BeanUtils.copyProperties(sysTableColumn, sysRoleTableColumn);
                    sysRoleTableColumn.setRoleTableId(roleTableId);
                    sysRoleTableColumn.setCreateBy(userId);
                    sysRoleTableColumn.setCreateTime(date);
                    sysRoleTableColumnMapper.insertSysRoleTableColumn(sysRoleTableColumn);
                }
            } else {
                SysRoleTableColumn sysRoleTableColumn = new SysRoleTableColumn();
                BeanUtils.copyProperties(sysTableColumn, sysRoleTableColumn);
                sysRoleTableColumn.setRoleTableId(roleTableId);
                sysRoleTableColumn.setCreateBy(userId);
                sysRoleTableColumn.setCreateTime(date);
                sysRoleTableColumnMapper.insertSysRoleTableColumn(sysRoleTableColumn);
            }
        }
        //可编辑配置项
        SysTableEditConfig sysTableEditConfig = sysTableEditConfigMapper.selectSysTableEditConfigByTableId(tableId);
        SysRoleTableEditConfig sysRoleTableEditConfig = sysRoleTableEditConfigMapper.selectSysRoleTableEditConfigByRoleTableId(roleTableId);
        if (null != sysTableEditConfig) {
            SysRoleTableEditConfig e = new SysRoleTableEditConfig();
            e.setRoleTableId(roleTableId);
            BeanUtils.copyProperties(sysTableEditConfig, e);
            if (null == sysRoleTableEditConfig) {
                e.setCreateBy(userId);
                e.setCreateTime(date);
                sysRoleTableEditConfigMapper.insertSysRoleTableEditConfig(e);
            }
//            else {
//                e.setRoleEditConfigId(sysRoleTableEditConfig.getRoleEditConfigId());
//                e.setUpdateBy(userId);
//                e.setUpdateTime(date);
//                sysRoleTableEditConfigMapper.updateSysRoleTableEditConfig(e);
//            }
        }
        //校验规则配置项
        List<SysTableEditRules> sysTableEditRulesList = sysTableEditRulesMapper.selectSysTableEditRulesByTableId(tableId);
        List<SysRoleTableEditRules> sysRoleTableEditRulesList = sysRoleTableEditRulesMapper.selectSysRoleTableEditRulesByRoleTableId(roleTableId);
        ArrayList<Long> editRules = new ArrayList<Long>();
        if (sysRoleTableEditRulesList.size() > 0) {
            for (SysRoleTableEditRules sysRoleTableEditRules : sysRoleTableEditRulesList) {
                editRules.add(sysRoleTableEditRules.getEditRulesId());
            }
        }
        for (SysTableEditRules sysTableEditRules : sysTableEditRulesList) {
            if (sysRoleTableEditRulesList.size() > 0) {
                //只插入角色校验规则配置中不存在的
                if (editRules.size() > 0 && !(editRules.indexOf(sysTableEditRules.getEditRulesId()) > -1)) {
                    SysRoleTableEditRules sysRoleTableEditRules = new SysRoleTableEditRules();
                    BeanUtils.copyProperties(sysTableEditRules, sysRoleTableEditRules);
                    sysRoleTableEditRules.setRoleTableId(roleTableId);
                    sysRoleTableEditRules.setCreateBy(userId);
                    sysRoleTableEditRules.setCreateTime(date);
                    sysRoleTableEditRulesMapper.insertSysRoleTableEditRules(sysRoleTableEditRules);
                }
            } else {
                SysRoleTableEditRules sysRoleTableEditRules = new SysRoleTableEditRules();
                BeanUtils.copyProperties(sysTableEditRules, sysRoleTableEditRules);
                sysRoleTableEditRules.setRoleTableId(roleTableId);
                sysRoleTableEditRules.setCreateBy(userId);
                sysRoleTableEditRules.setCreateTime(date);
                sysRoleTableEditRulesMapper.insertSysRoleTableEditRules(sysRoleTableEditRules);
            }
        }
        //查询配置
        SysTableForm sysTableForm = sysTableFormMapper.selectSysTableFormByTableId(tableId);
        SysRoleTableForm sysRoleTableForm = sysRoleTableFormMapper.selectSysRoleTableFormByRoleTableId(roleTableId);
        if (null != sysTableForm) {
            SysRoleTableForm f = new SysRoleTableForm();
            f.setRoleTableId(roleTableId);
            BeanUtils.copyProperties(sysTableForm, f);
            Long roleFormId = f.getRoleFormId();
            if (null == sysRoleTableForm) {
                f.setCreateBy(userId);
                f.setCreateTime(date);
                sysRoleTableFormMapper.insertSysRoleTableForm(f);
                roleFormId = f.getRoleFormId();
            } else {
                f.setRoleFormId(sysRoleTableForm.getRoleFormId());
                roleFormId = sysRoleTableForm.getRoleFormId();
//                f.setRoleFormId(sysRoleTableForm.getRoleFormId());
//                f.setUpdateBy(userId);
//                f.setUpdateTime(date);
//                sysRoleTableFormMapper.updateSysRoleTableForm(f);

            }
            List<SysTableFormItem> sysTableFormItemList = sysTableFormItemMapper.selectSysTableFormItemByFormId(sysTableForm.getFormId());
            List<SysRoleTableFormItem> sysRoleTableFormItemList = sysRoleTableFormItemMapper.selectSysRoleTableFormItemByRoleFormId(roleFormId);
            List<Long> roleFormItem = new ArrayList<Long>();
            if (sysRoleTableFormItemList.size() > 0) {
                for (SysRoleTableFormItem sysRoleTableFormItem : sysRoleTableFormItemList) {
                    roleFormItem.add(sysRoleTableFormItem.getItemId());
                }
            }
            for (SysTableFormItem sysTableFormItem : sysTableFormItemList) {
                if (sysRoleTableFormItemList.size() > 0) {
                    if (roleFormItem.size() > 0 && !(roleFormItem.indexOf(sysTableFormItem.getItemId()) > -1)) {
                        SysRoleTableFormItem sysRoleTableFormItem = new SysRoleTableFormItem();
                        BeanUtils.copyProperties(sysTableFormItem, sysRoleTableFormItem);
                        sysRoleTableFormItem.setRoleFormId(roleFormId);
                        sysRoleTableFormItem.setCreateBy(userId);
                        sysRoleTableFormItem.setCreateTime(date);
                        sysRoleTableFormItemMapper.insertSysRoleTableFormItem(sysRoleTableFormItem);
                    }
                } else {
                    SysRoleTableFormItem sysRoleTableFormItem = new SysRoleTableFormItem();
                    BeanUtils.copyProperties(sysTableFormItem, sysRoleTableFormItem);
                    sysRoleTableFormItem.setRoleFormId(roleFormId);
                    sysRoleTableFormItem.setCreateBy(userId);
                    sysRoleTableFormItem.setCreateTime(date);
                    sysRoleTableFormItemMapper.insertSysRoleTableFormItem(sysRoleTableFormItem);
                }
            }
        }
        //分页配置
        SysTablePager sysTablePager = sysTablePagerMapper.selectSysTablePagerByTableId(tableId);
        SysRoleTablePager sysRoleTablePager = sysRoleTablePagerMapper.selectSysRoleTablePagerByRoleTableId(roleTableId);
        if (null != sysTablePager) {
            SysRoleTablePager p = new SysRoleTablePager();
            p.setRoleTableId(roleTableId);
            BeanUtils.copyProperties(sysTablePager, p);
            if (null == sysRoleTablePager) {
                p.setCreateBy(userId);
                p.setCreateTime(date);
                sysRoleTablePagerMapper.insertSysRoleTablePager(p);
            }
//            else {
//                p.setRolePagerId(sysRoleTablePager.getRolePagerId());
//                p.setUpdateBy(userId);
//                p.setUpdateTime(date);
//                sysRoleTablePagerMapper.updateSysRoleTablePager(p);
//            }
        }
        //代理配置
        SysTableProxy sysTableProxy = sysTableProxyMapper.selectSysTableProxyByTableId(tableId);
        SysRoleTableProxy sysRoleTableProxy = sysRoleTableProxyMapper.selectSysRoleTableProxyByRoleTableId(roleTableId);
        if (null != sysTableProxy) {
            SysRoleTableProxy proxy = new SysRoleTableProxy();
            proxy.setRoleTableId(roleTableId);
            BeanUtils.copyProperties(sysTableProxy, proxy);
            if (null == sysRoleTableProxy) {
                proxy.setCreateBy(userId);
                proxy.setCreateTime(date);
                sysRoleTableProxyMapper.insertSysRoleTableProxy(proxy);
            }
//            else {
//                proxy.setRoleProxyId(sysRoleTableProxy.getRoleProxyId());
//                proxy.setUpdateBy(userId);
//                proxy.setUpdateTime(date);
//                sysRoleTableProxyMapper.updateSysRoleTableProxy(proxy);
//            }
        }
        //工具栏配置
        SysTableToolbarConfig sysTableToolbarConfig = sysTableToolbarConfigMapper.selectSysTableToolbarConfigByTableId(tableId);
        SysRoleTableToolbarConfig sysRoleTableToolbarConfig = sysRoleTableToolbarConfigMapper.selectSysRoleTableToolbarConfigByRoleTableId(roleTableId);
        if (null != sysTableToolbarConfig) {
            SysRoleTableToolbarConfig toolbarConfig = new SysRoleTableToolbarConfig();
            toolbarConfig.setRoleTableId(roleTableId);
            BeanUtils.copyProperties(sysTableToolbarConfig, toolbarConfig);
            if (null == sysRoleTableToolbarConfig) {
                toolbarConfig.setCreateBy(userId);
                toolbarConfig.setCreateTime(date);
                sysRoleTableToolbarConfigMapper.insertSysRoleTableToolbarConfig(toolbarConfig);
            }
//            else {
//                toolbarConfig.setRoleToolbarConfigId(sysRoleTableToolbarConfig.getRoleToolbarConfigId());
//                toolbarConfig.setUpdateBy(userId);
//                toolbarConfig.setUpdateTime(date);
//                sysRoleTableToolbarConfigMapper.updateSysRoleTableToolbarConfig(toolbarConfig);
//            }
        }
    }

    /**
     * 强制同步数据
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void forceSynchDb(Long roleTableId, Long tableId, Long roleId, Long menuId) {
        SysTable sysTable = sysTableMapper.selectSysTableByTableId(tableId);
        Long userId = SecurityUtils.getUserId();
        Date date = new Date();
        SysRoleTable sysRoleTable = new SysRoleTable();
        BeanUtils.copyProperties(sysTable, sysRoleTable);
        sysRoleTable.setRoleTableId(roleTableId);
        sysRoleTable.setRoleId(roleId);
        sysRoleTable.setMenuId(menuId);
        sysRoleTable.setUpdateBy(userId);
        sysRoleTable.setUpdateTime(date);
        sysRoleTableMapper.updateSysRoleTable(sysRoleTable);
        List<SysTableColumn> sysTableColumnList = sysTableColumnMapper.selectSysTableColumnByTableId(tableId);
        List<SysRoleTableColumn> sysRoleTableColumnList = sysRoleTableColumnMapper.selectSysRoleTableColumnByRoleTableId(roleTableId);
        ArrayList<Long> columnIds = new ArrayList<Long>();
        if (sysRoleTableColumnList.size() > 0) {
            for (SysRoleTableColumn sysRoleTableColumn : sysRoleTableColumnList) {
                columnIds.add(sysRoleTableColumn.getColumnId());
            }
        }
        //表格列
        for (SysTableColumn sysTableColumn : sysTableColumnList) {
            if (sysRoleTableColumnList.size() > 0) {
                //插入角色表格列中不存在的
                if (columnIds.size() > 0 && !(columnIds.indexOf(sysTableColumn.getColumnId()) > -1)) {
                    SysRoleTableColumn sysRoleTableColumn = new SysRoleTableColumn();
                    BeanUtils.copyProperties(sysTableColumn, sysRoleTableColumn);
                    sysRoleTableColumn.setRoleTableId(roleTableId);
                    sysRoleTableColumn.setCreateBy(userId);
                    sysRoleTableColumn.setCreateTime(date);
                    sysRoleTableColumnMapper.insertSysRoleTableColumn(sysRoleTableColumn);
                } else {
                    for (SysRoleTableColumn sysRoleTableColumn : sysRoleTableColumnList) {
                        if (sysTableColumn.getColumnId() == sysRoleTableColumn.getColumnId()) {
                            SysRoleTableColumn sysRoleTableColumn1 = new SysRoleTableColumn();
                            BeanUtils.copyProperties(sysTableColumn, sysRoleTableColumn1);
                            sysRoleTableColumn1.setRoleTableId(roleTableId);
                            sysRoleTableColumn1.setRoleColumnId(sysRoleTableColumn.getRoleColumnId());
                            sysRoleTableColumn1.setUpdateBy(userId);
                            sysRoleTableColumn1.setUpdateTime(date);
                            sysRoleTableColumnMapper.updateSysRoleTableColumn(sysRoleTableColumn1);
                        }
                    }
                }
            } else {
                SysRoleTableColumn sysRoleTableColumn = new SysRoleTableColumn();
                BeanUtils.copyProperties(sysTableColumn, sysRoleTableColumn);
                sysRoleTableColumn.setRoleTableId(roleTableId);
                sysRoleTableColumn.setCreateBy(userId);
                sysRoleTableColumn.setCreateTime(date);
                sysRoleTableColumnMapper.insertSysRoleTableColumn(sysRoleTableColumn);
            }
        }
        //可编辑配置项
        SysTableEditConfig sysTableEditConfig = sysTableEditConfigMapper.selectSysTableEditConfigByTableId(tableId);
        SysRoleTableEditConfig sysRoleTableEditConfig = sysRoleTableEditConfigMapper.selectSysRoleTableEditConfigByRoleTableId(roleTableId);
        if (null != sysTableEditConfig) {
            SysRoleTableEditConfig ec = new SysRoleTableEditConfig();
            ec.setRoleTableId(roleTableId);
            BeanUtils.copyProperties(sysTableEditConfig, ec);
            if (null == sysRoleTableEditConfig) {
                ec.setUpdateBy(userId);
                ec.setUpdateTime(date);
                sysRoleTableEditConfigMapper.insertSysRoleTableEditConfig(ec);
            } else {
                ec.setRoleEditConfigId(sysRoleTableEditConfig.getRoleEditConfigId());
                ec.setUpdateBy(userId);
                ec.setUpdateTime(date);
                sysRoleTableEditConfigMapper.updateSysRoleTableEditConfig(ec);
            }
        }
        //校验规则配置项
        List<SysTableEditRules> sysTableEditRulesList = sysTableEditRulesMapper.selectSysTableEditRulesByTableId(tableId);
        List<SysRoleTableEditRules> sysRoleTableEditRulesList = sysRoleTableEditRulesMapper.selectSysRoleTableEditRulesByRoleTableId(roleTableId);
        ArrayList<Long> editRules = new ArrayList<Long>();
        if (sysRoleTableEditRulesList.size() > 0) {
            for (SysRoleTableEditRules sysRoleTableEditRules : sysRoleTableEditRulesList) {
                editRules.add(sysRoleTableEditRules.getEditRulesId());
            }
        }
        for (SysTableEditRules sysTableEditRules : sysTableEditRulesList) {
            if (sysRoleTableEditRulesList.size() > 0) {
                //只插入角色校验规则配置中不存在的
                if (editRules.size() > 0 && !(editRules.indexOf(sysTableEditRules.getEditRulesId()) > -1)) {
                    SysRoleTableEditRules sysRoleTableEditRules = new SysRoleTableEditRules();
                    BeanUtils.copyProperties(sysTableEditRules, sysRoleTableEditRules);
                    sysRoleTableEditRules.setRoleTableId(roleTableId);
                    sysRoleTableEditRules.setCreateBy(userId);
                    sysRoleTableEditRules.setCreateTime(date);
                    sysRoleTableEditRulesMapper.insertSysRoleTableEditRules(sysRoleTableEditRules);
                } else {
                    for (SysRoleTableEditRules sysRoleTableEditRules : sysRoleTableEditRulesList) {
                        if (sysTableEditRules.getEditRulesId() == sysRoleTableEditRules.getEditRulesId()) {
                            SysRoleTableEditRules sysRoleTableEditRules1 = new SysRoleTableEditRules();
                            BeanUtils.copyProperties(sysTableEditRules, sysRoleTableEditRules1);
                            sysRoleTableEditRules1.setRoleTableId(roleTableId);
                            sysRoleTableEditRules1.setRoleEditRulesId(sysRoleTableEditRules.getRoleEditRulesId());
                            sysRoleTableEditRules1.setUpdateBy(userId);
                            sysRoleTableEditRules1.setUpdateTime(date);
                            sysRoleTableEditRulesMapper.updateSysRoleTableEditRules(sysRoleTableEditRules1);
                        }
                    }
                }
            } else {
                SysRoleTableEditRules sysRoleTableEditRules = new SysRoleTableEditRules();
                BeanUtils.copyProperties(sysTableEditRules, sysRoleTableEditRules);
                sysRoleTableEditRules.setRoleTableId(roleTableId);
                sysRoleTableEditRules.setCreateBy(userId);
                sysRoleTableEditRules.setCreateTime(date);
                sysRoleTableEditRulesMapper.insertSysRoleTableEditRules(sysRoleTableEditRules);
            }
        }
        //查询配置
        SysTableForm sysTableForm = sysTableFormMapper.selectSysTableFormByTableId(tableId);
        SysRoleTableForm sysRoleTableForm = sysRoleTableFormMapper.selectSysRoleTableFormByRoleTableId(roleTableId);
        if (null != sysTableForm) {
            SysRoleTableForm f = new SysRoleTableForm();
            f.setRoleTableId(roleTableId);
            BeanUtils.copyProperties(sysTableForm, f);
            Long roleFormId = f.getRoleFormId();
            if (null == sysRoleTableForm) {
                f.setCreateBy(userId);
                f.setCreateTime(date);
                sysRoleTableFormMapper.insertSysRoleTableForm(f);
                roleFormId = f.getRoleFormId();
            } else {
                f.setRoleFormId(sysRoleTableForm.getRoleFormId());
                f.setUpdateBy(userId);
                f.setUpdateTime(date);
                sysRoleTableFormMapper.updateSysRoleTableForm(f);
                roleFormId = f.getRoleFormId();
            }
            List<SysTableFormItem> sysTableFormItemList = sysTableFormItemMapper.selectSysTableFormItemByFormId(sysTableForm.getFormId());
            List<SysRoleTableFormItem> sysRoleTableFormItemList = sysRoleTableFormItemMapper.selectSysRoleTableFormItemByRoleFormId(roleFormId);
            List<Long> roleFormItem = new ArrayList<Long>();
            if (sysRoleTableFormItemList.size() > 0) {
                for (SysRoleTableFormItem sysRoleTableFormItem : sysRoleTableFormItemList) {
                    roleFormItem.add(sysRoleTableFormItem.getItemId());
                }
            }
            for (SysTableFormItem sysTableFormItem : sysTableFormItemList) {
                if (sysRoleTableFormItemList.size() > 0) {
                    if (roleFormItem.size() > 0 && !(roleFormItem.indexOf(sysTableFormItem.getItemId()) > -1)) {
                        SysRoleTableFormItem sysRoleTableFormItem = new SysRoleTableFormItem();
                        BeanUtils.copyProperties(sysTableFormItem, sysRoleTableFormItem);
                        sysRoleTableFormItem.setRoleFormId(roleFormId);
                        sysRoleTableFormItem.setCreateBy(userId);
                        sysRoleTableFormItem.setCreateTime(date);
                        sysRoleTableFormItemMapper.insertSysRoleTableFormItem(sysRoleTableFormItem);
                    } else {
                        for (SysRoleTableFormItem sysRoleTableFormItem : sysRoleTableFormItemList) {
                            if (sysTableFormItem.getItemId() == sysRoleTableFormItem.getItemId()) {
                                SysRoleTableFormItem sysRoleTableFormItem1 = new SysRoleTableFormItem();
                                BeanUtils.copyProperties(sysTableFormItem, sysRoleTableFormItem1);
                                sysRoleTableFormItem1.setRoleFormId(sysRoleTableFormItem.getRoleFormId());
                                sysRoleTableFormItem1.setRoleItemId(sysRoleTableFormItem.getRoleItemId());
                                sysRoleTableFormItem1.setCreateBy(userId);
                                sysRoleTableFormItem1.setCreateTime(date);
                                sysRoleTableFormItemMapper.updateSysRoleTableFormItem(sysRoleTableFormItem1);
                            }
                        }
                    }
                } else {
                    SysRoleTableFormItem sysRoleTableFormItem = new SysRoleTableFormItem();
                    BeanUtils.copyProperties(sysTableFormItem, sysRoleTableFormItem);
                    sysRoleTableFormItem.setRoleFormId(roleFormId);
                    sysRoleTableFormItem.setCreateBy(userId);
                    sysRoleTableFormItem.setCreateTime(date);
                    sysRoleTableFormItemMapper.insertSysRoleTableFormItem(sysRoleTableFormItem);
                }
            }
        }
        //分页配置
        SysTablePager sysTablePager = sysTablePagerMapper.selectSysTablePagerByTableId(tableId);
        SysRoleTablePager sysRoleTablePager = sysRoleTablePagerMapper.selectSysRoleTablePagerByRoleTableId(roleTableId);
        if (null != sysTablePager) {
            SysRoleTablePager p = new SysRoleTablePager();
            p.setRoleTableId(roleTableId);
            BeanUtils.copyProperties(sysTablePager, p);
            if (null == sysRoleTablePager) {
                p.setCreateBy(userId);
                p.setCreateTime(date);
                sysRoleTablePagerMapper.insertSysRoleTablePager(p);
            } else {
                p.setRolePagerId(sysRoleTablePager.getRolePagerId());
                p.setUpdateBy(userId);
                p.setUpdateTime(date);
                sysRoleTablePagerMapper.updateSysRoleTablePager(p);
            }
        }
        //代理配置
        SysTableProxy sysTableProxy = sysTableProxyMapper.selectSysTableProxyByTableId(tableId);
        SysRoleTableProxy sysRoleTableProxy = sysRoleTableProxyMapper.selectSysRoleTableProxyByRoleTableId(roleTableId);
        if (null != sysTableProxy) {
            SysRoleTableProxy proxy = new SysRoleTableProxy();
            proxy.setRoleTableId(roleTableId);
            BeanUtils.copyProperties(sysTableProxy, proxy);
            if (null == sysRoleTableProxy) {
                proxy.setCreateBy(userId);
                proxy.setCreateTime(date);
                sysRoleTableProxyMapper.insertSysRoleTableProxy(proxy);
            } else {
                proxy.setRoleProxyId(sysRoleTableProxy.getRoleProxyId());
                proxy.setUpdateBy(userId);
                proxy.setUpdateTime(date);
                sysRoleTableProxyMapper.updateSysRoleTableProxy(proxy);
            }
        }
        //工具栏配置
        SysTableToolbarConfig sysTableToolbarConfig = sysTableToolbarConfigMapper.selectSysTableToolbarConfigByTableId(tableId);
        SysRoleTableToolbarConfig sysRoleTableToolbarConfig = sysRoleTableToolbarConfigMapper.selectSysRoleTableToolbarConfigByRoleTableId(roleTableId);
        if (null != sysTableToolbarConfig) {
            SysRoleTableToolbarConfig toolbarConfig = new SysRoleTableToolbarConfig();
            toolbarConfig.setRoleTableId(roleTableId);
            BeanUtils.copyProperties(sysTableToolbarConfig, toolbarConfig);
            if (null == sysRoleTableToolbarConfig) {
                toolbarConfig.setCreateBy(userId);
                toolbarConfig.setCreateTime(date);
                sysRoleTableToolbarConfigMapper.insertSysRoleTableToolbarConfig(toolbarConfig);
            } else {
                toolbarConfig.setRoleToolbarConfigId(sysRoleTableToolbarConfig.getRoleToolbarConfigId());
                toolbarConfig.setUpdateBy(userId);
                toolbarConfig.setUpdateTime(date);
                sysRoleTableToolbarConfigMapper.updateSysRoleTableToolbarConfig(toolbarConfig);
            }
        }
    }


    /**
     * 查询表格权限业务表
     *
     * @param sysRoleTable 表格权限业务表
     * @return 表格权限业务表
     */
    @Override
    public SysRoleTable selectSysRoleTable(SysRoleTable sysRoleTable) {
        return sysRoleTableMapper.selectSysRoleTable(sysRoleTable);
    }

    /**
     * 查询表格权限业务表
     *
     * @param tableId 菜单表格id
     * @return 表格权限业务表
     */
    @Override
    public List<SysRoleTable> selectSysRoleTableByTableId(Long tableId) {
        return sysRoleTableMapper.selectSysRoleTableByTableId(tableId);
    }

    /**
     * 查询表格权限业务表
     *
     * @param menuId 菜单id
     * @return 表格权限业务表
     */
    @Override
    public List<SysRoleTable> selectSysRoleTableByMenuId(Long menuId) {
        return sysRoleTableMapper.selectSysRoleTableByMenuId(menuId);
    }

    /**
     * 查询表格权限业务表
     *
     * @param roleId 角色id
     * @return 表格权限业务表
     */
    @Override
    public List<SysRoleTable> selectSysRoleTableByRoleId(Long roleId) {
        return sysRoleTableMapper.selectSysRoleTableByRoleId(roleId);
    }

    /**
     * 查询表格权限业务表
     *
     * @param roleTableId 表格权限业务表主键
     * @return 表格权限业务表
     */
    @Override
    public SysRoleTable selectSysRoleTableByRoleTableId(Long roleTableId) {
        return sysRoleTableMapper.selectSysRoleTableByRoleTableId(roleTableId);
    }

    /**
     * 查询表格权限业务表列表
     *
     * @param sysRoleTable 表格权限业务表
     * @return 表格权限业务表
     */
    @Override
    public List<SysRoleTable> selectSysRoleTableList(SysRoleTable sysRoleTable) {
        return sysRoleTableMapper.selectSysRoleTableList(sysRoleTable);
    }

    /**
     * 查询表格权限业务表分页列表
     *
     * @param sysRoleTable 表格权限业务表
     * @param pageQuery    分页配置
     * @return
     */
    @Override
    public TableDataInfo<SysRoleTable> selectSysRoleTablePage(SysRoleTable sysRoleTable, PageQuery pageQuery) {
        LambdaQueryWrapper<SysRoleTable> lqw = buildQueryWrapper(sysRoleTable);
        Page<SysRoleTable> page = sysRoleTableMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增表格权限业务表
     *
     * @param sysRoleTable 表格权限业务表
     * @return 结果
     */
    @Override
    public int insertSysRoleTable(SysRoleTable sysRoleTable) {
        sysRoleTable.setCreateTime(DateUtils.getNowDate());
        return sysRoleTableMapper.insertSysRoleTable(sysRoleTable);
    }

    /**
     * 修改表格权限业务表
     *
     * @param sysRoleTable 表格权限业务表
     * @return 结果
     */
    @Override
    public int updateSysRoleTable(SysRoleTable sysRoleTable) {
        sysRoleTable.setUpdateTime(DateUtils.getNowDate());
        return sysRoleTableMapper.updateSysRoleTable(sysRoleTable);
    }

    /**
     * 批量删除表格权限业务表
     *
     * @param roleTableIds 需要删除的表格权限业务表主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int removeSysRoleTableByRoleTableIds(Long[] roleTableIds) {
        Integer i = 0;
        for (Long id : roleTableIds) {
            Integer t = sysRoleTableMapper.deleteSysRoleTableByRoleTableId(id);
            if (t > 0) {
                List<SysRoleTableColumn> sysRoleTableColumnList = sysRoleTableColumnMapper.selectSysRoleTableColumnByRoleTableId(id);
                if (sysRoleTableColumnList.size() > 0) {
                    sysRoleTableColumnMapper.deleteSysRoleTableColumnByRoleTableId(id);
                }
                SysRoleTableEditConfig sysRoleTableEditConfig = sysRoleTableEditConfigMapper.selectSysRoleTableEditConfigByRoleTableId(id);
                if (null != sysRoleTableEditConfig) {
                    sysRoleTableEditConfigMapper.deleteSysRoleTableEditConfigByRoleTableId(id);
                }
                List<SysRoleTableEditRules> sysRoleTableEditRulesList = sysRoleTableEditRulesMapper.selectSysRoleTableEditRulesByRoleTableId(id);
                if (sysRoleTableEditRulesList.size() > 0) {
                    sysRoleTableEditRulesMapper.deleteSysRoleTableEditRulesByRoleTableId(id);
                }
                SysRoleTableForm sysRoleTableForm = sysRoleTableFormMapper.selectSysRoleTableFormByRoleTableId(id);
                if (null != sysRoleTableForm && null != sysRoleTableForm.getRoleFormId()) {
                    sysRoleTableFormMapper.deleteSysRoleTableFormByRoleTableId(id);
                    List<SysRoleTableFormItem> sysRoleTableFormItemList = sysRoleTableFormItemMapper.selectSysRoleTableFormItemByRoleFormId(sysRoleTableForm.getRoleFormId());
                    if (sysRoleTableFormItemList.size() > 0) {
                        sysRoleTableFormItemMapper.deleteSysRoleTableFormItemByRoleFormId(sysRoleTableForm.getRoleFormId());
                    }
                }
                SysRoleTablePager sysRoleTablePager = sysRoleTablePagerMapper.selectSysRoleTablePagerByRoleTableId(id);
                if (null != sysRoleTablePager) {
                    sysRoleTablePagerMapper.deleteSysRoleTablePagerByRoleTableId(id);
                }
                SysRoleTableProxy sysRoleTableProxy = sysRoleTableProxyMapper.selectSysRoleTableProxyByRoleTableId(id);
                if (null != sysRoleTableProxy) {
                    sysRoleTableProxyMapper.deleteSysRoleTableProxyByRoleTableId(id);
                }
                SysRoleTableToolbarConfig sysRoleTableToolbarConfig = sysRoleTableToolbarConfigMapper.selectSysRoleTableToolbarConfigByRoleTableId(id);
                if (null != sysRoleTableToolbarConfig) {
                    sysRoleTableToolbarConfigMapper.deleteSysRoleTableToolbarConfigByRoleTableId(id);
                }
            }

            i++;
        }
        return i;
    }

    /**
     * 批量删除表格权限业务表
     *
     * @param roleTableIds 需要删除的表格权限业务表主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableByRoleTableIds(Long[] roleTableIds) {
        return sysRoleTableMapper.deleteSysRoleTableByRoleTableIds(roleTableIds);
    }

    /**
     * 删除表格权限业务表信息
     *
     * @param roleTableId 表格权限业务表主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableByRoleTableId(Long roleTableId) {
        return sysRoleTableMapper.deleteSysRoleTableByRoleTableId(roleTableId);
    }

    /**
     * 表格权限业务表查询条件
     *
     * @param sysRoleTable
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysRoleTable> buildQueryWrapper(SysRoleTable sysRoleTable) {
        LambdaQueryWrapper<SysRoleTable> lambdaQueryWrapper = new LambdaQueryWrapper<SysRoleTable>()
                .eq(!StringUtils.isNull(sysRoleTable.getRoleTableId()), SysRoleTable::getRoleTableId, sysRoleTable.getRoleTableId())
                .eq(!StringUtils.isNull(sysRoleTable.getTableName()), SysRoleTable::getTableName, sysRoleTable.getTableName())
                .eq(!StringUtils.isNull(sysRoleTable.getTableComment()), SysRoleTable::getTableComment, sysRoleTable.getTableComment())
                .eq(!StringUtils.isNull(sysRoleTable.getMenuId()), SysRoleTable::getMenuId, sysRoleTable.getMenuId())
                .eq(!StringUtils.isNull(sysRoleTable.getTableId()), SysRoleTable::getTableId, sysRoleTable.getTableId())
                .eq(!StringUtils.isNull(sysRoleTable.getRoleId()), SysRoleTable::getRoleId, sysRoleTable.getRoleId())
                .eq(!StringUtils.isNull(sysRoleTable.getId()), SysRoleTable::getId, sysRoleTable.getId())
                .eq(!StringUtils.isNull(sysRoleTable.getHeight()), SysRoleTable::getHeight, sysRoleTable.getHeight())
                .eq(!StringUtils.isNull(sysRoleTable.getUniqueId()), SysRoleTable::getUniqueId, sysRoleTable.getUniqueId())
                .eq(!StringUtils.isNull(sysRoleTable.getRowId()), SysRoleTable::getRowId, sysRoleTable.getRowId())
                .eq(!StringUtils.isNull(sysRoleTable.getMaxHeight()), SysRoleTable::getMaxHeight, sysRoleTable.getMaxHeight())
                .eq(!StringUtils.isNull(sysRoleTable.getAutoResize()), SysRoleTable::getAutoResize, sysRoleTable.getAutoResize())
                .eq(!StringUtils.isNull(sysRoleTable.getSyncResize()), SysRoleTable::getSyncResize, sysRoleTable.getSyncResize())
                .eq(!StringUtils.isNull(sysRoleTable.getStripe()), SysRoleTable::getStripe, sysRoleTable.getStripe())
                .eq(!StringUtils.isNull(sysRoleTable.getBorder()), SysRoleTable::getBorder, sysRoleTable.getBorder())
                .eq(!StringUtils.isNull(sysRoleTable.getRound()), SysRoleTable::getRound, sysRoleTable.getRound())
                .eq(!StringUtils.isNull(sysRoleTable.getSize()), SysRoleTable::getSize, sysRoleTable.getSize())
                .eq(!StringUtils.isNull(sysRoleTable.getAlign()), SysRoleTable::getAlign, sysRoleTable.getAlign())
                .eq(!StringUtils.isNull(sysRoleTable.getHeaderAlign()), SysRoleTable::getHeaderAlign, sysRoleTable.getHeaderAlign())
                .eq(!StringUtils.isNull(sysRoleTable.getFooterAlign()), SysRoleTable::getFooterAlign, sysRoleTable.getFooterAlign())
                .eq(!StringUtils.isNull(sysRoleTable.getShowHeader()), SysRoleTable::getShowHeader, sysRoleTable.getShowHeader())
                .like(!StringUtils.isNull(sysRoleTable.getRowClassName()), SysRoleTable::getRowClassName, sysRoleTable.getRowClassName())
                .like(!StringUtils.isNull(sysRoleTable.getCellClassName()), SysRoleTable::getCellClassName, sysRoleTable.getCellClassName())
                .like(!StringUtils.isNull(sysRoleTable.getHeaderRowClassName()), SysRoleTable::getHeaderRowClassName, sysRoleTable.getHeaderRowClassName())
                .like(!StringUtils.isNull(sysRoleTable.getHeaderCellClassName()), SysRoleTable::getHeaderCellClassName, sysRoleTable.getHeaderCellClassName())
                .like(!StringUtils.isNull(sysRoleTable.getFooterRowClassName()), SysRoleTable::getFooterRowClassName, sysRoleTable.getFooterRowClassName())
                .like(!StringUtils.isNull(sysRoleTable.getFooterCellClassName()), SysRoleTable::getFooterCellClassName, sysRoleTable.getFooterCellClassName())
                .eq(!StringUtils.isNull(sysRoleTable.getShowFooter()), SysRoleTable::getShowFooter, sysRoleTable.getShowFooter())
                .eq(!StringUtils.isNull(sysRoleTable.getFooterMethod()), SysRoleTable::getFooterMethod, sysRoleTable.getFooterMethod())
                .eq(!StringUtils.isNull(sysRoleTable.getShowOverflow()), SysRoleTable::getShowOverflow, sysRoleTable.getShowOverflow())
                .eq(!StringUtils.isNull(sysRoleTable.getShowHeaderOverflow()), SysRoleTable::getShowHeaderOverflow, sysRoleTable.getShowHeaderOverflow())
                .eq(!StringUtils.isNull(sysRoleTable.getShowFooterOverflow()), SysRoleTable::getShowFooterOverflow, sysRoleTable.getShowFooterOverflow())
                .eq(!StringUtils.isNull(sysRoleTable.getKeepSource()), SysRoleTable::getKeepSource, sysRoleTable.getKeepSource())
                .eq(!StringUtils.isNull(sysRoleTable.getEmptyText()), SysRoleTable::getEmptyText, sysRoleTable.getEmptyText())
                .eq(!StringUtils.isNull(sysRoleTable.getExtParams()), SysRoleTable::getExtParams, sysRoleTable.getExtParams())
                .eq(!StringUtils.isNull(sysRoleTable.getExtraConfig()), SysRoleTable::getExtraConfig, sysRoleTable.getExtraConfig())
                .eq(!StringUtils.isNull(sysRoleTable.getHandConfig()), SysRoleTable::getHandConfig, sysRoleTable.getHandConfig())
                .eq(!StringUtils.isNull(sysRoleTable.getCreateBy()), SysRoleTable::getCreateBy, sysRoleTable.getCreateBy())
                .ge(sysRoleTable.getParams().get("beginCreateTime") != null, SysRoleTable::getCreateTime, sysRoleTable.getParams().get("beginCreateTime"))
                .le(sysRoleTable.getParams().get("endCreateTime") != null, SysRoleTable::getCreateTime, sysRoleTable.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysRoleTable.getUpdateBy()), SysRoleTable::getUpdateBy, sysRoleTable.getUpdateBy())
                .ge(sysRoleTable.getParams().get("beginUpdateTime") != null, SysRoleTable::getUpdateTime, sysRoleTable.getParams().get("beginUpdateTime"))
                .le(sysRoleTable.getParams().get("endUpdateTime") != null, SysRoleTable::getUpdateTime, sysRoleTable.getParams().get("endUpdateTime"))
                .eq(!StringUtils.isNull(sysRoleTable.getRemark()), SysRoleTable::getRemark, sysRoleTable.getRemark()).orderByDesc(SysRoleTable::getRemark);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysRoleTable> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysRoleTable> iPage = sysRoleTableMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper<>());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysRoleTableMapper.selectBatchIds(exceel.getIds());
        } else {
            SysRoleTable sysRoleTable = JSON.parseObject(String.valueOf(exceel.getQuery()), SysRoleTable.class);
            list = sysRoleTableMapper.selectSysRoleTableList(sysRoleTable);
        }
        String fileName = exceel.getFilename().equals(null) ? "表格权限业务表数据" : exceel.getFilename();
        Set<String> column = Arrays.stream(exceel.getFields().stream().map(m -> m.getField()).collect(Collectors.toList()).stream().toArray(String[]::new)).collect(Collectors.toSet());
        ExcelUtil.exportExcel(list, fileName, SysRoleTable.class, response, column);
    }

    /**
     * 同步用户表格数据(所有角色)
     */
    @Override
    public void asyncUserTable(Boolean update) {
        List<SysRole> sysRoles = roleMapper.selectRoleAll();
        for (SysRole sysRole : sysRoles) {
            //查询角色下的用户
            List<SysUserRole> sysUserRoles = userRoleMapper.selectUserRoleByRoleId(sysRole.getRoleId());
            //查询角色表格
            List<SysRoleTable> sysRoleTableList = sysRoleTableMapper.selectList(new LambdaQueryWrapper<SysRoleTable>()
                    .eq(SysRoleTable::getRoleId, sysRole.getRoleId()));
            if (null != sysRoleTableList && sysRoleTableList.size() > 0) {
                for (SysUserRole sysUserRole : sysUserRoles) {
                    for (SysRoleTable sysRoleTable : sysRoleTableList) {
                        //查询角色表格列
                        List<SysRoleTableColumn> sysRoleTableColumnList = sysRoleTableColumnMapper.selectList(new LambdaQueryWrapper<SysRoleTableColumn>()
                                .eq(SysRoleTableColumn::getRoleTableId, sysRoleTable.getRoleTableId()));
                        //查询用户表格列
                        SysUserTableColumn sysUserTableColumn = new SysUserTableColumn();
                        sysUserTableColumn.setRoleTableId(sysRoleTable.getRoleTableId());
                        sysUserTableColumn.setUserId(sysUserRole.getUserId());
                        List<SysUserTableColumn> sysUserTableColumnList = sysUserTableColumnMapper.selectList(new LambdaQueryWrapper<SysUserTableColumn>()
                                .eq(SysUserTableColumn::getRoleTableId, sysRoleTable.getRoleTableId()).eq(SysUserTableColumn::getUserId, sysUserRole.getUserId()));
                        //用户表格列中的角色表格id
                        List<Long> userTableList = new ArrayList<>();
                        if (null != sysUserTableColumnList && sysUserTableColumnList.size() > 0) {
                            for (SysUserTableColumn userTableColumn : sysUserTableColumnList) {
                                userTableList.add(userTableColumn.getColumnId());
                            }
                        }
                        //更新用户表格列
                        if (null != sysRoleTableColumnList && sysRoleTableColumnList.size() > 0) {
                            for (SysRoleTableColumn sysRoleTableColumn : sysRoleTableColumnList) {
                                //用户表中不存在的插入
                                if (!(userTableList.indexOf(sysRoleTableColumn.getRoleColumnId()) > -1)) {
                                    SysUserTableColumn userTableColumn = new SysUserTableColumn();
                                    BeanUtils.copyProperties(sysRoleTableColumn, userTableColumn);
                                    userTableColumn.setUserId(sysUserRole.getUserId());
                                    userTableColumn.setColumnId(sysRoleTableColumn.getRoleColumnId());
                                    sysUserTableColumnMapper.insert(userTableColumn);
                                } else if (update) {
                                    SysUserTableColumn userTableColumn = new SysUserTableColumn();
                                    BeanUtils.copyProperties(sysRoleTableColumn, userTableColumn);
                                    userTableColumn.setColumnId(sysRoleTableColumn.getRoleColumnId());
                                    userTableColumn.setUserId(sysUserRole.getUserId());
                                    userTableColumn.setWidth(null);
                                    userTableColumn.setSortNo(null);
                                    sysUserTableColumnMapper.update(userTableColumn, new LambdaQueryWrapper<SysUserTableColumn>()
                                            .eq(SysUserTableColumn::getRoleTableId, userTableColumn.getRoleTableId())
                                            .eq(SysUserTableColumn::getColumnId, userTableColumn.getColumnId())
                                            .eq(SysUserTableColumn::getUserId, sysUserRole.getUserId()));
                                }
                            }
                        }

                        //查询表单
                        SysRoleTableForm sysRoleTableForm = sysRoleTableFormMapper.selectOne(new LambdaQueryWrapper<SysRoleTableForm>().eq(SysRoleTableForm::getRoleTableId, sysRoleTable.getRoleTableId()));
                        if (null != sysRoleTableForm) {
                            List<SysRoleTableFormItem> sysRoleTableFormItemList = sysRoleTableFormItemMapper.selectList(new LambdaQueryWrapper<SysRoleTableFormItem>().eq(SysRoleTableFormItem::getRoleFormId, sysRoleTableForm.getRoleFormId()));
                            //用户表单项
                            SysUserTableFormItem sysUserTableFormItem = new SysUserTableFormItem();
                            sysUserTableFormItem.setUserId(sysUserRole.getUserId());
                            sysUserTableFormItem.setRoleFormId(sysRoleTableForm.getRoleFormId());
                            List<SysUserTableFormItem> sysUserTableFormItemList = sysUserTableFormItemMapper.selectList(new LambdaQueryWrapper<SysUserTableFormItem>()
                                    .eq(SysUserTableFormItem::getUserId, sysUserRole.getUserId())
                                    .eq(SysUserTableFormItem::getRoleFormId, sysRoleTableForm.getRoleFormId()));

                            //用户表单项
                            List<Long> userTableItem = new ArrayList<>();
                            if (null != sysUserTableFormItemList && sysUserTableFormItemList.size() > 0) {
                                for (SysUserTableFormItem userTableFormItem : sysUserTableFormItemList) {
                                    userTableItem.add(userTableFormItem.getItemId());
                                }
                            }
                            //更新用户表单项
                            if (null != sysRoleTableFormItemList && sysRoleTableFormItemList.size() > 0) {
                                for (SysRoleTableFormItem sysRoleTableFormItem : sysRoleTableFormItemList) {
                                    //用户表中不存在的插入
                                    if (!(userTableItem.indexOf(sysRoleTableFormItem.getRoleItemId()) > -1)) {
                                        SysUserTableFormItem userTableFormItem = new SysUserTableFormItem();
                                        BeanUtils.copyProperties(sysRoleTableFormItem, userTableFormItem);
                                        userTableFormItem.setItemId(sysRoleTableFormItem.getRoleItemId());
                                        userTableFormItem.setUserId(sysUserRole.getUserId());
                                        sysUserTableFormItemMapper.insert(userTableFormItem);
                                    } else if (update) {
                                        SysUserTableFormItem userTableFormItem = new SysUserTableFormItem();
                                        BeanUtils.copyProperties(sysRoleTableFormItem, userTableFormItem);
                                        userTableFormItem.setItemId(sysRoleTableFormItem.getRoleItemId());
                                        userTableFormItem.setUserId(sysUserRole.getUserId());
                                        sysUserTableFormItemMapper.update(userTableFormItem, new LambdaQueryWrapper<SysUserTableFormItem>()
                                                .eq(SysUserTableFormItem::getRoleFormId, userTableFormItem.getRoleFormId())
                                                .eq(SysUserTableFormItem::getItemId, userTableFormItem.getItemId())
                                                .eq(SysUserTableFormItem::getUserId, sysUserRole.getUserId()));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
