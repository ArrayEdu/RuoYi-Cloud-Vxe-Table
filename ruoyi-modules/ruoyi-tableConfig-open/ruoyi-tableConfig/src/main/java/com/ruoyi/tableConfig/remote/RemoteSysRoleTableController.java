package com.ruoyi.tableConfig.remote;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.ruoyi.common.security.annotation.InnerAuth;
import com.ruoyi.tableConfig.api.domain.*;
import com.ruoyi.tableConfig.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "角色表格远程调用接口")
@RequestMapping("/remoteRole")
public class RemoteSysRoleTableController extends BaseController {
    @Autowired
    private ISysRoleTableService sysRoleTableService;
    @Autowired
    private ISysRoleTableColumnService sysRoleTableColumnService;
    @Autowired
    private ISysRoleTableEditConfigService sysRoleTableEditConfigService;
    @Autowired
    private ISysRoleTableEditRulesService sysRoleTableEditRulesService;
    @Autowired
    private ISysRoleTableFormService sysRoleTableFormService;
    @Autowired
    private ISysRoleTableFormItemService sysRoleTableFormItemService;
    @Autowired
    private ISysRoleTablePagerService sysRoleTablePagerService;
    @Autowired
    private ISysRoleTableProxyService sysRoleTableProxyService;
    @Autowired
    private ISysRoleTableToolbarConfigService sysRoleTableToolbarConfigService;

    /**
     * 通过角色表格id获取表格权限业务表详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过角色表格id获取表格权限业务表详细信息")
    @GetMapping(value = "/roleTable/{roleTableId}")
    public R getRoleTableInfo(@PathVariable("roleTableId") Long roleTableId) {
        return R.ok(sysRoleTableService.selectSysRoleTableByRoleTableId(roleTableId));
    }

    /**
     * 通过角色id获取表格权限业务表详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过角色id获取表格权限业务表详细信息")
    @GetMapping(value = "/roleTable/getRoleTableByRoleId/{roleId}")
    public R getRoleTableInfoByRoleId(@PathVariable("roleId") Long roleId) {
        return R.ok(sysRoleTableService.selectSysRoleTableByRoleId(roleId));
    }

    /**
     * 通过表格id获取表格权限业务表详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过表格id获取表格权限业务表详细信息")
    @GetMapping(value = "/roleTable/getRoleTableByTableId/{tableId}")
    public R getRoleTableInfoByTableId(@PathVariable("tableId") Long tableId) {
        return R.ok(sysRoleTableService.selectSysRoleTableByTableId(tableId));
    }

    /**
     * 通过菜单id获取表格权限业务表详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过菜单id获取表格权限业务表详细信息")
    @GetMapping(value = "/roleTable/getRoleTableByMenuId/{menuId}")
    public R getRoleTableInfoByMenuId(@PathVariable("menuId") Long menuId) {
        return R.ok(sysRoleTableService.selectSysRoleTableByMenuId(menuId));
    }

    /**
     * 通过菜单id获取表格权限业务表详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过菜单id获取表格权限业务表详细信息")
    @PostMapping(value = "/roleTable/getRoleTable")
    public R getRoleTableInfo(@RequestBody SysRoleTable sysRoleTable) {
        return R.ok(sysRoleTableService.selectSysRoleTable(sysRoleTable));
    }

    /**
     * 新增角色表格权限业务表
     */
    @InnerAuth
    @ApiOperation(value = "新增角色表格权限业务表")
    @PostMapping(value = "/roleTable")
    public R roleTableAdd(@RequestBody SysRoleTable sysRoleTable) {
        return R.ok(sysRoleTableService.insertSysRoleTable(sysRoleTable));
    }

    /**
     * 修改角色表格权限业务表
     */
    @InnerAuth
    @ApiOperation(value = "修改角色表格权限业务表")
    @PutMapping(value = "/roleTable")
    public R roleTableEdit(@RequestBody SysRoleTable sysRoleTable) {
        return R.ok(sysRoleTableService.updateSysRoleTable(sysRoleTable));
    }

    /**
     * 删除表格权限业务表
     */
    @InnerAuth
    @ApiOperation(value = "删除表格权限业务表")
    @DeleteMapping(value = "/roleTable/{roleTableIds}")
    public R roleTableRemoves(@PathVariable(value = "roleTableIds", required = true) Long[] roleTableIds) {
        return R.ok(sysRoleTableService.removeSysRoleTableByRoleTableIds(roleTableIds));
    }

    /**
     * 获取表格权限列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取表格权限列配置详细信息")
    @GetMapping(value = "/roleTableColumn/{roleColumnId}")
    public R getColumnInfoByRoleColumnId(@PathVariable("roleColumnId") Long roleColumnId) {
        return R.ok(sysRoleTableColumnService.selectSysRoleTableColumnByRoleColumnId(roleColumnId));
    }

    /**
     * 获取表格权限列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取表格权限列配置详细信息")
    @GetMapping(value = "/roleTableColumn/{roleTableId}/{columnId}")
    public R getColumnInfoByColumnId(@PathVariable("columnId") Long columnId, @PathVariable("roleTableId") Long roleTableId) {
        return R.ok(sysRoleTableColumnService.selectSysRoleTableColumnByColumnId(columnId, roleTableId));
    }

    /**
     * 通过角色表格id获取表格权限列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过角色表格id获取表格权限列配置详细信息")
    @GetMapping(value = "/roleTableColumn/getColumnByTableId/{roleTableId}")
    public R getColumnInfoByRoleTableId(@PathVariable("roleTableId") Long roleTableId) {
        return R.ok(sysRoleTableColumnService.selectSysRoleTableColumnByRoleTableId(roleTableId));
    }

    /**
     * 新增表格权限列配置
     */
    @InnerAuth
    @ApiOperation(value = "新增表格权限列配置")
    @PostMapping(value = "/roleTableColumn")
    public R columnAdd(@RequestBody SysRoleTableColumn sysRoleTableColumn) {
        return R.ok(sysRoleTableColumnService.insertSysRoleTableColumn(sysRoleTableColumn));
    }

    /**
     * 修改表格权限列配置
     */
    @InnerAuth
    @ApiOperation(value = "修改表格权限列配置")
    @PutMapping(value = "/roleTableColumn")
    public R columnEdit(@RequestBody SysRoleTableColumn sysRoleTableColumn) {
        return R.ok(sysRoleTableColumnService.updateSysRoleTableColumn(sysRoleTableColumn));
    }

    /**
     * 获取角色可编辑配置项详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取角色可编辑配置项详细信息")
    @GetMapping(value = "/roleEditConfig/{roleEditConfigId}")
    public R getEditConfigInfo(@PathVariable("roleEditConfigId") Long roleEditConfigId) {
        return R.ok(sysRoleTableEditConfigService.selectSysRoleTableEditConfigByRoleEditConfigId(roleEditConfigId));
    }

    /**
     * 获取角色可编辑配置项详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取角色可编辑配置项详细信息")
    @GetMapping(value = "/roleEditConfig/getEditConfigByTableId/{roleTableId}")
    public R getEditConfigInfoByTableId(@PathVariable("roleTableId") Long roleTableId) {
        return R.ok(sysRoleTableEditConfigService.selectSysRoleTableEditConfigByRoleTableId(roleTableId));
    }

    /**
     * 新增角色可编辑配置项
     */
    @InnerAuth
    @ApiOperation(value = "新增角色可编辑配置项")
    @PostMapping(value = "/roleEditConfig")
    public R editConfigAdd(SysRoleTableEditConfig sysRoleTableEditConfig) {
        return R.ok(sysRoleTableEditConfigService.insertSysRoleTableEditConfig(sysRoleTableEditConfig));
    }

    /**
     * 修改角色可编辑配置项
     */
    @InnerAuth
    @ApiOperation(value = "修改角色可编辑配置项")
    @PutMapping(value = "/roleEditConfig")
    public R editConfigEdit(@RequestBody SysRoleTableEditConfig sysRoleTableEditConfig) {
        return R.ok(sysRoleTableEditConfigService.updateSysRoleTableEditConfig(sysRoleTableEditConfig));
    }

    /**
     * 获取角色校验规则配置项详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取角色校验规则配置项详细信息")
    @GetMapping(value = "/roleTableRules/{roleEditRulesId}")
    public R getEditRulesInfo(@PathVariable("roleEditRulesId") Long roleEditRulesId) {
        return R.ok(sysRoleTableEditRulesService.selectSysRoleTableEditRulesByRoleEditRulesId(roleEditRulesId));
    }

    /**
     * 通过角色表格id获取角色校验规则配置项详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过角色表格id获取角色校验规则配置项详细信息")
    @GetMapping(value = "/roleTableRules/getEditRulesByTableId/{roleTableId}")
    public R getEditRulesInfoByRoleTableId(@PathVariable("roleTableId") Long roleTableId) {
        return R.ok(sysRoleTableEditRulesService.selectSysRoleTableEditRulesByRoleTableId(roleTableId));
    }

    /**
     * 新增角色校验规则配置项
     */
    @InnerAuth
    @ApiOperation(value = "新增角色校验规则配置项")
    @PostMapping(value = "/roleTableRules")
    public R roleRuleAdd(@RequestBody SysRoleTableEditRules sysRoleTableEditRules) {
        return R.ok(sysRoleTableEditRulesService.insertSysRoleTableEditRules(sysRoleTableEditRules));
    }

    /**
     * 修改角色校验规则配置项
     */
    @InnerAuth
    @ApiOperation(value = "修改角色校验规则配置项")
    @PutMapping(value = "/roleTableRules")
    public R roleRuleEdit(@RequestBody SysRoleTableEditRules sysRoleTableEditRules) {
        return R.ok(sysRoleTableEditRulesService.updateSysRoleTableEditRules(sysRoleTableEditRules));
    }

    /**
     * 获取角色功能表查询表详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取角色功能表查询表详细信息")
    @GetMapping(value = "/roleTableForm/{roleFormId}")
    public R getRoleFormInfo(@PathVariable("roleFormId") Long roleFormId) {
        return R.ok(sysRoleTableFormService.selectSysRoleTableFormByRoleFormId(roleFormId));
    }

    /**
     * 通过角色表格id获取角色功能表查询表详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过角色表格id获取角色功能表查询表详细信息")
    @GetMapping(value = "/roleTableForm/getRoleFormByTableId/{roleTableId}")
    public R getRoleFormInfoByRoleTableId(@PathVariable("roleTableId") Long roleTableId) {
        return R.ok(sysRoleTableFormService.selectSysRoleTableFormByRoleTableId(roleTableId));
    }

    /**
     * 新增角色功能表查询表
     */
    @InnerAuth
    @ApiOperation(value = "新增角色功能表查询表")
    @PostMapping(value = "/roleTableConfig/roleTableForm")
    public R roleTableFormAdd(@RequestBody SysRoleTableForm sysRoleTableForm) {
        return R.ok(sysRoleTableFormService.insertSysRoleTableForm(sysRoleTableForm));
    }

    /**
     * 修改角色功能表查询表
     */
    @InnerAuth
    @ApiOperation(value = "修改角色功能表查询表")
    @PutMapping(value = "/roleTableConfig/roleTableForm")
    public R roleTableFormEdit(@RequestBody SysRoleTableForm sysRoleTableForm) {
        return R.ok(sysRoleTableFormService.updateSysRoleTableForm(sysRoleTableForm));
    }


    /**
     * 获取角色表单配置项列表详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取角色表单配置项列表详细信息")
    @GetMapping(value = "/roleTableFormItem/{roleItemId}")
    public R getRoleItemInfo(@PathVariable("roleItemId") Long roleItemId) {
        return R.ok(sysRoleTableFormItemService.selectSysRoleTableFormItemByRoleItemId(roleItemId));
    }

    /**
     * 通过角色表单id获取角色表单配置项列表详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过角色表单id获取角色表单配置项列表详细信息")
    @GetMapping(value = "/roleTableFormItem/getRoleItemByFormId/{roleFormId}")
    public R getRoleItemInfoByFormId(@PathVariable("roleFormId") Long roleFormId) {
        return R.ok(sysRoleTableFormItemService.selectSysRoleTableFormItemByRoleFormId(roleFormId));
    }

    /**
     * 新增角色表单配置项列表
     */
    @InnerAuth
    @ApiOperation(value = "新增角色表单配置项列表")
    @PostMapping(value = "/roleTableFormItem")
    public R roleFormItemAdd(@RequestBody SysRoleTableFormItem sysRoleTableFormItem) {
        return R.ok(sysRoleTableFormItemService.insertSysRoleTableFormItem(sysRoleTableFormItem));
    }

    /**
     * 修改角色表单配置项列表
     */
    @InnerAuth
    @ApiOperation(value = "修改角色表单配置项列表")
    @PutMapping(value = "/roleTableFormItem")
    public R roleFormItemEdit(@RequestBody SysRoleTableFormItem sysRoleTableFormItem) {
        return R.ok(sysRoleTableFormItemService.updateSysRoleTableFormItem(sysRoleTableFormItem));
    }

    /**
     * 获取角色功能表分页配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取角色功能表分页配置详细信息")
    @GetMapping(value = "/roleTablePager/{rolePagerId}")
    public R getRolePagerInfo(@PathVariable("rolePagerId") Long rolePagerId) {
        return R.ok(sysRoleTablePagerService.selectSysRoleTablePagerByRolePagerId(rolePagerId));
    }

    /**
     * 通过角色表格id获取角色功能表分页配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过角色表格id获取角色功能表分页配置详细信息")
    @GetMapping(value = "/roleTablePager/getRolePagerByTableId/{roleTableId}")
    public R getRolePagerInfoByTableId(@PathVariable("roleTableId") Long roleTableId) {
        return R.ok(sysRoleTablePagerService.selectSysRoleTablePagerByRoleTableId(roleTableId));
    }

    /**
     * 新增角色功能表分页配置
     */
    @InnerAuth
    @ApiOperation(value = "新增角色功能表分页配置")
    @PostMapping(value = "/roleTablePager")
    public R rolePageAdd(@RequestBody SysRoleTablePager sysRoleTablePager) {
        return R.ok(sysRoleTablePagerService.insertSysRoleTablePager(sysRoleTablePager));
    }

    /**
     * 修改角色功能表分页配置
     */
    @InnerAuth
    @ApiOperation(value = "修改角色功能表分页配置")
    @PutMapping(value = "/roleTablePager")
    public R rolePageEdit(@RequestBody SysRoleTablePager sysRoleTablePager) {
        return R.ok(sysRoleTablePagerService.updateSysRoleTablePager(sysRoleTablePager));
    }

    /**
     * 获取角色功能表数据代理详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取角色功能表数据代理详细信息")
    @GetMapping(value = "/roleTableProxy/{roleProxyId}")
    public R getRoleProxyInfo(@PathVariable("roleProxyId") Long roleProxyId) {
        return R.ok(sysRoleTableProxyService.selectSysRoleTableProxyByRoleProxyId(roleProxyId));
    }

    /**
     * 通过角色表格id获取角色功能表数据代理详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过角色表格id获取角色功能表数据代理详细信息")
    @GetMapping(value = "/roleTableProxy/getRoleProxyByTableId/{roleTableId}")
    public R getRoleProxyInfoByTableId(@PathVariable("roleTableId") Long roleTableId) {
        return R.ok(sysRoleTableProxyService.selectSysRoleTableProxyByRoleTableId(roleTableId));
    }


    /**
     * 新增角色功能表数据代理
     */
    @InnerAuth
    @ApiOperation(value = "新增角色功能表数据代理")
    @PostMapping(value = "/roleTableProxy")
    public R roleProxyAdd(@RequestBody SysRoleTableProxy sysRoleTableProxy) {
        return R.ok(sysRoleTableProxyService.insertSysRoleTableProxy(sysRoleTableProxy));
    }

    /**
     * 修改角色功能表数据代理
     */
    @InnerAuth
    @ApiOperation(value = "修改角色功能表数据代理")
    @PutMapping(value = "/roleTableProxy")
    public R roleProxyEdit(@RequestBody SysRoleTableProxy sysRoleTableProxy) {
        return R.ok(sysRoleTableProxyService.updateSysRoleTableProxy(sysRoleTableProxy));
    }

    /**
     * 获取角色工具栏配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取角色工具栏配置详细信息")
    @GetMapping(value = "/roleTableToolbarConfig/{roleToolbarConfigId}")
    public R getRoleToolBarConfigInfo(@PathVariable("roleToolbarConfigId") Long roleToolbarConfigId) {
        return R.ok(sysRoleTableToolbarConfigService.selectSysRoleTableToolbarConfigByRoleToolbarConfigId(roleToolbarConfigId));
    }

    /**
     * 通过角色表格id获取角色工具栏配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过角色表格id获取角色工具栏配置详细信息")
    @GetMapping(value = "/roleTableToolbarConfig/{roleTableId}")
    public R getRoleToolBarConfigInfoByTableId(@PathVariable("roleTableId") Long roleTableId) {
        return R.ok(sysRoleTableToolbarConfigService.selectSysRoleTableToolbarConfigByRoleTableId(roleTableId));
    }


    /**
     * 新增角色工具栏配置
     */
    @InnerAuth
    @ApiOperation(value = "新增角色工具栏配置")
    @PostMapping(value = "/roleTableToolbarConfig")
    public R roleToolBarAdd(@RequestBody SysRoleTableToolbarConfig sysRoleTableToolbarConfig) {
        return R.ok(sysRoleTableToolbarConfigService.insertSysRoleTableToolbarConfig(sysRoleTableToolbarConfig));
    }

    /**
     * 修改角色工具栏配置
     */
    @InnerAuth
    @ApiOperation(value = "修改角色工具栏配置")
    @PutMapping(value = "/roleTableToolbarConfig")
    public R roleToolBarEdit(@RequestBody SysRoleTableToolbarConfig sysRoleTableToolbarConfig) {
        return R.ok(sysRoleTableToolbarConfigService.updateSysRoleTableToolbarConfig(sysRoleTableToolbarConfig));
    }
}
