package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysTableEditRules;

/**
 * 校验规则配置项Mapper接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface SysTableEditRulesMapper extends BaseMapperPlus<SysTableEditRules> {
    /**
     * 查询校验规则配置项
     *
     * @param tableId 表格id
     * @return 校验规则配置项
     */
    public List<SysTableEditRules> selectSysTableEditRulesByTableId(Long tableId);

    /**
     * 查询校验规则配置项
     *
     * @param editRulesId 校验规则配置项主键
     * @return 校验规则配置项
     */
    public SysTableEditRules selectSysTableEditRulesByEditRulesId(Long editRulesId);

    /**
     * 查询校验规则配置项列表
     *
     * @param sysTableEditRules 校验规则配置项
     * @return 校验规则配置项集合
     */
    public List<SysTableEditRules> selectSysTableEditRulesList(SysTableEditRules sysTableEditRules);


    /**
     * 新增校验规则配置项
     *
     * @param sysTableEditRules 校验规则配置项
     * @return 结果
     */
    public int insertSysTableEditRules(SysTableEditRules sysTableEditRules);

    /**
     * 修改校验规则配置项
     *
     * @param sysTableEditRules 校验规则配置项
     * @return 结果
     */
    public int updateSysTableEditRules(SysTableEditRules sysTableEditRules);

    /**
     * 删除校验规则配置项
     *
     * @param editRulesId 校验规则配置项主键
     * @return 结果
     */
    public int deleteSysTableEditRulesByEditRulesId(Long editRulesId);

    /**
     * 批量删除校验规则配置项
     *
     * @param editRulesIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysTableEditRulesByEditRulesIds(Long[] editRulesIds);

    /**
     * 批量删除校验规则配置项
     *
     * @param tableIds 需要删除的表格id
     * @return 结果
     */
    public int deleteSysTableEditRulesByTableIds(Long[] tableIds);

    /**
     * 删除校验规则配置项
     *
     * @param tableId 需要删除的表格id
     * @return 结果
     */
    public int deleteSysTableEditRulesByTableId(Long tableId);

}
