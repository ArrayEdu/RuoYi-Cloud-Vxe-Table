package com.ruoyi.tableConfig.service.impl;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson2.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.exception.CaptchaException;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.tableConfig.api.domain.*;
import com.ruoyi.tableConfig.domain.tableVo.UserColumnVo;
import com.ruoyi.tableConfig.mapper.*;
import com.ruoyi.tableConfig.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 功能表格Service业务层处理
 *
 * @author zly
 * @date 2023-04-25
 */
@Service
public class SysTableServiceImpl extends ServiceImpl<SysTableMapper, SysTable> implements ISysTableService {

    //菜单表格配置
    @Autowired
    private SysTableMapper sysTableMapper;
    @Autowired
    private SysTableColumnMapper sysTableColumnMapper;
    @Autowired
    private SysTableEditConfigMapper sysTableEditConfigMapper;
    @Autowired
    private SysTableEditRulesMapper sysTableEditRulesMapper;
    @Autowired
    private SysTableFormMapper sysTableFormMapper;
    @Autowired
    private SysTableFormItemMapper sysTableFormItemMapper;
    @Autowired
    private SysTablePagerMapper sysTablePagerMapper;
    @Autowired
    private SysTableProxyMapper sysTableProxyMapper;
    @Autowired
    private SysTableToolbarConfigMapper sysTableToolbarConfigMapper;

    @Autowired
    private ISysTableColumnService sysTableColumnService;
    @Autowired
    private ISysTableToolbarConfigService sysTableToolbarConfigService;
    //角色表格配置
    @Autowired
    private SysRoleTableMapper sysRoleTableMapper;
    @Autowired
    private SysRoleTableColumnMapper sysRoleTableColumnMapper;
    @Autowired
    private SysRoleTableEditConfigMapper sysRoleTableEditConfigMapper;
    @Autowired
    private SysRoleTableEditRulesMapper sysRoleTableEditRulesMapper;
    @Autowired
    private SysRoleTableFormMapper sysRoleTableFormMapper;
    @Autowired
    private SysRoleTableFormItemMapper sysRoleTableFormItemMapper;
    @Autowired
    private SysRoleTablePagerMapper sysRoleTablePagerMapper;
    @Autowired
    private SysRoleTableProxyMapper sysRoleTableProxyMapper;
    @Autowired
    private SysRoleTableToolbarConfigMapper sysRoleTableToolbarConfigMapper;

    @Autowired
    private ISysRoleTableService sysRoleTableService;
    @Autowired
    private ISysRoleTableColumnService sysRoleTableColumnService;
    @Autowired
    private ISysRoleTableEditConfigService sysRoleTableEditConfigService;
    @Autowired
    private ISysRoleTableEditRulesService sysRoleTableEditRulesService;
    @Autowired
    private ISysRoleTableFormService sysRoleTableFormService;
    @Autowired
    private ISysRoleTableFormItemService sysRoleTableFormItemService;
    @Autowired
    private ISysRoleTablePagerService sysRoleTablePagerService;
    @Autowired
    private ISysRoleTableProxyService sysRoleTableProxyService;
    @Autowired
    private ISysRoleTableToolbarConfigService sysRoleTableToolbarConfigService;

    //用户表格配置
    @Autowired
    private SysUserTableColumnMapper sysUserTableColumnMapper;
    @Autowired
    private SysUserTableFormItemMapper sysUserTableFormItemMapper;
    @Autowired
    private ISysUserTableColumnService sysUserTableColumnService;
    @Autowired
    private ISysUserTableFormItemService sysUserTableFormItemService;


    /**
     * 查询功能表格
     *
     * @param menuId 菜单id
     * @return 功能表格
     */
    @Override
    public List<SysTable> selectSysTableByMenuId(Long menuId) {
        return sysTableMapper.selectSysTableByMenuId(menuId);
    }

    /**
     * 查询功能表格
     *
     * @param tableId 功能表格主键
     * @return 功能表格
     */
    @Override
    public SysTable selectSysTableByTableId(Long tableId) {
        return sysTableMapper.selectSysTableByTableId(tableId);
    }

    /**
     * 查询功能表格列表
     *
     * @param sysTable 功能表格
     * @return 功能表格
     */
    @Override
    public List<SysTable> selectSysTableList(SysTable sysTable) {
        return sysTableMapper.selectSysTableList(sysTable);
    }

    /**
     * 查询功能表格分页列表
     *
     * @param sysTable  功能表格
     * @param pageQuery 查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysTable> selectSysTablePage(SysTable sysTable, PageQuery pageQuery) {
        LambdaQueryWrapper<SysTable> lqw = buildQueryWrapper(sysTable);
        Page<SysTable> page = sysTableMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增功能表格
     *
     * @param sysTable 功能表格
     * @return 结果
     */
    @Override
    public int insertSysTable(SysTable sysTable) {
        sysTable.setCreateTime(DateUtils.getNowDate());
        sysTable.setCreateBy(SecurityUtils.getUserId());
        return sysTableMapper.insertSysTable(sysTable);
    }

    /**
     * 修改功能表格
     *
     * @param sysTable 功能表格
     * @return 结果
     */
    @Override
    public int updateSysTable(SysTable sysTable) {
        sysTable.setUpdateTime(DateUtils.getNowDate());
        sysTable.setUpdateBy(SecurityUtils.getUserId());
        return sysTableMapper.updateSysTable(sysTable);
    }

    /**
     * 批量删除功能表格
     *
     * @param tableIds 需要删除的功能表格主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteSysTableByTableIds(Long[] tableIds) {
        sysTableColumnMapper.deleteSysTableColumnByTableIds(tableIds);
        sysTableEditConfigMapper.deleteSysTableEditConfigByTableIds(tableIds);
        sysTableEditRulesMapper.deleteSysTableEditRulesByTableIds(tableIds);
        sysTablePagerMapper.deleteSysTablePagerByTableIds(tableIds);
        sysTableProxyMapper.deleteSysTableProxyByProxytableIds(tableIds);
        for (Long table : tableIds) {
            SysTableForm sysTableForm = sysTableFormMapper.selectSysTableFormByTableId(table);
            sysTableFormItemMapper.deleteSysTableFormItemByFormId(sysTableForm.getFormId());
        }
        sysTableFormMapper.deleteSysTableFormByTableIds(tableIds);
        sysTableToolbarConfigMapper.deleteSysTableToolbarConfigByTableIds(tableIds);
        return sysTableMapper.deleteSysTableByTableIds(tableIds);
    }

    /**
     * 删除功能表格信息
     *
     * @param tableId 功能表格主键
     * @return 结果
     */
    @Override
    public int deleteSysTableByTableId(Long tableId) {
        return sysTableMapper.deleteSysTableByTableId(tableId);
    }

    /**
     * 查询据库列表
     *
     * @param sysTable 业务信息
     * @return 数据库表集合
     */
    @Override
    public TableDataInfo<SysTable> selectDbTablePage(SysTable sysTable, PageQuery pageQuery) {
        IPage<SysTable> page = sysTableMapper.selectDbTablePage(pageQuery.build(), sysTable);
        return TableDataInfo.build(page);
    }

    /**
     * 查询据库列表
     *
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    @Override
    public List<SysTable> selectDbTableListByNames(String[] tableNames) {
        return sysTableMapper.selectDbTableListByNames(tableNames);
    }

    /**
     * 导入表结构
     *
     * @param tableList 导入表列表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void importGenTable(List<SysTable> tableList, Long menuId) {
        Long userId = SecurityUtils.getUserId();
        try {
            for (SysTable table : tableList) {
                String tableName = table.getTableName();
                table.setMenuId(menuId);
                table.setCreateBy(userId);
                table.setCreateTime(DateUtils.getNowDate());
                table.setHeight("auto");
                table.setBorder(true);
                table.setShowHeader(true);
                table.setAutoResize(true);
                table.setSyncResize(true);
                int row = sysTableMapper.insertSysTable(table);
                if (row > 0) {
                    // 保存列信息
                    List<SysTableColumn> genTableColumns = sysTableColumnMapper.selectDbTableColumnsByName(tableName);
                    Integer i = 0;
                    for (SysTableColumn column : genTableColumns) {
                        i++;
                        initColumnField(column, table);
                        column.setSortNo(i);
                        column.setCreateTime(DateUtils.getNowDate());
                        sysTableColumnMapper.insert(column);
                    }
                    //查询配置
                    SysTableForm sysTableForm = new SysTableForm();
                    sysTableForm.setTableId(table.getTableId());
                    sysTableFormMapper.insertSysTableForm(sysTableForm);
                    Integer j = 0;
                    for (SysTableColumn column : genTableColumns) {
                        j++;
                        SysTableFormItem sysTableFormItem = new SysTableFormItem();
                        initFormField(column, sysTableFormItem, sysTableForm);
                        sysTableFormItem.setSortNo(j);
                        sysTableFormItem.setCreateTime(DateUtils.getNowDate());
                        sysTableFormItemMapper.insert(sysTableFormItem);
                    }
                    //查询操作
                    SysTableFormItem sysTableFormItem = new SysTableFormItem();
                    sysTableFormItem.setSortNo(j++);
                    sysTableFormItem.setCreateTime(DateUtils.getNowDate());
                    sysTableFormItem.setFormId(sysTableForm.getFormId());
                    sysTableFormItem.setCreateBy(SecurityUtils.getUserId());
                    sysTableFormItem.setItemRender("{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}");
                    sysTableFormItem.setSearchHandle(true);
                    sysTableFormItemMapper.insert(sysTableFormItem);
                    //分页配置
                    SysTablePager sysTablePager = new SysTablePager();
                    sysTablePager.setTableId(table.getTableId());
                    sysTablePager.setPageSize(10L);
                    sysTablePager.setPageSizes("5,10,15,20,50,100,200,500,1000");
                    sysTablePager.setPerfect(true);
                    sysTablePagerMapper.insertSysTablePager(sysTablePager);
                }
            }
        } catch (Exception e) {
            throw new ServiceException("导入失败：" + e.getMessage());
        }
    }

    /**
     * 同步数据库
     *
     * @param tableId 表id
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void synchDb(Long tableId) {
        SysTable table = sysTableMapper.selectById(tableId);
        List<SysTableColumn> dbTableColumns = sysTableColumnMapper.selectDbTableColumnsByName(table.getTableName());
        SysTableColumn query = new SysTableColumn();
        query.setTableId(tableId);
        List<SysTableColumn> sysRoleTableColumns = sysTableColumnMapper.selectList(sysTableColumnService.buildQueryWrapper(query));
        Map<String, SysTableColumn> tableColumnMap = sysRoleTableColumns.stream().collect(Collectors.toMap(SysTableColumn::getColumnName, Function.identity()));
        if (StringUtils.isEmpty(dbTableColumns)) {
            throw new ServiceException("同步数据失败，原表结构不存在");
        }
        dbTableColumns.forEach(column -> {
            if (!tableColumnMap.containsKey(column.getColumnName())) {
                initColumnField(column, table);
                sysTableColumnMapper.insert(column);
            }
        });
        //查询配置
        SysTableForm sysTableForm = sysTableFormMapper.selectSysTableFormByTableId(tableId);
        Integer j = 0;
        for (SysTableColumn column : dbTableColumns) {
            j++;
            if (!tableColumnMap.containsKey(column.getColumnName())) {
                SysTableFormItem sysTableFormItem = new SysTableFormItem();
                initFormField(column, sysTableFormItem, sysTableForm);
                sysTableFormItem.setSortNo(j);
                sysTableFormItem.setCreateTime(DateUtils.getNowDate());
                sysTableFormItemMapper.insert(sysTableFormItem);
            }
        }
    }

    /**
     * 获取表格配置(修改表配置时使用)
     *
     * @param tableId
     * @return
     */
    @Override
    public JSONObject getMenuTableConfig(Long tableId) {
        SysTable sysTable = sysTableMapper.selectSysTableByTableId(tableId);
        List<SysTableColumn> sysTableColumn = sysTableColumnService.selectSysTableColumnByTableId(tableId);
        SysTableEditConfig sysTableEditConfig = sysTableEditConfigMapper.selectSysTableEditConfigByTableId(tableId);
        SysTableEditRules sysTableEditRules = new SysTableEditRules();
        sysTableEditRules.setTableId(tableId);
        List<SysTableEditRules> sysTableEditRulesList = sysTableEditRulesMapper.selectSysTableEditRulesList(sysTableEditRules);
        SysTableForm sysTableForm = sysTableFormMapper.selectSysTableFormByTableId(tableId);
        List<SysTableFormItem> sysTableFormItemList = sysTableFormItemMapper.selectSysTableFormItemByFormId(sysTableForm.getFormId());
        SysTablePager sysTablePager = sysTablePagerMapper.selectSysTablePagerByTableId(tableId);
        SysTableProxy sysTableProxy = sysTableProxyMapper.selectSysTableProxyByTableId(tableId);
        SysTableToolbarConfig sysTableToolbarConfig = sysTableToolbarConfigMapper.selectSysTableToolbarConfigByTableId(tableId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("table", sysTable);
        jsonObject.put("columns", sysTableColumn);
        jsonObject.put("editConfig", sysTableEditConfig);
        jsonObject.put("editRile", sysTableEditRulesList);
        jsonObject.put("form", sysTableForm);
        jsonObject.put("formItem", sysTableFormItemList);
        jsonObject.put("pager", sysTablePager);
        jsonObject.put("proxy", sysTableProxy);
        jsonObject.put("toolbar", sysTableToolbarConfig);
        return jsonObject;
    }

    /**
     * 新增功能表格
     *
     * @param ajaxData 表格数据
     * @return
     */
    @Override
    @Transactional
    public boolean addMenuTableConfig(JSONObject ajaxData) {
        try {
            SysTable sysTable = JSON.toJavaObject(ajaxData.getJSONObject("table"), SysTable.class);
            List<SysTableColumn> sysTableColumnList = JSONArray.parseArray(JSONArray.toJSONString(ajaxData.get("columns")), SysTableColumn.class);
            SysTableEditConfig sysTableEditConfig = JSON.toJavaObject(ajaxData.getJSONObject("editConfig"), SysTableEditConfig.class);
            List<SysTableEditRules> sysTableEditRulesList = JSONArray.parseArray(JSONArray.toJSONString(ajaxData.get("editRile")), SysTableEditRules.class);
            SysTableForm sysTableForm = JSON.toJavaObject(ajaxData.getJSONObject("form"), SysTableForm.class);
            List<SysTableFormItem> sysTableFormItemList = JSONArray.parseArray(JSONArray.toJSONString(ajaxData.get("formItem")), SysTableFormItem.class);
            SysTablePager sysTablePager = JSON.toJavaObject(ajaxData.getJSONObject("pager"), SysTablePager.class);
            SysTableProxy sysTableProxy = JSON.toJavaObject(ajaxData.getJSONObject("proxy"), SysTableProxy.class);
            SysTableToolbarConfig sysTableToolbarConfig = JSON.toJavaObject(ajaxData.getJSONObject("toolbar"), SysTableToolbarConfig.class);

            sysTableMapper.updateSysTable(sysTable);
            for (SysTableColumn sysTableColumn : sysTableColumnList) {
                sysTableColumn.setTableId(sysTable.getTableId());
                if (null != sysTableColumn.getColumnId()) {
                    sysTableColumnService.updateSysTableColumn(sysTableColumn);
                } else {
                    sysTableColumnService.save(sysTableColumn);
                }
            }
            sysTableEditConfig.setTableId(sysTable.getTableId());
            if (null != sysTableEditConfig.getEditConfigId()) {
                sysTableEditConfigMapper.updateSysTableEditConfig(sysTableEditConfig);
            } else {
                sysTableEditConfigMapper.insertSysTableEditConfig(sysTableEditConfig);
            }
            for (SysTableEditRules sysTableEditRules : sysTableEditRulesList) {
                sysTableEditRules.setTableId(sysTable.getTableId());
                if (null != sysTableEditRules.getEditRulesId()) {
                    sysTableEditRulesMapper.updateSysTableEditRules(sysTableEditRules);
                } else {
                    sysTableEditRulesMapper.insert(sysTableEditRules);
                }
            }

            sysTableForm.setTableId(sysTable.getTableId());
            Long formId = -1L;
            if (null != sysTableForm.getFormId()) {
                formId = sysTableForm.getFormId();
                sysTableFormMapper.updateSysTableForm(sysTableForm);
            } else {
                sysTableFormMapper.insertSysTableForm(sysTableForm);
                formId = (long) sysTableForm.getFormId();
            }
            for (SysTableFormItem sysTableFormItem : sysTableFormItemList) {
                sysTableFormItem.setFormId(formId);
                if (null != sysTableFormItem.getItemId()) {
                    sysTableFormItemMapper.updateSysTableFormItem(sysTableFormItem);
                } else {
                    sysTableFormItemMapper.insert(sysTableFormItem);
                }
            }

            sysTablePager.setTableId(sysTable.getTableId());
            if (null != sysTablePager.getPagerId()) {
                sysTablePagerMapper.updateSysTablePager(sysTablePager);
            } else {
                sysTablePagerMapper.insertSysTablePager(sysTablePager);
            }

            sysTableProxy.setTableId(sysTable.getTableId());
            if (null != sysTableProxy.getProxyId()) {
                sysTableProxyMapper.updateSysTableProxy(sysTableProxy);
            } else {
                sysTableProxyMapper.insertSysTableProxy(sysTableProxy);
            }
            sysTableToolbarConfig.setTableId(sysTable.getTableId());
            if (null != sysTableToolbarConfig.getToolbarConfigId()) {
                sysTableToolbarConfigMapper.updateSysTableToolbarConfig(sysTableToolbarConfig);
            } else {
                sysTableToolbarConfigMapper.insertSysTableToolbarConfig(sysTableToolbarConfig);
            }
            return true;
        } catch (CaptchaException e) {
            return false;
        }
    }

    /**
     * 获取表格配置
     *
     * @param ajaxData
     * @return
     */
    @Override
    public SysRoleTable getTableConfig(JSONObject ajaxData) {
        SysRole sysRole = JSON.toJavaObject(ajaxData.getJSONObject("role"), SysRole.class);
        String tableName = ajaxData.get("tableName").toString();
        SysRoleTable query = new SysRoleTable();
        query.setTableName(tableName);
        query.setRoleId(sysRole.getRoleId());
        SysRoleTable sysRoleTable = sysRoleTableService.getOne(sysRoleTableService.buildQueryWrapper(query));
        //SysTable sysTable = sysTableService.selectSysTableByTableId(sysTableOne.getTableId());

        //List<SysRoleTableColumn> sysRoleTableColumns = sysRoleTableColumnService.selectSysRoleTableColumnByRoleTableId(sysRoleTable.getRoleTableId());
        // 用户表格列
        SysUserTableColumn sysUserTableColumn = new SysUserTableColumn();
        sysUserTableColumn.setUserId(SecurityUtils.getUserId());
        sysUserTableColumn.setRoleTableId(sysRoleTable.getRoleTableId());
        List<SysUserTableColumn> sysUserRoleTableColumns = sysUserTableColumnService.selectSysUserTableColumnList(sysUserTableColumn);

        SysRoleTableEditConfig sysRoleTableEditConfig = sysRoleTableEditConfigService.selectSysRoleTableEditConfigByRoleTableId(sysRoleTable.getRoleTableId());
        //SysRoleTableEditRules sysRoleTableEditRules = new SysRoleTableEditRules();
        //sysRoleTableEditRules.setRoleTableId(sysRoleTable.getRoleTableId());
        List<SysRoleTableEditRules> sysRoleTableEditRulesList = sysRoleTableEditRulesService.selectSysRoleTableEditRulesByRoleTableId(sysRoleTable.getRoleTableId());
        SysRoleTableForm sysRoleTableForm = sysRoleTableFormService.selectSysRoleTableFormByRoleTableId(sysRoleTable.getRoleTableId());

        SysRoleTablePager sysRoleTablePager = sysRoleTablePagerService.selectSysRoleTablePagerByRoleTableId(sysRoleTable.getRoleTableId());
        SysRoleTableProxy sysRoleTableProxy = sysRoleTableProxyService.selectSysRoleTableProxyByRoleTableId(sysRoleTable.getRoleTableId());
        SysRoleTableToolbarConfig sysRoleTableToolbarConfig = sysRoleTableToolbarConfigService.selectSysRoleTableToolbarConfigByRoleTableId(sysRoleTable.getRoleTableId());

        //List<SysTableFormItem> sysTableFormItemList = sysTableFormItemService.selectSysTableFormItemByFormId(sysTableForm.getFormId());
        //可添加的查询项
        SysUserTableFormItem sysUserTableFormItem = new SysUserTableFormItem();
        sysUserTableFormItem.setUserId(SecurityUtils.getUserId());
        sysUserTableFormItem.setRoleFormId(sysRoleTableForm.getRoleFormId());
        sysUserTableFormItem.setSearchVisible(false);
        List<SysUserTableFormItem> sysTableFormItemsFilter = sysUserTableFormItemService.selectSysUserTableFormItemList(sysUserTableFormItem);
        //查询项
        SysUserTableFormItem userTableFormItem = new SysUserTableFormItem();
        userTableFormItem.setUserId(SecurityUtils.getUserId());
        userTableFormItem.setRoleFormId(sysRoleTableForm.getRoleFormId());
        userTableFormItem.setSearchVisible(true);
        List<SysUserTableFormItem> sysUserTableFormItems = sysUserTableFormItemService.selectSysUserTableFormItemList(userTableFormItem);
        //表单项处理
        sysRoleTableForm.setItems(sysUserTableFormItems);
        sysRoleTableForm.setFilterItems(sysTableFormItemsFilter);

        sysRoleTable.setColumns(sysUserRoleTableColumns);
        sysRoleTable.setEditConfig(sysRoleTableEditConfig);
        sysRoleTable.setEditRules(sysRoleTableEditRulesList);
        sysRoleTable.setFormConfig(sysRoleTableForm);
        sysRoleTable.setPagerConfig(sysRoleTablePager);
        sysRoleTable.setProxyConfig(sysRoleTableProxy);
        sysRoleTable.setToolbarConfig(sysRoleTableToolbarConfig);
        return sysRoleTable;
    }

    /**
     * 表单筛选可添加项
     *
     * @return
     */
    @Override
    public SysRoleTable setUserFilterFormItem(JSONObject ajaxData) {
        SysUserTableFormItem sysUserTableFormItem = JSON.toJavaObject(ajaxData, SysUserTableFormItem.class);
        SysUserTableFormItem userFormItem = new SysUserTableFormItem();
        userFormItem.setUserId(SecurityUtils.getUserId());
        userFormItem.setSearchVisible(true);
        List<SysUserTableFormItem> sysUserTableFormItemFilter = sysUserTableFormItemService.selectSysUserTableFormItemList(userFormItem);

        SysUserTableFormItem userFormFilter = new SysUserTableFormItem();
        userFormFilter.setUserItemId(sysUserTableFormItem.getUserItemId());
        Integer sortNo = sysUserTableFormItemFilter.size();
        userFormFilter.setSearchNo(sortNo.longValue());
        userFormFilter.setSearchVisible(true);
        sysUserTableFormItemService.updateSysUserTableFormItem(userFormFilter);
        //
        SysUserTableFormItem userTableFormItem = new SysUserTableFormItem();
        userTableFormItem.setUserId(SecurityUtils.getUserId());
        userTableFormItem.setSearchVisible(true);
        List<SysUserTableFormItem> sysUserTableFormItems = sysUserTableFormItemService.selectSysUserTableFormItemList(userTableFormItem);
        //可添加的查询项
        SysUserTableFormItem tableFormItemFilter = new SysUserTableFormItem();
        tableFormItemFilter.setUserId(SecurityUtils.getUserId());
        tableFormItemFilter.setSearchVisible(false);
        List<SysUserTableFormItem> sysTableFormItemsFilter = sysUserTableFormItemService.selectSysUserTableFormItemList(tableFormItemFilter);

        SysRoleTableForm sysRoleTableForm = new SysRoleTableForm();
        sysRoleTableForm.setItems(sysUserTableFormItems);
        sysRoleTableForm.setFilterItems(sysTableFormItemsFilter);

        SysRoleTable sysRoleTable = new SysRoleTable();
        sysRoleTable.setFormConfig(sysRoleTableForm);
        return sysRoleTable;
    }

    /**
     * 删除用户表单筛选项
     *
     * @param ajaxData
     * @return
     */
    @Override
    public SysRoleTable deleteUserFilterFormItem(JSONObject ajaxData) {
        SysUserTableFormItem sysUserTableFormItem = JSON.toJavaObject(ajaxData, SysUserTableFormItem.class);
        SysUserTableFormItem userFormItem = new SysUserTableFormItem();
        userFormItem.setUserId(SecurityUtils.getUserId());
        userFormItem.setRoleFormId(sysUserTableFormItem.getRoleFormId());
        userFormItem.setSearchVisible(true);
        List<SysUserTableFormItem> sysUserTableFormItemFilter = sysUserTableFormItemService.selectSysUserTableFormItemList(userFormItem);

        SysUserTableFormItem userFormFilter = new SysUserTableFormItem();
        userFormFilter.setUserItemId(sysUserTableFormItem.getUserItemId());
        Integer sortNo = sysUserTableFormItemFilter.size();
        userFormFilter.setSearchNo(sortNo.longValue());
        userFormFilter.setSearchVisible(false);
        sysUserTableFormItemService.updateSysUserTableFormItem(userFormFilter);
        //
        SysUserTableFormItem userTableFormItem = new SysUserTableFormItem();
        userTableFormItem.setUserId(SecurityUtils.getUserId());
        userTableFormItem.setRoleFormId(sysUserTableFormItem.getRoleFormId());
        userTableFormItem.setSearchVisible(true);
        List<SysUserTableFormItem> sysUserTableFormItems = sysUserTableFormItemService.selectSysUserTableFormItemList(userTableFormItem);
        //可添加的查询项
        SysUserTableFormItem tableFormItemFilter = new SysUserTableFormItem();
        tableFormItemFilter.setUserId(SecurityUtils.getUserId());
        tableFormItemFilter.setSearchVisible(false);
        List<SysUserTableFormItem> sysTableFormItemsFilter = sysUserTableFormItemService.selectSysUserTableFormItemList(tableFormItemFilter);

        SysRoleTableForm sysRoleTableForm = new SysRoleTableForm();
        sysRoleTableForm.setItems(sysUserTableFormItems);
        sysRoleTableForm.setFilterItems(sysTableFormItemsFilter);

        SysRoleTable sysRoleTable = new SysRoleTable();
        sysRoleTable.setFormConfig(sysRoleTableForm);
        return sysRoleTable;
    }

    /**
     * 设置用户表格列宽
     *
     * @param ajaxData
     * @return
     */
    @Override
    public boolean columnWidth(JSONObject ajaxData) {
        try {
            SysUserTableColumn sysUserTableColumn = JSON.toJavaObject(ajaxData, SysUserTableColumn.class);
            SysUserTableColumn userTableColumn = new SysUserTableColumn();
            userTableColumn.setUserColumnId(sysUserTableColumn.getUserColumnId());
            userTableColumn.setWidth(sysUserTableColumn.getResizeWidth().toString());
            sysUserTableColumnService.updateSysUserTableColumn(userTableColumn);
            return true;
        } catch (CaptchaException e) {
            return false;
        }
    }

    /**
     * 设置用户表格列顺序
     *
     * @param ajaxData
     * @return
     */
    @Override
    public boolean columnOrder(JSONArray ajaxData) {
        try {
            Long userId = SecurityUtils.getUserId();
            List<UserColumnVo> userColumnVos = JSONArray.parseArray(ajaxData.toJSONString(), UserColumnVo.class);
            Integer i = 0;
            Long result = 0L;
            for (UserColumnVo userColumnVo : userColumnVos) {
                i++;
                SysUserTableColumn sysUserTableColumn = new SysUserTableColumn();
                sysUserTableColumn.setUserId(userId);
                sysUserTableColumn.setRoleTableId(userColumnVo.getColumnInfo().getRoleTableId());
                sysUserTableColumn.setUserColumnId(userColumnVo.getColumnInfo().getUserColumnId());
                SysUserTableColumn sysUserTableColumn1 = sysUserTableColumnService.selectSysUserTableColumnByUserColumnId(userColumnVo.getColumnInfo().getUserColumnId());
                if (null != sysUserTableColumn1) {
                    sysUserTableColumn.setSortNo(i);
                    if (sysUserTableColumn1.getVisible() != userColumnVo.getIsChecked()) {
                        sysUserTableColumn.setVisible(userColumnVo.getIsChecked());
                    }
                    result += sysUserTableColumnService.updateSysUserTableColumn(sysUserTableColumn);
                }
            }
            return true;
        } catch (CaptchaException e) {
            return false;
        }
    }

    /**
     * 初始化列属性字段
     */
    public static void initColumnField(SysTableColumn column, SysTable table) {
        String columnName = column.getColumnName();
        column.setTableId(table.getTableId());
        column.setCreateBy(table.getCreateBy());
        // 设置field字段名
        column.setField(StringUtils.toCamelCase(columnName));
        column.setTitle(column.getColumnComment());
    }

    /**
     * 初始化查询配置
     */
    public static void initFormField(SysTableColumn column, SysTableFormItem sysTableFormItem, SysTableForm sysTableForm) {
        String columnName = column.getColumnName();
        sysTableFormItem.setFormId(sysTableForm.getFormId());
        sysTableFormItem.setCreateBy(SecurityUtils.getUserId());
        // 设置field字段名
        sysTableFormItem.setField(StringUtils.toCamelCase(columnName));
        sysTableFormItem.setTitle(column.getColumnComment());
        sysTableFormItem.setSpan(5);
        String placeholder = "请输入" + column.getColumnComment();
        sysTableFormItem.setItemRender("{\"name\":\"$input\",\"props\":{\"placeholder\": \"" + placeholder + "\"}}");
    }

    /**
     * 功能表格查询条件
     *
     * @param sysTable
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysTable> buildQueryWrapper(SysTable sysTable) {
        LambdaQueryWrapper<SysTable> lambdaQueryWrapper = new LambdaQueryWrapper<SysTable>()
                .eq(!StringUtils.isNull(sysTable.getTableId()), SysTable::getTableId, sysTable.getTableId())
                .eq(!StringUtils.isNull(sysTable.getTableName()), SysTable::getTableName, sysTable.getTableName())
                .eq(!StringUtils.isNull(sysTable.getTableComment()), SysTable::getTableComment, sysTable.getTableComment())
                .eq(!StringUtils.isNull(sysTable.getMenuId()), SysTable::getMenuId, sysTable.getMenuId())
                .eq(!StringUtils.isNull(sysTable.getId()), SysTable::getId, sysTable.getId())
                .eq(!StringUtils.isNull(sysTable.getHeight()), SysTable::getHeight, sysTable.getHeight())
                .eq(!StringUtils.isNull(sysTable.getUniqueId()), SysTable::getUniqueId, sysTable.getUniqueId())
                .eq(!StringUtils.isNull(sysTable.getRowId()), SysTable::getRowId, sysTable.getRowId())
                .eq(!StringUtils.isNull(sysTable.getMaxHeight()), SysTable::getMaxHeight, sysTable.getMaxHeight())
                .eq(!StringUtils.isNull(sysTable.getAutoResize()), SysTable::getAutoResize, sysTable.getAutoResize())
                .eq(!StringUtils.isNull(sysTable.getSyncResize()), SysTable::getSyncResize, sysTable.getSyncResize())
                .eq(!StringUtils.isNull(sysTable.getStripe()), SysTable::getStripe, sysTable.getStripe())
                .eq(!StringUtils.isNull(sysTable.getBorder()), SysTable::getBorder, sysTable.getBorder())
                .eq(!StringUtils.isNull(sysTable.getRound()), SysTable::getRound, sysTable.getRound())
                .eq(!StringUtils.isNull(sysTable.getSize()), SysTable::getSize, sysTable.getSize())
                .eq(!StringUtils.isNull(sysTable.getAlign()), SysTable::getAlign, sysTable.getAlign())
                .eq(!StringUtils.isNull(sysTable.getHeaderAlign()), SysTable::getHeaderAlign, sysTable.getHeaderAlign())
                .eq(!StringUtils.isNull(sysTable.getFooterAlign()), SysTable::getFooterAlign, sysTable.getFooterAlign())
                .eq(!StringUtils.isNull(sysTable.getShowHeader()), SysTable::getShowHeader, sysTable.getShowHeader())
                .like(!StringUtils.isNull(sysTable.getRowClassName()), SysTable::getRowClassName, sysTable.getRowClassName())
                .like(!StringUtils.isNull(sysTable.getCellClassName()), SysTable::getCellClassName, sysTable.getCellClassName())
                .like(!StringUtils.isNull(sysTable.getHeaderRowClassName()), SysTable::getHeaderRowClassName, sysTable.getHeaderRowClassName())
                .like(!StringUtils.isNull(sysTable.getHeaderCellClassName()), SysTable::getHeaderCellClassName, sysTable.getHeaderCellClassName())
                .like(!StringUtils.isNull(sysTable.getFooterRowClassName()), SysTable::getFooterRowClassName, sysTable.getFooterRowClassName())
                .like(!StringUtils.isNull(sysTable.getFooterCellClassName()), SysTable::getFooterCellClassName, sysTable.getFooterCellClassName())
                .eq(!StringUtils.isNull(sysTable.getShowFooter()), SysTable::getShowFooter, sysTable.getShowFooter())
                .eq(!StringUtils.isNull(sysTable.getFooterMethod()), SysTable::getFooterMethod, sysTable.getFooterMethod())
                .eq(!StringUtils.isNull(sysTable.getShowOverflow()), SysTable::getShowOverflow, sysTable.getShowOverflow())
                .eq(!StringUtils.isNull(sysTable.getShowHeaderOverflow()), SysTable::getShowHeaderOverflow, sysTable.getShowHeaderOverflow())
                .eq(!StringUtils.isNull(sysTable.getShowFooterOverflow()), SysTable::getShowFooterOverflow, sysTable.getShowFooterOverflow())
                .eq(!StringUtils.isNull(sysTable.getKeepSource()), SysTable::getKeepSource, sysTable.getKeepSource())
                .eq(!StringUtils.isNull(sysTable.getEmptyText()), SysTable::getEmptyText, sysTable.getEmptyText())
                .eq(!StringUtils.isNull(sysTable.getExtParams()), SysTable::getExtParams, sysTable.getExtParams())
                .eq(!StringUtils.isNull(sysTable.getExtraConfig()), SysTable::getExtraConfig, sysTable.getExtraConfig())
                .eq(!StringUtils.isNull(sysTable.getHandConfig()), SysTable::getHandConfig, sysTable.getHandConfig())
                .eq(!StringUtils.isNull(sysTable.getCreateBy()), SysTable::getCreateBy, sysTable.getCreateBy())
                .ge(sysTable.getParams().get("beginCreateTime") != null, SysTable::getCreateTime, sysTable.getParams().get("beginCreateTime"))
                .le(sysTable.getParams().get("endCreateTime") != null, SysTable::getCreateTime, sysTable.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysTable.getUpdateBy()), SysTable::getUpdateBy, sysTable.getUpdateBy())
                .ge(sysTable.getParams().get("beginUpdateTime") != null, SysTable::getUpdateTime, sysTable.getParams().get("beginUpdateTime"))
                .le(sysTable.getParams().get("endUpdateTime") != null, SysTable::getUpdateTime, sysTable.getParams().get("endUpdateTime"))
                .eq(!StringUtils.isNull(sysTable.getRemark()), SysTable::getRemark, sysTable.getRemark())
                .orderByDesc(SysTable::getRemark);
        return lambdaQueryWrapper;
    }
}
