package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysTableProxy;

/**
 * 功能表数据代理Mapper接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface SysTableProxyMapper extends BaseMapperPlus<SysTableProxy> {

    /**
     * 查询功能表数据代理
     *
     * @param tableId 功能表id
     * @return 功能表数据代理
     */
    public SysTableProxy selectSysTableProxyByTableId(Long tableId);

    /**
     * 查询功能表数据代理
     *
     * @param proxyId 功能表数据代理主键
     * @return 功能表数据代理
     */
    public SysTableProxy selectSysTableProxyByProxyId(Long proxyId);

    /**
     * 查询功能表数据代理列表
     *
     * @param sysTableProxy 功能表数据代理
     * @return 功能表数据代理集合
     */
    public List<SysTableProxy> selectSysTableProxyList(SysTableProxy sysTableProxy);

    /**
     * 新增功能表数据代理
     *
     * @param sysTableProxy 功能表数据代理
     * @return 结果
     */
    public int insertSysTableProxy(SysTableProxy sysTableProxy);

    /**
     * 修改功能表数据代理
     *
     * @param sysTableProxy 功能表数据代理
     * @return 结果
     */
    public int updateSysTableProxy(SysTableProxy sysTableProxy);

    /**
     * 删除功能表数据代理
     *
     * @param proxyId 功能表数据代理主键
     * @return 结果
     */
    public int deleteSysTableProxyByProxyId(Long proxyId);

    /**
     * 批量删除功能表数据代理
     *
     * @param proxyIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysTableProxyByProxyIds(Long[] proxyIds);

    /**
     * 批量删除功能表数据代理
     *
     * @param tableIds 需要删除的表格id
     * @return 结果
     */
    public int deleteSysTableProxyByProxytableIds(Long[] tableIds);
}
