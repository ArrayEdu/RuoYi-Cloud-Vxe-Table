package com.ruoyi.tableConfig.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTableForm;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色功能表查询表Service接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface ISysRoleTableFormService extends IServicePlus<SysRoleTableForm> {
    /**
     * 查询角色功能表查询表
     *
     * @param roleTableId
     * @return 角色功能表查询表
     */
    public SysRoleTableForm selectSysRoleTableFormByRoleTableId(Long roleTableId);

    /**
     * 查询角色功能表查询表
     *
     * @param formId 角色功能表查询表主键
     * @return 角色功能表查询表
     */
    public SysRoleTableForm selectSysRoleTableFormByFormId(Long formId);

    /**
     * 查询角色功能表查询表
     *
     * @param roleFormId 角色功能表查询表主键
     * @return 角色功能表查询表
     */
    public SysRoleTableForm selectSysRoleTableFormByRoleFormId(Long roleFormId);

    /**
     * 查询角色功能表查询表列表
     *
     * @param sysRoleTableForm 角色功能表查询表
     * @return 角色功能表查询表集合
     */
    public List<SysRoleTableForm> selectSysRoleTableFormList(SysRoleTableForm sysRoleTableForm);

    /**
     * 查询角色功能表查询表分页列表
     *
     * @param sysRoleTableForm 角色功能表查询表
     * @param pageQuery        查询配置
     * @return
     */
    public TableDataInfo<SysRoleTableForm> selectSysRoleTableFormPage(SysRoleTableForm sysRoleTableForm, PageQuery pageQuery);

    /**
     * 新增角色功能表查询表
     *
     * @param sysRoleTableForm 角色功能表查询表
     * @return 结果
     */
    public int insertSysRoleTableForm(SysRoleTableForm sysRoleTableForm);

    /**
     * 修改角色功能表查询表
     *
     * @param sysRoleTableForm 角色功能表查询表
     * @return 结果
     */
    public int updateSysRoleTableForm(SysRoleTableForm sysRoleTableForm);

    /**
     * 批量删除角色功能表查询表
     *
     * @param roleFormIds 需要删除的角色功能表查询表主键集合
     * @return 结果
     */
    public int deleteSysRoleTableFormByRoleFormIds(Long[] roleFormIds);

    /**
     * 删除角色功能表查询表信息
     *
     * @param roleFormId 角色功能表查询表主键
     * @return 结果
     */
    public int deleteSysRoleTableFormByRoleFormId(Long roleFormId);

    /**
     * 角色功能表查询表查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysRoleTableForm> buildQueryWrapper(SysRoleTableForm sysRoleTableForm);

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
