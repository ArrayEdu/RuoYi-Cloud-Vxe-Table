package com.ruoyi.tableConfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableEditRules;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysTableEditRulesService;
import com.ruoyi.common.mybatis.core.controller.BaseController;

/**
 * 校验规则配置项Controller
 *
 * @author zly
 * @date 2023-04-25
 */
@RestController
@RequestMapping("/menuTableRules")
public class SysTableEditRulesController extends BaseController {
    @Autowired
    private ISysTableEditRulesService sysTableEditRulesService;

    /**
     * 查询校验规则配置项列表
     */
    @RequiresPermissions("tableConfig:tableRules:list")
    @GetMapping("/list")
    public TableDataInfo list(SysTableEditRules sysTableEditRules) {
        return TableDataInfo.build(sysTableEditRulesService.selectSysTableEditRulesList(sysTableEditRules));
    }

    /**
     * 查询校验规则配置项分页列表
     */
    @RequiresPermissions("tableConfig:tableRules:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysTableEditRulesService.selectSysTableEditRulesPage(ajaxData.toJavaObject(SysTableEditRules.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出校验规则配置项列表
     */
    @RequiresPermissions("tableConfig:tableRules:export")
    @Log(title = "校验规则配置项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysTableEditRules sysTableEditRules) {
        List<SysTableEditRules> list = sysTableEditRulesService.selectSysTableEditRulesList(sysTableEditRules);
        ExcelUtil.exportExcel(list, "校验规则配置项数据", SysTableEditRules.class, response);
    }

    /**
     * 获取校验规则配置项详细信息
     */
    @RequiresPermissions("tableConfig:tableRules:query")
    @GetMapping(value = "/{editRulesId}")
    public AjaxResult getInfo(@PathVariable("editRulesId") Long editRulesId) {
        return success(sysTableEditRulesService.selectSysTableEditRulesByEditRulesId(editRulesId));
    }

    /**
     * 新增校验规则配置项
     */
    @RequiresPermissions("tableConfig:tableRules:add")
    @Log(title = "校验规则配置项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTableEditRules sysTableEditRules) {
        return toAjax(sysTableEditRulesService.insertSysTableEditRules(sysTableEditRules));
    }

    /**
     * 修改校验规则配置项
     */
    @RequiresPermissions("tableConfig:tableRules:edit")
    @Log(title = "校验规则配置项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTableEditRules sysTableEditRules) {
        return toAjax(sysTableEditRulesService.updateSysTableEditRules(sysTableEditRules));
    }

    /**
     * 删除校验规则配置项
     */
    @RequiresPermissions("tableConfig:tableRules:remove")
    @Log(title = "校验规则配置项", businessType = BusinessType.DELETE)
    @DeleteMapping("/{editRulesIds}")
    public AjaxResult remove(@PathVariable Long[] editRulesIds) {
        return toAjax(sysTableEditRulesService.deleteSysTableEditRulesByEditRulesIds(editRulesIds));
    }
}
