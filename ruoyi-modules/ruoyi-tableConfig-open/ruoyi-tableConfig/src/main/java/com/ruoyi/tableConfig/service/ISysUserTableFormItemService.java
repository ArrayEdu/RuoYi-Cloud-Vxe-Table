package com.ruoyi.tableConfig.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysUserTableFormItem;

import javax.servlet.http.HttpServletResponse;

/**
 * 用户表单配置项列表Service接口
 *
 * @author zly
 * @date 2023-06-13
 */
public interface ISysUserTableFormItemService extends IServicePlus<SysUserTableFormItem> {

    /**
     * 查询用户表单配置项列表列表
     *
     * @param roleFormId 角色表格id
     * @return 用户表单配置项列表集合
     */
    public List<SysUserTableFormItem> selectSysUserTableFormItemByRoleFormId(Long roleFormId);

    /**
     * 查询用户表单配置项列表列表
     *
     * @param userId 用户id
     * @return 用户表单配置项列表集合
     */
    public List<SysUserTableFormItem> selectSysUserTableFormItemByUserId(Long userId);

    /**
     * 查询用户表单配置项列表
     *
     * @param userItemId 用户表单配置项列表主键
     * @return 用户表单配置项列表
     */
    public SysUserTableFormItem selectSysUserTableFormItemByUserItemId(Long userItemId);

    /**
     * 查询用户表单配置项列表列表
     *
     * @param sysUserTableFormItem 用户表单配置项列表
     * @return 用户表单配置项列表集合
     */
    public List<SysUserTableFormItem> selectSysUserTableFormItemList(SysUserTableFormItem sysUserTableFormItem);

    /**
     * 查询用户表单配置项列表分页列表
     *
     * @param sysUserTableFormItem 用户表单配置项列表
     * @param pageQuery            查询配置
     * @return
     */
    public TableDataInfo<SysUserTableFormItem> selectSysUserTableFormItemPage(SysUserTableFormItem sysUserTableFormItem, PageQuery pageQuery);

    /**
     * 新增用户表单配置项列表
     *
     * @param sysUserTableFormItem 用户表单配置项列表
     * @return 结果
     */
    public int insertSysUserTableFormItem(SysUserTableFormItem sysUserTableFormItem);

    /**
     * 修改用户表单配置项列表
     *
     * @param sysUserTableFormItem 用户表单配置项列表
     * @return 结果
     */
    public int updateSysUserTableFormItem(SysUserTableFormItem sysUserTableFormItem);

    /**
     * 批量删除用户表单配置项列表
     *
     * @param userItemIds 需要删除的用户表单配置项列表主键集合
     * @return 结果
     */
    public int deleteSysUserTableFormItemByUserItemIds(Long[] userItemIds);

    /**
     * 删除用户表单配置项列表信息
     *
     * @param userItemId 用户表单配置项列表主键
     * @return 结果
     */
    public int deleteSysUserTableFormItemByUserItemId(Long userItemId);

    /**
     * 删除用户表单配置项列表信息
     *
     * @param userId 用户id
     * @return 结果
     */
    public int deleteSysUserTableFormItemByUserId(Long userId);

    /**
     * 用户表单配置项列表查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysUserTableFormItem> buildQueryWrapper(SysUserTableFormItem sysUserTableFormItem);

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
