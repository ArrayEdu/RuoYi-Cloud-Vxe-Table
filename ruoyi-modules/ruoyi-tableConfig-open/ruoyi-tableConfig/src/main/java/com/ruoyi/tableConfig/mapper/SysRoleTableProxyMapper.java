package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysRoleTableProxy;

/**
 * 角色功能表数据代理Mapper接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface SysRoleTableProxyMapper extends BaseMapperPlus<SysRoleTableProxy> {

    /**
     * 查询角色功能表数据代理
     *
     * @param roleTableId
     * @return 角色功能表数据代理
     */
    public SysRoleTableProxy selectSysRoleTableProxyByRoleTableId(Long roleTableId);

    /**
     * 查询角色功能表数据代理
     *
     * @param proxyId 角色功能表数据代理主键
     * @return 角色功能表数据代理
     */
    public SysRoleTableProxy selectSysRoleTableProxyByProxyId(Long proxyId);

    /**
     * 查询角色功能表数据代理
     *
     * @param roleProxyId 角色功能表数据代理主键
     * @return 角色功能表数据代理
     */
    public SysRoleTableProxy selectSysRoleTableProxyByRoleProxyId(Long roleProxyId);

    /**
     * 查询角色功能表数据代理列表
     *
     * @param sysRoleTableProxy 角色功能表数据代理
     * @return 角色功能表数据代理集合
     */
    public List<SysRoleTableProxy> selectSysRoleTableProxyList(SysRoleTableProxy sysRoleTableProxy);

    /**
     * 新增角色功能表数据代理
     *
     * @param sysRoleTableProxy 角色功能表数据代理
     * @return 结果
     */
    public int insertSysRoleTableProxy(SysRoleTableProxy sysRoleTableProxy);

    /**
     * 修改角色功能表数据代理
     *
     * @param sysRoleTableProxy 角色功能表数据代理
     * @return 结果
     */
    public int updateSysRoleTableProxy(SysRoleTableProxy sysRoleTableProxy);

    /**
     * 删除角色功能表数据代理
     *
     * @param roleTableId 角色表格id
     * @return 结果
     */
    public int deleteSysRoleTableProxyByRoleTableId(Long roleTableId);

    /**
     * 删除角色功能表数据代理
     *
     * @param roleProxyId 角色功能表数据代理主键
     * @return 结果
     */
    public int deleteSysRoleTableProxyByRoleProxyId(Long roleProxyId);

    /**
     * 批量删除角色功能表数据代理
     *
     * @param roleProxyIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysRoleTableProxyByRoleProxyIds(Long[] roleProxyIds);
}
