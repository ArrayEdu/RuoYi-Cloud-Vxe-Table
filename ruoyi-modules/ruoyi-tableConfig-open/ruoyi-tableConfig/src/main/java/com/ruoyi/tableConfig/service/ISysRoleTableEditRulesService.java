package com.ruoyi.tableConfig.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTableEditRules;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色校验规则配置项Service接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface ISysRoleTableEditRulesService extends IServicePlus<SysRoleTableEditRules> {
    /**
     * 查询角色校验规则配置项
     *
     * @param roleTableId 角色校验规则配置项
     * @return 角色校验规则配置项
     */
    public List<SysRoleTableEditRules> selectSysRoleTableEditRulesByRoleTableId(Long roleTableId);

    /**
     * 查询角色校验规则配置项
     *
     * @param editRulesId 角色校验规则配置项主键
     * @return 角色校验规则配置项
     */
    public SysRoleTableEditRules selectSysRoleTableEditRulesByEditRulesId(Long editRulesId);

    /**
     * 查询角色校验规则配置项
     *
     * @param roleEditRulesId 角色校验规则配置项主键
     * @return 角色校验规则配置项
     */
    public SysRoleTableEditRules selectSysRoleTableEditRulesByRoleEditRulesId(Long roleEditRulesId);

    /**
     * 查询角色校验规则配置项列表
     *
     * @param sysRoleTableEditRules 角色校验规则配置项
     * @return 角色校验规则配置项集合
     */
    public List<SysRoleTableEditRules> selectSysRoleTableEditRulesList(SysRoleTableEditRules sysRoleTableEditRules);


    /**
     * 查询角色校验规则配置项分页列表
     *
     * @param sysRoleTableEditRules 角色校验规则配置项
     * @param pageQuery             查询配置
     * @return
     */
    public TableDataInfo<SysRoleTableEditRules> selectSysRoleTableEditRulesPage(SysRoleTableEditRules sysRoleTableEditRules, PageQuery pageQuery);

    /**
     * 新增角色校验规则配置项
     *
     * @param sysRoleTableEditRules 角色校验规则配置项
     * @return 结果
     */
    public int insertSysRoleTableEditRules(SysRoleTableEditRules sysRoleTableEditRules);

    /**
     * 修改角色校验规则配置项
     *
     * @param sysRoleTableEditRules 角色校验规则配置项
     * @return 结果
     */
    public int updateSysRoleTableEditRules(SysRoleTableEditRules sysRoleTableEditRules);

    /**
     * 批量删除角色校验规则配置项
     *
     * @param editRulesIds 需要删除的角色校验规则配置项主键集合
     * @return 结果
     */
    public int deleteSysRoleTableEditRulesByRoleEditRulesIds(Long[] editRulesIds);

    /**
     * 删除角色校验规则配置项信息
     *
     * @param editRulesId 角色校验规则配置项主键
     * @return 结果
     */
    public int deleteSysRoleTableEditRulesByRoleEditRulesId(Long editRulesId);

    /**
     * 角色校验规则配置项查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysRoleTableEditRules> buildQueryWrapper(SysRoleTableEditRules sysRoleTableEditRules);

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
