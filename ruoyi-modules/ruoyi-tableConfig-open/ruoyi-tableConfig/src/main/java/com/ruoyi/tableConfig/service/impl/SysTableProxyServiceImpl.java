package com.ruoyi.tableConfig.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysTableProxyMapper;
import com.ruoyi.tableConfig.service.ISysTableProxyService;

/**
 * 功能表数据代理Service业务层处理
 *
 * @author zly
 * @date 2023-04-25
 */
@Service
public class SysTableProxyServiceImpl extends ServiceImpl<SysTableProxyMapper, SysTableProxy> implements ISysTableProxyService {
    @Autowired
    private SysTableProxyMapper sysTableProxyMapper;


    /**
     * 查询功能表数据代理
     *
     * @param tableId 功能表id
     * @return 功能表数据代理
     */
    @Override
    public SysTableProxy selectSysTableProxyByTableId(Long tableId) {
        return sysTableProxyMapper.selectSysTableProxyByTableId(tableId);
    }

    /**
     * 查询功能表数据代理
     *
     * @param proxyId 功能表数据代理主键
     * @return 功能表数据代理
     */
    @Override
    public SysTableProxy selectSysTableProxyByProxyId(Long proxyId) {
        return sysTableProxyMapper.selectSysTableProxyByProxyId(proxyId);
    }

    /**
     * 查询功能表数据代理列表
     *
     * @param sysTableProxy 功能表数据代理
     * @return 功能表数据代理
     */
    @Override
    public List<SysTableProxy> selectSysTableProxyList(SysTableProxy sysTableProxy) {
        return sysTableProxyMapper.selectSysTableProxyList(sysTableProxy);
    }

    /**
     * 查询功能表数据代理分页列表
     *
     * @param sysTableProxy 功能表数据代理
     * @param pageQuery     查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysTableProxy> selectSysTableProxyPage(SysTableProxy sysTableProxy, PageQuery pageQuery) {
        LambdaQueryWrapper<SysTableProxy> lqw = buildQueryWrapper(sysTableProxy);
        Page<SysTableProxy> page = sysTableProxyMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增功能表数据代理
     *
     * @param sysTableProxy 功能表数据代理
     * @return 结果
     */
    @Override
    public int insertSysTableProxy(SysTableProxy sysTableProxy) {
        sysTableProxy.setCreateTime(DateUtils.getNowDate());
        return sysTableProxyMapper.insertSysTableProxy(sysTableProxy);
    }

    /**
     * 修改功能表数据代理
     *
     * @param sysTableProxy 功能表数据代理
     * @return 结果
     */
    @Override
    public int updateSysTableProxy(SysTableProxy sysTableProxy) {
        sysTableProxy.setUpdateTime(DateUtils.getNowDate());
        return sysTableProxyMapper.updateSysTableProxy(sysTableProxy);
    }

    /**
     * 批量删除功能表数据代理
     *
     * @param proxyIds 需要删除的功能表数据代理主键
     * @return 结果
     */
    @Override
    public int deleteSysTableProxyByProxyIds(Long[] proxyIds) {
        return sysTableProxyMapper.deleteSysTableProxyByProxyIds(proxyIds);
    }

    /**
     * 删除功能表数据代理信息
     *
     * @param proxyId 功能表数据代理主键
     * @return 结果
     */
    @Override
    public int deleteSysTableProxyByProxyId(Long proxyId) {
        return sysTableProxyMapper.deleteSysTableProxyByProxyId(proxyId);
    }

    @Override
    public LambdaQueryWrapper<SysTableProxy> buildQueryWrapper(SysTableProxy sysTableProxy) {
        LambdaQueryWrapper<SysTableProxy> lambdaQueryWrapper = new LambdaQueryWrapper<SysTableProxy>()
                .eq(!StringUtils.isNull(sysTableProxy.getProxyId()), SysTableProxy::getProxyId, sysTableProxy.getProxyId())
                .eq(!StringUtils.isNull(sysTableProxy.getTableId()), SysTableProxy::getTableId, sysTableProxy.getTableId())
                .like(!StringUtils.isNull(sysTableProxy.getRemark()), SysTableProxy::getRemark, sysTableProxy.getRemark())
                .like(!StringUtils.isNull(sysTableProxy.getCreateBy()), SysTableProxy::getCreateBy, sysTableProxy.getCreateBy())
                .like(!StringUtils.isNull(sysTableProxy.getUpdateBy()), SysTableProxy::getUpdateBy, sysTableProxy.getUpdateBy())
                .ge(sysTableProxy.getParams().get("beginCreateTime") != null, SysTableProxy::getCreateTime, sysTableProxy.getParams().get("beginCreateTime"))
                .le(sysTableProxy.getParams().get("endCreateTime") != null, SysTableProxy::getCreateTime, sysTableProxy.getParams().get("endCreateTime"))
                .ge(sysTableProxy.getParams().get("beginUpdateTime") != null, SysTableProxy::getUpdateTime, sysTableProxy.getParams().get("beginUpdateTime"))
                .le(sysTableProxy.getParams().get("endUpdateTime") != null, SysTableProxy::getUpdateTime, sysTableProxy.getParams().get("endUpdateTime"))
                .orderByDesc(SysTableProxy::getProxyId);
        return lambdaQueryWrapper;
    }
}
