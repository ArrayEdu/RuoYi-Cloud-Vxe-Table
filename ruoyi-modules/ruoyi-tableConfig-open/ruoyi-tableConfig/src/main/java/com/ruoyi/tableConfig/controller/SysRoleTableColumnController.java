package com.ruoyi.tableConfig.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTableColumn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysRoleTableColumnService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 表格权限列配置Controller
 *
 * @author zly
 * @date 2023-06-06
 */
@RestController
@RequestMapping("/roleTableColumn")
public class SysRoleTableColumnController extends BaseController {
    @Autowired
    private ISysRoleTableColumnService sysRoleTableColumnService;

    /**
     * 查询表格权限列配置列表
     */
    @RequiresPermissions("system:column:list")
    @GetMapping("/list")
    public TableDataInfo list(SysRoleTableColumn sysRoleTableColumn) {
        return TableDataInfo.build(sysRoleTableColumnService.selectSysRoleTableColumnList(sysRoleTableColumn));
    }

    /**
     * 查询表格权限列配置分页列表
     */
    @RequiresPermissions("system:column:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysRoleTableColumnService.selectSysRoleTableColumnPage(ajaxData.toJavaObject(SysRoleTableColumn.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出表格权限列配置列表
     */
    @RequiresPermissions("system:column:export")
    @Log(title = "表格权限列配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysRoleTableColumn sysRoleTableColumn) {
        List<SysRoleTableColumn> list = sysRoleTableColumnService.selectSysRoleTableColumnList(sysRoleTableColumn);
        ExcelUtil.exportExcel(list, "表格权限列配置数据", SysRoleTableColumn.class, response);
    }

    /**
     * vxe-table导出表格权限列配置列表
     */
    @RequiresPermissions("system:column:export")
    @Log(title = "表格权限列配置", businessType = BusinessType.EXPORT)
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        sysRoleTableColumnService.export(response, params);
    }

    /**
     * 获取表格权限列配置详细信息
     */
    @RequiresPermissions("system:column:query")
    @GetMapping(value = "/{roleColumnId}")
    public AjaxResult getInfo(@PathVariable("roleColumnId") Long roleColumnId) {
        return success(sysRoleTableColumnService.selectSysRoleTableColumnByRoleColumnId(roleColumnId));
    }

    /**
     * 获取表格权限列配置详细信息(批量)
     */
    @RequiresPermissions("system:column:query")
    @GetMapping(value = "/in/{columnIds}")
    public AjaxResult getInfos(@PathVariable(value = "columnIds", required = true) Long[] columnIds) {
        List<SysRoleTableColumn> list = sysRoleTableColumnService.listByIds(Arrays.asList(columnIds));
        return AjaxResult.success(list);
    }

    /**
     * 新增表格权限列配置
     */
    @RequiresPermissions("system:column:add")
    @Log(title = "表格权限列配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysRoleTableColumn sysRoleTableColumn) {
        return toAjax(sysRoleTableColumnService.insertSysRoleTableColumn(sysRoleTableColumn));
    }

    /**
     * 新增表格权限列配置(批量)
     */
    @RequiresPermissions("system:column:add")
    @Log(title = "表格权限列配置", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@RequestBody JSONArray jsonArray) {
        if (!sysRoleTableColumnService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableColumn.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改表格权限列配置
     */
    @RequiresPermissions("system:column:edit")
    @Log(title = "表格权限列配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysRoleTableColumn sysRoleTableColumn) {
        return toAjax(sysRoleTableColumnService.updateSysRoleTableColumn(sysRoleTableColumn));
    }

    /**
     * 修改表格权限列配置(批量)
     */
    @RequiresPermissions("system:column:edit")
    @Log(title = "表格权限列配置", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!sysRoleTableColumnService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableColumn.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除表格权限列配置
     */
    @RequiresPermissions("system:column:remove")
    @Log(title = "表格权限列配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{columnIds}")
    public AjaxResult removes(@PathVariable(value = "columnIds", required = true) Long[] columnIds) {
        return toAjax(sysRoleTableColumnService.removeByIds(Arrays.asList(columnIds)));
    }

    /**
     * 导入模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "表格权限列配置模板", SysRoleTableColumn.class, response);
    }
}
