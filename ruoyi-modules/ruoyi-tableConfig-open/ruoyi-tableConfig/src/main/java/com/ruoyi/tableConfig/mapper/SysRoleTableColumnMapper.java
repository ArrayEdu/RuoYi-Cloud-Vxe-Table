package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysRoleTableColumn;

/**
 * 表格权限列配置Mapper接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface SysRoleTableColumnMapper extends BaseMapperPlus<SysRoleTableColumn> {


    /**
     * 查询表格权限列配置
     *
     * @param roleTableId 表格权限列配置归属表编号
     * @return 表格权限列配置
     */
    public List<SysRoleTableColumn> selectSysRoleTableColumnByRoleTableId(Long roleTableId);

    /**
     * 查询表格权限列配置
     *
     * @param columnId 表格权限列配置主键
     * @return 表格权限列配置
     */
    public SysRoleTableColumn selectSysRoleTableColumnByColumnId(Long columnId, Long roleTableId);

    /**
     * 查询表格权限列配置
     *
     * @param roleColumnId 表格权限列配置主键
     * @return 表格权限列配置
     */
    public SysRoleTableColumn selectSysRoleTableColumnByRoleColumnId(Long roleColumnId);

    /**
     * 查询表格权限列配置列表
     *
     * @param sysRoleTableColumn 表格权限列配置
     * @return 表格权限列配置集合
     */
    public List<SysRoleTableColumn> selectSysRoleTableColumnList(SysRoleTableColumn sysRoleTableColumn);

    /**
     * 新增表格权限列配置
     *
     * @param sysRoleTableColumn 表格权限列配置
     * @return 结果
     */
    public int insertSysRoleTableColumn(SysRoleTableColumn sysRoleTableColumn);

    /**
     * 修改表格权限列配置
     *
     * @param sysRoleTableColumn 表格权限列配置
     * @return 结果
     */
    public int updateSysRoleTableColumn(SysRoleTableColumn sysRoleTableColumn);

    /**
     * 删除表格权限列配置
     *
     * @param columnId 表格权限列配置主键
     * @return 结果
     */
    public int deleteSysRoleTableColumnByColumnId(Long columnId);

    /**
     * 删除表格权限列配置
     *
     * @param roleTableId 角色表格id
     * @return 结果
     */
    public int deleteSysRoleTableColumnByRoleTableId(Long roleTableId);

    /**
     * 删除表格权限列配置
     *
     * @param roleColumnId 表格权限列配置主键
     * @return 结果
     */
    public int deleteSysRoleTableColumnByRoleColumnId(Long roleColumnId);

    /**
     * 批量删除表格权限列配置
     *
     * @param columnIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysRoleTableColumnByRoleColumnIds(Long[] columnIds);
}
