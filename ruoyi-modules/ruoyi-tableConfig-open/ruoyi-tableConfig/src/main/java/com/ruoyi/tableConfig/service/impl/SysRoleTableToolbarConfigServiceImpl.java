package com.ruoyi.tableConfig.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableToolbarConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysRoleTableToolbarConfigMapper;
import com.ruoyi.tableConfig.service.ISysRoleTableToolbarConfigService;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色工具栏配置Service业务层处理
 *
 * @author zly
 * @date 2023-06-06
 */
@Service
public class SysRoleTableToolbarConfigServiceImpl extends ServiceImpl<SysRoleTableToolbarConfigMapper, SysRoleTableToolbarConfig> implements ISysRoleTableToolbarConfigService {
    @Autowired
    private SysRoleTableToolbarConfigMapper sysRoleTableToolbarConfigMapper;

    /**
     * 查询角色工具栏配置
     *
     * @param roleTableId
     * @return 角色工具栏配置
     */
    @Override
    public SysRoleTableToolbarConfig selectSysRoleTableToolbarConfigByRoleTableId(Long roleTableId) {
        return sysRoleTableToolbarConfigMapper.selectSysRoleTableToolbarConfigByRoleTableId(roleTableId);
    }

    /**
     * 查询角色工具栏配置
     *
     * @param toolbarConfigId 角色工具栏配置主键
     * @return 角色工具栏配置
     */
    @Override
    public SysRoleTableToolbarConfig selectSysRoleTableToolbarConfigByToolbarConfigId(Long toolbarConfigId) {
        return sysRoleTableToolbarConfigMapper.selectSysRoleTableToolbarConfigByToolbarConfigId(toolbarConfigId);
    }

    /**
     * 查询角色工具栏配置
     *
     * @param roleToolbarConfigId 角色工具栏配置主键
     * @return 角色工具栏配置
     */
    @Override
    public SysRoleTableToolbarConfig selectSysRoleTableToolbarConfigByRoleToolbarConfigId(Long roleToolbarConfigId) {
        return sysRoleTableToolbarConfigMapper.selectSysRoleTableToolbarConfigByRoleToolbarConfigId(roleToolbarConfigId);
    }

    /**
     * 查询角色工具栏配置列表
     *
     * @param sysRoleTableToolbarConfig 角色工具栏配置
     * @return 角色工具栏配置
     */
    @Override
    public List<SysRoleTableToolbarConfig> selectSysRoleTableToolbarConfigList(SysRoleTableToolbarConfig sysRoleTableToolbarConfig) {
        return sysRoleTableToolbarConfigMapper.selectSysRoleTableToolbarConfigList(sysRoleTableToolbarConfig);
    }

    /**
     * 查询角色工具栏配置分页列表
     *
     * @param sysRoleTableToolbarConfig 角色工具栏配置
     * @param pageQuery                 查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysRoleTableToolbarConfig> selectSysRoleTableToolbarConfigPage(SysRoleTableToolbarConfig sysRoleTableToolbarConfig, PageQuery pageQuery) {
        LambdaQueryWrapper<SysRoleTableToolbarConfig> lqw = buildQueryWrapper(sysRoleTableToolbarConfig);
        Page<SysRoleTableToolbarConfig> page = sysRoleTableToolbarConfigMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增角色工具栏配置
     *
     * @param sysRoleTableToolbarConfig 角色工具栏配置
     * @return 结果
     */
    @Override
    public int insertSysRoleTableToolbarConfig(SysRoleTableToolbarConfig sysRoleTableToolbarConfig) {
        sysRoleTableToolbarConfig.setCreateTime(DateUtils.getNowDate());
        return sysRoleTableToolbarConfigMapper.insertSysRoleTableToolbarConfig(sysRoleTableToolbarConfig);
    }

    /**
     * 修改角色工具栏配置
     *
     * @param sysRoleTableToolbarConfig 角色工具栏配置
     * @return 结果
     */
    @Override
    public int updateSysRoleTableToolbarConfig(SysRoleTableToolbarConfig sysRoleTableToolbarConfig) {
        sysRoleTableToolbarConfig.setUpdateTime(DateUtils.getNowDate());
        return sysRoleTableToolbarConfigMapper.updateSysRoleTableToolbarConfig(sysRoleTableToolbarConfig);
    }

    /**
     * 批量删除角色工具栏配置
     *
     * @param roleToolbarConfigIds 需要删除的角色工具栏配置主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableToolbarConfigByRoleToolbarConfigIds(Long[] roleToolbarConfigIds) {
        return sysRoleTableToolbarConfigMapper.deleteSysRoleTableToolbarConfigByRoleToolbarConfigIds(roleToolbarConfigIds);
    }

    /**
     * 删除角色工具栏配置信息
     *
     * @param roleToolbarConfigId 角色工具栏配置主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableToolbarConfigByRoleToolbarConfigId(Long roleToolbarConfigId) {
        return sysRoleTableToolbarConfigMapper.deleteSysRoleTableToolbarConfigByRoleToolbarConfigId(roleToolbarConfigId);
    }

    /**
     * 角色工具栏配置查询条件
     *
     * @param sysRoleTableToolbarConfig
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysRoleTableToolbarConfig> buildQueryWrapper(SysRoleTableToolbarConfig sysRoleTableToolbarConfig) {
        LambdaQueryWrapper<SysRoleTableToolbarConfig> lambdaQueryWrapper = new LambdaQueryWrapper<SysRoleTableToolbarConfig>()
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getRoleToolbarConfigId()), SysRoleTableToolbarConfig::getRoleToolbarConfigId, sysRoleTableToolbarConfig.getRoleToolbarConfigId())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getToolbarConfigId()), SysRoleTableToolbarConfig::getToolbarConfigId, sysRoleTableToolbarConfig.getToolbarConfigId())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getRoleTableId()), SysRoleTableToolbarConfig::getRoleTableId, sysRoleTableToolbarConfig.getRoleTableId())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getExtSize()), SysRoleTableToolbarConfig::getExtSize, sysRoleTableToolbarConfig.getExtSize())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getLoading()), SysRoleTableToolbarConfig::getLoading, sysRoleTableToolbarConfig.getLoading())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getPerfect()), SysRoleTableToolbarConfig::getPerfect, sysRoleTableToolbarConfig.getPerfect())
                .like(!StringUtils.isNull(sysRoleTableToolbarConfig.getClassName()), SysRoleTableToolbarConfig::getClassName, sysRoleTableToolbarConfig.getClassName())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getExtImport()), SysRoleTableToolbarConfig::getExtImport, sysRoleTableToolbarConfig.getExtImport())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getExtExport()), SysRoleTableToolbarConfig::getExtExport, sysRoleTableToolbarConfig.getExtExport())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getPrint()), SysRoleTableToolbarConfig::getPrint, sysRoleTableToolbarConfig.getPrint())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getRefresh()), SysRoleTableToolbarConfig::getRefresh, sysRoleTableToolbarConfig.getRefresh())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getCustom()), SysRoleTableToolbarConfig::getCustom, sysRoleTableToolbarConfig.getCustom())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getButtons()), SysRoleTableToolbarConfig::getButtons, sysRoleTableToolbarConfig.getButtons())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getTools()), SysRoleTableToolbarConfig::getTools, sysRoleTableToolbarConfig.getTools())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getEnabled()), SysRoleTableToolbarConfig::getEnabled, sysRoleTableToolbarConfig.getEnabled())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getPrintDesign()), SysRoleTableToolbarConfig::getPrintDesign, sysRoleTableToolbarConfig.getPrintDesign())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getPrintDataQueryPort()), SysRoleTableToolbarConfig::getPrintDataQueryPort, sysRoleTableToolbarConfig.getPrintDataQueryPort())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getPrintDataQueryBefore()), SysRoleTableToolbarConfig::getPrintDataQueryBefore, sysRoleTableToolbarConfig.getPrintDataQueryBefore())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getPrintDataQueryAfter()), SysRoleTableToolbarConfig::getPrintDataQueryAfter, sysRoleTableToolbarConfig.getPrintDataQueryAfter())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getZoom()), SysRoleTableToolbarConfig::getZoom, sysRoleTableToolbarConfig.getZoom())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getSlots()), SysRoleTableToolbarConfig::getSlots, sysRoleTableToolbarConfig.getSlots())
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getCreateBy()), SysRoleTableToolbarConfig::getCreateBy, sysRoleTableToolbarConfig.getCreateBy())
                .ge(sysRoleTableToolbarConfig.getParams().get("beginCreateTime") != null, SysRoleTableToolbarConfig::getCreateTime, sysRoleTableToolbarConfig.getParams().get("beginCreateTime"))
                .le(sysRoleTableToolbarConfig.getParams().get("endCreateTime") != null, SysRoleTableToolbarConfig::getCreateTime, sysRoleTableToolbarConfig.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getUpdateBy()), SysRoleTableToolbarConfig::getUpdateBy, sysRoleTableToolbarConfig.getUpdateBy())
                .ge(sysRoleTableToolbarConfig.getParams().get("beginUpdateTime") != null, SysRoleTableToolbarConfig::getUpdateTime, sysRoleTableToolbarConfig.getParams().get("beginUpdateTime"))
                .le(sysRoleTableToolbarConfig.getParams().get("endUpdateTime") != null, SysRoleTableToolbarConfig::getUpdateTime, sysRoleTableToolbarConfig.getParams().get("endUpdateTime"))
                .eq(!StringUtils.isNull(sysRoleTableToolbarConfig.getRemark()), SysRoleTableToolbarConfig::getRemark, sysRoleTableToolbarConfig.getRemark())
                .orderByDesc(SysRoleTableToolbarConfig::getRemark);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysRoleTableToolbarConfig> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysRoleTableToolbarConfig> iPage = sysRoleTableToolbarConfigMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper<>());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysRoleTableToolbarConfigMapper.selectBatchIds(exceel.getIds());
        } else {
            SysRoleTableToolbarConfig sysRoleTableToolbarConfig = JSON.parseObject(String.valueOf(exceel.getQuery()), SysRoleTableToolbarConfig.class);
            list = sysRoleTableToolbarConfigMapper.selectSysRoleTableToolbarConfigList(sysRoleTableToolbarConfig);
        }
        String fileName = exceel.getFilename().equals(null) ? "角色工具栏配置数据" : exceel.getFilename();
        Set<String> column = Arrays.stream(exceel.getFields().stream().map(m -> m.getField()).collect(Collectors.toList()).stream().toArray(String[]::new)).collect(Collectors.toSet());
        ExcelUtil.exportExcel(list, fileName, SysRoleTableToolbarConfig.class, response, column);
    }
}
