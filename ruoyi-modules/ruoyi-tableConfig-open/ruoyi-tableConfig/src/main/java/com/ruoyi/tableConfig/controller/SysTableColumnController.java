package com.ruoyi.tableConfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableColumn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysTableColumnService;
import com.ruoyi.common.mybatis.core.controller.BaseController;

/**
 * 功能表格列配置Controller
 *
 * @author zly
 * @date 2023-04-25
 */
@RestController
@RequestMapping("/menuTableColumn")
public class SysTableColumnController extends BaseController {
    @Autowired
    private ISysTableColumnService sysTableColumnService;

    /**
     * 查询功能表格列配置列表
     */
    @RequiresPermissions("tableConfig:menuColumn:list")
    @GetMapping("/list")
    public TableDataInfo list(SysTableColumn sysTableColumn) {
        return TableDataInfo.build(sysTableColumnService.selectSysTableColumnList(sysTableColumn));
    }

    /**
     * 查询功能表格列配置分页列表
     */
    @RequiresPermissions("tableConfig:menuColumn:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysTableColumnService.selectSysTableColumnPage(ajaxData.toJavaObject(SysTableColumn.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出功能表格列配置列表
     */
    @RequiresPermissions("tableConfig:menuColumn:export")
    @Log(title = "功能表格列配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysTableColumn sysTableColumn) {
        List<SysTableColumn> list = sysTableColumnService.selectSysTableColumnList(sysTableColumn);
        ExcelUtil.exportExcel(list, "功能表格列配置数据", SysTableColumn.class, response);
    }

    /**
     * 获取功能表格列配置详细信息
     */
    @RequiresPermissions("tableConfig:menuColumn:query")
    @GetMapping(value = "/{columnId}")
    public AjaxResult getInfo(@PathVariable("columnId") Long columnId) {
        return success(sysTableColumnService.selectSysTableColumnByColumnId(columnId));
    }

    /**
     * 新增功能表格列配置
     */
    @RequiresPermissions("tableConfig:menuColumn:add")
    @Log(title = "功能表格列配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTableColumn sysTableColumn) {
        return toAjax(sysTableColumnService.insertSysTableColumn(sysTableColumn));
    }

    /**
     * 修改功能表格列配置
     */
    @RequiresPermissions("tableConfig:menuColumn:edit")
    @Log(title = "功能表格列配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTableColumn sysTableColumn) {
        return toAjax(sysTableColumnService.updateSysTableColumn(sysTableColumn));
    }

    /**
     * 删除功能表格列配置
     */
    @RequiresPermissions("tableConfig:menuColumn:remove")
    @Log(title = "功能表格列配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{columnIds}")
    public AjaxResult remove(@PathVariable Long[] columnIds) {
        return toAjax(sysTableColumnService.deleteSysTableColumnByColumnIds(columnIds));
    }
}
