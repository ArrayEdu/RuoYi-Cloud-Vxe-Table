package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysRoleTableToolbarConfig;

/**
 * 角色工具栏配置Mapper接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface SysRoleTableToolbarConfigMapper extends BaseMapperPlus<SysRoleTableToolbarConfig> {

    /**
     * 查询角色工具栏配置
     *
     * @param roleTableId
     * @return 角色工具栏配置
     */
    public SysRoleTableToolbarConfig selectSysRoleTableToolbarConfigByRoleTableId(Long roleTableId);

    /**
     * 查询角色工具栏配置
     *
     * @param toolbarConfigId 角色工具栏配置主键
     * @return 角色工具栏配置
     */
    public SysRoleTableToolbarConfig selectSysRoleTableToolbarConfigByToolbarConfigId(Long toolbarConfigId);

    /**
     * 查询角色工具栏配置
     *
     * @param roleToolbarConfigId 角色工具栏配置主键
     * @return 角色工具栏配置
     */
    public SysRoleTableToolbarConfig selectSysRoleTableToolbarConfigByRoleToolbarConfigId(Long roleToolbarConfigId);

    /**
     * 查询角色工具栏配置列表
     *
     * @param sysRoleTableToolbarConfig 角色工具栏配置
     * @return 角色工具栏配置集合
     */
    public List<SysRoleTableToolbarConfig> selectSysRoleTableToolbarConfigList(SysRoleTableToolbarConfig sysRoleTableToolbarConfig);

    /**
     * 新增角色工具栏配置
     *
     * @param sysRoleTableToolbarConfig 角色工具栏配置
     * @return 结果
     */
    public int insertSysRoleTableToolbarConfig(SysRoleTableToolbarConfig sysRoleTableToolbarConfig);

    /**
     * 修改角色工具栏配置
     *
     * @param sysRoleTableToolbarConfig 角色工具栏配置
     * @return 结果
     */
    public int updateSysRoleTableToolbarConfig(SysRoleTableToolbarConfig sysRoleTableToolbarConfig);

    /**
     * 删除角色工具栏配置
     *
     * @param roleTableId 角色表格id
     * @return 结果
     */
    public int deleteSysRoleTableToolbarConfigByRoleTableId(Long roleTableId);

    /**
     * 删除角色工具栏配置
     *
     * @param roleToolbarConfigId 角色工具栏配置主键
     * @return 结果
     */
    public int deleteSysRoleTableToolbarConfigByRoleToolbarConfigId(Long roleToolbarConfigId);

    /**
     * 批量删除角色工具栏配置
     *
     * @param roleToolbarConfigIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysRoleTableToolbarConfigByRoleToolbarConfigIds(Long[] roleToolbarConfigIds);
}
