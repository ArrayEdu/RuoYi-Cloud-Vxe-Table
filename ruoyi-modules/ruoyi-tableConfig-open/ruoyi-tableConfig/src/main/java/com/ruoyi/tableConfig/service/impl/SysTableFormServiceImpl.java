package com.ruoyi.tableConfig.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysTableFormMapper;
import com.ruoyi.tableConfig.service.ISysTableFormService;

/**
 * 功能表查询Service业务层处理
 *
 * @author zly
 * @date 2023-04-25
 */
@Service
public class SysTableFormServiceImpl extends ServiceImpl<SysTableFormMapper, SysTableForm> implements ISysTableFormService {
    @Autowired
    private SysTableFormMapper sysTableFormMapper;

    /**
     * 查询功能表查询
     *
     * @param tableId 功能表id
     * @return 功能表查询
     */
    @Override
    public SysTableForm selectSysTableFormByTableId(Long tableId) {
        return sysTableFormMapper.selectSysTableFormByTableId(tableId);
    }

    /**
     * 查询功能表查询
     *
     * @param formId 功能表查询主键
     * @return 功能表查询
     */
    @Override
    public SysTableForm selectSysTableFormByFormId(Long formId) {
        return sysTableFormMapper.selectSysTableFormByFormId(formId);
    }

    /**
     * 查询功能表查询列表
     *
     * @param sysTableForm 功能表查询
     * @return 功能表查询
     */
    @Override
    public List<SysTableForm> selectSysTableFormList(SysTableForm sysTableForm) {
        return sysTableFormMapper.selectSysTableFormList(sysTableForm);
    }

    /**
     * 查询功能表查询分页列表
     *
     * @param sysTableForm 功能表查询
     * @param pageQuery    查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysTableForm> selectSysTableFormPage(SysTableForm sysTableForm, PageQuery pageQuery) {
        LambdaQueryWrapper<SysTableForm> lqw = buildQueryWrapper(sysTableForm);
        Page<SysTableForm> page = sysTableFormMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增功能表查询
     *
     * @param sysTableForm 功能表查询
     * @return 结果
     */
    @Override
    public int insertSysTableForm(SysTableForm sysTableForm) {
        sysTableForm.setCreateTime(DateUtils.getNowDate());
        return sysTableFormMapper.insertSysTableForm(sysTableForm);
    }

    /**
     * 修改功能表查询
     *
     * @param sysTableForm 功能表查询
     * @return 结果
     */
    @Override
    public int updateSysTableForm(SysTableForm sysTableForm) {
        sysTableForm.setUpdateTime(DateUtils.getNowDate());
        return sysTableFormMapper.updateSysTableForm(sysTableForm);
    }

    /**
     * 批量删除功能表查询
     *
     * @param formIds 需要删除的功能表查询主键
     * @return 结果
     */
    @Override
    public int deleteSysTableFormByFormIds(Long[] formIds) {
        return sysTableFormMapper.deleteSysTableFormByFormIds(formIds);
    }

    /**
     * 删除功能表查询信息
     *
     * @param formId 功能表查询主键
     * @return 结果
     */
    @Override
    public int deleteSysTableFormByFormId(Long formId) {
        return sysTableFormMapper.deleteSysTableFormByFormId(formId);
    }

    @Override
    public LambdaQueryWrapper<SysTableForm> buildQueryWrapper(SysTableForm sysTableForm) {
        LambdaQueryWrapper<SysTableForm> lambdaQueryWrapper = new LambdaQueryWrapper<SysTableForm>()
                .eq(!StringUtils.isNull(sysTableForm.getFormId()), SysTableForm::getFormId, sysTableForm.getFormId())
                .eq(!StringUtils.isNull(sysTableForm.getTableId()), SysTableForm::getTableId, sysTableForm.getTableId())
                .like(!StringUtils.isNull(sysTableForm.getRemark()), SysTableForm::getRemark, sysTableForm.getRemark())
                .like(!StringUtils.isNull(sysTableForm.getCreateBy()), SysTableForm::getCreateBy, sysTableForm.getCreateBy())
                .like(!StringUtils.isNull(sysTableForm.getUpdateBy()), SysTableForm::getUpdateBy, sysTableForm.getUpdateBy())
                .ge(sysTableForm.getParams().get("beginCreateTime") != null, SysTableForm::getCreateTime, sysTableForm.getParams().get("beginCreateTime"))
                .le(sysTableForm.getParams().get("endCreateTime") != null, SysTableForm::getCreateTime, sysTableForm.getParams().get("endCreateTime"))
                .ge(sysTableForm.getParams().get("beginUpdateTime") != null, SysTableForm::getUpdateTime, sysTableForm.getParams().get("beginUpdateTime"))
                .le(sysTableForm.getParams().get("endUpdateTime") != null, SysTableForm::getUpdateTime, sysTableForm.getParams().get("endUpdateTime"))
                .orderByDesc(SysTableForm::getFormId);
        return lambdaQueryWrapper;
    }
}
