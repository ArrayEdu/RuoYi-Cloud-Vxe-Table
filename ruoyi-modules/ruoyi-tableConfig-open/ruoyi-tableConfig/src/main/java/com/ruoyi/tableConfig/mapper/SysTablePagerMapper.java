package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysTablePager;

/**
 * 功能表分页配置Mapper接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface SysTablePagerMapper extends BaseMapperPlus<SysTablePager> {

    /**
     * 查询功能表分页配置
     *
     * @param tableId 功能表id
     * @return 功能表分页配置
     */
    public SysTablePager selectSysTablePagerByTableId(Long tableId);

    /**
     * 查询功能表分页配置
     *
     * @param pagerId 功能表分页配置主键
     * @return 功能表分页配置
     */
    public SysTablePager selectSysTablePagerByPagerId(Long pagerId);

    /**
     * 查询功能表分页配置列表
     *
     * @param sysTablePager 功能表分页配置
     * @return 功能表分页配置集合
     */
    public List<SysTablePager> selectSysTablePagerList(SysTablePager sysTablePager);

    /**
     * 新增功能表分页配置
     *
     * @param sysTablePager 功能表分页配置
     * @return 结果
     */
    public int insertSysTablePager(SysTablePager sysTablePager);

    /**
     * 修改功能表分页配置
     *
     * @param sysTablePager 功能表分页配置
     * @return 结果
     */
    public int updateSysTablePager(SysTablePager sysTablePager);

    /**
     * 删除功能表分页配置
     *
     * @param pagerId 功能表分页配置主键
     * @return 结果
     */
    public int deleteSysTablePagerByPagerId(Long pagerId);

    /**
     * 批量删除功能表分页配置
     *
     * @param pagerIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysTablePagerByPagerIds(Long[] pagerIds);

    /**
     * 批量删除功能表分页配置
     *
     * @param tableIds 需要删除的表格id
     * @return 结果
     */
    public int deleteSysTablePagerByTableIds(Long[] tableIds);

    /**
     * 删除功能表分页配置
     *
     * @param tableId 需要删除的表格id
     * @return 结果
     */
    public int deleteSysTablePagerByTableId(Long tableId);
}
