package com.ruoyi.tableConfig.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTablePager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysTablePagerMapper;
import com.ruoyi.tableConfig.service.ISysTablePagerService;

/**
 * 功能表分页配置Service业务层处理
 *
 * @author zly
 * @date 2023-04-25
 */
@Service
public class SysTablePagerServiceImpl extends ServiceImpl<SysTablePagerMapper, SysTablePager> implements ISysTablePagerService {
    @Autowired
    private SysTablePagerMapper sysTablePagerMapper;

    /**
     * 查询功能表分页配置
     *
     * @param tableId 功能表id
     * @return 功能表分页配置
     */
    @Override
    public SysTablePager selectSysTablePagerByTableId(Long tableId) {
        return sysTablePagerMapper.selectSysTablePagerByTableId(tableId);
    }

    /**
     * 查询功能表分页配置
     *
     * @param pagerId 功能表分页配置主键
     * @return 功能表分页配置
     */
    @Override
    public SysTablePager selectSysTablePagerByPagerId(Long pagerId) {
        return sysTablePagerMapper.selectSysTablePagerByPagerId(pagerId);
    }

    /**
     * 查询功能表分页配置列表
     *
     * @param sysTablePager 功能表分页配置
     * @return 功能表分页配置
     */
    @Override
    public List<SysTablePager> selectSysTablePagerList(SysTablePager sysTablePager) {
        return sysTablePagerMapper.selectSysTablePagerList(sysTablePager);
    }

    /**
     * 查询功能表分页配置分页列表
     *
     * @param sysTablePager 功能表分页配置
     * @param pageQuery     查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysTablePager> selectSysTablePagerPage(SysTablePager sysTablePager, PageQuery pageQuery) {
        LambdaQueryWrapper<SysTablePager> lqw = buildQueryWrapper(sysTablePager);
        Page<SysTablePager> page = sysTablePagerMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增功能表分页配置
     *
     * @param sysTablePager 功能表分页配置
     * @return 结果
     */
    @Override
    public int insertSysTablePager(SysTablePager sysTablePager) {
        sysTablePager.setCreateTime(DateUtils.getNowDate());
        return sysTablePagerMapper.insertSysTablePager(sysTablePager);
    }

    /**
     * 修改功能表分页配置
     *
     * @param sysTablePager 功能表分页配置
     * @return 结果
     */
    @Override
    public int updateSysTablePager(SysTablePager sysTablePager) {
        sysTablePager.setUpdateTime(DateUtils.getNowDate());
        return sysTablePagerMapper.updateSysTablePager(sysTablePager);
    }

    /**
     * 批量删除功能表分页配置
     *
     * @param pagerIds 需要删除的功能表分页配置主键
     * @return 结果
     */
    @Override
    public int deleteSysTablePagerByPagerIds(Long[] pagerIds) {
        return sysTablePagerMapper.deleteSysTablePagerByPagerIds(pagerIds);
    }

    /**
     * 删除功能表分页配置信息
     *
     * @param pagerId 功能表分页配置主键
     * @return 结果
     */
    @Override
    public int deleteSysTablePagerByPagerId(Long pagerId) {
        return sysTablePagerMapper.deleteSysTablePagerByPagerId(pagerId);
    }

    @Override
    public LambdaQueryWrapper<SysTablePager> buildQueryWrapper(SysTablePager sysTablePager) {
        LambdaQueryWrapper<SysTablePager> lambdaQueryWrapper = new LambdaQueryWrapper<SysTablePager>()
                .eq(!StringUtils.isNull(sysTablePager.getPagerId()), SysTablePager::getPagerId, sysTablePager.getPagerId())
                .eq(!StringUtils.isNull(sysTablePager.getTableId()), SysTablePager::getTableId, sysTablePager.getTableId())
                .like(!StringUtils.isNull(sysTablePager.getRemark()), SysTablePager::getRemark, sysTablePager.getRemark())
                .like(!StringUtils.isNull(sysTablePager.getCreateBy()), SysTablePager::getCreateBy, sysTablePager.getCreateBy())
                .like(!StringUtils.isNull(sysTablePager.getUpdateBy()), SysTablePager::getUpdateBy, sysTablePager.getUpdateBy())
                .ge(sysTablePager.getParams().get("beginCreateTime") != null, SysTablePager::getCreateTime, sysTablePager.getParams().get("beginCreateTime"))
                .le(sysTablePager.getParams().get("endCreateTime") != null, SysTablePager::getCreateTime, sysTablePager.getParams().get("endCreateTime"))
                .ge(sysTablePager.getParams().get("beginUpdateTime") != null, SysTablePager::getUpdateTime, sysTablePager.getParams().get("beginUpdateTime"))
                .le(sysTablePager.getParams().get("endUpdateTime") != null, SysTablePager::getUpdateTime, sysTablePager.getParams().get("endUpdateTime"))
                .orderByDesc(SysTablePager::getPagerId);
        return lambdaQueryWrapper;
    }
}
