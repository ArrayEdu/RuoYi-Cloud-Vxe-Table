package com.ruoyi.tableConfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableToolbarConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysTableToolbarConfigService;
import com.ruoyi.common.mybatis.core.controller.BaseController;

/**
 * 工具栏配置Controller
 *
 * @author zly
 * @date 2023-05-15
 */
@RestController
@RequestMapping("/menuTableToolbarConfig")
public class SysTableToolbarConfigController extends BaseController {
    @Autowired
    private ISysTableToolbarConfigService sysTableToolbarConfigService;

    /**
     * 查询工具栏配置列表
     */
    @RequiresPermissions("tableConfig:sysTableToolbarConfig:list")
    @GetMapping("/list")
    public TableDataInfo list(SysTableToolbarConfig sysTableToolbarConfig) {
        return TableDataInfo.build(sysTableToolbarConfigService.selectSysTableToolbarConfigList(sysTableToolbarConfig));
    }

    /**
     * 查询工具栏配置分页列表
     */
    @RequiresPermissions("tableConfig:sysTableToolbarConfig:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysTableToolbarConfigService.selectSysTableToolbarConfigPage(ajaxData.toJavaObject(SysTableToolbarConfig.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出工具栏配置列表
     */
    @RequiresPermissions("tableConfig:sysTableToolbarConfig:export")
    @Log(title = "工具栏配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysTableToolbarConfig sysTableToolbarConfig) {
        List<SysTableToolbarConfig> list = sysTableToolbarConfigService.selectSysTableToolbarConfigList(sysTableToolbarConfig);
        ExcelUtil.exportExcel(list, "工具栏配置数据", SysTableToolbarConfig.class, response);
    }

    /**
     * 获取工具栏配置详细信息
     */
    @RequiresPermissions("tableConfig:sysTableToolbarConfig:query")
    @GetMapping(value = "/{toolbarConfigId}")
    public AjaxResult getInfo(@PathVariable("toolbarConfigId") Long toolbarConfigId) {
        return success(sysTableToolbarConfigService.selectSysTableToolbarConfigByToolbarConfigId(toolbarConfigId));
    }

    /**
     * 新增工具栏配置
     */
    @RequiresPermissions("tableConfig:sysTableToolbarConfig:add")
    @Log(title = "工具栏配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTableToolbarConfig sysTableToolbarConfig) {
        return toAjax(sysTableToolbarConfigService.insertSysTableToolbarConfig(sysTableToolbarConfig));
    }

    /**
     * 修改工具栏配置
     */
    @RequiresPermissions("tableConfig:sysTableToolbarConfig:edit")
    @Log(title = "工具栏配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTableToolbarConfig sysTableToolbarConfig) {
        return toAjax(sysTableToolbarConfigService.updateSysTableToolbarConfig(sysTableToolbarConfig));
    }

    /**
     * 删除工具栏配置
     */
    @RequiresPermissions("tableConfig:sysTableToolbarConfig:remove")
    @Log(title = "工具栏配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{toolbarConfigIds}")
    public AjaxResult remove(@PathVariable Long[] toolbarConfigIds) {
        return toAjax(sysTableToolbarConfigService.deleteSysTableToolbarConfigByToolbarConfigIds(toolbarConfigIds));
    }
}
