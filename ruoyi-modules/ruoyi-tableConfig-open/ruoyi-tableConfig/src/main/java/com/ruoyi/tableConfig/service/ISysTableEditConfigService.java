package com.ruoyi.tableConfig.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysTableEditConfig;

/**
 * 可编辑配置项Service接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface ISysTableEditConfigService extends IServicePlus<SysTableEditConfig> {
    /**
     * 查询可编辑配置项
     *
     * @param tableId 表格id
     * @return 可编辑配置项
     */
    public SysTableEditConfig selectSysTableEditConfigByTableId(Long tableId);

    /**
     * 查询可编辑配置项
     *
     * @param editConfigId 可编辑配置项主键
     * @return 可编辑配置项
     */
    public SysTableEditConfig selectSysTableEditConfigByEditConfigId(Long editConfigId);

    /**
     * 查询可编辑配置项列表
     *
     * @param sysTableEditConfig 可编辑配置项
     * @return 可编辑配置项集合
     */
    public List<SysTableEditConfig> selectSysTableEditConfigList(SysTableEditConfig sysTableEditConfig);

    /**
     * 查询可编辑配置项分页列表
     *
     * @param sysTableEditConfig 可编辑配置项
     * @param pageQuery          查询配置
     * @return
     */
    public TableDataInfo<SysTableEditConfig> selectSysTableEditConfigPage(SysTableEditConfig sysTableEditConfig, PageQuery pageQuery);

    /**
     * 新增可编辑配置项
     *
     * @param sysTableEditConfig 可编辑配置项
     * @return 结果
     */
    public int insertSysTableEditConfig(SysTableEditConfig sysTableEditConfig);

    /**
     * 修改可编辑配置项
     *
     * @param sysTableEditConfig 可编辑配置项
     * @return 结果
     */
    public int updateSysTableEditConfig(SysTableEditConfig sysTableEditConfig);

    /**
     * 批量删除可编辑配置项
     *
     * @param editConfigIds 需要删除的可编辑配置项主键集合
     * @return 结果
     */
    public int deleteSysTableEditConfigByEditConfigIds(Long[] editConfigIds);

    /**
     * 删除可编辑配置项信息
     *
     * @param editConfigId 可编辑配置项主键
     * @return 结果
     */
    public int deleteSysTableEditConfigByEditConfigId(Long editConfigId);

    public LambdaQueryWrapper<SysTableEditConfig> buildQueryWrapper(SysTableEditConfig sysTableEditConfig);
}
