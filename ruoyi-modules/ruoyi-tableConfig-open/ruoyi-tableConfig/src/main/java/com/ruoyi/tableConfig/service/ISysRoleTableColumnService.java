package com.ruoyi.tableConfig.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTableColumn;

import javax.servlet.http.HttpServletResponse;

/**
 * 表格权限列配置Service接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface ISysRoleTableColumnService extends IServicePlus<SysRoleTableColumn> {

    /**
     * 查询表格权限列配置
     *
     * @param roleTableId 表格权限列配置归属表编号
     * @return 表格权限列配置
     */
    public List<SysRoleTableColumn> selectSysRoleTableColumnByRoleTableId(Long roleTableId);

    /**
     * 查询表格权限列配置
     *
     * @param roleColumnId 表格权限列配置主键
     * @return 表格权限列配置
     */
    public SysRoleTableColumn selectSysRoleTableColumnByRoleColumnId(Long roleColumnId);

    /**
     * 查询表格权限列配置
     *
     * @param columnId    菜单表格列
     * @param roleTableId 表格权限列配置主键
     * @return 表格权限列配置
     */
    public SysRoleTableColumn selectSysRoleTableColumnByColumnId(Long columnId, Long roleTableId);


    /**
     * 查询表格权限列配置列表
     *
     * @param sysRoleTableColumn 表格权限列配置
     * @return 表格权限列配置集合
     */
    public List<SysRoleTableColumn> selectSysRoleTableColumnList(SysRoleTableColumn sysRoleTableColumn);

    /**
     * 查询表格权限列配置分页列表
     *
     * @param sysRoleTableColumn 表格权限列配置
     * @param pageQuery          分页配置
     * @return
     */
    public TableDataInfo<SysRoleTableColumn> selectSysRoleTableColumnPage(SysRoleTableColumn sysRoleTableColumn, PageQuery pageQuery);

    /**
     * 新增表格权限列配置
     *
     * @param sysRoleTableColumn 表格权限列配置
     * @return 结果
     */
    public int insertSysRoleTableColumn(SysRoleTableColumn sysRoleTableColumn);

    /**
     * 修改表格权限列配置
     *
     * @param sysRoleTableColumn 表格权限列配置
     * @return 结果
     */
    public int updateSysRoleTableColumn(SysRoleTableColumn sysRoleTableColumn);

    /**
     * 批量删除表格权限列配置
     *
     * @param roleColumnIds 需要删除的表格权限列配置主键集合
     * @return 结果
     */
    public int deleteSysRoleTableColumnByRoleColumnIds(Long[] roleColumnIds);

    /**
     * 删除表格权限列配置信息
     *
     * @param roleColumnId 表格权限列配置主键
     * @return 结果
     */
    public int deleteSysRoleTableColumnByRoleColumnId(Long roleColumnId);

    /**
     * 删除表格权限列配置信息
     *
     * @param columnId 表格权限列配置主键
     * @return 结果
     */
    public int deleteSysRoleTableColumnByColumnId(Long columnId);

    /**
     * 表格权限列配置查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysRoleTableColumn> buildQueryWrapper(SysRoleTableColumn sysRoleTableColumn);

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
