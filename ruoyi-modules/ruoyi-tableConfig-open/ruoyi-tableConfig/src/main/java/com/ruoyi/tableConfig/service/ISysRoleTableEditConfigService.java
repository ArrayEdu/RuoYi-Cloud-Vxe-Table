package com.ruoyi.tableConfig.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTableEditConfig;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色可编辑配置项Service接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface ISysRoleTableEditConfigService extends IServicePlus<SysRoleTableEditConfig> {

    /**
     * 查询角色可编辑配置项
     *
     * @param roleTableId 角色可编辑配置项归属表编号
     * @return 角色可编辑配置项
     */
    public SysRoleTableEditConfig selectSysRoleTableEditConfigByRoleTableId(Long roleTableId);

    /**
     * 查询角色可编辑配置项
     *
     * @param editConfigId 菜单可编辑配置项主键
     * @return 角色可编辑配置项
     */
    public SysRoleTableEditConfig selectSysRoleTableEditConfigByEditConfigId(Long editConfigId);

    /**
     * 查询角色可编辑配置项
     *
     * @param roleEditConfigId 角色可编辑配置项主键
     * @return 角色可编辑配置项
     */
    public SysRoleTableEditConfig selectSysRoleTableEditConfigByRoleEditConfigId(Long roleEditConfigId);

    /**
     * 查询角色可编辑配置项列表
     *
     * @param sysRoleTableEditConfig 角色可编辑配置项
     * @return 角色可编辑配置项集合
     */
    public List<SysRoleTableEditConfig> selectSysRoleTableEditConfigList(SysRoleTableEditConfig sysRoleTableEditConfig);

    /**
     * 查询角色可编辑配置项分页列表
     *
     * @param sysRoleTableEditConfig 角色可编辑配置项
     * @param pageQuery              查询配置
     * @return
     */
    public TableDataInfo<SysRoleTableEditConfig> selectSysRoleTableEditConfigPage(SysRoleTableEditConfig sysRoleTableEditConfig, PageQuery pageQuery);

    /**
     * 新增角色可编辑配置项
     *
     * @param sysRoleTableEditConfig 角色可编辑配置项
     * @return 结果
     */
    public int insertSysRoleTableEditConfig(SysRoleTableEditConfig sysRoleTableEditConfig);

    /**
     * 修改角色可编辑配置项
     *
     * @param sysRoleTableEditConfig 角色可编辑配置项
     * @return 结果
     */
    public int updateSysRoleTableEditConfig(SysRoleTableEditConfig sysRoleTableEditConfig);

    /**
     * 批量删除角色可编辑配置项
     *
     * @param editConfigIds 需要删除的角色可编辑配置项主键集合
     * @return 结果
     */
    public int deleteSysRoleTableEditConfigByRoleEditConfigIds(Long[] editConfigIds);

    /**
     * 删除角色可编辑配置项信息
     *
     * @param editConfigId 角色可编辑配置项主键
     * @return 结果
     */
    public int deleteSysRoleTableEditConfigByRoleEditConfigId(Long editConfigId);

    /**
     * 角色可编辑配置项查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysRoleTableEditConfig> buildQueryWrapper(SysRoleTableEditConfig sysRoleTableEditConfig);

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
