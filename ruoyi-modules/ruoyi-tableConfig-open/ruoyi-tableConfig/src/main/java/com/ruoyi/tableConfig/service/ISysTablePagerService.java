package com.ruoyi.tableConfig.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysTablePager;

/**
 * 功能表分页配置Service接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface ISysTablePagerService extends IServicePlus<SysTablePager> {
    /**
     * 查询功能表分页配置
     *
     * @param tableId 功能表id
     * @return 功能表分页配置
     */
    public SysTablePager selectSysTablePagerByTableId(Long tableId);

    /**
     * 查询功能表分页配置
     *
     * @param pagerId 功能表分页配置主键
     * @return 功能表分页配置
     */
    public SysTablePager selectSysTablePagerByPagerId(Long pagerId);

    /**
     * 查询功能表分页配置列表
     *
     * @param sysTablePager 功能表分页配置
     * @return 功能表分页配置集合
     */
    public List<SysTablePager> selectSysTablePagerList(SysTablePager sysTablePager);

    /**
     * 查询功能表分页配置分页列表
     *
     * @param sysTablePager 功能表分页配置
     * @param pageQuery     查询配置
     * @return
     */
    public TableDataInfo<SysTablePager> selectSysTablePagerPage(SysTablePager sysTablePager, PageQuery pageQuery);

    /**
     * 新增功能表分页配置
     *
     * @param sysTablePager 功能表分页配置
     * @return 结果
     */
    public int insertSysTablePager(SysTablePager sysTablePager);

    /**
     * 修改功能表分页配置
     *
     * @param sysTablePager 功能表分页配置
     * @return 结果
     */
    public int updateSysTablePager(SysTablePager sysTablePager);

    /**
     * 批量删除功能表分页配置
     *
     * @param pagerIds 需要删除的功能表分页配置主键集合
     * @return 结果
     */
    public int deleteSysTablePagerByPagerIds(Long[] pagerIds);

    /**
     * 删除功能表分页配置信息
     *
     * @param pagerId 功能表分页配置主键
     * @return 结果
     */
    public int deleteSysTablePagerByPagerId(Long pagerId);

    public LambdaQueryWrapper<SysTablePager> buildQueryWrapper(SysTablePager sysTablePager);
}
