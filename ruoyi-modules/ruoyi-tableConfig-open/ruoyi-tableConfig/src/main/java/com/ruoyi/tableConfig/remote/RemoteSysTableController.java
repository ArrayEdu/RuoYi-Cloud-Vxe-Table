package com.ruoyi.tableConfig.remote;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.ruoyi.common.security.annotation.InnerAuth;
import com.ruoyi.tableConfig.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "菜单表格远程调用接口")
@RequestMapping("/remoteTable")
public class RemoteSysTableController extends BaseController {
    @Autowired
    private ISysTableService sysTableService;
    @Autowired
    private ISysTableColumnService sysTableColumnService;
    @Autowired
    private ISysTableEditConfigService sysTableEditConfigService;
    @Autowired
    private ISysTableEditRulesService sysTableEditRulesService;
    @Autowired
    private ISysTableFormService sysTableFormService;
    @Autowired
    private ISysTableFormItemService sysTableFormItemService;
    @Autowired
    private ISysTablePagerService sysTablePagerService;
    @Autowired
    private ISysTableProxyService sysTableProxyService;
    @Autowired
    private ISysTableToolbarConfigService sysTableToolbarConfigService;

    /**
     * 获取功能表格详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取功能表格详细信息")
    @GetMapping(value = "/menuTable/{tableId}")
    public R getTableInfo(@PathVariable("tableId") Long tableId) {
        return R.ok(sysTableService.selectSysTableByTableId(tableId));
    }

    /**
     * 通过菜单id获取表格数据
     *
     * @param menuId
     * @return
     */
    @InnerAuth
    @ApiOperation(value = "通过菜单id获取表格数据")
    @GetMapping(value = "/menuTable/getTableByMenuId/{menuId}")
    public R getTableInfoByMenuId(@PathVariable("menuId") Long menuId) {
        return R.ok(sysTableService.selectSysTableByMenuId(menuId));
    }

    /**
     * 获取功能表格列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取功能表格列配置详细信息")
    @GetMapping(value = "/menuTableColumn/{columnId}")
    public R getColumnInfo(@PathVariable("columnId") Long columnId) {
        return R.ok(sysTableColumnService.selectSysTableColumnByColumnId(columnId));
    }

    /**
     * 通过表格id获取功能表格列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过表格id获取功能表格列配置详细信息")
    @GetMapping(value = "/menuTableColumn/getColumnByTableId/{tableId}")
    public R getColumnInfoByTableId(@PathVariable("tableId") Long tableId) {
        return R.ok(sysTableColumnService.selectSysTableColumnByTableId(tableId));
    }

    /**
     * 获取可编辑配置项详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取可编辑配置项详细信息")
    @GetMapping(value = "/menuTableEditConfig/{editConfigId}")
    public R getEditConfigInfo(@PathVariable("editConfigId") Long editConfigId) {
        return R.ok(sysTableEditConfigService.selectSysTableEditConfigByEditConfigId(editConfigId));
    }

    /**
     * 通过表格id获取可编辑配置项详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过表格id获取可编辑配置项详细信息")
    @GetMapping(value = "/menuTableEditConfig/getEditConfigByTableId/{tableId}")
    public R getEditConfigInfoByTableId(@PathVariable("tableId") Long tableId) {
        return R.ok(sysTableEditConfigService.selectSysTableEditConfigByTableId(tableId));
    }

    /**
     * 获取校验规则配置项详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取校验规则配置项详细信息")
    @GetMapping(value = "/menuTableRules/{editRulesId}")
    public R getEditRulesInfo(@PathVariable("editRulesId") Long editRulesId) {
        return R.ok(sysTableEditRulesService.selectSysTableEditRulesByEditRulesId(editRulesId));
    }

    /**
     * 通过表格id获取校验规则配置项详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过表格id获取校验规则配置项详细信息")
    @GetMapping(value = "/menuTableRules/getEditRulesByTableId/{tableId}")
    public R getEditRulesInfoByTableId(@PathVariable("tableId") Long tableId) {
        return R.ok(sysTableEditRulesService.selectSysTableEditRulesByTableId(tableId));
    }

    /**
     * 获取功能表查询详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取功能表查询详细信息")
    @GetMapping(value = "/menuTableForm/{formId}")
    public R getFormInfo(@PathVariable("formId") Long formId) {
        return R.ok(sysTableFormService.selectSysTableFormByFormId(formId));
    }

    /**
     * 通过表格id获取功能表查询详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过表格id获取功能表查询详细信息")
    @GetMapping(value = "/menuTableForm/getFormByTableId/{tableId}")
    public R getFormInfoByTableId(@PathVariable("tableId") Long tableId) {
        return R.ok(sysTableFormService.selectSysTableFormByTableId(tableId));
    }

    /**
     * 获取表单配置项列详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取表单配置项列详细信息")
    @GetMapping(value = "/menuTableFormItem/{itemId}")
    public R getFormItemInfo(@PathVariable("itemId") Long itemId) {
        return R.ok(sysTableFormItemService.selectSysTableFormItemByItemId(itemId));
    }

    /**
     * 通过表单id获取表单配置项列详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过表单id获取表单配置项列详细信息")
    @GetMapping(value = "/menuTableFormItem/getFormItemByFormId/{formId}")
    public R getFormItemInfoByFormId(@PathVariable("formId") Long formId) {
        return R.ok(sysTableFormItemService.selectSysTableFormItemByFormId(formId));
    }

    /**
     * 获取功能表分页配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取表单配置项列详细信息")
    @GetMapping(value = "/menuTablePager/{pagerId}")
    public R getPageInfo(@PathVariable("pagerId") Long pagerId) {
        return R.ok(sysTablePagerService.selectSysTablePagerByPagerId(pagerId));
    }

    /**
     * 通过表格id获取功能表分页配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过表格id获取表单配置项列详细信息")
    @GetMapping(value = "/menuTablePager/getPagerByTableId/{tableId}")
    public R getPageInfoByTableId(@PathVariable("tableId") Long tableId) {
        return R.ok(sysTablePagerService.selectSysTablePagerByTableId(tableId));
    }

    /**
     * 获取功能表数据代理详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取功能表数据代理详细信息")
    @GetMapping(value = "/menuTableProxy/{proxyId}")
    public R getProxyInfo(@PathVariable("proxyId") Long proxyId) {
        return R.ok(sysTableProxyService.selectSysTableProxyByProxyId(proxyId));
    }

    /**
     * 通过表格id获取功能表数据代理详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过表格id获取功能表数据代理详细信息")
    @GetMapping(value = "/menuTableProxy/getProxyByTableId/{tableId}")
    public R getProxyInfoByTableId(@PathVariable("tableId") Long tableId) {
        return R.ok(sysTableProxyService.selectSysTableProxyByTableId(tableId));
    }

    /**
     * 获取工具栏配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取工具栏配置详细信息")
    @GetMapping(value = "/menuTableToolbarConfig/{toolbarConfigId}")
    public R getToolBarConfigInfo(@PathVariable("toolbarConfigId") Long toolbarConfigId) {
        return R.ok(sysTableToolbarConfigService.selectSysTableToolbarConfigByToolbarConfigId(toolbarConfigId));
    }

    /**
     * 通过表格id获取工具栏配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过表格id获取工具栏配置详细信息")
    @GetMapping(value = "/menuTableToolbarConfig/getToolBarByTableId/{tableId}")
    public R getToolBarConfigInfoByTableId(@PathVariable("tableId") Long tableId) {
        return R.ok(sysTableToolbarConfigService.selectSysTableToolbarConfigByTableId(tableId));
    }
}
