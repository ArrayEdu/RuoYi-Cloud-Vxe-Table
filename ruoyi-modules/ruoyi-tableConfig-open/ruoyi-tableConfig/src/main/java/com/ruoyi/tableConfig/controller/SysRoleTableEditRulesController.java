package com.ruoyi.tableConfig.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;


import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableColumn;
import com.ruoyi.tableConfig.api.domain.SysRoleTableEditRules;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysRoleTableEditRulesService;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 角色校验规则配置项Controller
 *
 * @author zly
 * @date 2023-06-06
 */
@RestController
@RequestMapping("/roleTableRules")
public class SysRoleTableEditRulesController extends BaseController {
    @Autowired
    private ISysRoleTableEditRulesService sysRoleTableEditRulesService;

    /**
     * 查询角色校验规则配置项列表
     */
    @RequiresPermissions("system:rules:list")
    @GetMapping("/list")
    public TableDataInfo list(SysRoleTableEditRules sysRoleTableEditRules) {
        return TableDataInfo.build(sysRoleTableEditRulesService.selectSysRoleTableEditRulesList(sysRoleTableEditRules));
    }

    /**
     * 查询角色校验规则配置项分页列表
     */
    @RequiresPermissions("system:rules:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysRoleTableEditRulesService.selectSysRoleTableEditRulesPage(ajaxData.toJavaObject(SysRoleTableEditRules.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出角色校验规则配置项列表
     */
    @RequiresPermissions("system:rules:export")
    @Log(title = "角色校验规则配置项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysRoleTableEditRules sysRoleTableEditRules) {
        List<SysRoleTableEditRules> list = sysRoleTableEditRulesService.selectSysRoleTableEditRulesList(sysRoleTableEditRules);
        ExcelUtil.exportExcel(list, "角色校验规则配置项数据", SysRoleTableEditRules.class, response);
    }

    /**
     * vxe-table导出角色校验规则配置项列表
     */
    @RequiresPermissions("system:rules:export")
    @Log(title = "角色校验规则配置项", businessType = BusinessType.EXPORT)
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        sysRoleTableEditRulesService.export(response, params);
    }

    /**
     * 获取角色校验规则配置项详细信息
     */
    @RequiresPermissions("system:rules:query")
    @GetMapping(value = "/{roleEditRulesId}")
    public AjaxResult getInfo(@PathVariable("roleEditRulesId") Long roleEditRulesId) {
        return success(sysRoleTableEditRulesService.selectSysRoleTableEditRulesByRoleEditRulesId(roleEditRulesId));
    }

    /**
     * 获取角色校验规则配置项详细信息(批量)
     */
    @RequiresPermissions("system:rules:query")
    @GetMapping(value = "/in/{roleEditRulesIds}")
    public AjaxResult getInfos(@PathVariable(value = "roleEditRulesIds", required = true) Long[] roleEditRulesIds) {
        List<SysRoleTableEditRules> list = sysRoleTableEditRulesService.listByIds(Arrays.asList(roleEditRulesIds));
        return AjaxResult.success(list);
    }

    /**
     * 新增角色校验规则配置项
     */
    @RequiresPermissions("system:rules:add")
    @Log(title = "角色校验规则配置项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysRoleTableEditRules sysRoleTableEditRules) {
        return toAjax(sysRoleTableEditRulesService.insertSysRoleTableEditRules(sysRoleTableEditRules));
    }

    /**
     * 新增角色校验规则配置项(批量)
     */
    @RequiresPermissions("system:rules:add")
    @Log(title = "角色校验规则配置项", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@RequestBody JSONArray jsonArray) {
        if (!sysRoleTableEditRulesService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableEditRules.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改角色校验规则配置项
     */
    @RequiresPermissions("system:rules:edit")
    @Log(title = "角色校验规则配置项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysRoleTableEditRules sysRoleTableEditRules) {
        return toAjax(sysRoleTableEditRulesService.updateSysRoleTableEditRules(sysRoleTableEditRules));
    }

    /**
     * 修改角色校验规则配置项(批量)
     */
    @RequiresPermissions("system:rules:edit")
    @Log(title = "角色校验规则配置项", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!sysRoleTableEditRulesService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableEditRules.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除角色校验规则配置项
     */
    @RequiresPermissions("system:rules:remove")
    @Log(title = "角色校验规则配置项", businessType = BusinessType.DELETE)
    @DeleteMapping("/{editRulesIds}")
    public AjaxResult removes(@PathVariable(value = "editRulesIds", required = true) Long[] editRulesIds) {
        return toAjax(sysRoleTableEditRulesService.removeByIds(Arrays.asList(editRulesIds)));
    }

    /**
     * 导入模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "角色校验规则配置项模板", SysRoleTableEditRules.class, response);
    }
}
