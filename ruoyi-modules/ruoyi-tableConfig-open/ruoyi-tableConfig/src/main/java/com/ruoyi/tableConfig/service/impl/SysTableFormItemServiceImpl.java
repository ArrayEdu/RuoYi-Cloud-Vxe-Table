package com.ruoyi.tableConfig.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableFormItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysTableFormItemMapper;
import com.ruoyi.tableConfig.service.ISysTableFormItemService;

/**
 * 表单配置项列Service业务层处理
 *
 * @author zly
 * @date 2023-04-25
 */
@Service
public class SysTableFormItemServiceImpl extends ServiceImpl<SysTableFormItemMapper, SysTableFormItem> implements ISysTableFormItemService {
    @Autowired
    private SysTableFormItemMapper sysTableFormItemMapper;

    /**
     * 查询表单配置项列
     *
     * @param formId 表单配置id
     * @return 表单配置项列
     */
    @Override
    public List<SysTableFormItem> selectSysTableFormItemByFormId(Long formId) {
        return sysTableFormItemMapper.selectSysTableFormItemByFormId(formId);
    }

    /**
     * 查询表单配置项列
     *
     * @param itemId 表单配置项列主键
     * @return 表单配置项列
     */
    @Override
    public SysTableFormItem selectSysTableFormItemByItemId(Long itemId) {
        return sysTableFormItemMapper.selectSysTableFormItemByItemId(itemId);
    }

    /**
     * 查询表单配置项列列表
     *
     * @param sysTableFormItem 表单配置项列
     * @return 表单配置项列
     */
    @Override
    public List<SysTableFormItem> selectSysTableFormItemList(SysTableFormItem sysTableFormItem) {
        return sysTableFormItemMapper.selectSysTableFormItemList(sysTableFormItem);
    }

    /**
     * 查询表单配置项列分页列表
     *
     * @param sysTableFormItem 表单配置项列
     * @param pageQuery        查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysTableFormItem> selectSysTableFormItemPage(SysTableFormItem sysTableFormItem, PageQuery pageQuery) {
        LambdaQueryWrapper<SysTableFormItem> lqw = buildQueryWrapper(sysTableFormItem);
        Page<SysTableFormItem> page = sysTableFormItemMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增表单配置项列
     *
     * @param sysTableFormItem 表单配置项列
     * @return 结果
     */
    @Override
    public int insertSysTableFormItem(SysTableFormItem sysTableFormItem) {
        sysTableFormItem.setCreateTime(DateUtils.getNowDate());
        return sysTableFormItemMapper.insertSysTableFormItem(sysTableFormItem);
    }

    /**
     * 修改表单配置项列
     *
     * @param sysTableFormItem 表单配置项列
     * @return 结果
     */
    @Override
    public int updateSysTableFormItem(SysTableFormItem sysTableFormItem) {
        sysTableFormItem.setUpdateTime(DateUtils.getNowDate());
        return sysTableFormItemMapper.updateSysTableFormItem(sysTableFormItem);
    }

    /**
     * 批量删除表单配置项列
     *
     * @param itemIds 需要删除的表单配置项列主键
     * @return 结果
     */
    @Override
    public int deleteSysTableFormItemByItemIds(Long[] itemIds) {
        return sysTableFormItemMapper.deleteSysTableFormItemByItemIds(itemIds);
    }

    /**
     * 删除表单配置项列信息
     *
     * @param itemId 表单配置项列主键
     * @return 结果
     */
    @Override
    public int deleteSysTableFormItemByItemId(Long itemId) {
        return sysTableFormItemMapper.deleteSysTableFormItemByItemId(itemId);
    }

    @Override
    public LambdaQueryWrapper<SysTableFormItem> buildQueryWrapper(SysTableFormItem sysTableFormItem) {
        LambdaQueryWrapper<SysTableFormItem> lambdaQueryWrapper = new LambdaQueryWrapper<SysTableFormItem>()
                .eq(!StringUtils.isNull(sysTableFormItem.getFormId()), SysTableFormItem::getFormId, sysTableFormItem.getFormId())
                .eq(!StringUtils.isNull(sysTableFormItem.getItemId()), SysTableFormItem::getItemId, sysTableFormItem.getItemId())
                .like(!StringUtils.isNull(sysTableFormItem.getRemark()), SysTableFormItem::getRemark, sysTableFormItem.getRemark())
                .like(!StringUtils.isNull(sysTableFormItem.getCreateBy()), SysTableFormItem::getCreateBy, sysTableFormItem.getCreateBy())
                .like(!StringUtils.isNull(sysTableFormItem.getUpdateBy()), SysTableFormItem::getUpdateBy, sysTableFormItem.getUpdateBy())
                .ge(sysTableFormItem.getParams().get("beginCreateTime") != null, SysTableFormItem::getCreateTime, sysTableFormItem.getParams().get("beginCreateTime"))
                .le(sysTableFormItem.getParams().get("endCreateTime") != null, SysTableFormItem::getCreateTime, sysTableFormItem.getParams().get("endCreateTime"))
                .ge(sysTableFormItem.getParams().get("beginUpdateTime") != null, SysTableFormItem::getUpdateTime, sysTableFormItem.getParams().get("beginUpdateTime"))
                .le(sysTableFormItem.getParams().get("endUpdateTime") != null, SysTableFormItem::getUpdateTime, sysTableFormItem.getParams().get("endUpdateTime"))
                .orderByDesc(SysTableFormItem::getItemId);
        return lambdaQueryWrapper;
    }
}
