package com.ruoyi.tableConfig.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableFormItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysRoleTableFormItemMapper;
import com.ruoyi.tableConfig.service.ISysRoleTableFormItemService;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色表单配置项列表Service业务层处理
 *
 * @author zly
 * @date 2023-06-06
 */
@Service
public class SysRoleTableFormItemServiceImpl extends ServiceImpl<SysRoleTableFormItemMapper, SysRoleTableFormItem> implements ISysRoleTableFormItemService {
    @Autowired
    private SysRoleTableFormItemMapper sysRoleTableFormItemMapper;

    /**
     * 查询角色表单配置项列表
     *
     * @param roleFormId
     * @return 角色表单配置项列表
     */
    @Override
    public List<SysRoleTableFormItem> selectSysRoleTableFormItemByRoleFormId(Long roleFormId) {
        return sysRoleTableFormItemMapper.selectSysRoleTableFormItemByRoleFormId(roleFormId);
    }

    /**
     * 查询角色表单配置项列表
     *
     * @param roleItemId 角色表单配置项列表主键
     * @return 角色表单配置项列表
     */
    @Override
    public SysRoleTableFormItem selectSysRoleTableFormItemByRoleItemId(Long roleItemId) {
        return sysRoleTableFormItemMapper.selectSysRoleTableFormItemByRoleItemId(roleItemId);
    }

    @Override
    public SysRoleTableFormItem selectSysRoleTableFormItemByItemId(Long itemId) {
        return sysRoleTableFormItemMapper.selectSysRoleTableFormItemByItemId(itemId);
    }

    /**
     * 查询角色表单配置项列表列表
     *
     * @param sysRoleTableFormItem 角色表单配置项列表
     * @return 角色表单配置项列表
     */
    @Override
    public List<SysRoleTableFormItem> selectSysRoleTableFormItemList(SysRoleTableFormItem sysRoleTableFormItem) {
        return sysRoleTableFormItemMapper.selectSysRoleTableFormItemList(sysRoleTableFormItem);
    }

    /**
     * 查询角色表单配置项列表分页列表
     *
     * @param sysRoleTableFormItem 角色表单配置项列表
     * @param pageQuery            查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysRoleTableFormItem> selectSysRoleTableFormItemPage(SysRoleTableFormItem sysRoleTableFormItem, PageQuery pageQuery) {
        LambdaQueryWrapper<SysRoleTableFormItem> lqw = buildQueryWrapper(sysRoleTableFormItem);
        Page<SysRoleTableFormItem> page = sysRoleTableFormItemMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增角色表单配置项列表
     *
     * @param sysRoleTableFormItem 角色表单配置项列表
     * @return 结果
     */
    @Override
    public int insertSysRoleTableFormItem(SysRoleTableFormItem sysRoleTableFormItem) {
        sysRoleTableFormItem.setCreateTime(DateUtils.getNowDate());
        return sysRoleTableFormItemMapper.insertSysRoleTableFormItem(sysRoleTableFormItem);
    }

    /**
     * 修改角色表单配置项列表
     *
     * @param sysRoleTableFormItem 角色表单配置项列表
     * @return 结果
     */
    @Override
    public int updateSysRoleTableFormItem(SysRoleTableFormItem sysRoleTableFormItem) {
        sysRoleTableFormItem.setUpdateTime(DateUtils.getNowDate());
        return sysRoleTableFormItemMapper.updateSysRoleTableFormItem(sysRoleTableFormItem);
    }

    /**
     * 批量删除角色表单配置项列表
     *
     * @param roleItemIds 需要删除的角色表单配置项列表主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableFormItemByRoleItemIds(Long[] roleItemIds) {
        return sysRoleTableFormItemMapper.deleteSysRoleTableFormItemByRoleItemIds(roleItemIds);
    }

    /**
     * 删除角色表单配置项列表信息
     *
     * @param roleItemId 角色表单配置项列表主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableFormItemByRoleItemId(Long roleItemId) {
        return sysRoleTableFormItemMapper.deleteSysRoleTableFormItemByRoleItemId(roleItemId);
    }

    /**
     * 角色表单配置项列表查询条件
     *
     * @param sysRoleTableFormItem
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysRoleTableFormItem> buildQueryWrapper(SysRoleTableFormItem sysRoleTableFormItem) {
        LambdaQueryWrapper<SysRoleTableFormItem> lambdaQueryWrapper = new LambdaQueryWrapper<SysRoleTableFormItem>()
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getItemId()), SysRoleTableFormItem::getItemId, sysRoleTableFormItem.getItemId())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getRoleItemId()), SysRoleTableFormItem::getRoleItemId, sysRoleTableFormItem.getRoleItemId())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getRoleFormId()), SysRoleTableFormItem::getRoleFormId, sysRoleTableFormItem.getRoleFormId())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getField()), SysRoleTableFormItem::getField, sysRoleTableFormItem.getField())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getTitle()), SysRoleTableFormItem::getTitle, sysRoleTableFormItem.getTitle())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getSpan()), SysRoleTableFormItem::getSpan, sysRoleTableFormItem.getSpan())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getAlign()), SysRoleTableFormItem::getAlign, sysRoleTableFormItem.getAlign())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getTitleAlign()), SysRoleTableFormItem::getTitleAlign, sysRoleTableFormItem.getTitleAlign())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getTitleWidth()), SysRoleTableFormItem::getTitleWidth, sysRoleTableFormItem.getTitleWidth())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getTitleColon()), SysRoleTableFormItem::getTitleColon, sysRoleTableFormItem.getTitleColon())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getTitleAsterisk()), SysRoleTableFormItem::getTitleAsterisk, sysRoleTableFormItem.getTitleAsterisk())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getTitleOverflow()), SysRoleTableFormItem::getTitleOverflow, sysRoleTableFormItem.getTitleOverflow())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getShowTitle()), SysRoleTableFormItem::getShowTitle, sysRoleTableFormItem.getShowTitle())
                .like(!StringUtils.isNull(sysRoleTableFormItem.getClassName()), SysRoleTableFormItem::getClassName, sysRoleTableFormItem.getClassName())
                .like(!StringUtils.isNull(sysRoleTableFormItem.getContentClassName()), SysRoleTableFormItem::getContentClassName, sysRoleTableFormItem.getContentClassName())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getContentStyle()), SysRoleTableFormItem::getContentStyle, sysRoleTableFormItem.getContentStyle())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getVisible()), SysRoleTableFormItem::getVisible, sysRoleTableFormItem.getVisible())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getVisibleMethod()), SysRoleTableFormItem::getVisibleMethod, sysRoleTableFormItem.getVisibleMethod())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getFormFilter()), SysRoleTableFormItem::getFormFilter, sysRoleTableFormItem.getFormFilter())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getFolding()), SysRoleTableFormItem::getFolding, sysRoleTableFormItem.getFolding())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getCollapseNode()), SysRoleTableFormItem::getCollapseNode, sysRoleTableFormItem.getCollapseNode())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getResetValue()), SysRoleTableFormItem::getResetValue, sysRoleTableFormItem.getResetValue())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getItemRender()), SysRoleTableFormItem::getItemRender, sysRoleTableFormItem.getItemRender())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getChildren()), SysRoleTableFormItem::getChildren, sysRoleTableFormItem.getChildren())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getSlots()), SysRoleTableFormItem::getSlots, sysRoleTableFormItem.getSlots())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getSortNo()), SysRoleTableFormItem::getSortNo, sysRoleTableFormItem.getSortNo())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getDictType()), SysRoleTableFormItem::getDictType, sysRoleTableFormItem.getDictType())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getSearchHandle()), SysRoleTableFormItem::getSearchHandle, sysRoleTableFormItem.getSearchHandle())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getSearchNo()), SysRoleTableFormItem::getSearchNo, sysRoleTableFormItem.getSearchNo())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getSearchVisible()), SysRoleTableFormItem::getSearchVisible, sysRoleTableFormItem.getSearchVisible())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getSearchFixed()), SysRoleTableFormItem::getSearchFixed, sysRoleTableFormItem.getSearchFixed())
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getCreateBy()), SysRoleTableFormItem::getCreateBy, sysRoleTableFormItem.getCreateBy())
                .ge(sysRoleTableFormItem.getParams().get("beginCreateTime") != null, SysRoleTableFormItem::getCreateTime, sysRoleTableFormItem.getParams().get("beginCreateTime"))
                .le(sysRoleTableFormItem.getParams().get("endCreateTime") != null, SysRoleTableFormItem::getCreateTime, sysRoleTableFormItem.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getUpdateBy()), SysRoleTableFormItem::getUpdateBy, sysRoleTableFormItem.getUpdateBy())
                .ge(sysRoleTableFormItem.getParams().get("beginUpdateTime") != null, SysRoleTableFormItem::getUpdateTime, sysRoleTableFormItem.getParams().get("beginUpdateTime"))
                .le(sysRoleTableFormItem.getParams().get("endUpdateTime") != null, SysRoleTableFormItem::getUpdateTime, sysRoleTableFormItem.getParams().get("endUpdateTime"))
                .eq(!StringUtils.isNull(sysRoleTableFormItem.getRemark()), SysRoleTableFormItem::getRemark, sysRoleTableFormItem.getRemark())
                .orderByDesc(SysRoleTableFormItem::getRemark);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysRoleTableFormItem> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysRoleTableFormItem> iPage = sysRoleTableFormItemMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper<>());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysRoleTableFormItemMapper.selectBatchIds(exceel.getIds());
        } else {
            SysRoleTableFormItem sysRoleTableFormItem = JSON.parseObject(String.valueOf(exceel.getQuery()), SysRoleTableFormItem.class);
            list = sysRoleTableFormItemMapper.selectSysRoleTableFormItemList(sysRoleTableFormItem);
        }
        String fileName = exceel.getFilename().equals(null) ? "角色表单配置项列表数据" : exceel.getFilename();
        Set<String> column = Arrays.stream(exceel.getFields().stream().map(m -> m.getField()).collect(Collectors.toList()).stream().toArray(String[]::new)).collect(Collectors.toSet());
        ExcelUtil.exportExcel(list, fileName, SysRoleTableFormItem.class, response, column);
    }
}
