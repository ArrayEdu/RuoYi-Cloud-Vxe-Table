package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;

/**
 * 表格权限业务表Mapper接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface SysRoleTableMapper extends BaseMapperPlus<SysRoleTable> {

    /**
     * 查询表格权限业务表
     *
     * @param menuId 菜单id
     * @return 表格权限业务表
     */
    public List<SysRoleTable> selectSysRoleTableByMenuId(Long menuId);

    /**
     * 查询表格权限业务表
     *
     * @param roleId 角色id
     * @return 表格权限业务表
     */
    public List<SysRoleTable> selectSysRoleTableByRoleId(Long roleId);

    public SysRoleTable selectSysRoleTable(SysRoleTable sysRoleTable);

    /**
     * 查询表格权限业务表
     *
     * @param tableId 表格权限业务表主键
     * @return 表格权限业务表
     */
    public List<SysRoleTable> selectSysRoleTableByTableId(Long tableId);

    /**
     * 查询表格权限业务表
     *
     * @param roleTableId 表格权限业务表主键
     * @return 表格权限业务表
     */
    public SysRoleTable selectSysRoleTableByRoleTableId(Long roleTableId);

    /**
     * 查询表格权限业务表列表
     *
     * @param sysRoleTable 表格权限业务表
     * @return 表格权限业务表集合
     */
    public List<SysRoleTable> selectSysRoleTableList(SysRoleTable sysRoleTable);

    /**
     * 新增表格权限业务表
     *
     * @param sysRoleTable 表格权限业务表
     * @return 结果
     */
    public int insertSysRoleTable(SysRoleTable sysRoleTable);

    /**
     * 修改表格权限业务表
     *
     * @param sysRoleTable 表格权限业务表
     * @return 结果
     */
    public int updateSysRoleTable(SysRoleTable sysRoleTable);

    /**
     * 删除表格权限业务表
     *
     * @param roleTableId 表格权限业务表主键
     * @return 结果
     */
    public int deleteSysRoleTableByRoleTableId(Long roleTableId);

    /**
     * 批量删除表格权限业务表
     *
     * @param roleTableIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysRoleTableByRoleTableIds(Long[] roleTableIds);
}
