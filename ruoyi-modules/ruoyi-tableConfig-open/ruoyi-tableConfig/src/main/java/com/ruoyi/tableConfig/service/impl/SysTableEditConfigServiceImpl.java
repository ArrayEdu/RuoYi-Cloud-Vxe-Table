package com.ruoyi.tableConfig.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableEditConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysTableEditConfigMapper;
import com.ruoyi.tableConfig.service.ISysTableEditConfigService;

/**
 * 可编辑配置项Service业务层处理
 *
 * @author zly
 * @date 2023-04-25
 */
@Service
public class SysTableEditConfigServiceImpl extends ServiceImpl<SysTableEditConfigMapper, SysTableEditConfig> implements ISysTableEditConfigService {
    @Autowired
    private SysTableEditConfigMapper sysTableEditConfigMapper;

    /**
     * 查询可编辑配置项
     *
     * @param tableId 表格id
     * @return 可编辑配置项
     */
    @Override
    public SysTableEditConfig selectSysTableEditConfigByTableId(Long tableId) {
        return sysTableEditConfigMapper.selectSysTableEditConfigByTableId(tableId);
    }

    /**
     * 查询可编辑配置项
     *
     * @param editConfigId 可编辑配置项主键
     * @return 可编辑配置项
     */
    @Override
    public SysTableEditConfig selectSysTableEditConfigByEditConfigId(Long editConfigId) {
        return sysTableEditConfigMapper.selectSysTableEditConfigByEditConfigId(editConfigId);
    }

    /**
     * 查询可编辑配置项列表
     *
     * @param sysTableEditConfig 可编辑配置项
     * @return 可编辑配置项
     */
    @Override
    public List<SysTableEditConfig> selectSysTableEditConfigList(SysTableEditConfig sysTableEditConfig) {
        return sysTableEditConfigMapper.selectSysTableEditConfigList(sysTableEditConfig);
    }

    /**
     * 查询可编辑配置项分页列表
     *
     * @param sysTableEditConfig 可编辑配置项
     * @param pageQuery          查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysTableEditConfig> selectSysTableEditConfigPage(SysTableEditConfig sysTableEditConfig, PageQuery pageQuery) {
        LambdaQueryWrapper<SysTableEditConfig> lqw = buildQueryWrapper(sysTableEditConfig);
        Page<SysTableEditConfig> page = sysTableEditConfigMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增可编辑配置项
     *
     * @param sysTableEditConfig 可编辑配置项
     * @return 结果
     */
    @Override
    public int insertSysTableEditConfig(SysTableEditConfig sysTableEditConfig) {
        sysTableEditConfig.setCreateTime(DateUtils.getNowDate());
        return sysTableEditConfigMapper.insertSysTableEditConfig(sysTableEditConfig);
    }

    /**
     * 修改可编辑配置项
     *
     * @param sysTableEditConfig 可编辑配置项
     * @return 结果
     */
    @Override
    public int updateSysTableEditConfig(SysTableEditConfig sysTableEditConfig) {
        sysTableEditConfig.setUpdateTime(DateUtils.getNowDate());
        return sysTableEditConfigMapper.updateSysTableEditConfig(sysTableEditConfig);
    }

    /**
     * 批量删除可编辑配置项
     *
     * @param editConfigIds 需要删除的可编辑配置项主键
     * @return 结果
     */
    @Override
    public int deleteSysTableEditConfigByEditConfigIds(Long[] editConfigIds) {
        return sysTableEditConfigMapper.deleteSysTableEditConfigByEditConfigIds(editConfigIds);
    }

    /**
     * 删除可编辑配置项信息
     *
     * @param editConfigId 可编辑配置项主键
     * @return 结果
     */
    @Override
    public int deleteSysTableEditConfigByEditConfigId(Long editConfigId) {
        return sysTableEditConfigMapper.deleteSysTableEditConfigByEditConfigId(editConfigId);
    }

    @Override
    public LambdaQueryWrapper<SysTableEditConfig> buildQueryWrapper(SysTableEditConfig sysTableEditConfig) {
        LambdaQueryWrapper<SysTableEditConfig> lambdaQueryWrapper = new LambdaQueryWrapper<SysTableEditConfig>()
                .eq(!StringUtils.isNull(sysTableEditConfig.getTableId()), SysTableEditConfig::getTableId, sysTableEditConfig.getTableId())
                .eq(!StringUtils.isNull(sysTableEditConfig.getEditConfigId()), SysTableEditConfig::getEditConfigId, sysTableEditConfig.getEditConfigId())
                .like(!StringUtils.isNull(sysTableEditConfig.getRemark()), SysTableEditConfig::getRemark, sysTableEditConfig.getRemark())
                .like(!StringUtils.isNull(sysTableEditConfig.getCreateBy()), SysTableEditConfig::getCreateBy, sysTableEditConfig.getCreateBy())
                .like(!StringUtils.isNull(sysTableEditConfig.getUpdateBy()), SysTableEditConfig::getUpdateBy, sysTableEditConfig.getUpdateBy())
                .ge(sysTableEditConfig.getParams().get("beginCreateTime") != null, SysTableEditConfig::getCreateTime, sysTableEditConfig.getParams().get("beginCreateTime"))
                .le(sysTableEditConfig.getParams().get("endCreateTime") != null, SysTableEditConfig::getCreateTime, sysTableEditConfig.getParams().get("endCreateTime"))
                .ge(sysTableEditConfig.getParams().get("beginUpdateTime") != null, SysTableEditConfig::getUpdateTime, sysTableEditConfig.getParams().get("beginUpdateTime"))
                .le(sysTableEditConfig.getParams().get("endUpdateTime") != null, SysTableEditConfig::getUpdateTime, sysTableEditConfig.getParams().get("endUpdateTime"))
                .orderByDesc(SysTableEditConfig::getEditConfigId);
        return lambdaQueryWrapper;
    }
}
