package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysRoleTableEditRules;

/**
 * 角色校验规则配置项Mapper接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface SysRoleTableEditRulesMapper extends BaseMapperPlus<SysRoleTableEditRules> {


    /**
     * 查询角色校验规则配置项
     *
     * @param roleTableId 角色校验规则配置项
     * @return 角色校验规则配置项
     */
    public List<SysRoleTableEditRules> selectSysRoleTableEditRulesByRoleTableId(Long roleTableId);

    /**
     * 查询角色校验规则配置项
     *
     * @param editRulesId 角色校验规则配置项主键
     * @return 角色校验规则配置项
     */
    public SysRoleTableEditRules selectSysRoleTableEditRulesByEditRulesId(Long editRulesId);

    /**
     * 查询角色校验规则配置项
     *
     * @param roleEditRulesId 角色校验规则配置项主键
     * @return 角色校验规则配置项
     */
    public SysRoleTableEditRules selectSysRoleTableEditRulesByRoleEditRulesId(Long roleEditRulesId);

    /**
     * 查询角色校验规则配置项列表
     *
     * @param sysRoleTableEditRules 角色校验规则配置项
     * @return 角色校验规则配置项集合
     */
    public List<SysRoleTableEditRules> selectSysRoleTableEditRulesList(SysRoleTableEditRules sysRoleTableEditRules);

    /**
     * 新增角色校验规则配置项
     *
     * @param sysRoleTableEditRules 角色校验规则配置项
     * @return 结果
     */
    public int insertSysRoleTableEditRules(SysRoleTableEditRules sysRoleTableEditRules);

    /**
     * 修改角色校验规则配置项
     *
     * @param sysRoleTableEditRules 角色校验规则配置项
     * @return 结果
     */
    public int updateSysRoleTableEditRules(SysRoleTableEditRules sysRoleTableEditRules);

    /**
     * 删除角色校验规则配置项
     *
     * @param roleTableId 角色表格id
     * @return 结果
     */
    public int deleteSysRoleTableEditRulesByRoleTableId(Long roleTableId);

    /**
     * 删除角色校验规则配置项
     *
     * @param editRulesId 角色校验规则配置项主键
     * @return 结果
     */
    public int deleteSysRoleTableEditRulesByRoleEditRulesId(Long editRulesId);

    /**
     * 批量删除角色校验规则配置项
     *
     * @param editRulesIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysRoleTableEditRulesByRoleEditRulesIds(Long[] editRulesIds);
}
