package com.ruoyi.tableConfig.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysTableColumn;

/**
 * 功能表格列配置Service接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface ISysTableColumnService extends IServicePlus<SysTableColumn> {
    /**
     * 查询功能表格列配置
     *
     * @param tableId 功能表格id
     * @return 功能表格列配置
     */
    public List<SysTableColumn> selectSysTableColumnByTableId(Long tableId);

    /**
     * 查询功能表格列配置
     *
     * @param columnId 功能表格列配置主键
     * @return 功能表格列配置
     */
    public SysTableColumn selectSysTableColumnByColumnId(Long columnId);

    /**
     * 查询功能表格列配置列表
     *
     * @param sysTableColumn 功能表格列配置
     * @return 功能表格列配置集合
     */
    public List<SysTableColumn> selectSysTableColumnList(SysTableColumn sysTableColumn);

    /**
     * 查询功能表格列配置分页列表
     *
     * @param sysTableColumn 功能表格列配置
     * @param pageQuery      查询配置
     * @return
     */
    public TableDataInfo<SysTableColumn> selectSysTableColumnPage(SysTableColumn sysTableColumn, PageQuery pageQuery);

    /**
     * 新增功能表格列配置
     *
     * @param sysTableColumn 功能表格列配置
     * @return 结果
     */
    public int insertSysTableColumn(SysTableColumn sysTableColumn);

    /**
     * 修改功能表格列配置
     *
     * @param sysTableColumn 功能表格列配置
     * @return 结果
     */
    public int updateSysTableColumn(SysTableColumn sysTableColumn);

    /**
     * 批量删除功能表格列配置
     *
     * @param columnIds 需要删除的功能表格列配置主键集合
     * @return 结果
     */
    public int deleteSysTableColumnByColumnIds(Long[] columnIds);

    /**
     * 删除功能表格列配置信息
     *
     * @param columnId 功能表格列配置主键
     * @return 结果
     */
    public int deleteSysTableColumnByColumnId(Long columnId);

    public LambdaQueryWrapper<SysTableColumn> buildQueryWrapper(SysTableColumn sysTableColumn);
}
