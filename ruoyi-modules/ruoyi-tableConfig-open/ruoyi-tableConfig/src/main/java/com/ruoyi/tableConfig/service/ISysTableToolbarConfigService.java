package com.ruoyi.tableConfig.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysTableToolbarConfig;

/**
 * 工具栏配置Service接口
 *
 * @author zly
 * @date 2023-05-15
 */
public interface ISysTableToolbarConfigService extends IServicePlus<SysTableToolbarConfig> {
    /**
     * 查询工具栏配置
     *
     * @param tableId 功能表id
     * @return 工具栏配置
     */
    public SysTableToolbarConfig selectSysTableToolbarConfigByTableId(Long tableId);

    /**
     * 查询工具栏配置
     *
     * @param toolbarConfigId 工具栏配置主键
     * @return 工具栏配置
     */
    public SysTableToolbarConfig selectSysTableToolbarConfigByToolbarConfigId(Long toolbarConfigId);

    /**
     * 查询工具栏配置列表
     *
     * @param sysTableToolbarConfig 工具栏配置
     * @return 工具栏配置集合
     */
    public List<SysTableToolbarConfig> selectSysTableToolbarConfigList(SysTableToolbarConfig sysTableToolbarConfig);

    /**
     * 查询工具栏配置分页列表
     *
     * @param sysTableToolbarConfig 工具栏配置
     * @param pageQuery             查询配置
     * @return
     */
    public TableDataInfo<SysTableToolbarConfig> selectSysTableToolbarConfigPage(SysTableToolbarConfig sysTableToolbarConfig, PageQuery pageQuery);

    /**
     * 新增工具栏配置
     *
     * @param sysTableToolbarConfig 工具栏配置
     * @return 结果
     */
    public int insertSysTableToolbarConfig(SysTableToolbarConfig sysTableToolbarConfig);

    /**
     * 修改工具栏配置
     *
     * @param sysTableToolbarConfig 工具栏配置
     * @return 结果
     */
    public int updateSysTableToolbarConfig(SysTableToolbarConfig sysTableToolbarConfig);

    /**
     * 批量删除工具栏配置
     *
     * @param toolbarConfigIds 需要删除的工具栏配置主键集合
     * @return 结果
     */
    public int deleteSysTableToolbarConfigByToolbarConfigIds(Long[] toolbarConfigIds);

    /**
     * 删除工具栏配置信息
     *
     * @param toolbarConfigId 工具栏配置主键
     * @return 结果
     */
    public int deleteSysTableToolbarConfigByToolbarConfigId(Long toolbarConfigId);

    public LambdaQueryWrapper<SysTableToolbarConfig> buildQueryWrapper(SysTableToolbarConfig sysTableToolbarConfig);
}
