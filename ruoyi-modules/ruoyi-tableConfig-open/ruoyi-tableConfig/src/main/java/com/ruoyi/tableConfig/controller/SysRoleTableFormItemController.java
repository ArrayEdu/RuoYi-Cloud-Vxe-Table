package com.ruoyi.tableConfig.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableColumn;
import com.ruoyi.tableConfig.api.domain.SysRoleTableFormItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysRoleTableFormItemService;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 角色表单配置项列表Controller
 *
 * @author zly
 * @date 2023-06-06
 */
@RestController
@RequestMapping("/roleTableFormItem")
public class SysRoleTableFormItemController extends BaseController {
    @Autowired
    private ISysRoleTableFormItemService sysRoleTableFormItemService;

    /**
     * 查询角色表单配置项列表列表
     */
    @RequiresPermissions("system:item:list")
    @GetMapping("/list")
    public TableDataInfo list(SysRoleTableFormItem sysRoleTableFormItem) {
        return TableDataInfo.build(sysRoleTableFormItemService.selectSysRoleTableFormItemList(sysRoleTableFormItem));
    }

    /**
     * 查询角色表单配置项列表分页列表
     */
    @RequiresPermissions("system:item:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysRoleTableFormItemService.selectSysRoleTableFormItemPage(ajaxData.toJavaObject(SysRoleTableFormItem.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出角色表单配置项列表列表
     */
    @RequiresPermissions("system:item:export")
    @Log(title = "角色表单配置项列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysRoleTableFormItem sysRoleTableFormItem) {
        List<SysRoleTableFormItem> list = sysRoleTableFormItemService.selectSysRoleTableFormItemList(sysRoleTableFormItem);
        ExcelUtil.exportExcel(list, "角色表单配置项列表数据", SysRoleTableFormItem.class, response);
    }

    /**
     * vxe-table导出角色表单配置项列表列表
     */
    @RequiresPermissions("system:item:export")
    @Log(title = "角色表单配置项列表", businessType = BusinessType.EXPORT)
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        sysRoleTableFormItemService.export(response, params);
    }

    /**
     * 获取角色表单配置项列表详细信息
     */
    @RequiresPermissions("system:item:query")
    @GetMapping(value = "/{roleItemId}")
    public AjaxResult getInfo(@PathVariable("roleItemId") Long roleItemId) {
        return success(sysRoleTableFormItemService.selectSysRoleTableFormItemByRoleItemId(roleItemId));
    }

    /**
     * 获取角色表单配置项列表详细信息(批量)
     */
    @RequiresPermissions("system:item:query")
    @GetMapping(value = "/in/{roleItemIds}")
    public AjaxResult getInfos(@PathVariable(value = "roleItemIds", required = true) Long[] roleItemIds) {
        List<SysRoleTableFormItem> list = sysRoleTableFormItemService.listByIds(Arrays.asList(roleItemIds));
        return AjaxResult.success(list);
    }

    /**
     * 新增角色表单配置项列表
     */
    @RequiresPermissions("system:item:add")
    @Log(title = "角色表单配置项列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysRoleTableFormItem sysRoleTableFormItem) {
        return toAjax(sysRoleTableFormItemService.insertSysRoleTableFormItem(sysRoleTableFormItem));
    }

    /**
     * 新增角色表单配置项列表(批量)
     */
    @RequiresPermissions("system:item:add")
    @Log(title = "角色表单配置项列表", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@RequestBody JSONArray jsonArray) {
        if (!sysRoleTableFormItemService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableFormItem.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改角色表单配置项列表
     */
    @RequiresPermissions("system:item:edit")
    @Log(title = "角色表单配置项列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysRoleTableFormItem sysRoleTableFormItem) {
        return toAjax(sysRoleTableFormItemService.updateSysRoleTableFormItem(sysRoleTableFormItem));
    }

    /**
     * 修改角色表单配置项列表(批量)
     */
    @RequiresPermissions("system:item:edit")
    @Log(title = "角色表单配置项列表", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!sysRoleTableFormItemService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableFormItem.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除角色表单配置项列表
     */
    @RequiresPermissions("system:item:remove")
    @Log(title = "角色表单配置项列表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{roleItemIds}")
    public AjaxResult removes(@PathVariable(value = "roleItemIds", required = true) Long[] roleItemIds) {
        return toAjax(sysRoleTableFormItemService.removeByIds(Arrays.asList(roleItemIds)));
    }

    /**
     * 导入模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "角色表单配置项列表模板", SysRoleTableFormItem.class, response);
    }
}
