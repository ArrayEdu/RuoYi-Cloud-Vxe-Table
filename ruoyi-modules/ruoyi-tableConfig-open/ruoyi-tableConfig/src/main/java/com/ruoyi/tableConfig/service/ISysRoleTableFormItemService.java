package com.ruoyi.tableConfig.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTableFormItem;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色表单配置项列表Service接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface ISysRoleTableFormItemService extends IServicePlus<SysRoleTableFormItem> {
    /**
     * 查询角色表单配置项列表
     *
     * @param roleFormId 角色表单配置项列表
     * @return 角色表单配置项列表
     */
    public List<SysRoleTableFormItem> selectSysRoleTableFormItemByRoleFormId(Long roleFormId);

    /**
     * 查询角色表单配置项列表
     *
     * @param roleItemId 角色表单配置项列表主键
     * @return 角色表单配置项列表
     */
    public SysRoleTableFormItem selectSysRoleTableFormItemByRoleItemId(Long roleItemId);

    /**
     * 查询角色表单配置项列表
     *
     * @param itemId 角色表单配置项列表
     * @return 角色表单配置项列表
     */
    public SysRoleTableFormItem selectSysRoleTableFormItemByItemId(Long itemId);

    /**
     * 查询角色表单配置项列表列表
     *
     * @param sysRoleTableFormItem 角色表单配置项列表
     * @return 角色表单配置项列表集合
     */
    public List<SysRoleTableFormItem> selectSysRoleTableFormItemList(SysRoleTableFormItem sysRoleTableFormItem);

    /**
     * 查询角色表单配置项列表分页列表
     *
     * @param sysRoleTableFormItem 角色表单配置项列表
     * @param pageQuery            查询配置
     * @return
     */
    public TableDataInfo<SysRoleTableFormItem> selectSysRoleTableFormItemPage(SysRoleTableFormItem sysRoleTableFormItem, PageQuery pageQuery);

    /**
     * 新增角色表单配置项列表
     *
     * @param sysRoleTableFormItem 角色表单配置项列表
     * @return 结果
     */
    public int insertSysRoleTableFormItem(SysRoleTableFormItem sysRoleTableFormItem);

    /**
     * 修改角色表单配置项列表
     *
     * @param sysRoleTableFormItem 角色表单配置项列表
     * @return 结果
     */
    public int updateSysRoleTableFormItem(SysRoleTableFormItem sysRoleTableFormItem);

    /**
     * 批量删除角色表单配置项列表
     *
     * @param roleItemIds 需要删除的角色表单配置项列表主键集合
     * @return 结果
     */
    public int deleteSysRoleTableFormItemByRoleItemIds(Long[] roleItemIds);

    /**
     * 删除角色表单配置项列表信息
     *
     * @param roleItemId 角色表单配置项列表主键
     * @return 结果
     */
    public int deleteSysRoleTableFormItemByRoleItemId(Long roleItemId);

    /**
     * 角色表单配置项列表查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysRoleTableFormItem> buildQueryWrapper(SysRoleTableFormItem sysRoleTableFormItem);

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
