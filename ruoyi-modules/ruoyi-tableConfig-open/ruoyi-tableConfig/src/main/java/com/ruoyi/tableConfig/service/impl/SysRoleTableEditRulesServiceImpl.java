package com.ruoyi.tableConfig.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableEditRules;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysRoleTableEditRulesMapper;
import com.ruoyi.tableConfig.service.ISysRoleTableEditRulesService;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色校验规则配置项Service业务层处理
 *
 * @author zly
 * @date 2023-06-06
 */
@Service
public class SysRoleTableEditRulesServiceImpl extends ServiceImpl<SysRoleTableEditRulesMapper, SysRoleTableEditRules> implements ISysRoleTableEditRulesService {
    @Autowired
    private SysRoleTableEditRulesMapper sysRoleTableEditRulesMapper;


    /**
     * 查询角色校验规则配置项
     *
     * @param roleEditRulesId 角色校验规则配置项主键
     * @return 角色校验规则配置项
     */
    @Override
    public SysRoleTableEditRules selectSysRoleTableEditRulesByRoleEditRulesId(Long roleEditRulesId) {
        return sysRoleTableEditRulesMapper.selectSysRoleTableEditRulesByRoleEditRulesId(roleEditRulesId);
    }

    /**
     * 查询角色校验规则配置项
     *
     * @param roleTableId 角色校验规则配置项
     * @return 角色校验规则配置项
     */
    @Override
    public List<SysRoleTableEditRules> selectSysRoleTableEditRulesByRoleTableId(Long roleTableId) {
        return sysRoleTableEditRulesMapper.selectSysRoleTableEditRulesByRoleTableId(roleTableId);
    }

    /**
     * 查询角色校验规则配置项
     *
     * @param editRulesId 角色校验规则配置项主键
     * @return 角色校验规则配置项
     */
    @Override
    public SysRoleTableEditRules selectSysRoleTableEditRulesByEditRulesId(Long editRulesId) {
        return sysRoleTableEditRulesMapper.selectSysRoleTableEditRulesByEditRulesId(editRulesId);
    }

    /**
     * 查询角色校验规则配置项列表
     *
     * @param sysRoleTableEditRules 角色校验规则配置项
     * @return 角色校验规则配置项
     */
    @Override
    public List<SysRoleTableEditRules> selectSysRoleTableEditRulesList(SysRoleTableEditRules sysRoleTableEditRules) {
        return sysRoleTableEditRulesMapper.selectSysRoleTableEditRulesList(sysRoleTableEditRules);
    }

    /**
     * 查询角色校验规则配置项分页列表
     *
     * @param sysRoleTableEditRules 角色校验规则配置项
     * @param pageQuery             查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysRoleTableEditRules> selectSysRoleTableEditRulesPage(SysRoleTableEditRules sysRoleTableEditRules, PageQuery pageQuery) {
        LambdaQueryWrapper<SysRoleTableEditRules> lqw = buildQueryWrapper(sysRoleTableEditRules);
        Page<SysRoleTableEditRules> page = sysRoleTableEditRulesMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增角色校验规则配置项
     *
     * @param sysRoleTableEditRules 角色校验规则配置项
     * @return 结果
     */
    @Override
    public int insertSysRoleTableEditRules(SysRoleTableEditRules sysRoleTableEditRules) {
        sysRoleTableEditRules.setCreateTime(DateUtils.getNowDate());
        return sysRoleTableEditRulesMapper.insertSysRoleTableEditRules(sysRoleTableEditRules);
    }

    /**
     * 修改角色校验规则配置项
     *
     * @param sysRoleTableEditRules 角色校验规则配置项
     * @return 结果
     */
    @Override
    public int updateSysRoleTableEditRules(SysRoleTableEditRules sysRoleTableEditRules) {
        sysRoleTableEditRules.setUpdateTime(DateUtils.getNowDate());
        return sysRoleTableEditRulesMapper.updateSysRoleTableEditRules(sysRoleTableEditRules);
    }

    /**
     * 批量删除角色校验规则配置项
     *
     * @param editRulesIds 需要删除的角色校验规则配置项主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableEditRulesByRoleEditRulesIds(Long[] editRulesIds) {
        return sysRoleTableEditRulesMapper.deleteSysRoleTableEditRulesByRoleEditRulesIds(editRulesIds);
    }

    /**
     * 删除角色校验规则配置项信息
     *
     * @param editRulesId 角色校验规则配置项主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableEditRulesByRoleEditRulesId(Long editRulesId) {
        return sysRoleTableEditRulesMapper.deleteSysRoleTableEditRulesByRoleEditRulesId(editRulesId);
    }

    /**
     * 角色校验规则配置项查询条件
     *
     * @param sysRoleTableEditRules
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysRoleTableEditRules> buildQueryWrapper(SysRoleTableEditRules sysRoleTableEditRules) {
        LambdaQueryWrapper<SysRoleTableEditRules> lambdaQueryWrapper = new LambdaQueryWrapper<SysRoleTableEditRules>()
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getRoleEditRulesId()), SysRoleTableEditRules::getRoleEditRulesId, sysRoleTableEditRules.getRoleEditRulesId())
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getEditRulesId()), SysRoleTableEditRules::getEditRulesId, sysRoleTableEditRules.getEditRulesId())
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getRoleTableId()), SysRoleTableEditRules::getRoleTableId, sysRoleTableEditRules.getRoleTableId())
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getRequired()), SysRoleTableEditRules::getRequired, sysRoleTableEditRules.getRequired())
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getMin()), SysRoleTableEditRules::getMin, sysRoleTableEditRules.getMin())
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getMax()), SysRoleTableEditRules::getMax, sysRoleTableEditRules.getMax())
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getType()), SysRoleTableEditRules::getType, sysRoleTableEditRules.getType())
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getPattern()), SysRoleTableEditRules::getPattern, sysRoleTableEditRules.getPattern())
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getValidator()), SysRoleTableEditRules::getValidator, sysRoleTableEditRules.getValidator())
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getMessage()), SysRoleTableEditRules::getMessage, sysRoleTableEditRules.getMessage())
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getCreateBy()), SysRoleTableEditRules::getCreateBy, sysRoleTableEditRules.getCreateBy())
                .ge(sysRoleTableEditRules.getParams().get("beginCreateTime") != null, SysRoleTableEditRules::getCreateTime, sysRoleTableEditRules.getParams().get("beginCreateTime"))
                .le(sysRoleTableEditRules.getParams().get("endCreateTime") != null, SysRoleTableEditRules::getCreateTime, sysRoleTableEditRules.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getUpdateBy()), SysRoleTableEditRules::getUpdateBy, sysRoleTableEditRules.getUpdateBy())
                .ge(sysRoleTableEditRules.getParams().get("beginUpdateTime") != null, SysRoleTableEditRules::getUpdateTime, sysRoleTableEditRules.getParams().get("beginUpdateTime"))
                .le(sysRoleTableEditRules.getParams().get("endUpdateTime") != null, SysRoleTableEditRules::getUpdateTime, sysRoleTableEditRules.getParams().get("endUpdateTime"))
                .eq(!StringUtils.isNull(sysRoleTableEditRules.getRemark()), SysRoleTableEditRules::getRemark, sysRoleTableEditRules.getRemark())
                .orderByDesc(SysRoleTableEditRules::getRemark);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysRoleTableEditRules> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysRoleTableEditRules> iPage = sysRoleTableEditRulesMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper<>());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysRoleTableEditRulesMapper.selectBatchIds(exceel.getIds());
        } else {
            SysRoleTableEditRules sysRoleTableEditRules = JSON.parseObject(String.valueOf(exceel.getQuery()), SysRoleTableEditRules.class);
            list = sysRoleTableEditRulesMapper.selectSysRoleTableEditRulesList(sysRoleTableEditRules);
        }
        String fileName = exceel.getFilename().equals(null) ? "角色校验规则配置项数据" : exceel.getFilename();
        Set<String> column = Arrays.stream(exceel.getFields().stream().map(m -> m.getField()).collect(Collectors.toList()).stream().toArray(String[]::new)).collect(Collectors.toSet());
        ExcelUtil.exportExcel(list, fileName, SysRoleTableEditRules.class, response, column);
    }
}
