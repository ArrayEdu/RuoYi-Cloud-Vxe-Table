package com.ruoyi.tableConfig.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTableToolbarConfig;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色工具栏配置Service接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface ISysRoleTableToolbarConfigService extends IServicePlus<SysRoleTableToolbarConfig> {
    /**
     * 查询角色工具栏配置
     *
     * @param roleTableId
     * @return 角色工具栏配置
     */
    public SysRoleTableToolbarConfig selectSysRoleTableToolbarConfigByRoleTableId(Long roleTableId);

    /**
     * 查询角色工具栏配置
     *
     * @param toolbarConfigId 角色工具栏配置主键
     * @return 角色工具栏配置
     */
    public SysRoleTableToolbarConfig selectSysRoleTableToolbarConfigByToolbarConfigId(Long toolbarConfigId);

    /**
     * 查询角色工具栏配置
     *
     * @param roleToolbarConfigId 角色工具栏配置主键
     * @return 角色工具栏配置
     */
    public SysRoleTableToolbarConfig selectSysRoleTableToolbarConfigByRoleToolbarConfigId(Long roleToolbarConfigId);

    /**
     * 查询角色工具栏配置列表
     *
     * @param sysRoleTableToolbarConfig 角色工具栏配置
     * @return 角色工具栏配置集合
     */
    public List<SysRoleTableToolbarConfig> selectSysRoleTableToolbarConfigList(SysRoleTableToolbarConfig sysRoleTableToolbarConfig);

    /**
     * 查询角色工具栏配置分页列表
     *
     * @param sysRoleTableToolbarConfig 角色工具栏配置
     * @param pageQuery                 查询配置
     * @return
     */
    public TableDataInfo<SysRoleTableToolbarConfig> selectSysRoleTableToolbarConfigPage(SysRoleTableToolbarConfig sysRoleTableToolbarConfig, PageQuery pageQuery);

    /**
     * 新增角色工具栏配置
     *
     * @param sysRoleTableToolbarConfig 角色工具栏配置
     * @return 结果
     */
    public int insertSysRoleTableToolbarConfig(SysRoleTableToolbarConfig sysRoleTableToolbarConfig);

    /**
     * 修改角色工具栏配置
     *
     * @param sysRoleTableToolbarConfig 角色工具栏配置
     * @return 结果
     */
    public int updateSysRoleTableToolbarConfig(SysRoleTableToolbarConfig sysRoleTableToolbarConfig);

    /**
     * 批量删除角色工具栏配置
     *
     * @param roleToolbarConfigIds 需要删除的角色工具栏配置主键集合
     * @return 结果
     */
    public int deleteSysRoleTableToolbarConfigByRoleToolbarConfigIds(Long[] roleToolbarConfigIds);

    /**
     * 删除角色工具栏配置信息
     *
     * @param roleToolbarConfigId 角色工具栏配置主键
     * @return 结果
     */
    public int deleteSysRoleTableToolbarConfigByRoleToolbarConfigId(Long roleToolbarConfigId);

    /**
     * 角色工具栏配置查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysRoleTableToolbarConfig> buildQueryWrapper(SysRoleTableToolbarConfig sysRoleTableToolbarConfig);

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
