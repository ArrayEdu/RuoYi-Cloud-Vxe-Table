package com.ruoyi.tableConfig.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableColumn;
import com.ruoyi.tableConfig.api.domain.SysRoleTablePager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysRoleTablePagerService;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 角色功能表分页配置Controller
 *
 * @author zly
 * @date 2023-06-06
 */
@RestController
@RequestMapping("/roleTablePager")
public class SysRoleTablePagerController extends BaseController {
    @Autowired
    private ISysRoleTablePagerService sysRoleTablePagerService;

    /**
     * 查询角色功能表分页配置列表
     */
    @RequiresPermissions("system:pager:list")
    @GetMapping("/list")
    public TableDataInfo list(SysRoleTablePager sysRoleTablePager) {
        return TableDataInfo.build(sysRoleTablePagerService.selectSysRoleTablePagerList(sysRoleTablePager));
    }

    /**
     * 查询角色功能表分页配置分页列表
     */
    @RequiresPermissions("system:pager:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysRoleTablePagerService.selectSysRoleTablePagerPage(ajaxData.toJavaObject(SysRoleTablePager.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出角色功能表分页配置列表
     */
    @RequiresPermissions("system:pager:export")
    @Log(title = "角色功能表分页配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysRoleTablePager sysRoleTablePager) {
        List<SysRoleTablePager> list = sysRoleTablePagerService.selectSysRoleTablePagerList(sysRoleTablePager);
        ExcelUtil.exportExcel(list, "角色功能表分页配置数据", SysRoleTablePager.class, response);
    }

    /**
     * vxe-table导出角色功能表分页配置列表
     */
    @RequiresPermissions("system:pager:export")
    @Log(title = "角色功能表分页配置", businessType = BusinessType.EXPORT)
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        sysRoleTablePagerService.export(response, params);
    }

    /**
     * 获取角色功能表分页配置详细信息
     */
    @RequiresPermissions("system:pager:query")
    @GetMapping(value = "/{rolePagerId}")
    public AjaxResult getInfo(@PathVariable("rolePagerId") Long rolePagerId) {
        return success(sysRoleTablePagerService.selectSysRoleTablePagerByRolePagerId(rolePagerId));
    }

    /**
     * 获取角色功能表分页配置详细信息(批量)
     */
    @RequiresPermissions("system:pager:query")
    @GetMapping(value = "/in/{pagerIds}")
    public AjaxResult getInfos(@PathVariable(value = "pagerIds", required = true) Long[] pagerIds) {
        List<SysRoleTablePager> list = sysRoleTablePagerService.listByIds(Arrays.asList(pagerIds));
        return AjaxResult.success(list);
    }

    /**
     * 新增角色功能表分页配置
     */
    @RequiresPermissions("system:pager:add")
    @Log(title = "角色功能表分页配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysRoleTablePager sysRoleTablePager) {
        return toAjax(sysRoleTablePagerService.insertSysRoleTablePager(sysRoleTablePager));
    }

    /**
     * 新增角色功能表分页配置(批量)
     */
    @RequiresPermissions("system:pager:add")
    @Log(title = "角色功能表分页配置", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@RequestBody JSONArray jsonArray) {
        if (!sysRoleTablePagerService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTablePager.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改角色功能表分页配置
     */
    @RequiresPermissions("system:pager:edit")
    @Log(title = "角色功能表分页配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysRoleTablePager sysRoleTablePager) {
        return toAjax(sysRoleTablePagerService.updateSysRoleTablePager(sysRoleTablePager));
    }

    /**
     * 修改角色功能表分页配置(批量)
     */
    @RequiresPermissions("system:pager:edit")
    @Log(title = "角色功能表分页配置", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!sysRoleTablePagerService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTablePager.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除角色功能表分页配置
     */
    @RequiresPermissions("system:pager:remove")
    @Log(title = "角色功能表分页配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{pagerIds}")
    public AjaxResult removes(@PathVariable(value = "pagerIds", required = true) Long[] pagerIds) {
        return toAjax(sysRoleTablePagerService.removeByIds(Arrays.asList(pagerIds)));
    }

    /**
     * 导入模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "角色功能表分页配置模板", SysRoleTablePager.class, response);
    }
}
