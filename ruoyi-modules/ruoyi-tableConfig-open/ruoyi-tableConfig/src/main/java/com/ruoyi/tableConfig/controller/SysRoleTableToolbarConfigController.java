package com.ruoyi.tableConfig.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableColumn;
import com.ruoyi.tableConfig.api.domain.SysRoleTableToolbarConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysRoleTableToolbarConfigService;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 角色工具栏配置Controller
 *
 * @author zly
 * @date 2023-06-06
 */
@RestController
@RequestMapping("/roleTableToolbarConfig")
public class SysRoleTableToolbarConfigController extends BaseController {
    @Autowired
    private ISysRoleTableToolbarConfigService sysRoleTableToolbarConfigService;

    /**
     * 查询角色工具栏配置列表
     */
    @RequiresPermissions("system:config:list")
    @GetMapping("/list")
    public TableDataInfo list(SysRoleTableToolbarConfig sysRoleTableToolbarConfig) {
        return TableDataInfo.build(sysRoleTableToolbarConfigService.selectSysRoleTableToolbarConfigList(sysRoleTableToolbarConfig));
    }

    /**
     * 查询角色工具栏配置分页列表
     */
    @RequiresPermissions("system:config:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysRoleTableToolbarConfigService.selectSysRoleTableToolbarConfigPage(ajaxData.toJavaObject(SysRoleTableToolbarConfig.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出角色工具栏配置列表
     */
    @RequiresPermissions("system:config:export")
    @Log(title = "角色工具栏配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysRoleTableToolbarConfig sysRoleTableToolbarConfig) {
        List<SysRoleTableToolbarConfig> list = sysRoleTableToolbarConfigService.selectSysRoleTableToolbarConfigList(sysRoleTableToolbarConfig);
        ExcelUtil.exportExcel(list, "角色工具栏配置数据", SysRoleTableToolbarConfig.class, response);
    }

    /**
     * vxe-table导出角色工具栏配置列表
     */
    @RequiresPermissions("system:config:export")
    @Log(title = "角色工具栏配置", businessType = BusinessType.EXPORT)
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        sysRoleTableToolbarConfigService.export(response, params);
    }

    /**
     * 获取角色工具栏配置详细信息
     */
    @RequiresPermissions("system:config:query")
    @GetMapping(value = "/{roleToolbarConfigId}")
    public AjaxResult getInfo(@PathVariable("roleToolbarConfigId") Long roleToolbarConfigId) {
        return success(sysRoleTableToolbarConfigService.selectSysRoleTableToolbarConfigByRoleToolbarConfigId(roleToolbarConfigId));
    }

    /**
     * 获取角色工具栏配置详细信息(批量)
     */
    @RequiresPermissions("system:config:query")
    @GetMapping(value = "/in/{toolbarConfigIds}")
    public AjaxResult getInfos(@PathVariable(value = "toolbarConfigIds", required = true) Long[] toolbarConfigIds) {
        List<SysRoleTableToolbarConfig> list = sysRoleTableToolbarConfigService.listByIds(Arrays.asList(toolbarConfigIds));
        return AjaxResult.success(list);
    }

    /**
     * 新增角色工具栏配置
     */
    @RequiresPermissions("system:config:add")
    @Log(title = "角色工具栏配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysRoleTableToolbarConfig sysRoleTableToolbarConfig) {
        return toAjax(sysRoleTableToolbarConfigService.insertSysRoleTableToolbarConfig(sysRoleTableToolbarConfig));
    }

    /**
     * 新增角色工具栏配置(批量)
     */
    @RequiresPermissions("system:config:add")
    @Log(title = "角色工具栏配置", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@RequestBody JSONArray jsonArray) {
        if (!sysRoleTableToolbarConfigService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableToolbarConfig.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改角色工具栏配置
     */
    @RequiresPermissions("system:config:edit")
    @Log(title = "角色工具栏配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysRoleTableToolbarConfig sysRoleTableToolbarConfig) {
        return toAjax(sysRoleTableToolbarConfigService.updateSysRoleTableToolbarConfig(sysRoleTableToolbarConfig));
    }

    /**
     * 修改角色工具栏配置(批量)
     */
    @RequiresPermissions("system:config:edit")
    @Log(title = "角色工具栏配置", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!sysRoleTableToolbarConfigService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableToolbarConfig.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除角色工具栏配置
     */
    @RequiresPermissions("system:config:remove")
    @Log(title = "角色工具栏配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{toolbarConfigIds}")
    public AjaxResult removes(@PathVariable(value = "toolbarConfigIds", required = true) Long[] toolbarConfigIds) {
        return toAjax(sysRoleTableToolbarConfigService.removeByIds(Arrays.asList(toolbarConfigIds)));
    }

    /**
     * 导入模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "角色工具栏配置模板", SysRoleTableToolbarConfig.class, response);
    }
}
