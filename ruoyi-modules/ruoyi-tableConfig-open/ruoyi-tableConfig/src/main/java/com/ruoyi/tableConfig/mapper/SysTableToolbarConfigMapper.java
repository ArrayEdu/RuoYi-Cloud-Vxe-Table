package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysTableToolbarConfig;

/**
 * 工具栏配置Mapper接口
 *
 * @author zly
 * @date 2023-05-15
 */
public interface SysTableToolbarConfigMapper extends BaseMapperPlus<SysTableToolbarConfig> {


    /**
     * 查询工具栏配置
     *
     * @param tableId 功能表id
     * @return 工具栏配置
     */
    public SysTableToolbarConfig selectSysTableToolbarConfigByTableId(Long tableId);

    /**
     * 查询工具栏配置
     *
     * @param toolbarConfigId 工具栏配置主键
     * @return 工具栏配置
     */
    public SysTableToolbarConfig selectSysTableToolbarConfigByToolbarConfigId(Long toolbarConfigId);

    /**
     * 查询工具栏配置列表
     *
     * @param sysTableToolbarConfig 工具栏配置
     * @return 工具栏配置集合
     */
    public List<SysTableToolbarConfig> selectSysTableToolbarConfigList(SysTableToolbarConfig sysTableToolbarConfig);

    /**
     * 新增工具栏配置
     *
     * @param sysTableToolbarConfig 工具栏配置
     * @return 结果
     */
    public int insertSysTableToolbarConfig(SysTableToolbarConfig sysTableToolbarConfig);

    /**
     * 修改工具栏配置
     *
     * @param sysTableToolbarConfig 工具栏配置
     * @return 结果
     */
    public int updateSysTableToolbarConfig(SysTableToolbarConfig sysTableToolbarConfig);

    /**
     * 删除工具栏配置
     *
     * @param toolbarConfigId 工具栏配置主键
     * @return 结果
     */
    public int deleteSysTableToolbarConfigByToolbarConfigId(Long toolbarConfigId);

    /**
     * 批量删除工具栏配置
     *
     * @param toolbarConfigIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysTableToolbarConfigByToolbarConfigIds(Long[] toolbarConfigIds);

    /**
     * 删除工具栏配置
     *
     * @param tableId 需要删除的表格id
     * @return 结果
     */
    public int deleteSysTableToolbarConfigByTableId(Long tableId);

    /**
     * 批量删除工具栏配置
     *
     * @param tableIds 需要删除的表格id
     * @return 结果
     */
    public int deleteSysTableToolbarConfigByTableIds(Long[] tableIds);
}
