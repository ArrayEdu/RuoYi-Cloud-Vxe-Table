package com.ruoyi.tableConfig.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysTableProxy;

/**
 * 功能表数据代理Service接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface ISysTableProxyService extends IServicePlus<SysTableProxy> {
    /**
     * 查询功能表数据代理
     *
     * @param tableId 功能表id
     * @return 功能表数据代理
     */
    public SysTableProxy selectSysTableProxyByTableId(Long tableId);

    /**
     * 查询功能表数据代理
     *
     * @param proxyId 功能表数据代理主键
     * @return 功能表数据代理
     */
    public SysTableProxy selectSysTableProxyByProxyId(Long proxyId);

    /**
     * 查询功能表数据代理列表
     *
     * @param sysTableProxy 功能表数据代理
     * @return 功能表数据代理集合
     */
    public List<SysTableProxy> selectSysTableProxyList(SysTableProxy sysTableProxy);

    /**
     * 查询功能表数据代理分页列表
     *
     * @param sysTableProxy 功能表数据代理
     * @param pageQuery     查询配置
     * @return
     */
    public TableDataInfo<SysTableProxy> selectSysTableProxyPage(SysTableProxy sysTableProxy, PageQuery pageQuery);

    /**
     * 新增功能表数据代理
     *
     * @param sysTableProxy 功能表数据代理
     * @return 结果
     */
    public int insertSysTableProxy(SysTableProxy sysTableProxy);

    /**
     * 修改功能表数据代理
     *
     * @param sysTableProxy 功能表数据代理
     * @return 结果
     */
    public int updateSysTableProxy(SysTableProxy sysTableProxy);

    /**
     * 批量删除功能表数据代理
     *
     * @param proxyIds 需要删除的功能表数据代理主键集合
     * @return 结果
     */
    public int deleteSysTableProxyByProxyIds(Long[] proxyIds);

    /**
     * 删除功能表数据代理信息
     *
     * @param proxyId 功能表数据代理主键
     * @return 结果
     */
    public int deleteSysTableProxyByProxyId(Long proxyId);

    public LambdaQueryWrapper<SysTableProxy> buildQueryWrapper(SysTableProxy sysTableProxy);
}
