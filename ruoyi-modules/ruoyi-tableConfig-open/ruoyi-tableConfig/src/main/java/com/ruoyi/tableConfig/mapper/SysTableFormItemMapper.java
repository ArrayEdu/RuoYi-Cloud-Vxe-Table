package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysTableFormItem;

/**
 * 表单配置项列Mapper接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface SysTableFormItemMapper extends BaseMapperPlus<SysTableFormItem> {

    /**
     * 查询表单配置项列
     *
     * @param formId 表单配置id
     * @return 表单配置项列
     */
    public List<SysTableFormItem> selectSysTableFormItemByFormId(Long formId);

    /**
     * 查询表单配置项列
     *
     * @param itemId 表单配置项列主键
     * @return 表单配置项列
     */
    public SysTableFormItem selectSysTableFormItemByItemId(Long itemId);

    /**
     * 查询表单配置项列列表
     *
     * @param sysTableFormItem 表单配置项列
     * @return 表单配置项列集合
     */
    public List<SysTableFormItem> selectSysTableFormItemList(SysTableFormItem sysTableFormItem);

    /**
     * 新增表单配置项列
     *
     * @param sysTableFormItem 表单配置项列
     * @return 结果
     */
    public int insertSysTableFormItem(SysTableFormItem sysTableFormItem);

    /**
     * 修改表单配置项列
     *
     * @param sysTableFormItem 表单配置项列
     * @return 结果
     */
    public int updateSysTableFormItem(SysTableFormItem sysTableFormItem);

    /**
     * 删除表单配置项列
     *
     * @param formId 表单id
     * @return 结果
     */
    public int deleteSysTableFormItemByFormId(Long formId);

    /**
     * 删除表单配置项列
     *
     * @param itemId 表单配置项列主键
     * @return 结果
     */
    public int deleteSysTableFormItemByItemId(Long itemId);

    /**
     * 批量删除表单配置项列
     *
     * @param itemIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysTableFormItemByItemIds(Long[] itemIds);
}
