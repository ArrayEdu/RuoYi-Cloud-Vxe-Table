package com.ruoyi.tableConfig.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysUserTableColumn;

import javax.servlet.http.HttpServletResponse;

/**
 * 用户表格列配置Service接口
 *
 * @author zly
 * @date 2023-06-13
 */
public interface ISysUserTableColumnService extends IServicePlus<SysUserTableColumn> {

    /**
     * 查询用户表格列配置
     *
     * @param userId 用户id
     * @return 用户表格列配置
     */
    public List<SysUserTableColumn> selectSysUserTableColumnByUserId(Long userId);

    /**
     * 查询用户表格列配置
     *
     * @param userColumnId 用户表格列配置主键
     * @return 用户表格列配置
     */
    public SysUserTableColumn selectSysUserTableColumnByUserColumnId(Long userColumnId);

    /**
     * 查询用户表格列配置
     *
     * @param roleTableId 角色表格id
     * @return 用户表格列配置
     */
    public List<SysUserTableColumn> selectSysUserTableColumnByRoleTableId(Long roleTableId);

    /**
     * 查询用户表格列配置列表
     *
     * @param sysUserTableColumn 用户表格列配置
     * @return 用户表格列配置集合
     */
    public List<SysUserTableColumn> selectSysUserTableColumnList(SysUserTableColumn sysUserTableColumn);

    /**
     * 查询用户表格列配置分页列表
     *
     * @param sysUserTableColumn 用户表格列配置
     * @param pageQuery          查询配置
     * @return
     */
    public TableDataInfo<SysUserTableColumn> selectSysUserTableColumnPage(SysUserTableColumn sysUserTableColumn, PageQuery pageQuery);

    /**
     * 新增用户表格列配置
     *
     * @param sysUserTableColumn 用户表格列配置
     * @return 结果
     */
    public int insertSysUserTableColumn(SysUserTableColumn sysUserTableColumn);

    /**
     * 修改用户表格列配置
     *
     * @param sysUserTableColumn 用户表格列配置
     * @return 结果
     */
    public int updateSysUserTableColumn(SysUserTableColumn sysUserTableColumn);

    /**
     * 批量删除用户表格列配置
     *
     * @param userColumnIds 需要删除的用户表格列配置主键集合
     * @return 结果
     */
    public int deleteSysUserTableColumnByUserColumnIds(Long[] userColumnIds);

    /**
     * 删除用户表格列配置信息
     *
     * @param userColumnId 用户表格列配置主键
     * @return 结果
     */
    public int deleteSysUserTableColumnByUserColumnId(Long userColumnId);

    /**
     * 删除用户表格列配置信息
     *
     * @param userId 用户id
     * @return 结果
     */
    public int deleteSysUserTableColumnByUserId(Long userId);

    /**
     * 用户表格列配置查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysUserTableColumn> buildQueryWrapper(SysUserTableColumn sysUserTableColumn);
    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
