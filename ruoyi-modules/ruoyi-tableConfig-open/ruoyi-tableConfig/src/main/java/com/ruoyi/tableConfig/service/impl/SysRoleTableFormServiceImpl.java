package com.ruoyi.tableConfig.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysRoleTableFormMapper;
import com.ruoyi.tableConfig.service.ISysRoleTableFormService;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色功能表查询表Service业务层处理
 *
 * @author zly
 * @date 2023-06-06
 */
@Service
public class SysRoleTableFormServiceImpl extends ServiceImpl<SysRoleTableFormMapper, SysRoleTableForm> implements ISysRoleTableFormService {
    @Autowired
    private SysRoleTableFormMapper sysRoleTableFormMapper;

    /**
     * 查询角色功能表查询表
     *
     * @param roleFormId 角色功能表查询表主键
     * @return 角色功能表查询表
     */
    @Override
    public SysRoleTableForm selectSysRoleTableFormByRoleFormId(Long roleFormId) {
        return sysRoleTableFormMapper.selectSysRoleTableFormByRoleTableId(roleFormId);
    }

    /**
     * 查询角色功能表查询表
     *
     * @param roleTableId
     * @return 角色功能表查询表
     */
    @Override
    public SysRoleTableForm selectSysRoleTableFormByRoleTableId(Long roleTableId) {
        return sysRoleTableFormMapper.selectSysRoleTableFormByRoleTableId(roleTableId);
    }

    /**
     * 查询角色功能表查询表
     *
     * @param formId 角色功能表查询表主键
     * @return 角色功能表查询表
     */
    @Override
    public SysRoleTableForm selectSysRoleTableFormByFormId(Long formId) {
        return sysRoleTableFormMapper.selectSysRoleTableFormByFormId(formId);
    }

    /**
     * 查询角色功能表查询表列表
     *
     * @param sysRoleTableForm 角色功能表查询表
     * @return 角色功能表查询表
     */
    @Override
    public List<SysRoleTableForm> selectSysRoleTableFormList(SysRoleTableForm sysRoleTableForm) {
        return sysRoleTableFormMapper.selectSysRoleTableFormList(sysRoleTableForm);
    }

    /**
     * 查询角色功能表查询表分页列表
     *
     * @param sysRoleTableForm 角色功能表查询表
     * @param pageQuery        查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysRoleTableForm> selectSysRoleTableFormPage(SysRoleTableForm sysRoleTableForm, PageQuery pageQuery) {
        LambdaQueryWrapper<SysRoleTableForm> lqw = buildQueryWrapper(sysRoleTableForm);
        Page<SysRoleTableForm> page = sysRoleTableFormMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增角色功能表查询表
     *
     * @param sysRoleTableForm 角色功能表查询表
     * @return 结果
     */
    @Override
    public int insertSysRoleTableForm(SysRoleTableForm sysRoleTableForm) {
        sysRoleTableForm.setCreateTime(DateUtils.getNowDate());
        return sysRoleTableFormMapper.insertSysRoleTableForm(sysRoleTableForm);
    }

    /**
     * 修改角色功能表查询表
     *
     * @param sysRoleTableForm 角色功能表查询表
     * @return 结果
     */
    @Override
    public int updateSysRoleTableForm(SysRoleTableForm sysRoleTableForm) {
        sysRoleTableForm.setUpdateTime(DateUtils.getNowDate());
        return sysRoleTableFormMapper.updateSysRoleTableForm(sysRoleTableForm);
    }

    /**
     * 批量删除角色功能表查询表
     *
     * @param formIds 需要删除的角色功能表查询表主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableFormByRoleFormIds(Long[] formIds) {
        return sysRoleTableFormMapper.deleteSysRoleTableFormByRoleFormIds(formIds);
    }

    /**
     * 删除角色功能表查询表信息
     *
     * @param formId 角色功能表查询表主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableFormByRoleFormId(Long formId) {
        return sysRoleTableFormMapper.deleteSysRoleTableFormByRoleFormId(formId);
    }

    /**
     * 角色功能表查询表查询条件
     *
     * @param sysRoleTableForm
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysRoleTableForm> buildQueryWrapper(SysRoleTableForm sysRoleTableForm) {
        LambdaQueryWrapper<SysRoleTableForm> lambdaQueryWrapper = new LambdaQueryWrapper<SysRoleTableForm>()
                .eq(!StringUtils.isNull(sysRoleTableForm.getRoleFormId()), SysRoleTableForm::getRoleFormId, sysRoleTableForm.getRoleFormId())
                .eq(!StringUtils.isNull(sysRoleTableForm.getFormId()), SysRoleTableForm::getFormId, sysRoleTableForm.getFormId())
                .eq(!StringUtils.isNull(sysRoleTableForm.getRoleTableId()), SysRoleTableForm::getRoleTableId, sysRoleTableForm.getRoleTableId())
                .eq(!StringUtils.isNull(sysRoleTableForm.getSpan()), SysRoleTableForm::getSpan, sysRoleTableForm.getSpan())
                .eq(!StringUtils.isNull(sysRoleTableForm.getAlign()), SysRoleTableForm::getAlign, sysRoleTableForm.getAlign())
                .eq(!StringUtils.isNull(sysRoleTableForm.getSize()), SysRoleTableForm::getSize, sysRoleTableForm.getSize())
                .eq(!StringUtils.isNull(sysRoleTableForm.getTitleAlign()), SysRoleTableForm::getTitleAlign, sysRoleTableForm.getTitleAlign())
                .eq(!StringUtils.isNull(sysRoleTableForm.getTitleWidth()), SysRoleTableForm::getTitleWidth, sysRoleTableForm.getTitleWidth())
                .eq(!StringUtils.isNull(sysRoleTableForm.getTitleColon()), SysRoleTableForm::getTitleColon, sysRoleTableForm.getTitleColon())
                .eq(!StringUtils.isNull(sysRoleTableForm.getTitleAsterisk()), SysRoleTableForm::getTitleAsterisk, sysRoleTableForm.getTitleAsterisk())
                .eq(!StringUtils.isNull(sysRoleTableForm.getTitleOverflow()), SysRoleTableForm::getTitleOverflow, sysRoleTableForm.getTitleOverflow())
                .like(!StringUtils.isNull(sysRoleTableForm.getClassName()), SysRoleTableForm::getClassName, sysRoleTableForm.getClassName())
                .eq(!StringUtils.isNull(sysRoleTableForm.getCollapseStatus()), SysRoleTableForm::getCollapseStatus, sysRoleTableForm.getCollapseStatus())
                .eq(!StringUtils.isNull(sysRoleTableForm.getCustomLayout()), SysRoleTableForm::getCustomLayout, sysRoleTableForm.getCustomLayout())
                .eq(!StringUtils.isNull(sysRoleTableForm.getPreventSubmit()), SysRoleTableForm::getPreventSubmit, sysRoleTableForm.getPreventSubmit())
                .eq(!StringUtils.isNull(sysRoleTableForm.getEnabled()), SysRoleTableForm::getEnabled, sysRoleTableForm.getEnabled())
                .eq(!StringUtils.isNull(sysRoleTableForm.getRules()), SysRoleTableForm::getRules, sysRoleTableForm.getRules())
                .eq(!StringUtils.isNull(sysRoleTableForm.getValidConfig()), SysRoleTableForm::getValidConfig, sysRoleTableForm.getValidConfig())
                .eq(!StringUtils.isNull(sysRoleTableForm.getCreateBy()), SysRoleTableForm::getCreateBy, sysRoleTableForm.getCreateBy())
                .ge(sysRoleTableForm.getParams().get("beginCreateTime") != null, SysRoleTableForm::getCreateTime, sysRoleTableForm.getParams().get("beginCreateTime"))
                .le(sysRoleTableForm.getParams().get("endCreateTime") != null, SysRoleTableForm::getCreateTime, sysRoleTableForm.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysRoleTableForm.getUpdateBy()), SysRoleTableForm::getUpdateBy, sysRoleTableForm.getUpdateBy())
                .ge(sysRoleTableForm.getParams().get("beginUpdateTime") != null, SysRoleTableForm::getUpdateTime, sysRoleTableForm.getParams().get("beginUpdateTime"))
                .le(sysRoleTableForm.getParams().get("endUpdateTime") != null, SysRoleTableForm::getUpdateTime, sysRoleTableForm.getParams().get("endUpdateTime"))
                .eq(!StringUtils.isNull(sysRoleTableForm.getRemark()), SysRoleTableForm::getRemark, sysRoleTableForm.getRemark())
                .orderByDesc(SysRoleTableForm::getRemark);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysRoleTableForm> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysRoleTableForm> iPage = sysRoleTableFormMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper<>());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysRoleTableFormMapper.selectBatchIds(exceel.getIds());
        } else {
            SysRoleTableForm sysRoleTableForm = JSON.parseObject(String.valueOf(exceel.getQuery()), SysRoleTableForm.class);
            list = sysRoleTableFormMapper.selectSysRoleTableFormList(sysRoleTableForm);
        }
        String fileName = exceel.getFilename().equals(null) ? "角色功能表查询表数据" : exceel.getFilename();
        Set<String> column = Arrays.stream(exceel.getFields().stream().map(m -> m.getField()).collect(Collectors.toList()).stream().toArray(String[]::new)).collect(Collectors.toSet());
        ExcelUtil.exportExcel(list, fileName, SysRoleTableForm.class, response, column);
    }
}
