package com.ruoyi.tableConfig.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysUserTableFormItemMapper;
import com.ruoyi.tableConfig.api.domain.SysUserTableFormItem;
import com.ruoyi.tableConfig.service.ISysUserTableFormItemService;

import javax.servlet.http.HttpServletResponse;

/**
 * 用户表单配置项列表Service业务层处理
 *
 * @author zly
 * @date 2023-06-13
 */
@Service
public class SysUserTableFormItemServiceImpl extends ServiceImpl<SysUserTableFormItemMapper, SysUserTableFormItem> implements ISysUserTableFormItemService {
    @Autowired
    private SysUserTableFormItemMapper sysUserTableFormItemMapper;

    /**
     * 查询用户表单配置项列表
     *
     * @param roleFormId 角色表格id
     * @return 用户表单配置项列表
     */
    @Override
    public List<SysUserTableFormItem> selectSysUserTableFormItemByRoleFormId(Long roleFormId) {
        return sysUserTableFormItemMapper.selectSysUserTableFormItemByRoleFormId(roleFormId);
    }

    /**
     * 查询用户表单配置项列表
     *
     * @param userId 用户id
     * @return 用户表单配置项列表
     */
    @Override
    public List<SysUserTableFormItem> selectSysUserTableFormItemByUserId(Long userId) {
        return sysUserTableFormItemMapper.selectSysUserTableFormItemByUserId(userId);
    }

    /**
     * 查询用户表单配置项列表
     *
     * @param userItemId 用户表单配置项列表主键
     * @return 用户表单配置项列表
     */
    @Override
    public SysUserTableFormItem selectSysUserTableFormItemByUserItemId(Long userItemId) {
        return sysUserTableFormItemMapper.selectSysUserTableFormItemByUserItemId(userItemId);
    }

    /**
     * 查询用户表单配置项列表列表
     *
     * @param sysUserTableFormItem 用户表单配置项列表
     * @return 用户表单配置项列表
     */
    @Override
    public List<SysUserTableFormItem> selectSysUserTableFormItemList(SysUserTableFormItem sysUserTableFormItem) {
        return sysUserTableFormItemMapper.selectSysUserTableFormItemList(sysUserTableFormItem);
    }

    /**
     * 查询用户表单配置项列表分页列表
     *
     * @param sysUserTableFormItem 用户表单配置项列表
     * @param pageQuery            查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysUserTableFormItem> selectSysUserTableFormItemPage(SysUserTableFormItem sysUserTableFormItem, PageQuery pageQuery) {
        LambdaQueryWrapper<SysUserTableFormItem> lqw = buildQueryWrapper(sysUserTableFormItem);
        Page<SysUserTableFormItem> page = sysUserTableFormItemMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增用户表单配置项列表
     *
     * @param sysUserTableFormItem 用户表单配置项列表
     * @return 结果
     */
    @Override
    public int insertSysUserTableFormItem(SysUserTableFormItem sysUserTableFormItem) {
        sysUserTableFormItem.setCreateTime(DateUtils.getNowDate());
        return sysUserTableFormItemMapper.insertSysUserTableFormItem(sysUserTableFormItem);
    }

    /**
     * 修改用户表单配置项列表
     *
     * @param sysUserTableFormItem 用户表单配置项列表
     * @return 结果
     */
    @Override
    public int updateSysUserTableFormItem(SysUserTableFormItem sysUserTableFormItem) {
        sysUserTableFormItem.setUpdateTime(DateUtils.getNowDate());
        return sysUserTableFormItemMapper.updateSysUserTableFormItem(sysUserTableFormItem);
    }

    /**
     * 批量删除用户表单配置项列表
     *
     * @param userItemIds 需要删除的用户表单配置项列表主键
     * @return 结果
     */
    @Override
    public int deleteSysUserTableFormItemByUserItemIds(Long[] userItemIds) {
        return sysUserTableFormItemMapper.deleteSysUserTableFormItemByUserItemIds(userItemIds);
    }

    /**
     * 删除用户表单配置项列表信息
     *
     * @param userItemId 用户表单配置项列表主键
     * @return 结果
     */
    @Override
    public int deleteSysUserTableFormItemByUserItemId(Long userItemId) {
        return sysUserTableFormItemMapper.deleteSysUserTableFormItemByUserItemId(userItemId);
    }

    /**
     * 删除用户表单配置项列表信息
     *
     * @param userId 用户id
     * @return 结果
     */
    @Override
    public int deleteSysUserTableFormItemByUserId(Long userId) {
        return sysUserTableFormItemMapper.deleteSysUserTableFormItemByUserId(userId);
    }

    /**
     * 用户表单配置项列表查询条件
     *
     * @param sysUserTableFormItem
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysUserTableFormItem> buildQueryWrapper(SysUserTableFormItem sysUserTableFormItem) {
        LambdaQueryWrapper<SysUserTableFormItem> lambdaQueryWrapper = new LambdaQueryWrapper<SysUserTableFormItem>()
                .eq(!StringUtils.isNull(sysUserTableFormItem.getUserId()), SysUserTableFormItem::getUserId, sysUserTableFormItem.getUserId())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getUserItemId()), SysUserTableFormItem::getUserItemId, sysUserTableFormItem.getUserItemId())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getItemId()), SysUserTableFormItem::getItemId, sysUserTableFormItem.getItemId())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getRoleFormId()), SysUserTableFormItem::getRoleFormId, sysUserTableFormItem.getRoleFormId())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getField()), SysUserTableFormItem::getField, sysUserTableFormItem.getField())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getTitle()), SysUserTableFormItem::getTitle, sysUserTableFormItem.getTitle())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getSpan()), SysUserTableFormItem::getSpan, sysUserTableFormItem.getSpan())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getAlign()), SysUserTableFormItem::getAlign, sysUserTableFormItem.getAlign())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getTitleAlign()), SysUserTableFormItem::getTitleAlign, sysUserTableFormItem.getTitleAlign())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getTitleWidth()), SysUserTableFormItem::getTitleWidth, sysUserTableFormItem.getTitleWidth())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getTitleColon()), SysUserTableFormItem::getTitleColon, sysUserTableFormItem.getTitleColon())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getTitleAsterisk()), SysUserTableFormItem::getTitleAsterisk, sysUserTableFormItem.getTitleAsterisk())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getTitleOverflow()), SysUserTableFormItem::getTitleOverflow, sysUserTableFormItem.getTitleOverflow())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getShowTitle()), SysUserTableFormItem::getShowTitle, sysUserTableFormItem.getShowTitle())
                .like(!StringUtils.isNull(sysUserTableFormItem.getClassName()), SysUserTableFormItem::getClassName, sysUserTableFormItem.getClassName())
                .like(!StringUtils.isNull(sysUserTableFormItem.getContentClassName()), SysUserTableFormItem::getContentClassName, sysUserTableFormItem.getContentClassName())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getContentStyle()), SysUserTableFormItem::getContentStyle, sysUserTableFormItem.getContentStyle())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getVisible()), SysUserTableFormItem::getVisible, sysUserTableFormItem.getVisible())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getVisibleMethod()), SysUserTableFormItem::getVisibleMethod, sysUserTableFormItem.getVisibleMethod())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getFormFilter()), SysUserTableFormItem::getFormFilter, sysUserTableFormItem.getFormFilter())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getFolding()), SysUserTableFormItem::getFolding, sysUserTableFormItem.getFolding())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getCollapseNode()), SysUserTableFormItem::getCollapseNode, sysUserTableFormItem.getCollapseNode())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getResetValue()), SysUserTableFormItem::getResetValue, sysUserTableFormItem.getResetValue())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getItemRender()), SysUserTableFormItem::getItemRender, sysUserTableFormItem.getItemRender())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getChildren()), SysUserTableFormItem::getChildren, sysUserTableFormItem.getChildren())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getSlots()), SysUserTableFormItem::getSlots, sysUserTableFormItem.getSlots())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getSortNo()), SysUserTableFormItem::getSortNo, sysUserTableFormItem.getSortNo())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getDictType()), SysUserTableFormItem::getDictType, sysUserTableFormItem.getDictType())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getSearchHandle()), SysUserTableFormItem::getSearchHandle, sysUserTableFormItem.getSearchHandle())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getSearchNo()), SysUserTableFormItem::getSearchNo, sysUserTableFormItem.getSearchNo())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getSearchVisible()), SysUserTableFormItem::getSearchVisible, sysUserTableFormItem.getSearchVisible())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getSearchFixed()), SysUserTableFormItem::getSearchFixed, sysUserTableFormItem.getSearchFixed())
                .eq(!StringUtils.isNull(sysUserTableFormItem.getCreateBy()), SysUserTableFormItem::getCreateBy, sysUserTableFormItem.getCreateBy())
                .ge(sysUserTableFormItem.getParams().get("beginCreateTime") != null, SysUserTableFormItem::getCreateTime, sysUserTableFormItem.getParams().get("beginCreateTime"))
                .le(sysUserTableFormItem.getParams().get("endCreateTime") != null, SysUserTableFormItem::getCreateTime, sysUserTableFormItem.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysUserTableFormItem.getUpdateBy()), SysUserTableFormItem::getUpdateBy, sysUserTableFormItem.getUpdateBy())
                .ge(sysUserTableFormItem.getParams().get("beginUpdateTime") != null, SysUserTableFormItem::getUpdateTime, sysUserTableFormItem.getParams().get("beginUpdateTime"))
                .le(sysUserTableFormItem.getParams().get("endUpdateTime") != null, SysUserTableFormItem::getUpdateTime, sysUserTableFormItem.getParams().get("endUpdateTime"))
                .eq(!StringUtils.isNull(sysUserTableFormItem.getRemark()), SysUserTableFormItem::getRemark, sysUserTableFormItem.getRemark())
                .orderByDesc(SysUserTableFormItem::getSearchFixed)
                .orderByAsc(SysUserTableFormItem::getSearchHandle)
                .orderByAsc(SysUserTableFormItem::getSearchNo);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysUserTableFormItem> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysUserTableFormItem> iPage = sysUserTableFormItemMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper<>());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysUserTableFormItemMapper.selectBatchIds(exceel.getIds());
        } else {
            SysUserTableFormItem sysUserTableFormItem = JSON.parseObject(String.valueOf(exceel.getQuery()), SysUserTableFormItem.class);
            list = sysUserTableFormItemMapper.selectSysUserTableFormItemList(sysUserTableFormItem);
        }
        String fileName = exceel.getFilename().equals(null) ? "用户表单配置项列表数据" : exceel.getFilename();
        Set<String> column = Arrays.stream(exceel.getFields().stream().map(m -> m.getField()).collect(Collectors.toList()).stream().toArray(String[]::new)).collect(Collectors.toSet());
        ExcelUtil.exportExcel(list, fileName, SysUserTableFormItem.class, response, column);
    }
}
