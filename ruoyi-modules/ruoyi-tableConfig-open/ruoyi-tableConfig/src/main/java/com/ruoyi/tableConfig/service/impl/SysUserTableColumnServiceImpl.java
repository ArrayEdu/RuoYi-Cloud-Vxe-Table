package com.ruoyi.tableConfig.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysUserTableColumnMapper;
import com.ruoyi.tableConfig.api.domain.SysUserTableColumn;
import com.ruoyi.tableConfig.service.ISysUserTableColumnService;

import javax.servlet.http.HttpServletResponse;

/**
 * 用户表格列配置Service业务层处理
 *
 * @author zly
 * @date 2023-06-13
 */
@Service
public class SysUserTableColumnServiceImpl extends ServiceImpl<SysUserTableColumnMapper, SysUserTableColumn> implements ISysUserTableColumnService {
    @Autowired
    private SysUserTableColumnMapper sysUserTableColumnMapper;

    /**
     * 查询用户表格列配置
     *
     * @param userId 用户id
     * @return 用户表格列配置
     */
    @Override
    public List<SysUserTableColumn> selectSysUserTableColumnByUserId(Long userId) {
        return sysUserTableColumnMapper.selectSysUserTableColumnByUserId(userId);
    }

    /**
     * 查询用户表格列配置
     *
     * @param userColumnId 用户表格列配置主键
     * @return 用户表格列配置
     */
    @Override
    public SysUserTableColumn selectSysUserTableColumnByUserColumnId(Long userColumnId) {
        return sysUserTableColumnMapper.selectSysUserTableColumnByUserColumnId(userColumnId);
    }

    /**
     * 查询用户表格列配置
     *
     * @param roleTableId 角色表格id
     * @return 用户表格列配置
     */
    @Override
    public List<SysUserTableColumn> selectSysUserTableColumnByRoleTableId(Long roleTableId) {
        return sysUserTableColumnMapper.selectSysUserTableColumnByRoleTableId(roleTableId);
    }


    /**
     * 查询用户表格列配置列表
     *
     * @param sysUserTableColumn 用户表格列配置
     * @return 用户表格列配置
     */
    @Override
    public List<SysUserTableColumn> selectSysUserTableColumnList(SysUserTableColumn sysUserTableColumn) {
        return sysUserTableColumnMapper.selectSysUserTableColumnList(sysUserTableColumn);
    }

    /**
     * 查询用户表格列配置分页列表
     *
     * @param sysUserTableColumn 用户表格列配置
     * @param pageQuery          查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysUserTableColumn> selectSysUserTableColumnPage(SysUserTableColumn sysUserTableColumn, PageQuery pageQuery) {
        LambdaQueryWrapper<SysUserTableColumn> lqw = buildQueryWrapper(sysUserTableColumn);
        Page<SysUserTableColumn> page = sysUserTableColumnMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增用户表格列配置
     *
     * @param sysUserTableColumn 用户表格列配置
     * @return 结果
     */
    @Override
    public int insertSysUserTableColumn(SysUserTableColumn sysUserTableColumn) {
        sysUserTableColumn.setCreateTime(DateUtils.getNowDate());
        return sysUserTableColumnMapper.insertSysUserTableColumn(sysUserTableColumn);
    }

    /**
     * 修改用户表格列配置
     *
     * @param sysUserTableColumn 用户表格列配置
     * @return 结果
     */
    @Override
    public int updateSysUserTableColumn(SysUserTableColumn sysUserTableColumn) {
        sysUserTableColumn.setUpdateTime(DateUtils.getNowDate());
        return sysUserTableColumnMapper.updateSysUserTableColumn(sysUserTableColumn);
    }

    /**
     * 批量删除用户表格列配置
     *
     * @param userColumnIds 需要删除的用户表格列配置主键
     * @return 结果
     */
    @Override
    public int deleteSysUserTableColumnByUserColumnIds(Long[] userColumnIds) {
        return sysUserTableColumnMapper.deleteSysUserTableColumnByUserColumnIds(userColumnIds);
    }

    /**
     * 删除用户表格列配置信息
     *
     * @param userColumnId 用户表格列配置主键
     * @return 结果
     */
    @Override
    public int deleteSysUserTableColumnByUserColumnId(Long userColumnId) {
        return sysUserTableColumnMapper.deleteSysUserTableColumnByUserColumnId(userColumnId);
    }

    /**
     * 删除用户表格列配置信息
     *
     * @param userId 用户id
     * @return 结果
     */
    @Override
    public int deleteSysUserTableColumnByUserId(Long userId) {
        return sysUserTableColumnMapper.deleteSysUserTableColumnByUserId(userId);
    }

    /**
     * 用户表格列配置查询条件
     *
     * @param sysUserTableColumn
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysUserTableColumn> buildQueryWrapper(SysUserTableColumn sysUserTableColumn) {
        LambdaQueryWrapper<SysUserTableColumn> lambdaQueryWrapper = new LambdaQueryWrapper<SysUserTableColumn>()
                .eq(!StringUtils.isNull(sysUserTableColumn.getUserId()), SysUserTableColumn::getUserId, sysUserTableColumn.getUserId())
                .eq(!StringUtils.isNull(sysUserTableColumn.getUserColumnId()), SysUserTableColumn::getUserColumnId, sysUserTableColumn.getUserColumnId())
                .eq(!StringUtils.isNull(sysUserTableColumn.getColumnId()), SysUserTableColumn::getColumnId, sysUserTableColumn.getColumnId())
                .eq(!StringUtils.isNull(sysUserTableColumn.getRoleTableId()), SysUserTableColumn::getRoleTableId, sysUserTableColumn.getRoleTableId())
                .like(!StringUtils.isNull(sysUserTableColumn.getColumnName()), SysUserTableColumn::getColumnName, sysUserTableColumn.getColumnName())
                .eq(!StringUtils.isNull(sysUserTableColumn.getColumnComment()), SysUserTableColumn::getColumnComment, sysUserTableColumn.getColumnComment())
                .eq(!StringUtils.isNull(sysUserTableColumn.getField()), SysUserTableColumn::getField, sysUserTableColumn.getField())
                .eq(!StringUtils.isNull(sysUserTableColumn.getTitle()), SysUserTableColumn::getTitle, sysUserTableColumn.getTitle())
                .eq(!StringUtils.isNull(sysUserTableColumn.getIsIgnore()), SysUserTableColumn::getIsIgnore, sysUserTableColumn.getIsIgnore())
                .eq(!StringUtils.isNull(sysUserTableColumn.getVisible()), SysUserTableColumn::getVisible, sysUserTableColumn.getVisible())
                .eq(!StringUtils.isNull(sysUserTableColumn.getWidth()), SysUserTableColumn::getWidth, sysUserTableColumn.getWidth())
                .eq(!StringUtils.isNull(sysUserTableColumn.getMinWidth()), SysUserTableColumn::getMinWidth, sysUserTableColumn.getMinWidth())
                .eq(!StringUtils.isNull(sysUserTableColumn.getResizable()), SysUserTableColumn::getResizable, sysUserTableColumn.getResizable())
                .eq(!StringUtils.isNull(sysUserTableColumn.getAlign()), SysUserTableColumn::getAlign, sysUserTableColumn.getAlign())
                .eq(!StringUtils.isNull(sysUserTableColumn.getType()), SysUserTableColumn::getType, sysUserTableColumn.getType())
                .eq(!StringUtils.isNull(sysUserTableColumn.getFixed()), SysUserTableColumn::getFixed, sysUserTableColumn.getFixed())
                .eq(!StringUtils.isNull(sysUserTableColumn.getHeaderAlign()), SysUserTableColumn::getHeaderAlign, sysUserTableColumn.getHeaderAlign())
                .eq(!StringUtils.isNull(sysUserTableColumn.getFooterAlign()), SysUserTableColumn::getFooterAlign, sysUserTableColumn.getFooterAlign())
                .eq(!StringUtils.isNull(sysUserTableColumn.getShowOverflow()), SysUserTableColumn::getShowOverflow, sysUserTableColumn.getShowOverflow())
                .eq(!StringUtils.isNull(sysUserTableColumn.getShowHeaderOverflow()), SysUserTableColumn::getShowHeaderOverflow, sysUserTableColumn.getShowHeaderOverflow())
                .eq(!StringUtils.isNull(sysUserTableColumn.getShowFooterOverflow()), SysUserTableColumn::getShowFooterOverflow, sysUserTableColumn.getShowFooterOverflow())
                .like(!StringUtils.isNull(sysUserTableColumn.getClassName()), SysUserTableColumn::getClassName, sysUserTableColumn.getClassName())
                .like(!StringUtils.isNull(sysUserTableColumn.getHeaderClassName()), SysUserTableColumn::getHeaderClassName, sysUserTableColumn.getHeaderClassName())
                .like(!StringUtils.isNull(sysUserTableColumn.getFooterClassName()), SysUserTableColumn::getFooterClassName, sysUserTableColumn.getFooterClassName())
                .eq(!StringUtils.isNull(sysUserTableColumn.getFormatter()), SysUserTableColumn::getFormatter, sysUserTableColumn.getFormatter())
                .eq(!StringUtils.isNull(sysUserTableColumn.getSortable()), SysUserTableColumn::getSortable, sysUserTableColumn.getSortable())
                .eq(!StringUtils.isNull(sysUserTableColumn.getSortBy()), SysUserTableColumn::getSortBy, sysUserTableColumn.getSortBy())
                .eq(!StringUtils.isNull(sysUserTableColumn.getSortType()), SysUserTableColumn::getSortType, sysUserTableColumn.getSortType())
                .eq(!StringUtils.isNull(sysUserTableColumn.getExtParams()), SysUserTableColumn::getExtParams, sysUserTableColumn.getExtParams())
                .eq(!StringUtils.isNull(sysUserTableColumn.getTreeNode()), SysUserTableColumn::getTreeNode, sysUserTableColumn.getTreeNode())
                .eq(!StringUtils.isNull(sysUserTableColumn.getSlots()), SysUserTableColumn::getSlots, sysUserTableColumn.getSlots())
                .eq(!StringUtils.isNull(sysUserTableColumn.getFilters()), SysUserTableColumn::getFilters, sysUserTableColumn.getFilters())
                .eq(!StringUtils.isNull(sysUserTableColumn.getFilterMultiple()), SysUserTableColumn::getFilterMultiple, sysUserTableColumn.getFilterMultiple())
                .eq(!StringUtils.isNull(sysUserTableColumn.getFilterMethod()), SysUserTableColumn::getFilterMethod, sysUserTableColumn.getFilterMethod())
                .eq(!StringUtils.isNull(sysUserTableColumn.getFilterResetMethod()), SysUserTableColumn::getFilterResetMethod, sysUserTableColumn.getFilterResetMethod())
                .eq(!StringUtils.isNull(sysUserTableColumn.getFilterRecoverMethod()), SysUserTableColumn::getFilterRecoverMethod, sysUserTableColumn.getFilterRecoverMethod())
                .eq(!StringUtils.isNull(sysUserTableColumn.getFilterRender()), SysUserTableColumn::getFilterRender, sysUserTableColumn.getFilterRender())
                .eq(!StringUtils.isNull(sysUserTableColumn.getExportMethod()), SysUserTableColumn::getExportMethod, sysUserTableColumn.getExportMethod())
                .eq(!StringUtils.isNull(sysUserTableColumn.getFooterExportMethod()), SysUserTableColumn::getFooterExportMethod, sysUserTableColumn.getFooterExportMethod())
                .eq(!StringUtils.isNull(sysUserTableColumn.getTitlePrefix()), SysUserTableColumn::getTitlePrefix, sysUserTableColumn.getTitlePrefix())
                .eq(!StringUtils.isNull(sysUserTableColumn.getCellType()), SysUserTableColumn::getCellType, sysUserTableColumn.getCellType())
                .eq(!StringUtils.isNull(sysUserTableColumn.getCellRender()), SysUserTableColumn::getCellRender, sysUserTableColumn.getCellRender())
                .eq(!StringUtils.isNull(sysUserTableColumn.getEditRender()), SysUserTableColumn::getEditRender, sysUserTableColumn.getEditRender())
                .eq(!StringUtils.isNull(sysUserTableColumn.getContentRender()), SysUserTableColumn::getContentRender, sysUserTableColumn.getContentRender())
                .eq(!StringUtils.isNull(sysUserTableColumn.getColId()), SysUserTableColumn::getColId, sysUserTableColumn.getColId())
                .eq(!StringUtils.isNull(sysUserTableColumn.getSortNo()), SysUserTableColumn::getSortNo, sysUserTableColumn.getSortNo())
                .eq(!StringUtils.isNull(sysUserTableColumn.getDictType()), SysUserTableColumn::getDictType, sysUserTableColumn.getDictType())
                .eq(!StringUtils.isNull(sysUserTableColumn.getRemark()), SysUserTableColumn::getRemark, sysUserTableColumn.getRemark())
                .eq(!StringUtils.isNull(sysUserTableColumn.getCreateBy()), SysUserTableColumn::getCreateBy, sysUserTableColumn.getCreateBy())
                .ge(sysUserTableColumn.getParams().get("beginCreateTime") != null, SysUserTableColumn::getCreateTime, sysUserTableColumn.getParams().get("beginCreateTime"))
                .le(sysUserTableColumn.getParams().get("endCreateTime") != null, SysUserTableColumn::getCreateTime, sysUserTableColumn.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysUserTableColumn.getUpdateBy()), SysUserTableColumn::getUpdateBy, sysUserTableColumn.getUpdateBy())
                .ge(sysUserTableColumn.getParams().get("beginUpdateTime") != null, SysUserTableColumn::getUpdateTime, sysUserTableColumn.getParams().get("beginUpdateTime"))
                .le(sysUserTableColumn.getParams().get("endUpdateTime") != null, SysUserTableColumn::getUpdateTime, sysUserTableColumn.getParams().get("endUpdateTime"))
                .orderByDesc(SysUserTableColumn::getUpdateTime);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysUserTableColumn> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysUserTableColumn> iPage = sysUserTableColumnMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysUserTableColumnMapper.selectVoBatchIds(exceel.getIds());
        } else {
            SysUserTableColumn sysUserTableColumn = JSON.parseObject(String.valueOf(exceel.getQuery()), SysUserTableColumn.class);
            list = sysUserTableColumnMapper.selectSysUserTableColumnList(sysUserTableColumn);
        }
        String fileName = exceel.getFilename().equals(null) ? "用户表格列配置数据" : exceel.getFilename();
        Set<String> column = Arrays.stream(exceel.getFields().stream().map(m -> m.getField()).collect(Collectors.toList()).stream().toArray(String[]::new)).collect(Collectors.toSet());
        ExcelUtil.exportExcel(list, fileName, SysUserTableColumn.class, response, column);
    }
}
