package com.ruoyi.tableConfig.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysTableEditRules;

/**
 * 校验规则配置项Service接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface ISysTableEditRulesService extends IServicePlus<SysTableEditRules> {

    /**
     * 查询校验规则配置项
     *
     * @param tableId 表格id
     * @return 校验规则配置项
     */
    public List<SysTableEditRules> selectSysTableEditRulesByTableId(Long tableId);

    /**
     * 查询校验规则配置项
     *
     * @param editRulesId 校验规则配置项主键
     * @return 校验规则配置项
     */
    public SysTableEditRules selectSysTableEditRulesByEditRulesId(Long editRulesId);

    /**
     * 查询校验规则配置项列表
     *
     * @param sysTableEditRules 校验规则配置项
     * @return 校验规则配置项集合
     */
    public List<SysTableEditRules> selectSysTableEditRulesList(SysTableEditRules sysTableEditRules);

    /**
     * 查询校验规则配置项分页列表
     *
     * @param sysTableEditRules 校验规则配置项
     * @param pageQuery         查询配置
     * @return
     */
    public TableDataInfo<SysTableEditRules> selectSysTableEditRulesPage(SysTableEditRules sysTableEditRules, PageQuery pageQuery);

    /**
     * 新增校验规则配置项
     *
     * @param sysTableEditRules 校验规则配置项
     * @return 结果
     */
    public int insertSysTableEditRules(SysTableEditRules sysTableEditRules);

    /**
     * 修改校验规则配置项
     *
     * @param sysTableEditRules 校验规则配置项
     * @return 结果
     */
    public int updateSysTableEditRules(SysTableEditRules sysTableEditRules);

    /**
     * 批量删除校验规则配置项
     *
     * @param editRulesIds 需要删除的校验规则配置项主键集合
     * @return 结果
     */
    public int deleteSysTableEditRulesByEditRulesIds(Long[] editRulesIds);

    /**
     * 删除校验规则配置项信息
     *
     * @param editRulesId 校验规则配置项主键
     * @return 结果
     */
    public int deleteSysTableEditRulesByEditRulesId(Long editRulesId);

    public LambdaQueryWrapper<SysTableEditRules> buildQueryWrapper(SysTableEditRules sysTableEditRules);
}
