package com.ruoyi.tableConfig.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableColumn;
import com.ruoyi.tableConfig.api.domain.SysRoleTableForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysRoleTableFormService;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 角色功能表查询表Controller
 *
 * @author zly
 * @date 2023-06-06
 */
@RestController
@RequestMapping("/roleTableForm")
public class SysRoleTableFormController extends BaseController {
    @Autowired
    private ISysRoleTableFormService sysRoleTableFormService;

    /**
     * 查询角色功能表查询表列表
     */
    @RequiresPermissions("system:form:list")
    @GetMapping("/list")
    public TableDataInfo list(SysRoleTableForm sysRoleTableForm) {
        return TableDataInfo.build(sysRoleTableFormService.selectSysRoleTableFormList(sysRoleTableForm));
    }

    /**
     * 查询角色功能表查询表分页列表
     */
    @RequiresPermissions("system:form:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysRoleTableFormService.selectSysRoleTableFormPage(ajaxData.toJavaObject(SysRoleTableForm.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出角色功能表查询表列表
     */
    @RequiresPermissions("system:form:export")
    @Log(title = "角色功能表查询表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysRoleTableForm sysRoleTableForm) {
        List<SysRoleTableForm> list = sysRoleTableFormService.selectSysRoleTableFormList(sysRoleTableForm);
        ExcelUtil.exportExcel(list, "角色功能表查询表数据", SysRoleTableForm.class, response);
    }

    /**
     * vxe-table导出角色功能表查询表列表
     */
    @RequiresPermissions("system:form:export")
    @Log(title = "角色功能表查询表", businessType = BusinessType.EXPORT)
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        sysRoleTableFormService.export(response, params);
    }

    /**
     * 获取角色功能表查询表详细信息
     */
    @RequiresPermissions("system:form:query")
    @GetMapping(value = "/{roleFormId}")
    public AjaxResult getInfo(@PathVariable("roleFormId") Long roleFormId) {
        return success(sysRoleTableFormService.selectSysRoleTableFormByRoleFormId(roleFormId));
    }

    /**
     * 获取角色功能表查询表详细信息(批量)
     */
    @RequiresPermissions("system:form:query")
    @GetMapping(value = "/in/{formIds}")
    public AjaxResult getInfos(@PathVariable(value = "formIds", required = true) Long[] formIds) {
        List<SysRoleTableForm> list = sysRoleTableFormService.listByIds(Arrays.asList(formIds));
        return AjaxResult.success(list);
    }

    /**
     * 新增角色功能表查询表
     */
    @RequiresPermissions("system:form:add")
    @Log(title = "角色功能表查询表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysRoleTableForm sysRoleTableForm) {
        return toAjax(sysRoleTableFormService.insertSysRoleTableForm(sysRoleTableForm));
    }

    /**
     * 新增角色功能表查询表(批量)
     */
    @RequiresPermissions("system:form:add")
    @Log(title = "角色功能表查询表", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@RequestBody JSONArray jsonArray) {
        if (!sysRoleTableFormService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableForm.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改角色功能表查询表
     */
    @RequiresPermissions("system:form:edit")
    @Log(title = "角色功能表查询表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysRoleTableForm sysRoleTableForm) {
        return toAjax(sysRoleTableFormService.updateSysRoleTableForm(sysRoleTableForm));
    }

    /**
     * 修改角色功能表查询表(批量)
     */
    @RequiresPermissions("system:form:edit")
    @Log(title = "角色功能表查询表", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!sysRoleTableFormService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableForm.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除角色功能表查询表
     */
    @RequiresPermissions("system:form:remove")
    @Log(title = "角色功能表查询表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{formIds}")
    public AjaxResult removes(@PathVariable(value = "formIds", required = true) Long[] formIds) {
        return toAjax(sysRoleTableFormService.removeByIds(Arrays.asList(formIds)));
    }

    /**
     * 导入模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "角色功能表查询表模板", SysRoleTableForm.class, response);
    }
}
