package com.ruoyi.tableConfig.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableToolbarConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysTableToolbarConfigMapper;
import com.ruoyi.tableConfig.service.ISysTableToolbarConfigService;

/**
 * 工具栏配置Service业务层处理
 *
 * @author zly
 * @date 2023-05-15
 */
@Service
public class SysTableToolbarConfigServiceImpl extends ServiceImpl<SysTableToolbarConfigMapper, SysTableToolbarConfig> implements ISysTableToolbarConfigService {
    @Autowired
    private SysTableToolbarConfigMapper sysTableToolbarConfigMapper;


    /**
     * 查询工具栏配置
     *
     * @param tableId 功能表id
     * @return 工具栏配置
     */
    @Override
    public SysTableToolbarConfig selectSysTableToolbarConfigByTableId(Long tableId) {
        return sysTableToolbarConfigMapper.selectSysTableToolbarConfigByTableId(tableId);
    }

    /**
     * 查询工具栏配置
     *
     * @param toolbarConfigId 工具栏配置主键
     * @return 工具栏配置
     */
    @Override
    public SysTableToolbarConfig selectSysTableToolbarConfigByToolbarConfigId(Long toolbarConfigId) {
        return sysTableToolbarConfigMapper.selectSysTableToolbarConfigByToolbarConfigId(toolbarConfigId);
    }

    /**
     * 查询工具栏配置列表
     *
     * @param sysTableToolbarConfig 工具栏配置
     * @return 工具栏配置
     */
    @Override
    public List<SysTableToolbarConfig> selectSysTableToolbarConfigList(SysTableToolbarConfig sysTableToolbarConfig) {
        return sysTableToolbarConfigMapper.selectSysTableToolbarConfigList(sysTableToolbarConfig);
    }

    /**
     * 查询工具栏配置分页列表
     *
     * @param sysTableToolbarConfig 工具栏配置
     * @param pageQuery             查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysTableToolbarConfig> selectSysTableToolbarConfigPage(SysTableToolbarConfig sysTableToolbarConfig, PageQuery pageQuery) {
        LambdaQueryWrapper<SysTableToolbarConfig> lqw = buildQueryWrapper(sysTableToolbarConfig);
        Page<SysTableToolbarConfig> page = sysTableToolbarConfigMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增工具栏配置
     *
     * @param sysTableToolbarConfig 工具栏配置
     * @return 结果
     */
    @Override
    public int insertSysTableToolbarConfig(SysTableToolbarConfig sysTableToolbarConfig) {
        sysTableToolbarConfig.setCreateTime(DateUtils.getNowDate());
        return sysTableToolbarConfigMapper.insertSysTableToolbarConfig(sysTableToolbarConfig);
    }

    /**
     * 修改工具栏配置
     *
     * @param sysTableToolbarConfig 工具栏配置
     * @return 结果
     */
    @Override
    public int updateSysTableToolbarConfig(SysTableToolbarConfig sysTableToolbarConfig) {
        sysTableToolbarConfig.setUpdateTime(DateUtils.getNowDate());
        return sysTableToolbarConfigMapper.updateSysTableToolbarConfig(sysTableToolbarConfig);
    }

    /**
     * 批量删除工具栏配置
     *
     * @param toolbarConfigIds 需要删除的工具栏配置主键
     * @return 结果
     */
    @Override
    public int deleteSysTableToolbarConfigByToolbarConfigIds(Long[] toolbarConfigIds) {
        return sysTableToolbarConfigMapper.deleteSysTableToolbarConfigByToolbarConfigIds(toolbarConfigIds);
    }

    /**
     * 删除工具栏配置信息
     *
     * @param toolbarConfigId 工具栏配置主键
     * @return 结果
     */
    @Override
    public int deleteSysTableToolbarConfigByToolbarConfigId(Long toolbarConfigId) {
        return sysTableToolbarConfigMapper.deleteSysTableToolbarConfigByToolbarConfigId(toolbarConfigId);
    }

    @Override
    public LambdaQueryWrapper<SysTableToolbarConfig> buildQueryWrapper(SysTableToolbarConfig sysTableToolbarConfig) {
        LambdaQueryWrapper<SysTableToolbarConfig> lambdaQueryWrapper = new LambdaQueryWrapper<SysTableToolbarConfig>()
                .eq(!StringUtils.isNull(sysTableToolbarConfig.getToolbarConfigId()), SysTableToolbarConfig::getToolbarConfigId, sysTableToolbarConfig.getToolbarConfigId())
                .eq(!StringUtils.isNull(sysTableToolbarConfig.getTableId()), SysTableToolbarConfig::getTableId, sysTableToolbarConfig.getTableId())
                .ge(sysTableToolbarConfig.getParams().get("beginCreateTime") != null, SysTableToolbarConfig::getCreateTime, sysTableToolbarConfig.getParams().get("beginCreateTime"))
                .le(sysTableToolbarConfig.getParams().get("endCreateTime") != null, SysTableToolbarConfig::getCreateTime, sysTableToolbarConfig.getParams().get("endCreateTime"))
                .ge(sysTableToolbarConfig.getParams().get("beginUpdateTime") != null, SysTableToolbarConfig::getUpdateTime, sysTableToolbarConfig.getParams().get("beginUpdateTime"))
                .le(sysTableToolbarConfig.getParams().get("endUpdateTime") != null, SysTableToolbarConfig::getUpdateTime, sysTableToolbarConfig.getParams().get("endUpdateTime"))
                .orderByDesc(SysTableToolbarConfig::getTableId);
        return lambdaQueryWrapper;
    }
}
