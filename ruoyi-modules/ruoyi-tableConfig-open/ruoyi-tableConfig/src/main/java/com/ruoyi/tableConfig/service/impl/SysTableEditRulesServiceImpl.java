package com.ruoyi.tableConfig.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableEditRules;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysTableEditRulesMapper;
import com.ruoyi.tableConfig.service.ISysTableEditRulesService;

/**
 * 校验规则配置项Service业务层处理
 *
 * @author zly
 * @date 2023-04-25
 */
@Service
public class SysTableEditRulesServiceImpl extends ServiceImpl<SysTableEditRulesMapper, SysTableEditRules> implements ISysTableEditRulesService {
    @Autowired
    private SysTableEditRulesMapper sysTableEditRulesMapper;


    /**
     * 查询校验规则配置项
     *
     * @param tableId 表格id
     * @return 校验规则配置项
     */
    @Override
    public List<SysTableEditRules> selectSysTableEditRulesByTableId(Long tableId) {
        return sysTableEditRulesMapper.selectSysTableEditRulesByTableId(tableId);
    }

    /**
     * 查询校验规则配置项
     *
     * @param editRulesId 校验规则配置项主键
     * @return 校验规则配置项
     */
    @Override
    public SysTableEditRules selectSysTableEditRulesByEditRulesId(Long editRulesId) {
        return sysTableEditRulesMapper.selectSysTableEditRulesByEditRulesId(editRulesId);
    }

    /**
     * 查询校验规则配置项列表
     *
     * @param sysTableEditRules 校验规则配置项
     * @return 校验规则配置项
     */
    @Override
    public List<SysTableEditRules> selectSysTableEditRulesList(SysTableEditRules sysTableEditRules) {
        return sysTableEditRulesMapper.selectSysTableEditRulesList(sysTableEditRules);
    }

    /**
     * 查询校验规则配置项分页列表
     *
     * @param sysTableEditRules 校验规则配置项
     * @param pageQuery         查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysTableEditRules> selectSysTableEditRulesPage(SysTableEditRules sysTableEditRules, PageQuery pageQuery) {
        LambdaQueryWrapper<SysTableEditRules> lqw = buildQueryWrapper(sysTableEditRules);
        Page<SysTableEditRules> page = sysTableEditRulesMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增校验规则配置项
     *
     * @param sysTableEditRules 校验规则配置项
     * @return 结果
     */
    @Override
    public int insertSysTableEditRules(SysTableEditRules sysTableEditRules) {
        sysTableEditRules.setCreateTime(DateUtils.getNowDate());
        return sysTableEditRulesMapper.insertSysTableEditRules(sysTableEditRules);
    }

    /**
     * 修改校验规则配置项
     *
     * @param sysTableEditRules 校验规则配置项
     * @return 结果
     */
    @Override
    public int updateSysTableEditRules(SysTableEditRules sysTableEditRules) {
        sysTableEditRules.setUpdateTime(DateUtils.getNowDate());
        return sysTableEditRulesMapper.updateSysTableEditRules(sysTableEditRules);
    }

    /**
     * 批量删除校验规则配置项
     *
     * @param editRulesIds 需要删除的校验规则配置项主键
     * @return 结果
     */
    @Override
    public int deleteSysTableEditRulesByEditRulesIds(Long[] editRulesIds) {
        return sysTableEditRulesMapper.deleteSysTableEditRulesByEditRulesIds(editRulesIds);
    }

    /**
     * 删除校验规则配置项信息
     *
     * @param editRulesId 校验规则配置项主键
     * @return 结果
     */
    @Override
    public int deleteSysTableEditRulesByEditRulesId(Long editRulesId) {
        return sysTableEditRulesMapper.deleteSysTableEditRulesByEditRulesId(editRulesId);
    }

    @Override
    public LambdaQueryWrapper<SysTableEditRules> buildQueryWrapper(SysTableEditRules sysTableEditRules) {
        LambdaQueryWrapper<SysTableEditRules> lambdaQueryWrapper = new LambdaQueryWrapper<SysTableEditRules>()
                .eq(!StringUtils.isNull(sysTableEditRules.getTableId()), SysTableEditRules::getTableId, sysTableEditRules.getTableId())
                .eq(!StringUtils.isNull(sysTableEditRules.getEditRulesId()), SysTableEditRules::getEditRulesId, sysTableEditRules.getEditRulesId())
                .like(!StringUtils.isNull(sysTableEditRules.getRemark()), SysTableEditRules::getRemark, sysTableEditRules.getRemark())
                .like(!StringUtils.isNull(sysTableEditRules.getCreateBy()), SysTableEditRules::getCreateBy, sysTableEditRules.getCreateBy())
                .like(!StringUtils.isNull(sysTableEditRules.getUpdateBy()), SysTableEditRules::getUpdateBy, sysTableEditRules.getUpdateBy())
                .ge(sysTableEditRules.getParams().get("beginCreateTime") != null, SysTableEditRules::getCreateTime, sysTableEditRules.getParams().get("beginCreateTime"))
                .le(sysTableEditRules.getParams().get("endCreateTime") != null, SysTableEditRules::getCreateTime, sysTableEditRules.getParams().get("endCreateTime"))
                .ge(sysTableEditRules.getParams().get("beginUpdateTime") != null, SysTableEditRules::getUpdateTime, sysTableEditRules.getParams().get("beginUpdateTime"))
                .le(sysTableEditRules.getParams().get("endUpdateTime") != null, SysTableEditRules::getUpdateTime, sysTableEditRules.getParams().get("endUpdateTime"))
                .orderByDesc(SysTableEditRules::getEditRulesId);
        return lambdaQueryWrapper;
    }
}
