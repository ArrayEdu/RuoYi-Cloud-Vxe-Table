package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysRoleTablePager;

/**
 * 角色功能表分页配置Mapper接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface SysRoleTablePagerMapper extends BaseMapperPlus<SysRoleTablePager> {

    /**
     * 查询角色功能表分页配置
     *
     * @param roleTableId
     * @return 角色功能表分页配置
     */
    public SysRoleTablePager selectSysRoleTablePagerByRoleTableId(Long roleTableId);

    /**
     * 查询角色功能表分页配置
     *
     * @param pagerId 角色功能表分页配置主键
     * @return 角色功能表分页配置
     */
    public SysRoleTablePager selectSysRoleTablePagerByPagerId(Long pagerId);

    /**
     * 查询角色功能表分页配置
     *
     * @param rolePagerId 角色功能表分页配置主键
     * @return 角色功能表分页配置
     */
    public SysRoleTablePager selectSysRoleTablePagerByRolePagerId(Long rolePagerId);

    /**
     * 查询角色功能表分页配置列表
     *
     * @param sysRoleTablePager 角色功能表分页配置
     * @return 角色功能表分页配置集合
     */
    public List<SysRoleTablePager> selectSysRoleTablePagerList(SysRoleTablePager sysRoleTablePager);

    /**
     * 新增角色功能表分页配置
     *
     * @param sysRoleTablePager 角色功能表分页配置
     * @return 结果
     */
    public int insertSysRoleTablePager(SysRoleTablePager sysRoleTablePager);

    /**
     * 修改角色功能表分页配置
     *
     * @param sysRoleTablePager 角色功能表分页配置
     * @return 结果
     */
    public int updateSysRoleTablePager(SysRoleTablePager sysRoleTablePager);

    /**
     * 删除角色功能表分页配置
     *
     * @param roleTableId 角色表格id
     * @return 结果
     */
    public int deleteSysRoleTablePagerByRoleTableId(Long roleTableId);

    /**
     * 删除角色功能表分页配置
     *
     * @param rolePagerId 角色功能表分页配置主键
     * @return 结果
     */
    public int deleteSysRoleTablePagerByRolePagerId(Long rolePagerId);

    /**
     * 批量删除角色功能表分页配置
     *
     * @param rolePagerIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysRoleTablePagerByRolePagerIds(Long[] rolePagerIds);
}
