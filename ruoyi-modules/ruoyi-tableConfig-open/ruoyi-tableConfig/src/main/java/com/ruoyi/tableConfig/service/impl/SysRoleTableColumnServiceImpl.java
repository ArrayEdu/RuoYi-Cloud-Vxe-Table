package com.ruoyi.tableConfig.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTableColumn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysRoleTableColumnMapper;
import com.ruoyi.tableConfig.service.ISysRoleTableColumnService;

import javax.servlet.http.HttpServletResponse;

/**
 * 表格权限列配置Service业务层处理
 *
 * @author zly
 * @date 2023-06-06
 */
@Service
public class SysRoleTableColumnServiceImpl extends ServiceImpl<SysRoleTableColumnMapper, SysRoleTableColumn> implements ISysRoleTableColumnService {
    @Autowired
    private SysRoleTableColumnMapper sysRoleTableColumnMapper;


    /**
     * 查询表格权限列配置
     *
     * @param roleTableId 表格权限列配置归属表编号
     * @return 表格权限列配置
     */
    @Override
    public List<SysRoleTableColumn> selectSysRoleTableColumnByRoleTableId(Long roleTableId) {
        return sysRoleTableColumnMapper.selectSysRoleTableColumnByRoleTableId(roleTableId);
    }

    /**
     * 查询表格权限列配置
     *
     * @param roleColumnId 表格权限列配置主键
     * @return 表格权限列配置
     */
    @Override
    public SysRoleTableColumn selectSysRoleTableColumnByRoleColumnId(Long roleColumnId) {
        return sysRoleTableColumnMapper.selectSysRoleTableColumnByRoleColumnId(roleColumnId);
    }

    /**
     * 查询表格权限列配置
     *
     * @param columnId    菜单表格列
     * @param roleTableId 角色表格id
     * @return 表格权限列配置
     */
    @Override
    public SysRoleTableColumn selectSysRoleTableColumnByColumnId(Long columnId, Long roleTableId) {
        return sysRoleTableColumnMapper.selectSysRoleTableColumnByColumnId(columnId, roleTableId);
    }

    /**
     * 查询表格权限列配置列表
     *
     * @param sysRoleTableColumn 表格权限列配置
     * @return 表格权限列配置
     */
    @Override
    public List<SysRoleTableColumn> selectSysRoleTableColumnList(SysRoleTableColumn sysRoleTableColumn) {
        return sysRoleTableColumnMapper.selectSysRoleTableColumnList(sysRoleTableColumn);
    }

    /**
     * 查询表格权限列配置分页列表
     *
     * @param sysRoleTableColumn 表格权限列配置
     * @param pageQuery          分页配置
     * @return
     */
    @Override
    public TableDataInfo<SysRoleTableColumn> selectSysRoleTableColumnPage(SysRoleTableColumn sysRoleTableColumn, PageQuery pageQuery) {
        LambdaQueryWrapper<SysRoleTableColumn> lqw = buildQueryWrapper(sysRoleTableColumn);
        Page<SysRoleTableColumn> page = sysRoleTableColumnMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增表格权限列配置
     *
     * @param sysRoleTableColumn 表格权限列配置
     * @return 结果
     */
    @Override
    public int insertSysRoleTableColumn(SysRoleTableColumn sysRoleTableColumn) {
        sysRoleTableColumn.setCreateTime(DateUtils.getNowDate());
        return sysRoleTableColumnMapper.insertSysRoleTableColumn(sysRoleTableColumn);
    }

    /**
     * 修改表格权限列配置
     *
     * @param sysRoleTableColumn 表格权限列配置
     * @return 结果
     */
    @Override
    public int updateSysRoleTableColumn(SysRoleTableColumn sysRoleTableColumn) {
        sysRoleTableColumn.setUpdateTime(DateUtils.getNowDate());
        return sysRoleTableColumnMapper.updateSysRoleTableColumn(sysRoleTableColumn);
    }

    /**
     * 批量删除表格权限列配置
     *
     * @param roleColumnIds 需要删除的表格权限列配置主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableColumnByRoleColumnIds(Long[] roleColumnIds) {
        return sysRoleTableColumnMapper.deleteSysRoleTableColumnByRoleColumnIds(roleColumnIds);
    }

    /**
     * 删除表格权限列配置信息
     *
     * @param columnId 表格权限列配置主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableColumnByColumnId(Long columnId) {
        return sysRoleTableColumnMapper.deleteSysRoleTableColumnByColumnId(columnId);
    }

    /**
     * 删除表格权限列配置信息
     *
     * @param roleColumnId 表格权限列配置主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableColumnByRoleColumnId(Long roleColumnId) {
        return sysRoleTableColumnMapper.deleteSysRoleTableColumnByRoleColumnId(roleColumnId);
    }

    /**
     * 表格权限列配置查询条件
     *
     * @param sysRoleTableColumn
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysRoleTableColumn> buildQueryWrapper(SysRoleTableColumn sysRoleTableColumn) {
        LambdaQueryWrapper<SysRoleTableColumn> lambdaQueryWrapper = new LambdaQueryWrapper<SysRoleTableColumn>()
                .eq(!StringUtils.isNull(sysRoleTableColumn.getColumnId()), SysRoleTableColumn::getColumnId, sysRoleTableColumn.getColumnId())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getRoleColumnId()), SysRoleTableColumn::getRoleColumnId, sysRoleTableColumn.getRoleColumnId())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getRoleTableId()), SysRoleTableColumn::getRoleTableId, sysRoleTableColumn.getRoleTableId())
                .like(!StringUtils.isNull(sysRoleTableColumn.getColumnName()), SysRoleTableColumn::getColumnName, sysRoleTableColumn.getColumnName())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getColumnComment()), SysRoleTableColumn::getColumnComment, sysRoleTableColumn.getColumnComment())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getField()), SysRoleTableColumn::getField, sysRoleTableColumn.getField())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getTitle()), SysRoleTableColumn::getTitle, sysRoleTableColumn.getTitle())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getIsIgnore()), SysRoleTableColumn::getIsIgnore, sysRoleTableColumn.getIsIgnore())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getVisible()), SysRoleTableColumn::getVisible, sysRoleTableColumn.getVisible())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getWidth()), SysRoleTableColumn::getWidth, sysRoleTableColumn.getWidth())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getMinWidth()), SysRoleTableColumn::getMinWidth, sysRoleTableColumn.getMinWidth())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getResizable()), SysRoleTableColumn::getResizable, sysRoleTableColumn.getResizable())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getAlign()), SysRoleTableColumn::getAlign, sysRoleTableColumn.getAlign())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getType()), SysRoleTableColumn::getType, sysRoleTableColumn.getType())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getFixed()), SysRoleTableColumn::getFixed, sysRoleTableColumn.getFixed())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getHeaderAlign()), SysRoleTableColumn::getHeaderAlign, sysRoleTableColumn.getHeaderAlign())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getFooterAlign()), SysRoleTableColumn::getFooterAlign, sysRoleTableColumn.getFooterAlign())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getShowOverflow()), SysRoleTableColumn::getShowOverflow, sysRoleTableColumn.getShowOverflow())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getShowHeaderOverflow()), SysRoleTableColumn::getShowHeaderOverflow, sysRoleTableColumn.getShowHeaderOverflow())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getShowFooterOverflow()), SysRoleTableColumn::getShowFooterOverflow, sysRoleTableColumn.getShowFooterOverflow())
                .like(!StringUtils.isNull(sysRoleTableColumn.getClassName()), SysRoleTableColumn::getClassName, sysRoleTableColumn.getClassName())
                .like(!StringUtils.isNull(sysRoleTableColumn.getHeaderClassName()), SysRoleTableColumn::getHeaderClassName, sysRoleTableColumn.getHeaderClassName())
                .like(!StringUtils.isNull(sysRoleTableColumn.getFooterClassName()), SysRoleTableColumn::getFooterClassName, sysRoleTableColumn.getFooterClassName())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getFormatter()), SysRoleTableColumn::getFormatter, sysRoleTableColumn.getFormatter())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getSortable()), SysRoleTableColumn::getSortable, sysRoleTableColumn.getSortable())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getSortBy()), SysRoleTableColumn::getSortBy, sysRoleTableColumn.getSortBy())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getSortType()), SysRoleTableColumn::getSortType, sysRoleTableColumn.getSortType())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getExtParams()), SysRoleTableColumn::getExtParams, sysRoleTableColumn.getExtParams())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getTreeNode()), SysRoleTableColumn::getTreeNode, sysRoleTableColumn.getTreeNode())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getSlots()), SysRoleTableColumn::getSlots, sysRoleTableColumn.getSlots())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getFilters()), SysRoleTableColumn::getFilters, sysRoleTableColumn.getFilters())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getFilterMultiple()), SysRoleTableColumn::getFilterMultiple, sysRoleTableColumn.getFilterMultiple())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getFilterMethod()), SysRoleTableColumn::getFilterMethod, sysRoleTableColumn.getFilterMethod())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getFilterResetMethod()), SysRoleTableColumn::getFilterResetMethod, sysRoleTableColumn.getFilterResetMethod())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getFilterRecoverMethod()), SysRoleTableColumn::getFilterRecoverMethod, sysRoleTableColumn.getFilterRecoverMethod())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getFilterRender()), SysRoleTableColumn::getFilterRender, sysRoleTableColumn.getFilterRender())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getExportMethod()), SysRoleTableColumn::getExportMethod, sysRoleTableColumn.getExportMethod())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getFooterExportMethod()), SysRoleTableColumn::getFooterExportMethod, sysRoleTableColumn.getFooterExportMethod())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getTitlePrefix()), SysRoleTableColumn::getTitlePrefix, sysRoleTableColumn.getTitlePrefix())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getCellType()), SysRoleTableColumn::getCellType, sysRoleTableColumn.getCellType())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getCellRender()), SysRoleTableColumn::getCellRender, sysRoleTableColumn.getCellRender())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getEditRender()), SysRoleTableColumn::getEditRender, sysRoleTableColumn.getEditRender())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getContentRender()), SysRoleTableColumn::getContentRender, sysRoleTableColumn.getContentRender())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getColId()), SysRoleTableColumn::getColId, sysRoleTableColumn.getColId())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getSortNo()), SysRoleTableColumn::getSortNo, sysRoleTableColumn.getSortNo())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getDictType()), SysRoleTableColumn::getDictType, sysRoleTableColumn.getDictType())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getRemark()), SysRoleTableColumn::getRemark, sysRoleTableColumn.getRemark())
                .eq(!StringUtils.isNull(sysRoleTableColumn.getCreateBy()), SysRoleTableColumn::getCreateBy, sysRoleTableColumn.getCreateBy())
                .ge(sysRoleTableColumn.getParams().get("beginCreateTime") != null, SysRoleTableColumn::getCreateTime, sysRoleTableColumn.getParams().get("beginCreateTime"))
                .le(sysRoleTableColumn.getParams().get("endCreateTime") != null, SysRoleTableColumn::getCreateTime, sysRoleTableColumn.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysRoleTableColumn.getUpdateBy()), SysRoleTableColumn::getUpdateBy, sysRoleTableColumn.getUpdateBy())
                .ge(sysRoleTableColumn.getParams().get("beginUpdateTime") != null, SysRoleTableColumn::getUpdateTime, sysRoleTableColumn.getParams().get("beginUpdateTime"))
                .le(sysRoleTableColumn.getParams().get("endUpdateTime") != null, SysRoleTableColumn::getUpdateTime, sysRoleTableColumn.getParams().get("endUpdateTime"))
                .orderByDesc(SysRoleTableColumn::getUpdateTime);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysRoleTableColumn> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysRoleTableColumn> iPage = sysRoleTableColumnMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper<>());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysRoleTableColumnMapper.selectBatchIds(exceel.getIds());
        } else {
            SysRoleTableColumn sysRoleTableColumn = JSON.parseObject(String.valueOf(exceel.getQuery()), SysRoleTableColumn.class);
            list = sysRoleTableColumnMapper.selectSysRoleTableColumnList(sysRoleTableColumn);
        }
        String fileName = exceel.getFilename().equals(null) ? "表格权限列配置数据" : exceel.getFilename();
        Set<String> column = Arrays.stream(exceel.getFields().stream().map(m -> m.getField()).collect(Collectors.toList()).stream().toArray(String[]::new)).collect(Collectors.toSet());
        ExcelUtil.exportExcel(list, fileName, SysRoleTableColumn.class, response, column);
    }
}
