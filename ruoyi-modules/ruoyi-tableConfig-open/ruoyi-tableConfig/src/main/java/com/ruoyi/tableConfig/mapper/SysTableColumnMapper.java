package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysTableColumn;
import org.apache.ibatis.annotations.Param;

/**
 * 功能表格列配置Mapper接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface SysTableColumnMapper extends BaseMapperPlus<SysTableColumn> {

    /**
     * 查询功能表格列配置
     *
     * @param tableId 功能表格id
     * @return 功能表格列配置
     */
    public List<SysTableColumn> selectSysTableColumnByTableId(Long tableId);

    /**
     * 查询功能表格列配置
     *
     * @param columnId 功能表格列配置主键
     * @return 功能表格列配置
     */
    public SysTableColumn selectSysTableColumnByColumnId(Long columnId);

    /**
     * 查询功能表格列配置列表
     *
     * @param sysTableColumn 功能表格列配置
     * @return 功能表格列配置集合
     */
    public List<SysTableColumn> selectSysTableColumnList(SysTableColumn sysTableColumn);

    /**
     * 新增功能表格列配置
     *
     * @param sysTableColumn 功能表格列配置
     * @return 结果
     */
    public int insertSysTableColumn(SysTableColumn sysTableColumn);

    /**
     * 修改功能表格列配置
     *
     * @param sysTableColumn 功能表格列配置
     * @return 结果
     */
    public int updateSysTableColumn(SysTableColumn sysTableColumn);

    /**
     * 删除功能表格列配置
     *
     * @param columnId 功能表格列配置主键
     * @return 结果
     */
    public int deleteSysTableColumnByColumnId(Long columnId);

    /**
     * 删除功能表格列配置
     *
     * @param columnId 功能表格列配置主键
     * @return 结果
     */
    public int deleteSysTableColumnByColumnIds(Long[] columnId);

    /**
     * 删除功能表格列配置
     *
     * @param tableIds 功能表格id
     * @return 结果
     */
    public int deleteSysTableColumnByTableIds(Long[] tableIds);

    /**
     * 删除功能表格列配置
     *
     * @param tableId 功能表格id
     * @return 结果
     */
    public int deleteSysTableColumnByTableId(Long tableId);

    /**
     * 根据表名称查询列信息
     *
     * @param tableName 表名称
     * @return 列信息
     */
    @InterceptorIgnore(dataPermission = "true")
    public List<SysTableColumn> selectDbTableColumnsByName(String tableName);
}
