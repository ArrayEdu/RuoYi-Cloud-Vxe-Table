package com.ruoyi.tableConfig.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableColumn;
import com.ruoyi.tableConfig.api.domain.SysRoleTableProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysRoleTableProxyService;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 角色功能表数据代理Controller
 *
 * @author zly
 * @date 2023-06-06
 */
@RestController
@RequestMapping("/roleTableProxy")
public class SysRoleTableProxyController extends BaseController {
    @Autowired
    private ISysRoleTableProxyService sysRoleTableProxyService;

    /**
     * 查询角色功能表数据代理列表
     */
    @RequiresPermissions("system:proxy:list")
    @GetMapping("/list")
    public TableDataInfo list(SysRoleTableProxy sysRoleTableProxy) {
        return TableDataInfo.build(sysRoleTableProxyService.selectSysRoleTableProxyList(sysRoleTableProxy));
    }

    /**
     * 查询角色功能表数据代理分页列表
     */
    @RequiresPermissions("system:proxy:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysRoleTableProxyService.selectSysRoleTableProxyPage(ajaxData.toJavaObject(SysRoleTableProxy.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出角色功能表数据代理列表
     */
    @RequiresPermissions("system:proxy:export")
    @Log(title = "角色功能表数据代理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysRoleTableProxy sysRoleTableProxy) {
        List<SysRoleTableProxy> list = sysRoleTableProxyService.selectSysRoleTableProxyList(sysRoleTableProxy);
        ExcelUtil.exportExcel(list, "角色功能表数据代理数据", SysRoleTableProxy.class, response);
    }

    /**
     * vxe-table导出角色功能表数据代理列表
     */
    @RequiresPermissions("system:proxy:export")
    @Log(title = "角色功能表数据代理", businessType = BusinessType.EXPORT)
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        sysRoleTableProxyService.export(response, params);
    }

    /**
     * 获取角色功能表数据代理详细信息
     */
    @RequiresPermissions("system:proxy:query")
    @GetMapping(value = "/{roleProxyId}")
    public AjaxResult getInfo(@PathVariable("roleProxyId") Long roleProxyId) {
        return success(sysRoleTableProxyService.selectSysRoleTableProxyByRoleProxyId(roleProxyId));
    }

    /**
     * 获取角色功能表数据代理详细信息(批量)
     */
    @RequiresPermissions("system:proxy:query")
    @GetMapping(value = "/in/{proxyIds}")
    public AjaxResult getInfos(@PathVariable(value = "proxyIds", required = true) Long[] proxyIds) {
        List<SysRoleTableProxy> list = sysRoleTableProxyService.listByIds(Arrays.asList(proxyIds));
        return AjaxResult.success(list);
    }

    /**
     * 新增角色功能表数据代理
     */
    @RequiresPermissions("system:proxy:add")
    @Log(title = "角色功能表数据代理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysRoleTableProxy sysRoleTableProxy) {
        return toAjax(sysRoleTableProxyService.insertSysRoleTableProxy(sysRoleTableProxy));
    }

    /**
     * 新增角色功能表数据代理(批量)
     */
    @RequiresPermissions("system:proxy:add")
    @Log(title = "角色功能表数据代理", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@RequestBody JSONArray jsonArray) {
        if (!sysRoleTableProxyService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableProxy.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改角色功能表数据代理
     */
    @RequiresPermissions("system:proxy:edit")
    @Log(title = "角色功能表数据代理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysRoleTableProxy sysRoleTableProxy) {
        return toAjax(sysRoleTableProxyService.updateSysRoleTableProxy(sysRoleTableProxy));
    }

    /**
     * 修改角色功能表数据代理(批量)
     */
    @RequiresPermissions("system:proxy:edit")
    @Log(title = "角色功能表数据代理", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!sysRoleTableProxyService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableProxy.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除角色功能表数据代理
     */
    @RequiresPermissions("system:proxy:remove")
    @Log(title = "角色功能表数据代理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{proxyIds}")
    public AjaxResult removes(@PathVariable(value = "proxyIds", required = true) Long[] proxyIds) {
        return toAjax(sysRoleTableProxyService.removeByIds(Arrays.asList(proxyIds)));
    }

    /**
     * 导入模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "角色功能表数据代理模板", SysRoleTableProxy.class, response);
    }
}
