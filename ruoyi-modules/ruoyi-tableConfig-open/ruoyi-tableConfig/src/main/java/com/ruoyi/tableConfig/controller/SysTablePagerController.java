package com.ruoyi.tableConfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTablePager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysTablePagerService;
import com.ruoyi.common.mybatis.core.controller.BaseController;

/**
 * 功能表分页配置Controller
 *
 * @author zly
 * @date 2023-04-25
 */
@RestController
@RequestMapping("/menuTablePager")
public class SysTablePagerController extends BaseController {
    @Autowired
    private ISysTablePagerService sysTablePagerService;

    /**
     * 查询功能表分页配置列表
     */
    @RequiresPermissions("tableConfig:tablePager:list")
    @GetMapping("/list")
    public TableDataInfo list(SysTablePager sysTablePager) {
        return TableDataInfo.build(sysTablePagerService.selectSysTablePagerList(sysTablePager));
    }

    /**
     * 查询功能表分页配置分页列表
     */
    @RequiresPermissions("tableConfig:tablePager:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysTablePagerService.selectSysTablePagerPage(ajaxData.toJavaObject(SysTablePager.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出功能表分页配置列表
     */
    @RequiresPermissions("tableConfig:tablePager:export")
    @Log(title = "功能表分页配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysTablePager sysTablePager) {
        List<SysTablePager> list = sysTablePagerService.selectSysTablePagerList(sysTablePager);
        ExcelUtil.exportExcel(list, "功能表分页配置数据", SysTablePager.class, response);
    }

    /**
     * 获取功能表分页配置详细信息
     */
    @RequiresPermissions("tableConfig:tablePager:query")
    @GetMapping(value = "/{pagerId}")
    public AjaxResult getInfo(@PathVariable("pagerId") Long pagerId) {
        return success(sysTablePagerService.selectSysTablePagerByPagerId(pagerId));
    }

    /**
     * 新增功能表分页配置
     */
    @RequiresPermissions("tableConfig:tablePager:add")
    @Log(title = "功能表分页配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTablePager sysTablePager) {
        return toAjax(sysTablePagerService.insertSysTablePager(sysTablePager));
    }

    /**
     * 修改功能表分页配置
     */
    @RequiresPermissions("tableConfig:tablePager:edit")
    @Log(title = "功能表分页配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTablePager sysTablePager) {
        return toAjax(sysTablePagerService.updateSysTablePager(sysTablePager));
    }

    /**
     * 删除功能表分页配置
     */
    @RequiresPermissions("tableConfig:tablePager:remove")
    @Log(title = "功能表分页配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{pagerIds}")
    public AjaxResult remove(@PathVariable Long[] pagerIds) {
        return toAjax(sysTablePagerService.deleteSysTablePagerByPagerIds(pagerIds));
    }
}
