package com.ruoyi.tableConfig.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.api.domain.SysUserTableColumn;
import com.ruoyi.tableConfig.service.ISysUserTableColumnService;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.http.HttpServletResponse;

/**
 * 用户表格列配置Controller
 *
 * @author zly
 * @date 2023-06-13
 */
@RestController
@RequestMapping("/userColumn")
public class SysUserTableColumnController extends BaseController {
    @Autowired
    private ISysUserTableColumnService sysUserTableColumnService;

    /**
     * 查询用户表格列配置列表
     */
    @RequiresPermissions("system:userColumn:list")
    @GetMapping("/list")
    public TableDataInfo list(SysUserTableColumn sysUserTableColumn) {
        return TableDataInfo.build(sysUserTableColumnService.selectSysUserTableColumnList(sysUserTableColumn));
    }

    /**
     * 查询用户表格列配置分页列表
     */
    @RequiresPermissions("system:userColumn:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysUserTableColumnService.selectSysUserTableColumnPage(ajaxData.toJavaObject(SysUserTableColumn.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出用户表格列配置列表
     */
    @RequiresPermissions("system:userColumn:export")
    @Log(title = "用户表格列配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysUserTableColumn sysUserTableColumn) {
        List<SysUserTableColumn> list = sysUserTableColumnService.selectSysUserTableColumnList(sysUserTableColumn);
        ExcelUtil.exportExcel(list, "用户表格列配置数据", SysUserTableColumn.class, response);
    }

    /**
     * vxe-table导出用户表格列配置列表
     */
    @RequiresPermissions("system:userColumn:export")
    @Log(title = "用户表格列配置", businessType = BusinessType.EXPORT)
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        sysUserTableColumnService.export(response, params);
    }

    /**
     * 获取用户表格列配置详细信息
     */
    @RequiresPermissions("system:userColumn:query")
    @GetMapping(value = "/{userColumnId}")
    public AjaxResult getInfo(@PathVariable("userColumnId") Long userColumnId) {
        return success(sysUserTableColumnService.selectSysUserTableColumnByUserColumnId(userColumnId));
    }

    /**
     * 获取用户表格列配置详细信息(批量)
     */
    @RequiresPermissions("system:userColumn:query")
    @GetMapping(value = "/in/{userColumnIds}")
    public AjaxResult getInfos(@PathVariable(value = "userColumnIds", required = true) Long[] userColumnIds) {
        List<SysUserTableColumn> list = sysUserTableColumnService.listByIds(Arrays.asList(userColumnIds));
        return AjaxResult.success(list);
    }

    /**
     * 新增用户表格列配置
     */
    @RequiresPermissions("system:userColumn:add")
    @Log(title = "用户表格列配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysUserTableColumn sysUserTableColumn) {
        return toAjax(sysUserTableColumnService.insertSysUserTableColumn(sysUserTableColumn));
    }

    /**
     * 新增用户表格列配置(批量)
     */
    @RequiresPermissions("system:userColumn:add")
    @Log(title = "用户表格列配置", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@RequestBody JSONArray jsonArray) {
        if (!sysUserTableColumnService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysUserTableColumn.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改用户表格列配置
     */
    @RequiresPermissions("system:userColumn:edit")
    @Log(title = "用户表格列配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysUserTableColumn sysUserTableColumn) {
        return toAjax(sysUserTableColumnService.updateSysUserTableColumn(sysUserTableColumn));
    }

    /**
     * 修改用户表格列配置(批量)
     */
    @RequiresPermissions("system:userColumn:edit")
    @Log(title = "用户表格列配置", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!sysUserTableColumnService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysUserTableColumn.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除用户表格列配置
     */
    @RequiresPermissions("system:userColumn:remove")
    @Log(title = "用户表格列配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userColumnIds}")
    public AjaxResult removes(@PathVariable(value = "userColumnIds", required = true) Long[] userColumnIds) {
        return toAjax(sysUserTableColumnService.removeByIds(Arrays.asList(userColumnIds)));
    }

    /**
     * 导入模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "用户表格列配置模板", SysUserTableColumn.class, response);
    }
}
