package com.ruoyi.tableConfig.remote;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.ruoyi.common.security.annotation.InnerAuth;
import com.ruoyi.tableConfig.api.domain.SysUserTableColumn;
import com.ruoyi.tableConfig.api.domain.SysUserTableFormItem;
import com.ruoyi.tableConfig.service.ISysRoleTableService;
import com.ruoyi.tableConfig.service.ISysUserTableColumnService;
import com.ruoyi.tableConfig.service.ISysUserTableFormItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@Api(tags = "用户表格远程调用接口")
@RequestMapping("/remoteUser")
public class RemoteSysUserTableController extends BaseController {

    @Autowired
    private ISysUserTableColumnService sysUserTableColumnService;
    @Autowired
    private ISysUserTableFormItemService sysUserTableFormItemService;
    @Autowired
    private ISysRoleTableService sysRoleTableService;

    /**
     * 获取用户表格列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取用户表格列配置详细信息")
    @PostMapping(value = "/userTableColumn/getUserColumnInfo")
    public R getUserColumnInfo(SysUserTableColumn sysUserTableColumn) {
        return R.ok(sysUserTableColumnService.selectSysUserTableColumnList(sysUserTableColumn));
    }

    /**
     * 获取用户表格列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取用户表格列配置详细信息")
    @GetMapping(value = "/userTableColumn/getInfoByColumnId/{userColumnId}")
    public R getColumnInfoByColumnId(@PathVariable("userColumnId") Long userColumnId) {
        return R.ok(sysUserTableColumnService.selectSysUserTableColumnByUserColumnId(userColumnId));
    }

    /**
     * 通过角色表格id获取用户表格列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过角色表格id获取用户表格列配置详细信息")
    @GetMapping(value = "/userTableColumn/getInfoByRoleTableId/{roleTableId}")
    public R getColumnInfoByRoleTableId(@PathVariable("roleTableId") Long roleTableId) {
        return R.ok(sysUserTableColumnService.selectSysUserTableColumnByRoleTableId(roleTableId));
    }

    /**
     * 通过用户id获取用户表格列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过用户id获取用户表格列配置详细信息")
    @GetMapping(value = "/userTableColumn/getInfoByUserId/{userId}")
    public R getColumnInfoByUserId(@PathVariable("userId") Long userId) {
        return R.ok(sysUserTableColumnService.selectSysUserTableColumnByUserId(userId));
    }

    /**
     * 新增用户表格列配置
     */
    @InnerAuth
    @ApiOperation(value = "新增用户表格列配置")
    @PostMapping(value = "/userTableColumn/add")
    public R userTableColumnAdd(@RequestBody SysUserTableColumn sysUserTableColumn) {
        return R.ok(sysUserTableColumnService.insertSysUserTableColumn(sysUserTableColumn));
    }

    /**
     * 修改用户表格列配置
     */
    @InnerAuth
    @ApiOperation(value = "新增用户表格列配置")
    @PutMapping(value = "/userTableColumn/edit")
    public R userTableColumnEdit(@RequestBody SysUserTableColumn sysUserTableColumn) {
        return R.ok(sysUserTableColumnService.updateSysUserTableColumn(sysUserTableColumn));
    }

    /**
     * 删除用户表格列配置
     */
    @InnerAuth
    @ApiOperation(value = "删除用户表格列配置")
    @DeleteMapping("/userTableColumn/deleteUserColumnId/{userColumnIds}")
    public R userTableColumnRemoves(@PathVariable(value = "userColumnIds", required = true) Long[] userColumnIds) {
        return R.ok(sysUserTableColumnService.removeByIds(Arrays.asList(userColumnIds)));
    }

    /**
     * 通过用户id删除用户表格列配置
     */
    @InnerAuth
    @ApiOperation(value = "通过用户id删除用户表格列配置")
    @DeleteMapping("/userTableColumn/deleteSysUserTableColumnByUserId/{userId}")
    public R userTableColumnRemoveByUserId(@PathVariable(value = "userId", required = true) Long userId) {
        return R.ok(sysUserTableColumnService.deleteSysUserTableColumnByUserId(userId));
    }

    /**
     * 获取用户表单列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取用户表单列配置详细信息")
    @PostMapping(value = "/userTableFormItem/getInfo")
    public R getFormItemInfo(SysUserTableFormItem sysUserTableFormItem) {
        return R.ok(sysUserTableFormItemService.selectSysUserTableFormItemList(sysUserTableFormItem));
    }

    /**
     * 获取用户表单列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "获取用户表单列配置详细信息")
    @GetMapping(value = "/userTableFormItem/getInfoByColumnId/{userItemId}")
    public R getFormItemInfoByUserItemId(@PathVariable("userItemId") Long userItemId) {
        return R.ok(sysUserTableFormItemService.selectSysUserTableFormItemByUserItemId(userItemId));
    }

    /**
     * 通过角色表单id获取用户表单列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过角色表单id获取用户表单列配置详细信息")
    @GetMapping(value = "/userTableFormItem/getInfoByRoleTableId/{roleFormId}")
    public R getFormItemInfoByRoleFormId(@PathVariable("roleFormId") Long roleFormId) {
        return R.ok(sysUserTableFormItemService.selectSysUserTableFormItemByRoleFormId(roleFormId));
    }

    /**
     * 通过用户id获取用户表单列配置详细信息
     */
    @InnerAuth
    @ApiOperation(value = "通过用户id获取用户表单列配置详细信息")
    @GetMapping(value = "/userTableFormItem/getInfoByUserId/{userId}")
    public R getFormItemInfoByUserId(@PathVariable("userId") Long userId) {
        return R.ok(sysUserTableFormItemService.selectSysUserTableFormItemByUserId(userId));
    }

    /**
     * 新增用户表单列配置
     */
    @InnerAuth
    @ApiOperation(value = "新增用户表单列配置")
    @PostMapping(value = "/userTableFormItem/add")
    public R userTableFormItemAdd(@RequestBody SysUserTableFormItem sysUserTableFormItem) {
        return R.ok(sysUserTableFormItemService.insertSysUserTableFormItem(sysUserTableFormItem));
    }

    /**
     * 修改用户表单列配置
     */
    @InnerAuth
    @ApiOperation(value = "修改用户表单列配置")
    @PutMapping(value = "/userTableFormItem/edit")
    public R userTableFormItemEdit(@RequestBody SysUserTableFormItem sysUserTableFormItem) {
        return R.ok(sysUserTableFormItemService.updateSysUserTableFormItem(sysUserTableFormItem));
    }

    /**
     * 删除用户表单列配置
     */
    @InnerAuth
    @ApiOperation(value = "删除用户表单列配置")
    @DeleteMapping("/userTableFormItem/deleteUserItemId/{userItemIds}")
    public R userTableFormItemRemoves(@PathVariable(value = "userItemIds", required = true) Long[] userItemIds) {
        return R.ok(sysUserTableFormItemService.removeByIds(Arrays.asList(userItemIds)));
    }

    /**
     * 通过用户id删除用户表单列配置
     */
    @InnerAuth
    @ApiOperation(value = "通过用户id删除用户表单列配置")
    @DeleteMapping("/userTableFormItem/deleteSysUserTableFormItemByUserId/{userId}")
    public R userTableFormItemRemoveByUserId(@PathVariable(value = "userId", required = true) Long userId) {
        return R.ok(sysUserTableFormItemService.deleteSysUserTableFormItemByUserId(userId));
    }

    /**
     * 同步用户表格数据
     *
     * @return
     */
    @InnerAuth
    @GetMapping("/asyncUserTable/{update}")
    public R asyncUserTable(@PathVariable("update") Boolean update) {
        sysRoleTableService.asyncUserTable(update);
        return R.ok();
    }

}
