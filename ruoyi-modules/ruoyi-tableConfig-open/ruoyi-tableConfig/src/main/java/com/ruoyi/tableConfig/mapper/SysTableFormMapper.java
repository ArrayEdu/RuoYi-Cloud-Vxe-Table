package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysTableForm;

/**
 * 功能表查询Mapper接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface SysTableFormMapper extends BaseMapperPlus<SysTableForm> {
    /**
     * 查询功能表查询
     *
     * @param tableId 功能表查询 表格id
     * @return 功能表查询
     */
    public SysTableForm selectSysTableFormByTableId(Long tableId);

    /**
     * 查询功能表查询
     *
     * @param formId 功能表查询主键
     * @return 功能表查询
     */
    public SysTableForm selectSysTableFormByFormId(Long formId);

    /**
     * 查询功能表查询列表
     *
     * @param sysTableForm 功能表查询
     * @return 功能表查询集合
     */
    public List<SysTableForm> selectSysTableFormList(SysTableForm sysTableForm);

    /**
     * 新增功能表查询
     *
     * @param sysTableForm 功能表查询
     * @return 结果
     */
    public int insertSysTableForm(SysTableForm sysTableForm);

    /**
     * 修改功能表查询
     *
     * @param sysTableForm 功能表查询
     * @return 结果
     */
    public int updateSysTableForm(SysTableForm sysTableForm);

    /**
     * 删除功能表查询
     *
     * @param formId 功能表查询主键
     * @return 结果
     */
    public int deleteSysTableFormByFormId(Long formId);

    /**
     * 批量删除功能表查询
     *
     * @param formIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysTableFormByFormIds(Long[] formIds);

    /**
     * 批量删除功能表查询
     *
     * @param tableIds 需要删除的表格id
     * @return 结果
     */
    public int deleteSysTableFormByTableIds(Long[] tableIds);

    /**
     * 删除功能表查询
     *
     * @param tableId 需要删除的表格id
     * @return 结果
     */
    public int deleteSysTableFormByTableId(Long tableId);
}
