package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysTableEditConfig;

/**
 * 可编辑配置项Mapper接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface SysTableEditConfigMapper extends BaseMapperPlus<SysTableEditConfig> {


    /**
     * 查询可编辑配置项
     *
     * @param tableId 表格id
     * @return 可编辑配置项
     */
    public SysTableEditConfig selectSysTableEditConfigByTableId(Long tableId);

    /**
     * 查询可编辑配置项
     *
     * @param editConfigId 可编辑配置项主键
     * @return 可编辑配置项
     */
    public SysTableEditConfig selectSysTableEditConfigByEditConfigId(Long editConfigId);

    /**
     * 查询可编辑配置项列表
     *
     * @param sysTableEditConfig 可编辑配置项
     * @return 可编辑配置项集合
     */
    public List<SysTableEditConfig> selectSysTableEditConfigList(SysTableEditConfig sysTableEditConfig);

    /**
     * 新增可编辑配置项
     *
     * @param sysTableEditConfig 可编辑配置项
     * @return 结果
     */
    public int insertSysTableEditConfig(SysTableEditConfig sysTableEditConfig);

    /**
     * 修改可编辑配置项
     *
     * @param sysTableEditConfig 可编辑配置项
     * @return 结果
     */
    public int updateSysTableEditConfig(SysTableEditConfig sysTableEditConfig);

    /**
     * 删除可编辑配置项
     *
     * @param editConfigId 可编辑配置项主键
     * @return 结果
     */
    public int deleteSysTableEditConfigByEditConfigId(Long editConfigId);

    /**
     * 批量删除可编辑配置项
     *
     * @param editConfigIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysTableEditConfigByEditConfigIds(Long[] editConfigIds);

    /**
     * 批量删除可编辑配置项
     *
     * @param tableIds 需要删除的表格id
     * @return 结果
     */
    public int deleteSysTableEditConfigByTableIds(Long[] tableIds);

    /**
     * 删除可编辑配置项
     *
     * @param tableId 需要删除的表格id
     * @return 结果
     */
    public int deleteSysTableEditConfigByTableId(Long tableId);
}
