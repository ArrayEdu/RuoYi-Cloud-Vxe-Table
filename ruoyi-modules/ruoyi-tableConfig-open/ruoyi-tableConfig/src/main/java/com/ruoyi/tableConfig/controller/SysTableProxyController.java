package com.ruoyi.tableConfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysTableProxyService;
import com.ruoyi.common.mybatis.core.controller.BaseController;

/**
 * 功能表数据代理Controller
 *
 * @author zly
 * @date 2023-04-25
 */
@RestController
@RequestMapping("/menuTableProxy")
public class SysTableProxyController extends BaseController {
    @Autowired
    private ISysTableProxyService sysTableProxyService;

    /**
     * 查询功能表数据代理列表
     */
    @RequiresPermissions("tableConfig:tableProxy:list")
    @GetMapping("/list")
    public TableDataInfo list(SysTableProxy sysTableProxy) {
        return TableDataInfo.build(sysTableProxyService.selectSysTableProxyList(sysTableProxy));
    }

    /**
     * 查询功能表数据代理分页列表
     */
    @RequiresPermissions("tableConfig:tableProxy:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysTableProxyService.selectSysTableProxyPage(ajaxData.toJavaObject(SysTableProxy.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出功能表数据代理列表
     */
    @RequiresPermissions("tableConfig:tableProxy:export")
    @Log(title = "功能表数据代理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysTableProxy sysTableProxy) {
        List<SysTableProxy> list = sysTableProxyService.selectSysTableProxyList(sysTableProxy);
        ExcelUtil.exportExcel(list, "功能表数据代理数据", SysTableProxy.class, response);
    }

    /**
     * 获取功能表数据代理详细信息
     */
    @RequiresPermissions("tableConfig:tableProxy:query")
    @GetMapping(value = "/{proxyId}")
    public AjaxResult getInfo(@PathVariable("proxyId") Long proxyId) {
        return success(sysTableProxyService.selectSysTableProxyByProxyId(proxyId));
    }

    /**
     * 新增功能表数据代理
     */
    @RequiresPermissions("tableConfig:tableProxy:add")
    @Log(title = "功能表数据代理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTableProxy sysTableProxy) {
        return toAjax(sysTableProxyService.insertSysTableProxy(sysTableProxy));
    }

    /**
     * 修改功能表数据代理
     */
    @RequiresPermissions("tableConfig:tableProxy:edit")
    @Log(title = "功能表数据代理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTableProxy sysTableProxy) {
        return toAjax(sysTableProxyService.updateSysTableProxy(sysTableProxy));
    }

    /**
     * 删除功能表数据代理
     */
    @RequiresPermissions("tableConfig:tableProxy:remove")
    @Log(title = "功能表数据代理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{proxyIds}")
    public AjaxResult remove(@PathVariable Long[] proxyIds) {
        return toAjax(sysTableProxyService.deleteSysTableProxyByProxyIds(proxyIds));
    }
}
