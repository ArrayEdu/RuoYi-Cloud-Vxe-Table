package com.ruoyi.tableConfig.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTablePager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysRoleTablePagerMapper;
import com.ruoyi.tableConfig.service.ISysRoleTablePagerService;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色功能表分页配置Service业务层处理
 *
 * @author zly
 * @date 2023-06-06
 */
@Service
public class SysRoleTablePagerServiceImpl extends ServiceImpl<SysRoleTablePagerMapper, SysRoleTablePager> implements ISysRoleTablePagerService {
    @Autowired
    private SysRoleTablePagerMapper sysRoleTablePagerMapper;

    /**
     * 查询角色功能表分页配置
     *
     * @param roleTableId
     * @return 角色功能表分页配置
     */
    @Override
    public SysRoleTablePager selectSysRoleTablePagerByRoleTableId(Long roleTableId) {
        return sysRoleTablePagerMapper.selectSysRoleTablePagerByRoleTableId(roleTableId);
    }

    /**
     * 查询角色功能表分页配置
     *
     * @param pagerId 角色功能表分页配置主键
     * @return 角色功能表分页配置
     */
    @Override
    public SysRoleTablePager selectSysRoleTablePagerByPagerId(Long pagerId) {
        return sysRoleTablePagerMapper.selectSysRoleTablePagerByPagerId(pagerId);
    }

    @Override
    public SysRoleTablePager selectSysRoleTablePagerByRolePagerId(Long rolePagerId) {
        return sysRoleTablePagerMapper.selectSysRoleTablePagerByRolePagerId(rolePagerId);
    }

    /**
     * 查询角色功能表分页配置列表
     *
     * @param sysRoleTablePager 角色功能表分页配置
     * @return 角色功能表分页配置
     */
    @Override
    public List<SysRoleTablePager> selectSysRoleTablePagerList(SysRoleTablePager sysRoleTablePager) {
        return sysRoleTablePagerMapper.selectSysRoleTablePagerList(sysRoleTablePager);
    }

    /**
     * 查询角色功能表分页配置分页列表
     *
     * @param sysRoleTablePager 角色功能表分页配置
     * @param pageQuery         查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysRoleTablePager> selectSysRoleTablePagerPage(SysRoleTablePager sysRoleTablePager, PageQuery pageQuery) {
        LambdaQueryWrapper<SysRoleTablePager> lqw = buildQueryWrapper(sysRoleTablePager);
        Page<SysRoleTablePager> page = sysRoleTablePagerMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增角色功能表分页配置
     *
     * @param sysRoleTablePager 角色功能表分页配置
     * @return 结果
     */
    @Override
    public int insertSysRoleTablePager(SysRoleTablePager sysRoleTablePager) {
        sysRoleTablePager.setCreateTime(DateUtils.getNowDate());
        return sysRoleTablePagerMapper.insertSysRoleTablePager(sysRoleTablePager);
    }

    /**
     * 修改角色功能表分页配置
     *
     * @param sysRoleTablePager 角色功能表分页配置
     * @return 结果
     */
    @Override
    public int updateSysRoleTablePager(SysRoleTablePager sysRoleTablePager) {
        sysRoleTablePager.setUpdateTime(DateUtils.getNowDate());
        return sysRoleTablePagerMapper.updateSysRoleTablePager(sysRoleTablePager);
    }

    /**
     * 批量删除角色功能表分页配置
     *
     * @param rolePagerIds 需要删除的角色功能表分页配置主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTablePagerByRolePagerIds(Long[] rolePagerIds) {
        return sysRoleTablePagerMapper.deleteSysRoleTablePagerByRolePagerIds(rolePagerIds);
    }

    /**
     * 删除角色功能表分页配置信息
     *
     * @param rolePagerId 角色功能表分页配置主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTablePagerByRolePagerId(Long rolePagerId) {
        return sysRoleTablePagerMapper.deleteSysRoleTablePagerByRolePagerId(rolePagerId);
    }

    /**
     * 角色功能表分页配置查询条件
     *
     * @param sysRoleTablePager
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysRoleTablePager> buildQueryWrapper(SysRoleTablePager sysRoleTablePager) {
        LambdaQueryWrapper<SysRoleTablePager> lambdaQueryWrapper = new LambdaQueryWrapper<SysRoleTablePager>()
                .eq(!StringUtils.isNull(sysRoleTablePager.getRolePagerId()), SysRoleTablePager::getRolePagerId, sysRoleTablePager.getRolePagerId())
                .eq(!StringUtils.isNull(sysRoleTablePager.getPagerId()), SysRoleTablePager::getPagerId, sysRoleTablePager.getPagerId())
                .eq(!StringUtils.isNull(sysRoleTablePager.getRoleTableId()), SysRoleTablePager::getRoleTableId, sysRoleTablePager.getRoleTableId())
                .eq(!StringUtils.isNull(sysRoleTablePager.getLayouts()), SysRoleTablePager::getLayouts, sysRoleTablePager.getLayouts())
                .eq(!StringUtils.isNull(sysRoleTablePager.getCurrentPage()), SysRoleTablePager::getCurrentPage, sysRoleTablePager.getCurrentPage())
                .eq(!StringUtils.isNull(sysRoleTablePager.getPageSize()), SysRoleTablePager::getPageSize, sysRoleTablePager.getPageSize())
                .eq(!StringUtils.isNull(sysRoleTablePager.getPagerCount()), SysRoleTablePager::getPagerCount, sysRoleTablePager.getPagerCount())
                .eq(!StringUtils.isNull(sysRoleTablePager.getPageSizes()), SysRoleTablePager::getPageSizes, sysRoleTablePager.getPageSizes())
                .eq(!StringUtils.isNull(sysRoleTablePager.getAlign()), SysRoleTablePager::getAlign, sysRoleTablePager.getAlign())
                .eq(!StringUtils.isNull(sysRoleTablePager.getBorder()), SysRoleTablePager::getBorder, sysRoleTablePager.getBorder())
                .eq(!StringUtils.isNull(sysRoleTablePager.getBackground()), SysRoleTablePager::getBackground, sysRoleTablePager.getBackground())
                .eq(!StringUtils.isNull(sysRoleTablePager.getPerfect()), SysRoleTablePager::getPerfect, sysRoleTablePager.getPerfect())
                .like(!StringUtils.isNull(sysRoleTablePager.getClassName()), SysRoleTablePager::getClassName, sysRoleTablePager.getClassName())
                .eq(!StringUtils.isNull(sysRoleTablePager.getAutoHidden()), SysRoleTablePager::getAutoHidden, sysRoleTablePager.getAutoHidden())
                .eq(!StringUtils.isNull(sysRoleTablePager.getIconPrevPage()), SysRoleTablePager::getIconPrevPage, sysRoleTablePager.getIconPrevPage())
                .eq(!StringUtils.isNull(sysRoleTablePager.getIconJumpPrev()), SysRoleTablePager::getIconJumpPrev, sysRoleTablePager.getIconJumpPrev())
                .eq(!StringUtils.isNull(sysRoleTablePager.getIconJumpNext()), SysRoleTablePager::getIconJumpNext, sysRoleTablePager.getIconJumpNext())
                .eq(!StringUtils.isNull(sysRoleTablePager.getIconnextPage()), SysRoleTablePager::getIconnextPage, sysRoleTablePager.getIconnextPage())
                .eq(!StringUtils.isNull(sysRoleTablePager.getIconJumpMore()), SysRoleTablePager::getIconJumpMore, sysRoleTablePager.getIconJumpMore())
                .eq(!StringUtils.isNull(sysRoleTablePager.getEnabled()), SysRoleTablePager::getEnabled, sysRoleTablePager.getEnabled())
                .eq(!StringUtils.isNull(sysRoleTablePager.getSlots()), SysRoleTablePager::getSlots, sysRoleTablePager.getSlots())
                .eq(!StringUtils.isNull(sysRoleTablePager.getCreateBy()), SysRoleTablePager::getCreateBy, sysRoleTablePager.getCreateBy())
                .ge(sysRoleTablePager.getParams().get("beginCreateTime") != null, SysRoleTablePager::getCreateTime, sysRoleTablePager.getParams().get("beginCreateTime"))
                .le(sysRoleTablePager.getParams().get("endCreateTime") != null, SysRoleTablePager::getCreateTime, sysRoleTablePager.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysRoleTablePager.getUpdateBy()), SysRoleTablePager::getUpdateBy, sysRoleTablePager.getUpdateBy())
                .ge(sysRoleTablePager.getParams().get("beginUpdateTime") != null, SysRoleTablePager::getUpdateTime, sysRoleTablePager.getParams().get("beginUpdateTime"))
                .le(sysRoleTablePager.getParams().get("endUpdateTime") != null, SysRoleTablePager::getUpdateTime, sysRoleTablePager.getParams().get("endUpdateTime"))
                .eq(!StringUtils.isNull(sysRoleTablePager.getRemark()), SysRoleTablePager::getRemark, sysRoleTablePager.getRemark())
                .orderByDesc(SysRoleTablePager::getRemark);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysRoleTablePager> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysRoleTablePager> iPage = sysRoleTablePagerMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper<>());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysRoleTablePagerMapper.selectBatchIds(exceel.getIds());
        } else {
            SysRoleTablePager sysRoleTablePager = JSON.parseObject(String.valueOf(exceel.getQuery()), SysRoleTablePager.class);
            list = sysRoleTablePagerMapper.selectSysRoleTablePagerList(sysRoleTablePager);
        }
        String fileName = exceel.getFilename().equals(null) ? "角色功能表分页配置数据" : exceel.getFilename();
        Set<String> column = Arrays.stream(exceel.getFields().stream().map(m -> m.getField()).collect(Collectors.toList()).stream().toArray(String[]::new)).collect(Collectors.toSet());
        ExcelUtil.exportExcel(list, fileName, SysRoleTablePager.class, response, column);
    }
}
