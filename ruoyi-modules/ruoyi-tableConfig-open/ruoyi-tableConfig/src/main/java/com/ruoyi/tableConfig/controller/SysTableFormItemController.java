package com.ruoyi.tableConfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableFormItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysTableFormItemService;
import com.ruoyi.common.mybatis.core.controller.BaseController;

/**
 * 表单配置项列Controller
 *
 * @author zly
 * @date 2023-04-25
 */
@RestController
@RequestMapping("/menuTableFormItem")
public class SysTableFormItemController extends BaseController {
    @Autowired
    private ISysTableFormItemService sysTableFormItemService;

    /**
     * 查询表单配置项列列表
     */
    @RequiresPermissions("tableConfig:tableFormItem:list")
    @GetMapping("/list")
    public TableDataInfo list(SysTableFormItem sysTableFormItem) {
        return TableDataInfo.build(sysTableFormItemService.selectSysTableFormItemList(sysTableFormItem));
    }

    /**
     * 查询表单配置项列分页列表
     */
    @RequiresPermissions("tableConfig:tableFormItem:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysTableFormItemService.selectSysTableFormItemPage(ajaxData.toJavaObject(SysTableFormItem.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出表单配置项列列表
     */
    @RequiresPermissions("tableConfig:tableFormItem:export")
    @Log(title = "表单配置项列", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysTableFormItem sysTableFormItem) {
        List<SysTableFormItem> list = sysTableFormItemService.selectSysTableFormItemList(sysTableFormItem);
        ExcelUtil.exportExcel(list, "表单配置项列数据", SysTableFormItem.class, response);
    }

    /**
     * 获取表单配置项列详细信息
     */
    @RequiresPermissions("tableConfig:tableFormItem:query")
    @GetMapping(value = "/{itemId}")
    public AjaxResult getInfo(@PathVariable("itemId") Long itemId) {
        return success(sysTableFormItemService.selectSysTableFormItemByItemId(itemId));
    }

    /**
     * 新增表单配置项列
     */
    @RequiresPermissions("tableConfig:tableFormItem:add")
    @Log(title = "表单配置项列", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTableFormItem sysTableFormItem) {
        return toAjax(sysTableFormItemService.insertSysTableFormItem(sysTableFormItem));
    }

    /**
     * 修改表单配置项列
     */
    @RequiresPermissions("tableConfig:tableFormItem:edit")
    @Log(title = "表单配置项列", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTableFormItem sysTableFormItem) {
        return toAjax(sysTableFormItemService.updateSysTableFormItem(sysTableFormItem));
    }

    /**
     * 删除表单配置项列
     */
    @RequiresPermissions("tableConfig:tableFormItem:remove")
    @Log(title = "表单配置项列", businessType = BusinessType.DELETE)
    @DeleteMapping("/{itemIds}")
    public AjaxResult remove(@PathVariable Long[] itemIds) {
        return toAjax(sysTableFormItemService.deleteSysTableFormItemByItemIds(itemIds));
    }
}
