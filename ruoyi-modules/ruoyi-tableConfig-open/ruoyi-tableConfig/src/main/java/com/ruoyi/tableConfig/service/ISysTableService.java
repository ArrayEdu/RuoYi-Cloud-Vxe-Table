package com.ruoyi.tableConfig.service;

import java.sql.SQLException;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson2.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTable;

/**
 * 功能表格Service接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface ISysTableService extends IServicePlus<SysTable> {

    /**
     * 查询功能表格
     *
     * @param menuId 表格id
     * @return 功能表格
     */
    public List<SysTable> selectSysTableByMenuId(Long menuId);

    /**
     * 查询功能表格
     *
     * @param tableId 功能表格主键
     * @return 功能表格
     */
    public SysTable selectSysTableByTableId(Long tableId);

    /**
     * 查询功能表格列表
     *
     * @param sysTable 功能表格
     * @return 功能表格集合
     */
    public List<SysTable> selectSysTableList(SysTable sysTable);

    /**
     * 查询功能表格分页列表
     *
     * @param sysTable  功能表格
     * @param pageQuery 查询配置
     * @return
     */
    public TableDataInfo<SysTable> selectSysTablePage(SysTable sysTable, PageQuery pageQuery);

    /**
     * 新增功能表格
     *
     * @param sysTable 功能表格
     * @return 结果
     */
    public int insertSysTable(SysTable sysTable);

    /**
     * 修改功能表格
     *
     * @param sysTable 功能表格
     * @return 结果
     */
    public int updateSysTable(SysTable sysTable);

    /**
     * 批量删除功能表格
     *
     * @param tableIds 需要删除的功能表格主键集合
     * @return 结果
     */
    public int deleteSysTableByTableIds(Long[] tableIds);

    /**
     * 删除功能表格信息
     *
     * @param tableId 功能表格主键
     * @return 结果
     */
    public int deleteSysTableByTableId(Long tableId);

    public LambdaQueryWrapper<SysTable> buildQueryWrapper(SysTable sysTable);

    /**
     * 查询据库列表
     *
     * @param sysTable 业务信息
     * @return 数据库表集合
     */
    public TableDataInfo<SysTable> selectDbTablePage(SysTable sysTable, PageQuery pageQuery);

    /**
     * 查询据库列表
     *
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    public List<SysTable> selectDbTableListByNames(String[] tableNames);

    /**
     * 导入表结构
     *
     * @param tableList 导入表列表
     */
    public void importGenTable(List<SysTable> tableList, Long menuId);

    /**
     * 同步数据库
     *
     * @param tableId 表id
     */
    public void synchDb(Long tableId);

    /**
     * 获取表格配置(修改表配置时使用)
     */
    public JSONObject getMenuTableConfig(Long tableId);

    /**
     * 新增功能表格
     *
     * @param jsonObject 表格数据
     * @return
     */
    public boolean addMenuTableConfig(JSONObject jsonObject);

    /**
     * 获取表格配置
     *
     * @param jsonObject
     * @return
     */
    public SysRoleTable getTableConfig(JSONObject jsonObject);

    /**
     * 表单筛选可添加项
     *
     * @param jsonObject
     * @return
     */
    public SysRoleTable setUserFilterFormItem(JSONObject jsonObject);

    /**
     * 删除用户表单筛选项
     *
     * @param jsonObject
     * @return
     */
    public SysRoleTable deleteUserFilterFormItem(JSONObject jsonObject);

    /**
     * 设置用户表格列宽
     *
     * @return
     */
    public boolean columnWidth(JSONObject jsonObject);

    /**
     * 设置用户表格列顺序
     *
     * @param jsonArray
     * @return
     */
    public boolean columnOrder(JSONArray jsonArray);
}
