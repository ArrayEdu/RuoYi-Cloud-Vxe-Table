package com.ruoyi.tableConfig.domain.tableVo;

import com.ruoyi.tableConfig.api.domain.SysUserTableColumn;
import lombok.Data;

/**
 * 表格列设置
 */
@Data
public class UserColumnVo {
    private String field;
    private String fiexd;
    private Boolean isChecked;
    private Boolean isDisabled;
    private String title;
    private SysUserTableColumn columnInfo;
}
