package com.ruoyi.tableConfig.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTableProxy;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色功能表数据代理Service接口
 *
 * @author zly
 * @date 2023-06-06
 */
public interface ISysRoleTableProxyService extends IServicePlus<SysRoleTableProxy> {
    /**
     * 查询角色功能表数据代理
     *
     * @param roleTableId
     * @return 角色功能表数据代理
     */
    public SysRoleTableProxy selectSysRoleTableProxyByRoleTableId(Long roleTableId);

    /**
     * 查询角色功能表数据代理
     *
     * @param proxyId 角色功能表数据代理主键
     * @return 角色功能表数据代理
     */
    public SysRoleTableProxy selectSysRoleTableProxyByProxyId(Long proxyId);

    /**
     * 查询角色功能表数据代理
     *
     * @param roleProxyId 角色功能表数据代理
     * @return 角色功能表数据代理
     */
    public SysRoleTableProxy selectSysRoleTableProxyByRoleProxyId(Long roleProxyId);

    /**
     * 查询角色功能表数据代理列表
     *
     * @param sysRoleTableProxy 角色功能表数据代理
     * @return 角色功能表数据代理集合
     */
    public List<SysRoleTableProxy> selectSysRoleTableProxyList(SysRoleTableProxy sysRoleTableProxy);

    /**
     * 查询角色功能表数据代理分页列表
     *
     * @param sysRoleTableProxy 角色功能表数据代理
     * @param pageQuery         查询配置
     * @return
     */
    public TableDataInfo<SysRoleTableProxy> selectSysRoleTableProxyPage(SysRoleTableProxy sysRoleTableProxy, PageQuery pageQuery);

    /**
     * 新增角色功能表数据代理
     *
     * @param sysRoleTableProxy 角色功能表数据代理
     * @return 结果
     */
    public int insertSysRoleTableProxy(SysRoleTableProxy sysRoleTableProxy);

    /**
     * 修改角色功能表数据代理
     *
     * @param sysRoleTableProxy 角色功能表数据代理
     * @return 结果
     */
    public int updateSysRoleTableProxy(SysRoleTableProxy sysRoleTableProxy);

    /**
     * 批量删除角色功能表数据代理
     *
     * @param roleProxyIds 需要删除的角色功能表数据代理主键集合
     * @return 结果
     */
    public int deleteSysRoleTableProxyByRoleProxyIds(Long[] roleProxyIds);

    /**
     * 删除角色功能表数据代理信息
     *
     * @param roleProxyId 角色功能表数据代理主键
     * @return 结果
     */
    public int deleteSysRoleTableProxyByRoleProxyId(Long roleProxyId);

    /**
     * 角色功能表数据代理查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysRoleTableProxy> buildQueryWrapper(SysRoleTableProxy sysRoleTableProxy);

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
