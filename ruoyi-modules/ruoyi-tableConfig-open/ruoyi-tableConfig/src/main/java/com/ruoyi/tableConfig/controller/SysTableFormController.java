package com.ruoyi.tableConfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysTableFormService;
import com.ruoyi.common.mybatis.core.controller.BaseController;

/**
 * 功能表查询Controller
 *
 * @author zly
 * @date 2023-04-25
 */
@RestController
@RequestMapping("/menuTableForm")
public class SysTableFormController extends BaseController {
    @Autowired
    private ISysTableFormService sysTableFormService;

    /**
     * 查询功能表查询列表
     */
    @RequiresPermissions("tableConfig:tableForm:list")
    @GetMapping("/list")
    public TableDataInfo list(SysTableForm sysTableForm) {
        return TableDataInfo.build(sysTableFormService.selectSysTableFormList(sysTableForm));
    }

    /**
     * 查询功能表查询分页列表
     */
    @RequiresPermissions("tableConfig:tableForm:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysTableFormService.selectSysTableFormPage(ajaxData.toJavaObject(SysTableForm.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出功能表查询列表
     */
    @RequiresPermissions("tableConfig:tableForm:export")
    @Log(title = "功能表查询", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysTableForm sysTableForm) {
        List<SysTableForm> list = sysTableFormService.selectSysTableFormList(sysTableForm);
        ExcelUtil.exportExcel(list, "功能表查询数据", SysTableForm.class, response);
    }

    /**
     * 获取功能表查询详细信息
     */
    @RequiresPermissions("tableConfig:tableForm:query")
    @GetMapping(value = "/{formId}")
    public AjaxResult getInfo(@PathVariable("formId") Long formId) {
        return success(sysTableFormService.selectSysTableFormByFormId(formId));
    }

    /**
     * 新增功能表查询
     */
    @RequiresPermissions("tableConfig:tableForm:add")
    @Log(title = "功能表查询", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTableForm sysTableForm) {
        return toAjax(sysTableFormService.insertSysTableForm(sysTableForm));
    }

    /**
     * 修改功能表查询
     */
    @RequiresPermissions("tableConfig:tableForm:edit")
    @Log(title = "功能表查询", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTableForm sysTableForm) {
        return toAjax(sysTableFormService.updateSysTableForm(sysTableForm));
    }

    /**
     * 删除功能表查询
     */
    @RequiresPermissions("tableConfig:tableForm:remove")
    @Log(title = "功能表查询", businessType = BusinessType.DELETE)
    @DeleteMapping("/{formIds}")
    public AjaxResult remove(@PathVariable Long[] formIds) {
        return toAjax(sysTableFormService.deleteSysTableFormByFormIds(formIds));
    }
}
