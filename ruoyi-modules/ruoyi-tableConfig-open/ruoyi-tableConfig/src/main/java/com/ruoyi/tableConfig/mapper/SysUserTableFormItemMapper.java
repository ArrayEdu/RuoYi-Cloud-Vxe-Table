package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysUserTableFormItem;

/**
 * 用户表单配置项列表Mapper接口
 *
 * @author zly
 * @date 2023-06-13
 */
public interface SysUserTableFormItemMapper extends BaseMapperPlus<SysUserTableFormItem> {

    /**
     * 查询用户表单配置项列表列表
     *
     * @param roleFormId 角色表格id
     * @return 用户表单配置项列表集合
     */
    public List<SysUserTableFormItem> selectSysUserTableFormItemByRoleFormId(Long roleFormId);

    /**
     * 查询用户表单配置项列表列表
     *
     * @param userId 用户id
     * @return 用户表单配置项列表集合
     */
    public List<SysUserTableFormItem> selectSysUserTableFormItemByUserId(Long userId);

    /**
     * 查询用户表单配置项列表
     *
     * @param userItemId 用户表单配置项列表主键
     * @return 用户表单配置项列表
     */
    public SysUserTableFormItem selectSysUserTableFormItemByUserItemId(Long userItemId);

    /**
     * 查询用户表单配置项列表列表
     *
     * @param sysUserTableFormItem 用户表单配置项列表
     * @return 用户表单配置项列表集合
     */
    public List<SysUserTableFormItem> selectSysUserTableFormItemList(SysUserTableFormItem sysUserTableFormItem);

    /**
     * 新增用户表单配置项列表
     *
     * @param sysUserTableFormItem 用户表单配置项列表
     * @return 结果
     */
    public int insertSysUserTableFormItem(SysUserTableFormItem sysUserTableFormItem);

    /**
     * 修改用户表单配置项列表
     *
     * @param sysUserTableFormItem 用户表单配置项列表
     * @return 结果
     */
    public int updateSysUserTableFormItem(SysUserTableFormItem sysUserTableFormItem);

    /**
     * 删除用户表单配置项列表
     *
     * @param userItemId 用户表单配置项列表主键
     * @return 结果
     */
    public int deleteSysUserTableFormItemByUserItemId(Long userItemId);

    /**
     * 删除用户表单配置项列表
     *
     * @param userId 用户id
     * @return 结果
     */
    public int deleteSysUserTableFormItemByUserId(Long userId);

    /**
     * 批量删除用户表单配置项列表
     *
     * @param userItemIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysUserTableFormItemByUserItemIds(Long[] userItemIds);
}
