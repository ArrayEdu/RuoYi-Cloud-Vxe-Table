package com.ruoyi.tableConfig.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTableEditConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysRoleTableEditConfigService;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 角色可编辑配置项Controller
 *
 * @author zly
 * @date 2023-06-06
 */
@RestController
@RequestMapping("/roleTableConfig")
public class SysRoleTableEditConfigController extends BaseController {
    @Autowired
    private ISysRoleTableEditConfigService sysRoleTableEditConfigService;

    /**
     * 查询角色可编辑配置项列表
     */
    @RequiresPermissions("system:config:list")
    @GetMapping("/list")
    public TableDataInfo list(SysRoleTableEditConfig sysRoleTableEditConfig) {
        return TableDataInfo.build(sysRoleTableEditConfigService.selectSysRoleTableEditConfigList(sysRoleTableEditConfig));
    }

    /**
     * 查询角色可编辑配置项分页列表
     */
    @RequiresPermissions("system:config:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysRoleTableEditConfigService.selectSysRoleTableEditConfigPage(ajaxData.toJavaObject(SysRoleTableEditConfig.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出角色可编辑配置项列表
     */
    @RequiresPermissions("system:config:export")
    @Log(title = "角色可编辑配置项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysRoleTableEditConfig sysRoleTableEditConfig) {
        List<SysRoleTableEditConfig> list = sysRoleTableEditConfigService.selectSysRoleTableEditConfigList(sysRoleTableEditConfig);
        ExcelUtil.exportExcel(list, "角色可编辑配置项数据", SysRoleTableEditConfig.class, response);
    }

    /**
     * vxe-table导出角色可编辑配置项列表
     */
    @RequiresPermissions("system:config:export")
    @Log(title = "角色可编辑配置项", businessType = BusinessType.EXPORT)
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        sysRoleTableEditConfigService.export(response, params);
    }

    /**
     * 获取角色可编辑配置项详细信息
     */
    @RequiresPermissions("system:config:query")
    @GetMapping(value = "/{roleEditConfigId}")
    public AjaxResult getInfo(@PathVariable("roleEditConfigId") Long roleEditConfigId) {
        return success(sysRoleTableEditConfigService.selectSysRoleTableEditConfigByRoleEditConfigId(roleEditConfigId));
    }

    /**
     * 获取角色可编辑配置项详细信息(批量)
     */
    @RequiresPermissions("system:config:query")
    @GetMapping(value = "/in/{editConfigIds}")
    public AjaxResult getInfos(@PathVariable(value = "editConfigIds", required = true) Long[] editConfigIds) {
        List<SysRoleTableEditConfig> list = sysRoleTableEditConfigService.listByIds(Arrays.asList(editConfigIds));
        return AjaxResult.success(list);
    }

    /**
     * 新增角色可编辑配置项
     */
    @RequiresPermissions("system:config:add")
    @Log(title = "角色可编辑配置项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysRoleTableEditConfig sysRoleTableEditConfig) {
        return toAjax(sysRoleTableEditConfigService.insertSysRoleTableEditConfig(sysRoleTableEditConfig));
    }

    /**
     * 新增角色可编辑配置项(批量)
     */
    @RequiresPermissions("system:config:add")
    @Log(title = "角色可编辑配置项", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@RequestBody JSONArray jsonArray) {
        if (!sysRoleTableEditConfigService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableEditConfig.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改角色可编辑配置项
     */
    @RequiresPermissions("system:config:edit")
    @Log(title = "角色可编辑配置项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysRoleTableEditConfig sysRoleTableEditConfig) {
        return toAjax(sysRoleTableEditConfigService.updateSysRoleTableEditConfig(sysRoleTableEditConfig));
    }

    /**
     * 修改角色可编辑配置项(批量)
     */
    @RequiresPermissions("system:config:edit")
    @Log(title = "角色可编辑配置项", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!sysRoleTableEditConfigService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleTableEditConfig.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除角色可编辑配置项
     */
    @RequiresPermissions("system:config:remove")
    @Log(title = "角色可编辑配置项", businessType = BusinessType.DELETE)
    @DeleteMapping("/{editConfigIds}")
    public AjaxResult removes(@PathVariable(value = "editConfigIds", required = true) Long[] editConfigIds) {
        return toAjax(sysRoleTableEditConfigService.removeByIds(Arrays.asList(editConfigIds)));
    }

    /**
     * 导入模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "角色可编辑配置项模板", SysRoleTableEditConfig.class, response);
    }
}
