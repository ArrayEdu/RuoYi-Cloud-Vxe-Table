package com.ruoyi.tableConfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysTableEditConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.service.ISysTableEditConfigService;
import com.ruoyi.common.mybatis.core.controller.BaseController;

/**
 * 可编辑配置项Controller
 *
 * @author zly
 * @date 2023-04-25
 */
@RestController
@RequestMapping("/menuTableEditConfig")
public class SysTableEditConfigController extends BaseController {
    @Autowired
    private ISysTableEditConfigService sysTableEditConfigService;

    /**
     * 查询可编辑配置项列表
     */
    @RequiresPermissions("tableConfig:TableEditConfig:list")
    @GetMapping("/list")
    public TableDataInfo list(SysTableEditConfig sysTableEditConfig) {
        return TableDataInfo.build(sysTableEditConfigService.selectSysTableEditConfigList(sysTableEditConfig));
    }

    /**
     * 询可编辑配置项分页列表
     */
    @RequiresPermissions("tableConfig:TableEditConfig:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysTableEditConfigService.selectSysTableEditConfigPage(ajaxData.toJavaObject(SysTableEditConfig.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出可编辑配置项列表
     */
    @RequiresPermissions("tableConfig:TableEditConfig:export")
    @Log(title = "可编辑配置项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysTableEditConfig sysTableEditConfig) {
        List<SysTableEditConfig> list = sysTableEditConfigService.selectSysTableEditConfigList(sysTableEditConfig);
        ExcelUtil.exportExcel(list, "可编辑配置项数据", SysTableEditConfig.class, response);
    }

    /**
     * 获取可编辑配置项详细信息
     */
    @RequiresPermissions("tableConfig:TableEditConfig:query")
    @GetMapping(value = "/{editConfigId}")
    public AjaxResult getInfo(@PathVariable("editConfigId") Long editConfigId) {
        return success(sysTableEditConfigService.selectSysTableEditConfigByEditConfigId(editConfigId));
    }

    /**
     * 新增可编辑配置项
     */
    @RequiresPermissions("tableConfig:TableEditConfig:add")
    @Log(title = "可编辑配置项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTableEditConfig sysTableEditConfig) {
        return toAjax(sysTableEditConfigService.insertSysTableEditConfig(sysTableEditConfig));
    }

    /**
     * 修改可编辑配置项
     */
    @RequiresPermissions("tableConfig:TableEditConfig:edit")
    @Log(title = "可编辑配置项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTableEditConfig sysTableEditConfig) {
        return toAjax(sysTableEditConfigService.updateSysTableEditConfig(sysTableEditConfig));
    }

    /**
     * 删除可编辑配置项
     */
    @RequiresPermissions("tableConfig:TableEditConfig:remove")
    @Log(title = "可编辑配置项", businessType = BusinessType.DELETE)
    @DeleteMapping("/{editConfigIds}")
    public AjaxResult remove(@PathVariable Long[] editConfigIds) {
        return toAjax(sysTableEditConfigService.deleteSysTableEditConfigByEditConfigIds(editConfigIds));
    }
}
