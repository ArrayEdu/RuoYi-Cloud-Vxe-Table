package com.ruoyi.tableConfig.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysTableForm;

/**
 * 功能表查询Service接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface ISysTableFormService extends IServicePlus<SysTableForm> {
    /**
     * 查询功能表查询
     *
     * @param tableId 功能表id
     * @return 功能表查询
     */
    public SysTableForm selectSysTableFormByTableId(Long tableId);

    /**
     * 查询功能表查询
     *
     * @param formId 功能表查询主键
     * @return 功能表查询
     */
    public SysTableForm selectSysTableFormByFormId(Long formId);

    /**
     * 查询功能表查询列表
     *
     * @param sysTableForm 功能表查询
     * @return 功能表查询集合
     */
    public List<SysTableForm> selectSysTableFormList(SysTableForm sysTableForm);

    /**
     * 查询功能表查询分页列表
     *
     * @param sysTableForm 功能表查询
     * @param pageQuery    查询配置
     * @return
     */
    public TableDataInfo<SysTableForm> selectSysTableFormPage(SysTableForm sysTableForm, PageQuery pageQuery);

    /**
     * 新增功能表查询
     *
     * @param sysTableForm 功能表查询
     * @return 结果
     */
    public int insertSysTableForm(SysTableForm sysTableForm);

    /**
     * 修改功能表查询
     *
     * @param sysTableForm 功能表查询
     * @return 结果
     */
    public int updateSysTableForm(SysTableForm sysTableForm);

    /**
     * 批量删除功能表查询
     *
     * @param formIds 需要删除的功能表查询主键集合
     * @return 结果
     */
    public int deleteSysTableFormByFormIds(Long[] formIds);

    /**
     * 删除功能表查询信息
     *
     * @param formId 功能表查询主键
     * @return 结果
     */
    public int deleteSysTableFormByFormId(Long formId);

    public LambdaQueryWrapper<SysTableForm> buildQueryWrapper(SysTableForm sysTableForm);
}
