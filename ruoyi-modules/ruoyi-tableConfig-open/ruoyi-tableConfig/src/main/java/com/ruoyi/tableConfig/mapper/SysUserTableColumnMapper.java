package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysUserTableColumn;

/**
 * 用户表格列配置Mapper接口
 *
 * @author zly
 * @date 2023-06-13
 */
public interface SysUserTableColumnMapper extends BaseMapperPlus<SysUserTableColumn> {

    /**
     * 查询用户表格列配置
     *
     * @param userId 用户id
     * @return 用户表格列配置
     */
    public List<SysUserTableColumn> selectSysUserTableColumnByUserId(Long userId);

    /**
     * 查询用户表格列配置
     *
     * @param roleTableId 角色表格id
     * @return 用户表格列配置
     */
    public List<SysUserTableColumn> selectSysUserTableColumnByRoleTableId(Long roleTableId);

    /**
     * 查询用户表格列配置
     *
     * @param userColumnId 用户表格列配置主键
     * @return 用户表格列配置
     */
    public SysUserTableColumn selectSysUserTableColumnByUserColumnId(Long userColumnId);

    /**
     * 查询用户表格列配置列表
     *
     * @param sysUserTableColumn 用户表格列配置
     * @return 用户表格列配置集合
     */
    public List<SysUserTableColumn> selectSysUserTableColumnList(SysUserTableColumn sysUserTableColumn);

    /**
     * 新增用户表格列配置
     *
     * @param sysUserTableColumn 用户表格列配置
     * @return 结果
     */
    public int insertSysUserTableColumn(SysUserTableColumn sysUserTableColumn);

    /**
     * 修改用户表格列配置
     *
     * @param sysUserTableColumn 用户表格列配置
     * @return 结果
     */
    public int updateSysUserTableColumn(SysUserTableColumn sysUserTableColumn);

    /**
     * 删除用户表格列配置
     *
     * @param userColumnId 用户表格列配置主键
     * @return 结果
     */
    public int deleteSysUserTableColumnByUserColumnId(Long userColumnId);

    /**
     * 删除用户表格列配置
     *
     * @param userId 用户id
     * @return 结果
     */
    public int deleteSysUserTableColumnByUserId(Long userId);

    /**
     * 批量删除用户表格列配置
     *
     * @param userColumnIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysUserTableColumnByUserColumnIds(Long[] userColumnIds);
}
