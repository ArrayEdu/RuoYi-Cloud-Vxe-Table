package com.ruoyi.tableConfig.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableEditConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tableConfig.mapper.SysRoleTableEditConfigMapper;
import com.ruoyi.tableConfig.service.ISysRoleTableEditConfigService;

import javax.servlet.http.HttpServletResponse;

/**
 * 角色可编辑配置项Service业务层处理
 *
 * @author zly
 * @date 2023-06-06
 */
@Service
public class SysRoleTableEditConfigServiceImpl extends ServiceImpl<SysRoleTableEditConfigMapper, SysRoleTableEditConfig> implements ISysRoleTableEditConfigService {
    @Autowired
    private SysRoleTableEditConfigMapper sysRoleTableEditConfigMapper;

    @Override
    public SysRoleTableEditConfig selectSysRoleTableEditConfigByRoleEditConfigId(Long roleEditConfigId) {
        return sysRoleTableEditConfigMapper.selectSysRoleTableEditConfigByRoleEditConfigId(roleEditConfigId);
    }

    /**
     * 查询角色可编辑配置项
     *
     * @param roleTableId 角色可编辑配置项
     * @return 角色可编辑配置项
     */
    @Override
    public SysRoleTableEditConfig selectSysRoleTableEditConfigByRoleTableId(Long roleTableId) {
        return sysRoleTableEditConfigMapper.selectSysRoleTableEditConfigByRoleTableId(roleTableId);
    }

    /**
     * 查询角色可编辑配置项
     *
     * @param editConfigId 角色可编辑配置项主键
     * @return 角色可编辑配置项
     */
    @Override
    public SysRoleTableEditConfig selectSysRoleTableEditConfigByEditConfigId(Long editConfigId) {
        return sysRoleTableEditConfigMapper.selectSysRoleTableEditConfigByEditConfigId(editConfigId);
    }

    /**
     * 查询角色可编辑配置项列表
     *
     * @param sysRoleTableEditConfig 角色可编辑配置项
     * @return 角色可编辑配置项
     */
    @Override
    public List<SysRoleTableEditConfig> selectSysRoleTableEditConfigList(SysRoleTableEditConfig sysRoleTableEditConfig) {
        return sysRoleTableEditConfigMapper.selectSysRoleTableEditConfigList(sysRoleTableEditConfig);
    }

    /**
     * 查询角色可编辑配置项分页列表
     *
     * @param sysRoleTableEditConfig 角色可编辑配置项
     * @param pageQuery              查询配置
     * @return
     */
    @Override
    public TableDataInfo<SysRoleTableEditConfig> selectSysRoleTableEditConfigPage(SysRoleTableEditConfig sysRoleTableEditConfig, PageQuery pageQuery) {
        LambdaQueryWrapper<SysRoleTableEditConfig> lqw = buildQueryWrapper(sysRoleTableEditConfig);
        Page<SysRoleTableEditConfig> page = sysRoleTableEditConfigMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增角色可编辑配置项
     *
     * @param sysRoleTableEditConfig 角色可编辑配置项
     * @return 结果
     */
    @Override
    public int insertSysRoleTableEditConfig(SysRoleTableEditConfig sysRoleTableEditConfig) {
        sysRoleTableEditConfig.setCreateTime(DateUtils.getNowDate());
        return sysRoleTableEditConfigMapper.insertSysRoleTableEditConfig(sysRoleTableEditConfig);
    }

    /**
     * 修改角色可编辑配置项
     *
     * @param sysRoleTableEditConfig 角色可编辑配置项
     * @return 结果
     */
    @Override
    public int updateSysRoleTableEditConfig(SysRoleTableEditConfig sysRoleTableEditConfig) {
        sysRoleTableEditConfig.setUpdateTime(DateUtils.getNowDate());
        return sysRoleTableEditConfigMapper.updateSysRoleTableEditConfig(sysRoleTableEditConfig);
    }

    /**
     * 批量删除角色可编辑配置项
     *
     * @param editConfigIds 需要删除的角色可编辑配置项主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableEditConfigByRoleEditConfigIds(Long[] editConfigIds) {
        return sysRoleTableEditConfigMapper.deleteSysRoleTableEditConfigByRoleEditConfigIds(editConfigIds);
    }

    /**
     * 删除角色可编辑配置项信息
     *
     * @param editConfigId 角色可编辑配置项主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleTableEditConfigByRoleEditConfigId(Long editConfigId) {
        return sysRoleTableEditConfigMapper.deleteSysRoleTableEditConfigByRoleEditConfigId(editConfigId);
    }

    /**
     * 角色可编辑配置项查询条件
     *
     * @param sysRoleTableEditConfig
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysRoleTableEditConfig> buildQueryWrapper(SysRoleTableEditConfig sysRoleTableEditConfig) {
        LambdaQueryWrapper<SysRoleTableEditConfig> lambdaQueryWrapper = new LambdaQueryWrapper<SysRoleTableEditConfig>()
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getEditConfigId()), SysRoleTableEditConfig::getEditConfigId, sysRoleTableEditConfig.getEditConfigId())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getRoleEditConfigId()), SysRoleTableEditConfig::getRoleEditConfigId, sysRoleTableEditConfig.getRoleEditConfigId())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getRoleTableId()), SysRoleTableEditConfig::getRoleTableId, sysRoleTableEditConfig.getRoleTableId())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getTriggerbase()), SysRoleTableEditConfig::getTriggerbase, sysRoleTableEditConfig.getTriggerbase())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getEnabled()), SysRoleTableEditConfig::getEnabled, sysRoleTableEditConfig.getEnabled())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getMode()), SysRoleTableEditConfig::getMode, sysRoleTableEditConfig.getMode())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getShowIcon()), SysRoleTableEditConfig::getShowIcon, sysRoleTableEditConfig.getShowIcon())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getShowStatus()), SysRoleTableEditConfig::getShowStatus, sysRoleTableEditConfig.getShowStatus())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getShowUpdateStatus()), SysRoleTableEditConfig::getShowUpdateStatus, sysRoleTableEditConfig.getShowUpdateStatus())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getShowInsertStatus()), SysRoleTableEditConfig::getShowInsertStatus, sysRoleTableEditConfig.getShowInsertStatus())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getShowAsterisk()), SysRoleTableEditConfig::getShowAsterisk, sysRoleTableEditConfig.getShowAsterisk())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getAutoClear()), SysRoleTableEditConfig::getAutoClear, sysRoleTableEditConfig.getAutoClear())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getBeforeEditMethod()), SysRoleTableEditConfig::getBeforeEditMethod, sysRoleTableEditConfig.getBeforeEditMethod())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getIcon()), SysRoleTableEditConfig::getIcon, sysRoleTableEditConfig.getIcon())
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getCreateBy()), SysRoleTableEditConfig::getCreateBy, sysRoleTableEditConfig.getCreateBy())
                .ge(sysRoleTableEditConfig.getParams().get("beginCreateTime") != null, SysRoleTableEditConfig::getCreateTime, sysRoleTableEditConfig.getParams().get("beginCreateTime"))
                .le(sysRoleTableEditConfig.getParams().get("endCreateTime") != null, SysRoleTableEditConfig::getCreateTime, sysRoleTableEditConfig.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getUpdateBy()), SysRoleTableEditConfig::getUpdateBy, sysRoleTableEditConfig.getUpdateBy())
                .ge(sysRoleTableEditConfig.getParams().get("beginUpdateTime") != null, SysRoleTableEditConfig::getUpdateTime, sysRoleTableEditConfig.getParams().get("beginUpdateTime"))
                .le(sysRoleTableEditConfig.getParams().get("endUpdateTime") != null, SysRoleTableEditConfig::getUpdateTime, sysRoleTableEditConfig.getParams().get("endUpdateTime"))
                .eq(!StringUtils.isNull(sysRoleTableEditConfig.getRemark()), SysRoleTableEditConfig::getRemark, sysRoleTableEditConfig.getRemark())
                .orderByDesc(SysRoleTableEditConfig::getRemark);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysRoleTableEditConfig> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysRoleTableEditConfig> iPage = sysRoleTableEditConfigMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper<>());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysRoleTableEditConfigMapper.selectBatchIds(exceel.getIds());
        } else {
            SysRoleTableEditConfig sysRoleTableEditConfig = JSON.parseObject(String.valueOf(exceel.getQuery()), SysRoleTableEditConfig.class);
            list = sysRoleTableEditConfigMapper.selectSysRoleTableEditConfigList(sysRoleTableEditConfig);
        }
        String fileName = exceel.getFilename().equals(null) ? "角色可编辑配置项数据" : exceel.getFilename();
        Set<String> column = Arrays.stream(exceel.getFields().stream().map(m -> m.getField()).collect(Collectors.toList()).stream().toArray(String[]::new)).collect(Collectors.toSet());
        ExcelUtil.exportExcel(list, fileName, SysRoleTableEditConfig.class, response, column);
    }
}
