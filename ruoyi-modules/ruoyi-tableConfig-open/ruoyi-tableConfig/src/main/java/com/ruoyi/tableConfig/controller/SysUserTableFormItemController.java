package com.ruoyi.tableConfig.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableColumn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.tableConfig.api.domain.SysUserTableFormItem;
import com.ruoyi.tableConfig.service.ISysUserTableFormItemService;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 用户表单配置项列表Controller
 *
 * @author zly
 * @date 2023-06-13
 */
@RestController
@RequestMapping("/userFormItem")
public class SysUserTableFormItemController extends BaseController {
    @Autowired
    private ISysUserTableFormItemService sysUserTableFormItemService;

    /**
     * 查询用户表单配置项列表列表
     */
    @RequiresPermissions("system:userFormItem:list")
    @GetMapping("/list")
    public TableDataInfo list(SysUserTableFormItem sysUserTableFormItem) {
        return TableDataInfo.build(sysUserTableFormItemService.selectSysUserTableFormItemList(sysUserTableFormItem));
    }

    /**
     * 查询用户表单配置项列表分页列表
     */
    @RequiresPermissions("system:userFormItem:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysUserTableFormItemService.selectSysUserTableFormItemPage(ajaxData.toJavaObject(SysUserTableFormItem.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出用户表单配置项列表列表
     */
    @RequiresPermissions("system:userFormItem:export")
    @Log(title = "用户表单配置项列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysUserTableFormItem sysUserTableFormItem) {
        List<SysUserTableFormItem> list = sysUserTableFormItemService.selectSysUserTableFormItemList(sysUserTableFormItem);
        ExcelUtil.exportExcel(list, "用户表单配置项列表数据", SysUserTableFormItem.class, response);
    }

    /**
     * vxe-table导出用户表单配置项列表列表
     */
    @RequiresPermissions("system:userFormItem:export")
    @Log(title = "用户表单配置项列表", businessType = BusinessType.EXPORT)
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        sysUserTableFormItemService.export(response, params);
    }

    /**
     * 获取用户表单配置项列表详细信息
     */
    @RequiresPermissions("system:userFormItem:query")
    @GetMapping(value = "/{userItemId}")
    public AjaxResult getInfo(@PathVariable("userItemId") Long userItemId) {
        return success(sysUserTableFormItemService.selectSysUserTableFormItemByUserItemId(userItemId));
    }

    /**
     * 获取用户表单配置项列表详细信息(批量)
     */
    @RequiresPermissions("system:userFormItem:query")
    @GetMapping(value = "/in/{userItemIds}")
    public AjaxResult getInfos(@PathVariable(value = "userItemIds", required = true) Long[] userItemIds) {
        List<SysUserTableFormItem> list = sysUserTableFormItemService.listByIds(Arrays.asList(userItemIds));
        return AjaxResult.success(list);
    }

    /**
     * 新增用户表单配置项列表
     */
    @RequiresPermissions("system:userFormItem:add")
    @Log(title = "用户表单配置项列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysUserTableFormItem sysUserTableFormItem) {
        return toAjax(sysUserTableFormItemService.insertSysUserTableFormItem(sysUserTableFormItem));
    }

    /**
     * 新增用户表单配置项列表(批量)
     */
    @RequiresPermissions("system:userFormItem:add")
    @Log(title = "用户表单配置项列表", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@RequestBody JSONArray jsonArray) {
        if (!sysUserTableFormItemService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysUserTableFormItem.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改用户表单配置项列表
     */
    @RequiresPermissions("system:userFormItem:edit")
    @Log(title = "用户表单配置项列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysUserTableFormItem sysUserTableFormItem) {
        return toAjax(sysUserTableFormItemService.updateSysUserTableFormItem(sysUserTableFormItem));
    }

    /**
     * 修改用户表单配置项列表(批量)
     */
    @RequiresPermissions("system:userFormItem:edit")
    @Log(title = "用户表单配置项列表", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!sysUserTableFormItemService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysUserTableFormItem.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除用户表单配置项列表
     */
    @RequiresPermissions("system:userFormItem:remove")
    @Log(title = "用户表单配置项列表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userItemIds}")
    public AjaxResult removes(@PathVariable(value = "userItemIds", required = true) Long[] userItemIds) {
        return toAjax(sysUserTableFormItemService.removeByIds(Arrays.asList(userItemIds)));
    }

    /**
     * 导入模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "用户表单配置项列表模板", SysUserTableFormItem.class, response);
    }
}
