package com.ruoyi.tableConfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson2.JSONArray;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.tableConfig.api.domain.*;
import com.ruoyi.tableConfig.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.mybatis.core.controller.BaseController;

/**
 * 功能表格Controller
 *
 * @author zly
 * @date 2023-04-25
 */
@RestController
@RequestMapping("/menuTable")
public class SysTableController extends BaseController {
    @Autowired
    private ISysTableService sysTableService;

    /**
     * 查询功能表格列表
     */
    @RequiresPermissions("tableConfig:menuTable:list")
    @GetMapping("/list")
    public TableDataInfo list(SysTable sysTable) {
        return TableDataInfo.build(sysTableService.selectSysTableList(sysTable));
    }

    /**
     * 查询功能表格分页列表
     */
    @RequiresPermissions("tableConfig:menuTable:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysTableService.selectSysTablePage(ajaxData.toJavaObject(SysTable.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出功能表格列表
     */
    @RequiresPermissions("tableConfig:menuTable:export")
    @Log(title = "功能表格", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysTable sysTable) {
        List<SysTable> list = sysTableService.selectSysTableList(sysTable);
        ExcelUtil.exportExcel(list, "功能表格数据", SysTable.class, response);
    }

    /**
     * 获取功能表格详细信息
     */
    @RequiresPermissions("tableConfig:menuTable:query")
    @GetMapping(value = "/{tableId}")
    public AjaxResult getInfo(@PathVariable("tableId") Long tableId) {
        return success(sysTableService.selectSysTableByTableId(tableId));
    }

    /**
     * 新增功能表格
     */
    @RequiresPermissions("tableConfig:menuTable:add")
    @Log(title = "功能表格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTable sysTable) {
        return toAjax(sysTableService.insertSysTable(sysTable));
    }

    /**
     * 修改功能表格
     */
    @RequiresPermissions("tableConfig:menuTable:edit")
    @Log(title = "功能表格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTable sysTable) {
        return toAjax(sysTableService.updateSysTable(sysTable));
    }

    /**
     * 删除功能表格
     */
    @RequiresPermissions("tableConfig:menuTable:remove")
    @Log(title = "功能表格", businessType = BusinessType.DELETE)
    @DeleteMapping("/{tableIds}")
    public AjaxResult remove(@PathVariable Long[] tableIds) {
        return toAjax(sysTableService.deleteSysTableByTableIds(tableIds));
    }

    /**
     * 查询数据库列表
     */
    @RequiresPermissions("system:menuTable:dbList")
    @PostMapping("/db/list")
    public TableDataInfo dataList(@RequestBody JSONObject ajaxData) {
        return sysTableService.selectDbTablePage(ajaxData.toJavaObject(SysTable.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导入表结构（保存）
     */
    @RequiresPermissions("tableConfig:menuTable:importDb")
    @Log(title = "表格权限业务", businessType = BusinessType.IMPORT)
    @PostMapping("/importTable")
    public AjaxResult importTableSave(@RequestBody JSONObject ajaxData) {
        String[] tableNames = Convert.toStrArray(ajaxData.getString("tables"));
        // 查询表信息
        List<SysTable> tableList = sysTableService.selectDbTableListByNames(tableNames);
        sysTableService.importGenTable(tableList, ajaxData.getLong("menuId"));
        return success();
    }

    /**
     * 同步数据库表
     */
    @RequiresPermissions("tableConfig:menuTable:syncDb")
    @Log(title = "表格权限业务", businessType = BusinessType.UPDATE)
    @GetMapping("/synchDb/{tableId}")
    public AjaxResult synchDb(@PathVariable("tableId") Long tableId) {
        sysTableService.synchDb(tableId);
        return success();
    }

    /**
     * 获取表格配置(修改表配置时使用)
     */
    @RequiresPermissions("tableConfig:menuTable:query")
    @GetMapping("/MenuTableConfig/{tableId}")
    public AjaxResult getMenuTableConfig(@PathVariable("tableId") Long tableId) {
        return success(sysTableService.getMenuTableConfig(tableId));
    }

    /**
     * 新增功能表格
     */
    @RequiresPermissions("tableConfig:menuTable:add")
    @Log(title = "功能表格", businessType = BusinessType.INSERT)
    @PostMapping("/MenuTableConfig")
    public AjaxResult addMenuTableConfig(@RequestBody JSONObject ajaxData) {
        return success(sysTableService.addMenuTableConfig(ajaxData));
    }

    /**
     * 获取表格配置
     */
    @RequiresPermissions("tableConfig:menuTable:query")
    @PostMapping("/getTableConfig")
    public AjaxResult getTableConfig(@RequestBody JSONObject ajaxData) {
        return success(sysTableService.getTableConfig(ajaxData));
    }

    /**
     * 表单筛选可添加项
     *
     * @param
     * @return
     */
    @PostMapping("/generate_from_filter")
    public AjaxResult setUserFilterFormItem(@RequestBody JSONObject ajaxData) {
        return success(sysTableService.setUserFilterFormItem(ajaxData));
    }

    /**
     * 删除用户表单筛选项
     *
     * @param
     * @return
     */
    @PostMapping("/delete_from_filter")
    public AjaxResult deleteUserFilterFormItem(@RequestBody JSONObject ajaxData) {
        return success(sysTableService.deleteUserFilterFormItem(ajaxData));
    }

    /**
     * 设置用户表格列宽
     *
     * @return
     */
    @PostMapping("/userColumnWidth")
    public AjaxResult columnWidth(@RequestBody JSONObject ajaxData) {
        return toAjax(sysTableService.columnWidth(ajaxData));
    }

    /**
     * 设置用户表格列顺序
     *
     * @return
     */
    @PostMapping("/userColumnOrder")
    public AjaxResult columnOrder(@RequestBody JSONArray ajaxData) {
        return toAjax(sysTableService.columnOrder(ajaxData));
    }
}
