package com.ruoyi.tableConfig.mapper;

import java.util.List;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.tableConfig.api.domain.SysTable;
import org.apache.ibatis.annotations.Param;

/**
 * 功能表格Mapper接口
 *
 * @author zly
 * @date 2023-04-25
 */
public interface SysTableMapper extends BaseMapperPlus<SysTable> {
    /**
     * 查询功能表格
     *
     * @param menuId 菜单id
     * @return 功能表格
     */
    public List<SysTable> selectSysTableByMenuId(Long menuId);

    /**
     * 查询功能表格
     *
     * @param tableId 功能表格主键
     * @return 功能表格
     */
    public SysTable selectSysTableByTableId(Long tableId);

    /**
     * 查询功能表格列表
     *
     * @param sysTable 功能表格
     * @return 功能表格集合
     */
    public List<SysTable> selectSysTableList(SysTable sysTable);

    /**
     * 新增功能表格
     *
     * @param sysTable 功能表格
     * @return 结果
     */
    public int insertSysTable(SysTable sysTable);

    /**
     * 修改功能表格
     *
     * @param sysTable 功能表格
     * @return 结果
     */
    public int updateSysTable(SysTable sysTable);

    /**
     * 删除功能表格
     *
     * @param tableId 功能表格主键
     * @return 结果
     */
    public int deleteSysTableByTableId(Long tableId);

    /**
     * 批量删除功能表格
     *
     * @param tableIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysTableByTableIds(Long[] tableIds);

    /**
     * 查询据库列表
     *
     * @param sysTable 业务信息
     * @return 数据库表集合
     */
    @InterceptorIgnore(dataPermission = "true")
    public IPage<SysTable> selectDbTablePage(@Param("page") Page<SysTable> page, @Param(Constants.WRAPPER) SysTable sysTable);

    /**
     * 查询据库列表
     *
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    @InterceptorIgnore(dataPermission = "true")
    public List<SysTable> selectDbTableListByNames(String[] tableNames);
}
