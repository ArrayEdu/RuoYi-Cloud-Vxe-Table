package com.ruoyi.job.task;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.tableConfig.api.RemoteUserTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 角色定时任务调度
 *
 * @author zly
 */
@Component("roleTask")
public class RoleTask {

    @Autowired
    private RemoteUserTableService remoteUserTableService;

    //从角色表格中同步到用户
    public void asyncUserTable(Boolean update) {
        remoteUserTableService.asyncUserTable(update, SecurityConstants.INNER);
    }

}
