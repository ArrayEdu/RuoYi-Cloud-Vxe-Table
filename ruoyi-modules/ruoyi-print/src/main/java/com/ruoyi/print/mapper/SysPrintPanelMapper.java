package com.ruoyi.print.mapper;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.print.domain.SysPrintPanel;
import org.apache.ibatis.annotations.Mapper;
/**
 * 打印模板 数据层
 *
 * @author zly
 */
@Mapper
public interface SysPrintPanelMapper extends BaseMapperPlus<SysPrintPanel> {
}
