package com.ruoyi.print.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.print.domain.SysPrintPanel;
import com.ruoyi.print.domain.SysPrintProvider;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 打印面板 服务层
 *
 * @author zly
 */
public interface ISysPrintProviderService extends IServicePlus<SysPrintProvider> {

    List<SysPrintProvider> getList(SysPrintProvider printProvider);

    IPage<SysPrintProvider> getPage(Page page, SysPrintProvider printProvider);

    TableDataInfo<SysPrintProvider> selectPrintProviderPage(SysPrintProvider printProvider, PageQuery pageQuery);

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
