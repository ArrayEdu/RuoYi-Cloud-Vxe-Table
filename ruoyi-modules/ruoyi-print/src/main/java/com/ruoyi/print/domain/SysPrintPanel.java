package com.ruoyi.print.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * 打印模板 sys_print_panel
 *
 * @author zly
 */
@Data
@TableName("sys_print_panel")
public class SysPrintPanel extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 模板ID
     */
    @ExcelProperty(value = "模板ID")
    @TableId(value = "panel_id")
    private Long panelId;

    /**
     * 面板ID
     */
    @ExcelProperty(value = "面板ID")
    private Long providerId;

    /**
     * 纸张宽度
     */
    @ExcelProperty(value = "纸张宽度")
    private String panelPaperWidth;

    /**
     * 纸张高度
     */
    @ExcelProperty(value = "纸张高度")
    private String panelPaperHeight;

    /**
     * 模板名称
     */
    @ExcelProperty(value = "模板名称")
    private String panelTitle;

    /**
     * 模板名称
     */
    @ExcelProperty(value = "模板数据")
    private String panelData;

    /**
     * 纸张类型
     */
    @ExcelProperty(value = "纸张类型")
    private String panelPaperType;

    /**
     * 旋转
     */
    @ExcelProperty(value = "旋转")
    private Boolean rotate;

}
