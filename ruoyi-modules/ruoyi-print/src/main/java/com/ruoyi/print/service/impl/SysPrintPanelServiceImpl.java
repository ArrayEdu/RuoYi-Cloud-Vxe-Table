package com.ruoyi.print.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.print.domain.SysPrintPanel;
import com.ruoyi.print.mapper.SysPrintPanelMapper;
import com.ruoyi.print.service.ISysPrintPanelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * 打印模板 服务层实现
 *
 * @author zly
 */
@Service
public class SysPrintPanelServiceImpl extends ServiceImpl<SysPrintPanelMapper, SysPrintPanel> implements ISysPrintPanelService {
    @Autowired
    private SysPrintPanelMapper sysPrintPanelMapper;

    @Override
    public List<SysPrintPanel> getList(SysPrintPanel printPanel) {
        List<SysPrintPanel> list = sysPrintPanelMapper.selectList(buildQueryWrapper(printPanel));
        return list;
    }

    @Override
    public IPage<SysPrintPanel> getPage(Page page, SysPrintPanel printPanel) {
        IPage iPage = new Page(page.getCurrent(), page.getSize());
        sysPrintPanelMapper.selectPage(iPage, buildQueryWrapper(printPanel));
        //如果当前页码值大于总页码值，执行查询操作时使用最大页码值为当前页码值
        if (page.getCurrent() > iPage.getPages()) {
            page.setCurrent(iPage.getPages());
            iPage = getPage(page, printPanel);
        }
        return iPage;
    }

    @Override
    public TableDataInfo<SysPrintPanel> selectPrintPanelPage(SysPrintPanel printPanel, PageQuery pageQuery) {
        LambdaQueryWrapper<SysPrintPanel> lqw = buildQueryWrapper(printPanel);
        Page<SysPrintPanel> page = sysPrintPanelMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    public LambdaQueryWrapper<SysPrintPanel> buildQueryWrapper(SysPrintPanel printPanel) {
        LambdaQueryWrapper<SysPrintPanel> lambdaQueryWrapper = new LambdaQueryWrapper<SysPrintPanel>()
                .like(!StringUtils.isNull(printPanel.getPanelTitle()), SysPrintPanel::getPanelTitle, printPanel.getPanelTitle())
                .eq(!StringUtils.isNull(printPanel.getPanelId()), SysPrintPanel::getPanelId, printPanel.getPanelId())
                .eq(!StringUtils.isNull(printPanel.getProviderId()), SysPrintPanel::getProviderId, printPanel.getProviderId())
                .eq(!StringUtils.isNull(printPanel.getPanelPaperWidth()), SysPrintPanel::getPanelPaperWidth, printPanel.getPanelPaperWidth())
                .eq(!StringUtils.isNull(printPanel.getPanelPaperHeight()), SysPrintPanel::getPanelPaperHeight, printPanel.getPanelPaperHeight())
                .eq(!StringUtils.isNull(printPanel.getPanelPaperType()), SysPrintPanel::getPanelPaperType, printPanel.getPanelPaperType())
                .like(!StringUtils.isNull(printPanel.getCreateBy()), SysPrintPanel::getCreateBy, printPanel.getCreateBy())
                .like(!StringUtils.isNull(printPanel.getUpdateBy()), SysPrintPanel::getUpdateBy, printPanel.getUpdateBy())
                .ge(printPanel.getParams().get("beginCreateTime") != null, SysPrintPanel::getCreateTime, printPanel.getParams().get("beginCreateTime"))
                .le(printPanel.getParams().get("endCreateTime") != null, SysPrintPanel::getCreateTime, printPanel.getParams().get("endCreateTime"))
                .ge(printPanel.getParams().get("beginUpdateTime") != null, SysPrintPanel::getUpdateTime, printPanel.getParams().get("beginUpdateTime"))
                .le(printPanel.getParams().get("endUpdateTime") != null, SysPrintPanel::getUpdateTime, printPanel.getParams().get("endUpdateTime"))
                .orderByDesc(SysPrintPanel::getPanelId);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysPrintPanel> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysPrintPanel> iPage = sysPrintPanelMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper<>());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysPrintPanelMapper.selectBatchIds(exceel.getIds());
        } else {
            SysPrintPanel printPanel = JSON.parseObject(String.valueOf(exceel.getQuery()), SysPrintPanel.class);
            LambdaQueryWrapper<SysPrintPanel> lqw = buildQueryWrapper(printPanel);
            list = sysPrintPanelMapper.selectList(lqw);
        }
        String fileName = exceel.getFilename().equals(null) ? "角色数据" : exceel.getFilename();
        ExcelUtil.exportExcel(list, fileName, SysPrintPanel.class, response);
    }
}
