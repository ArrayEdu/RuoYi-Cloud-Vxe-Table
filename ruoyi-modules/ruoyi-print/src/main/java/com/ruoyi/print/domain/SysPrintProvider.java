package com.ruoyi.print.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * 打印面板 sys_print_provider
 *
 * @author zly
 */
@Data
@TableName("sys_print_provider")
public class SysPrintProvider extends BaseEntity {

    /**
     * 面板ID
     */
    @ExcelProperty(value = "面板ID")
    @TableId(value = "provider_id")
    private Long providerId;

    /**
     * 面板名称
     */
    @ExcelProperty(value = "面板名称")
    private String providerTitle;

    /**
     * 面板数据
     */
    @ExcelProperty(value = "面板数据")
    private String providerData;
}
