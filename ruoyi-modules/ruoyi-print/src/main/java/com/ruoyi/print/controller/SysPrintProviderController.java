package com.ruoyi.print.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.print.domain.SysPrintProvider;
import com.ruoyi.print.service.ISysPrintProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 打印面板
 *
 * @author zly
 */
@RestController
@RequestMapping("/printProvider")
public class SysPrintProviderController extends BaseController {
    @Autowired
    private ISysPrintProviderService printProviderService;

    /**
     * 获取面板列表
     */
    @RequiresPermissions("system:printProvider:query")
    @GetMapping("/list")
    public AjaxResult list(@RequestParam int currentPage, @RequestParam int pageSize, SysPrintProvider printProvider) {
        List<SysPrintProvider> page = printProviderService.getList(printProvider);
        return AjaxResult.success(page);
    }

    @RequiresPermissions("system:printProvider:page")
    @PostMapping("/page")
    public TableDataInfo getPage(@RequestBody JSONObject ajaxData) {
        return printProviderService.selectPrintProviderPage(ajaxData.toJavaObject(SysPrintProvider.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 根据id查询面板
     */
    @RequiresPermissions("system:printProvider:query")
    @GetMapping(value = "/{providerId}")
    public AjaxResult getInfo(@PathVariable(value = "providerId", required = true) Long providerId) {
        SysPrintProvider printProvider = printProviderService.getById(providerId);
        return AjaxResult.success(printProvider);
    }

    /**
     * 根据id批量查询面板
     */
    @RequiresPermissions("system:printProvider:query")
    @GetMapping(value = {"/in/{providerIds}"})
    public AjaxResult getInfos(@PathVariable(value = "providerIds", required = true) Long[] providerIds) {
        List<SysPrintProvider> list = printProviderService.listByIds(Arrays.asList(providerIds));
        return AjaxResult.success(list);
    }

    /**
     * 新增面板
     */
    @RequiresPermissions("system:printProvider:add")
    @Log(title = "打印管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysPrintProvider printProvider) {
        if (!printProviderService.save(printProvider)) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 新增面板(批量)
     */
    @RequiresPermissions("system:printProvider:add")
    @Log(title = "打印管理", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@Validated @RequestBody JSONArray jsonArray) {
        if (!printProviderService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysPrintProvider.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改面板
     */
    @RequiresPermissions("system:printProvider:edit")
    @Log(title = "打印管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysPrintProvider printProvider) {
        if (!printProviderService.updateById(printProvider)) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改面板(批量)
     */
    @RequiresPermissions("system:printProvider:edit")
    @Log(title = "打印管理", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!printProviderService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysPrintProvider.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除面板（批量）
     */
    @RequiresPermissions("system:printProvider:remove")
    @Log(title = "打印管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult removes(@PathVariable(value = "ids", required = true) Long[] ids) {
        return toAjax(printProviderService.removeByIds(Arrays.asList(ids)));
    }

    /**
     * 导出
     */
    @Log(title = "打印管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:printProvider:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysPrintProvider printProvider) {
        List<SysPrintProvider> list = printProviderService.getList(printProvider);
        ExcelUtil.exportExcel(list, "打印面板", SysPrintProvider.class, response);
    }

    /**
     * vxe-table导出
     */
    @Log(title = "打印管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:printPanel:export")
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        printProviderService.export(response, params);
    }

    @Log(title = "打印管理", businessType = BusinessType.IMPORT)
    @RequiresPermissions("system:printProvider:import")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {

        return AjaxResult.success();
    }

    /**
     * 导入面板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "打印面板", SysPrintProvider.class, response);
    }
}
