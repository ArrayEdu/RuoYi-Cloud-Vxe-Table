package com.ruoyi.print.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.print.domain.SysPrintProvider;
import com.ruoyi.print.mapper.SysPrintProviderMapper;
import com.ruoyi.print.service.ISysPrintProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 打印面板 服务层实现
 *
 * @author zly
 */
@Service
public class SysPrintProviderServiceImpl extends ServiceImpl<SysPrintProviderMapper, SysPrintProvider> implements ISysPrintProviderService {
    @Autowired
    private SysPrintProviderMapper sysPrintProviderMapper;

    @Override
    public List<SysPrintProvider> getList(SysPrintProvider printProvider) {
        List<SysPrintProvider> list = sysPrintProviderMapper.selectList(null);
        return list;
    }

    @Override
    public IPage<SysPrintProvider> getPage(Page page, SysPrintProvider printProvider) {
        IPage iPage = new Page(page.getCurrent(), page.getSize());
        sysPrintProviderMapper.selectPage(iPage, buildQueryWrapper(printProvider));
        //如果当前页码值大于总页码值，执行查询操作时使用最大页码值为当前页码值
        if (page.getCurrent() > iPage.getPages()) {
            page.setCurrent(iPage.getPages());
            iPage = getPage(page, printProvider);
        }
        return iPage;
    }

    @Override
    public TableDataInfo<SysPrintProvider> selectPrintProviderPage(SysPrintProvider printProvider, PageQuery pageQuery) {
        LambdaQueryWrapper<SysPrintProvider> lqw = buildQueryWrapper(printProvider);
        Page<SysPrintProvider> page = sysPrintProviderMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    public LambdaQueryWrapper<SysPrintProvider> buildQueryWrapper(SysPrintProvider printProvider) {
        LambdaQueryWrapper<SysPrintProvider> lambdaQueryWrapper = new LambdaQueryWrapper<SysPrintProvider>()
                .like(!StringUtils.isNull(printProvider.getProviderTitle()), SysPrintProvider::getProviderTitle, printProvider.getProviderTitle())
                .eq(!StringUtils.isNull(printProvider.getProviderId()), SysPrintProvider::getProviderId, printProvider.getProviderId())
                .orderByDesc(SysPrintProvider::getProviderId);
        return lambdaQueryWrapper;
    }

    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysPrintProvider> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            Page<SysPrintProvider> iPage = sysPrintProviderMapper.selectPage(pageQuery.build(), new LambdaQueryWrapper<>());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysPrintProviderMapper.selectBatchIds(exceel.getIds());
        } else {
            SysPrintProvider printProvider = JSON.parseObject(String.valueOf(exceel.getQuery()), SysPrintProvider.class);
            LambdaQueryWrapper<SysPrintProvider> lqw = buildQueryWrapper(printProvider);
            list = sysPrintProviderMapper.selectList(lqw);
        }
        String fileName = exceel.getFilename().equals(null) ? "角色数据" : exceel.getFilename();
        ExcelUtil.exportExcel(list, fileName, SysPrintProvider.class, response);
    }
}
