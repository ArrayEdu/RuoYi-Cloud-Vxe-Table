package com.ruoyi.print.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.print.domain.SysPrintPanel;
import com.ruoyi.system.api.domain.SysRoleMenuPower;

import javax.servlet.http.HttpServletResponse;

/**
 * 打印模板 服务层
 *
 * @author zly
 */
public interface ISysPrintPanelService extends IServicePlus<SysPrintPanel> {

    List<SysPrintPanel> getList(SysPrintPanel printPanel);

    IPage<SysPrintPanel> getPage(Page page, SysPrintPanel printPanel);

    TableDataInfo<SysPrintPanel> selectPrintPanelPage(SysPrintPanel printPanel, PageQuery pageQuery);

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
