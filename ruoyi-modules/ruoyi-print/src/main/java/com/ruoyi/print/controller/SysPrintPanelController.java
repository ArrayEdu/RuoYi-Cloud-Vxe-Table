package com.ruoyi.print.controller;

import java.io.IOException;
import java.util.*;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.print.domain.SysPrintPanel;
import com.ruoyi.print.service.ISysPrintPanelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 打印模板
 *
 * @author zly
 */
@RestController
@RequestMapping("/printPanel")
public class SysPrintPanelController extends BaseController {
    @Autowired
    private ISysPrintPanelService sysPrintPanelService;

    /**
     * 获取参数配置列表
     */
    @RequiresPermissions("system:printPanel:query")
    @GetMapping("/list")
    public AjaxResult list(@RequestParam int currentPage, @RequestParam int pageSize, SysPrintPanel printPanel) {
        List<SysPrintPanel> page = sysPrintPanelService.getList(printPanel);
        return AjaxResult.success(page);
    }

    @RequiresPermissions("system:printPanel:page")
    @PostMapping("/page")
    public TableDataInfo getPage(@RequestBody JSONObject ajaxData) {
        return sysPrintPanelService.selectPrintPanelPage(ajaxData.toJavaObject(SysPrintPanel.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 根据id查询模板
     */
    @RequiresPermissions("system:printPanel:query")
    @GetMapping(value = "/{panelId}")
    public AjaxResult getInfo(@PathVariable(value = "panelId", required = true) Long panelId) {
        SysPrintPanel printPanel = sysPrintPanelService.getById(panelId);
        return AjaxResult.success(printPanel);
    }

    /**
     * 根据id批量查询模板
     */
    @RequiresPermissions("system:printPanel:query")
    @GetMapping(value = {"/in/{ids}"})
    public AjaxResult getInfos(@PathVariable(value = "ids", required = true) Long[] ids) {
        List<SysPrintPanel> list = sysPrintPanelService.listByIds(Arrays.asList(ids));
        return AjaxResult.success(list);
    }

    /**
     * 新增模板
     */
    @RequiresPermissions("system:printPanel:add")
    @Log(title = "打印管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysPrintPanel printPanel) {
        if (!sysPrintPanelService.save(printPanel)) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 新增模板(批量)
     */
    @RequiresPermissions("system:printPanel:add")
    @Log(title = "打印管理", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@RequestBody JSONArray jsonArray) {
        if (!sysPrintPanelService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysPrintPanel.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改模板
     */
    @RequiresPermissions("system:printPanel:edit")
    @Log(title = "打印管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysPrintPanel printPanel) {
        if (!sysPrintPanelService.updateById(printPanel)) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改模板(批量)
     */
    @RequiresPermissions("system:printPanel:edit")
    @Log(title = "打印管理", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!sysPrintPanelService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysPrintPanel.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除模板(批量)
     */
    @RequiresPermissions("system:printPanel:remove")
    @Log(title = "打印管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult removes(@PathVariable(value = "ids", required = true) Long[] ids) {
        return toAjax(sysPrintPanelService.removeByIds(Arrays.asList(ids)));
    }

    /**
     * 导出
     */
    @Log(title = "打印管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:printPanel:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysPrintPanel printPanel) {
        List<SysPrintPanel> list = sysPrintPanelService.getList(printPanel);
        ExcelUtil.exportExcel(list, "打印模板", SysPrintPanel.class, response);
    }

    /**
     * vxe-table导出
     */
    @Log(title = "打印管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:printPanel:export")
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        sysPrintPanelService.export(response, params);
    }

    /**
     * 导入
     */
    @Log(title = "打印管理", businessType = BusinessType.IMPORT)
    @RequiresPermissions("system:printPanel:import")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        return success();
    }

    /**
     * 导入模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "打印模板", SysPrintPanel.class, response);
    }

}
