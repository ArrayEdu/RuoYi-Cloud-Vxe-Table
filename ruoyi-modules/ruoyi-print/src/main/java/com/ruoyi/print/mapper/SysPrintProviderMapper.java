package com.ruoyi.print.mapper;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.print.domain.SysPrintProvider;
import org.apache.ibatis.annotations.Mapper;
/**
 * 打印面板 数据层
 *
 * @author zly
 */
@Mapper
public interface SysPrintProviderMapper extends BaseMapperPlus<SysPrintProvider> {
}
