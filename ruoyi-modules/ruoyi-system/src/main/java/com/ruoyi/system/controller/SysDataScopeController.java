package com.ruoyi.system.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.security.annotation.InnerAuth;
import com.ruoyi.system.api.domain.SysRoleMenuPower;
import com.ruoyi.system.service.ISysDataScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/dataScope")
public class SysDataScopeController {
    @Autowired
    private ISysDataScope sysDataScope;

    /**
     * 获取角色自定义权限语句
     */
    @InnerAuth
    @GetMapping("/roleCustom/{roleId}")
    public R<String> getRoleCustom(@PathVariable("roleId") Long roleId) {
        return R.ok(sysDataScope.getRoleCustom(roleId));
    }

    /**
     * 获取部门和下级权限语句
     */
    @InnerAuth
    @GetMapping("/dept/{deptId}")
    public R<String> getDeptAndChild(@PathVariable("deptId") Long deptId) {
        return R.ok(sysDataScope.getDeptAndChild(deptId));
    }

    /**
     * 获取角色菜单授权
     */
    @InnerAuth
    @PostMapping("/RoleMenuPower")
    public R<String> getRoleMenuPower(@RequestBody SysRoleMenuPower sysRoleMenuPower) {
        String sysRoleMenuPower1 = sysDataScope.getRoleMenuPower(sysRoleMenuPower);
        return R.ok(sysRoleMenuPower1);
    }
}
