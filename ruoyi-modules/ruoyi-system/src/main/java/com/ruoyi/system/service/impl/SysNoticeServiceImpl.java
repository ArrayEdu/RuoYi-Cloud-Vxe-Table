package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.mapper.SysNoticeMapper;
import com.ruoyi.system.service.ISysNoticeService;

/**
 * 公告 服务层实现
 *
 * @author ruoyi
 */
@Service
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeMapper, SysNotice> implements ISysNoticeService {
    @Autowired
    private SysNoticeMapper noticeMapper;

    /**
     * 查询公告信息
     *
     * @param noticeId 公告ID
     * @return 公告信息
     */
    @Override
    public SysNotice selectNoticeById(Long noticeId) {
        return noticeMapper.selectNoticeById(noticeId);
    }

    /**
     * 查询公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    @Override
    public List<SysNotice> selectNoticeList(SysNotice notice) {
        return noticeMapper.selectNoticeList(notice);
    }

    /**
     * 查询分页公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    @Override
    public TableDataInfo<SysNotice> selectNoticePage(SysNotice notice, PageQuery pageQuery) {
        LambdaQueryWrapper<SysNotice> lqw = buildQueryWrapper(notice);
        Page<SysNotice> page = noticeMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int insertNotice(SysNotice notice) {
        return noticeMapper.insertNotice(notice);
    }

    /**
     * 修改公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int updateNotice(SysNotice notice) {
        return noticeMapper.updateNotice(notice);
    }

    /**
     * 删除公告对象
     *
     * @param noticeId 公告ID
     * @return 结果
     */
    @Override
    public int deleteNoticeById(Long noticeId) {
        return noticeMapper.deleteNoticeById(noticeId);
    }

    /**
     * 批量删除公告信息
     *
     * @param noticeIds 需要删除的公告ID
     * @return 结果
     */
    @Override
    public int deleteNoticeByIds(Long[] noticeIds) {
        return noticeMapper.deleteNoticeByIds(noticeIds);
    }

    @Override
    public LambdaQueryWrapper<SysNotice> buildQueryWrapper(SysNotice notice) {
        Map<String, Object> params = notice.getParams();
        LambdaQueryWrapper<SysNotice> lambdaQueryWrapper = new LambdaQueryWrapper<SysNotice>()
                .eq(!StringUtils.isNull(notice.getNoticeId()), SysNotice::getNoticeId, notice.getNoticeId())
                .eq(!StringUtils.isNull(notice.getNoticeType()), SysNotice::getNoticeType, notice.getNoticeType())
                .eq(!StringUtils.isNull(notice.getStatus()), SysNotice::getStatus, notice.getStatus())
                .like(!StringUtils.isNull(notice.getNoticeTitle()), SysNotice::getNoticeTitle, notice.getNoticeTitle())
                .like(!StringUtils.isNull(notice.getNoticeContent()), SysNotice::getNoticeContent, notice.getNoticeContent())
                .ge(params.get("beginCreateTime") != null, SysNotice::getCreateTime, params.get("beginCreateTime"))
                .le(params.get("endCreateTime") != null, SysNotice::getCreateTime, params.get("endCreateTime"))
                .eq(!StringUtils.isNull(notice.getUpdateBy()), SysNotice::getUpdateBy, notice.getUpdateBy())
                .ge(params.get("beginUpdateTime") != null, SysNotice::getUpdateTime, params.get("beginUpdateTime"))
                .le(params.get("endUpdateTime") != null, SysNotice::getUpdateTime, params.get("endUpdateTime"))
                .eq(!StringUtils.isNull(notice.getRemark()), SysNotice::getRemark, notice.getRemark()).orderByDesc(SysNotice::getRemark);
        return lambdaQueryWrapper;
    }
}
