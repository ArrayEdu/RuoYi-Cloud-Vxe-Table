package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysRoleMenuMapper;
import com.ruoyi.system.api.domain.SysRoleMenu;
import com.ruoyi.system.service.ISysRoleMenuService;

/**
 * 角色和菜单关联表Service业务层处理
 *
 * @author zly
 * @date 2023-06-07
 */
@Service
public class SysRoleMenuServiceImpl implements ISysRoleMenuService {
    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;

    @Override
    public List<SysRoleMenu> selectSysRoleMenuByMenuId(Long menuId) {
        return sysRoleMenuMapper.selectSysRoleMenuByMenuId(menuId);
    }

    /**
     * 查询角色和菜单关联表
     *
     * @param roleId 角色和菜单关联表主键
     * @return 角色和菜单关联表
     */
    @Override
    public List<SysRoleMenu> selectSysRoleMenuByRoleId(Long roleId) {
        return sysRoleMenuMapper.selectSysRoleMenuByRoleId(roleId);
    }

    /**
     * 查询角色和菜单关联表列表
     *
     * @param sysRoleMenu 角色和菜单关联表
     * @return 角色和菜单关联表
     */
    @Override
    public List<SysRoleMenu> selectSysRoleMenuList(SysRoleMenu sysRoleMenu) {
        return sysRoleMenuMapper.selectSysRoleMenuList(sysRoleMenu);
    }

    /**
     * 角色和菜单关联表查询条件
     *
     * @param sysRoleMenu
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysRoleMenu> queryWrapper(SysRoleMenu sysRoleMenu) {
        LambdaQueryWrapper<SysRoleMenu> lambdaQueryWrapper = new LambdaQueryWrapper<SysRoleMenu>()
                .eq(!StringUtils.isNull(sysRoleMenu.getRoleId()), SysRoleMenu::getRoleId, sysRoleMenu.getRoleId())
                .eq(!StringUtils.isNull(sysRoleMenu.getMenuId()), SysRoleMenu::getMenuId, sysRoleMenu.getMenuId())
                .orderByDesc(SysRoleMenu::getMenuId);
        return lambdaQueryWrapper;
    }

}
