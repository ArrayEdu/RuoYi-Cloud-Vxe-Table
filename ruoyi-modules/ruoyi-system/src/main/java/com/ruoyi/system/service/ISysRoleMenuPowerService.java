package com.ruoyi.system.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.mybatis.core.page.IServicePlus;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.system.api.domain.SysRoleMenuPower;

import javax.servlet.http.HttpServletResponse;

/**
 * 数据角色功能授权Service接口
 *
 * @author zly
 * @date 2023-05-23
 */
public interface ISysRoleMenuPowerService extends IServicePlus<SysRoleMenuPower> {
    /**
     * 查询数据角色功能授权
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 数据角色功能授权
     */
    public SysRoleMenuPower selectSysRoleMenuPowerByRoleIdMenuId(SysRoleMenuPower sysRoleMenuPower);

    /**
     * 查询数据角色功能授权
     *
     * @param rmpId 数据角色功能授权主键
     * @return 数据角色功能授权
     */
    public SysRoleMenuPower selectSysRoleMenuPowerByRmpId(Long rmpId);

    /**
     * 查询数据角色功能授权列表
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 数据角色功能授权集合
     */
    public List<SysRoleMenuPower> selectSysRoleMenuPowerList(SysRoleMenuPower sysRoleMenuPower);

    /**
     * 新增数据角色功能授权
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 结果
     */
    public int insertSysRoleMenuPower(SysRoleMenuPower sysRoleMenuPower);

    /**
     * 修改数据角色功能授权
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 结果
     */
    public int updateSysRoleMenuPower(SysRoleMenuPower sysRoleMenuPower);

    /**
     * 批量删除数据角色功能授权
     *
     * @param rmpIds 需要删除的数据角色功能授权主键集合
     * @return 结果
     */
    public int deleteSysRoleMenuPowerByRmpIds(Long[] rmpIds);

    /**
     * 删除数据角色功能授权信息
     *
     * @param rmpId 数据角色功能授权主键
     * @return 结果
     */
    public int deleteSysRoleMenuPowerByRmpId(Long rmpId);

    /**
     * 数据角色功能授权查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysRoleMenuPower> buildQueryWrapper(SysRoleMenuPower sysRoleMenuPower);

    TableDataInfo<SysRoleMenuPower> selectSysRoleMenuPowerPage(SysRoleMenuPower sysRoleMenuPower, PageQuery pageQuery);
    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    public void export(HttpServletResponse response, JSONObject jsonObject);
}
