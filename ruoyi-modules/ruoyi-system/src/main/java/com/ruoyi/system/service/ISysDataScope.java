package com.ruoyi.system.service;

import com.ruoyi.system.api.domain.SysRoleMenuPower;

/**
 * 数据权限服务
 */
public interface ISysDataScope {
    /**
     * 获取角色自定义权限语句
     */
    String getRoleCustom(Long roleId);

    /**
     * 获取部门和下级权限语句
     */
    String getDeptAndChild(Long deptId);

    /**
     * 自定义角色菜单权限
     *
     * @param sysRoleMenuPower
     * @return
     */
    String getRoleMenuPower(SysRoleMenuPower sysRoleMenuPower);
}
