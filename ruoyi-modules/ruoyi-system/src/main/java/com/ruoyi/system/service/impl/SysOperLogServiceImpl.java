package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.api.domain.SysOperLog;
import com.ruoyi.system.mapper.SysOperLogMapper;
import com.ruoyi.system.service.ISysOperLogService;

/**
 * 操作日志 服务层处理
 *
 * @author ruoyi
 */
@Service
public class SysOperLogServiceImpl extends ServiceImpl<SysOperLogMapper, SysOperLog> implements ISysOperLogService {
    @Autowired
    private SysOperLogMapper operLogMapper;

    /**
     * 新增操作日志
     *
     * @param operLog 操作日志对象
     * @return 结果
     */
    @Override
    public int insertOperlog(SysOperLog operLog) {
        return operLogMapper.insertOperlog(operLog);
    }

    /**
     * 查询系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    @Override
    public List<SysOperLog> selectOperLogList(SysOperLog operLog) {
        return operLogMapper.selectOperLogList(operLog);
    }

    /**
     * 查询分页系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    @Override
    public TableDataInfo<SysOperLog> selectOperLogPage(SysOperLog operLog, PageQuery pageQuery) {

        LambdaQueryWrapper<SysOperLog> lqw = buildQueryWrapper(operLog);
        Page<SysOperLog> page = operLogMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    @Override
    public LambdaQueryWrapper<SysOperLog> buildQueryWrapper(SysOperLog operLog) {
        Map<String, Object> params = operLog.getParams();
        LambdaQueryWrapper<SysOperLog> lambdaQueryWrapper = new LambdaQueryWrapper<SysOperLog>();
        lambdaQueryWrapper.like(!StringUtils.isEmpty(operLog.getTitle()), SysOperLog::getOperatorType, operLog.getTitle())
                .eq(!StringUtils.isNull(operLog.getBusinessType()), SysOperLog::getBusinessType, operLog.getBusinessType())
                .eq(!StringUtils.isNull(operLog.getStatus()), SysOperLog::getStatus, operLog.getStatus())
                .like(!StringUtils.isEmpty(operLog.getOperName()), SysOperLog::getOperName, operLog.getOperName())
                .ge(params.get("beginoperTime") != null, SysOperLog::getOperTime, params.get("beginoperTime"))
                .le(params.get("endoperTime") != null, SysOperLog::getOperTime, params.get("endoperTime"))
                .in(!StringUtils.isEmpty(operLog.getBusinessTypes()), SysOperLog::getBusinessType, operLog.getBusinessTypes())
                .orderByDesc(true, SysOperLog::getOperId);
        return lambdaQueryWrapper;
    }

    /**
     * 批量删除系统操作日志
     *
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    @Override
    public int deleteOperLogByIds(Long[] operIds) {
        return operLogMapper.deleteOperLogByIds(operIds);
    }

    /**
     * 查询操作日志详细
     *
     * @param operId 操作ID
     * @return 操作日志对象
     */
    @Override
    public SysOperLog selectOperLogById(Long operId) {
        return operLogMapper.selectOperLogById(operId);
    }

    /**
     * 清空操作日志
     */
    @Override
    public void cleanOperLog() {
        operLogMapper.cleanOperLog();
    }
}
