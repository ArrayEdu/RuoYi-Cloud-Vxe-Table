package com.ruoyi.system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.common.security.annotation.InnerAuth;
import com.ruoyi.tableConfig.api.domain.SysRoleTable;
import com.ruoyi.tableConfig.api.domain.SysRoleTableColumn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.api.domain.SysRoleMenuPower;
import com.ruoyi.system.service.ISysRoleMenuPowerService;
import com.ruoyi.common.mybatis.core.controller.BaseController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;


/**
 * 数据角色功能授权Controller
 *
 * @author zly
 * @date 2023-05-23
 */
@RestController
@RequestMapping("/power")
public class SysRoleMenuPowerController extends BaseController {
    @Autowired
    private ISysRoleMenuPowerService sysRoleMenuPowerService;

    /**
     * 查询数据角色功能授权列表
     */
    @RequiresPermissions("system:power:list")
    @GetMapping("/list")
    public TableDataInfo list(SysRoleMenuPower sysRoleMenuPower) {
        return TableDataInfo.build(sysRoleMenuPowerService.selectSysRoleMenuPowerList(sysRoleMenuPower));
    }

    @InnerAuth
    @PostMapping("/listFeign")
    public R<SysRoleMenuPower> listFeign(@RequestBody SysRoleMenuPower sysRoleMenuPower) {
        SysRoleMenuPower sysRoleMenuPower1 = sysRoleMenuPowerService.selectSysRoleMenuPowerByRoleIdMenuId(sysRoleMenuPower);
        return R.ok(sysRoleMenuPower1);
    }

    /**
     * 查询数据角色功能授权分页列表
     */
    @RequiresPermissions("system:power:page")
    @PostMapping("/page")
    public TableDataInfo page(@RequestBody JSONObject ajaxData) {
        return sysRoleMenuPowerService.selectSysRoleMenuPowerPage(ajaxData.toJavaObject(SysRoleMenuPower.class), ajaxData.toJavaObject(PageQuery.class));
    }

    /**
     * 导出数据角色功能授权列表
     */
    @RequiresPermissions("system:power:export")
    @Log(title = "数据角色功能授权", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysRoleMenuPower sysRoleMenuPower) {
        List<SysRoleMenuPower> list = sysRoleMenuPowerService.selectSysRoleMenuPowerList(sysRoleMenuPower);
        ExcelUtil.exportExcel(list, "数据角色功能授权数据", SysRoleMenuPower.class, response);
    }

    /**
     * vxe-table导出数据角色功能授权列表
     */
    @RequiresPermissions("system:power:export")
    @Log(title = "数据角色功能授权", businessType = BusinessType.EXPORT)
    @PostMapping("/exportVxe")
    public void exportVxe(HttpServletResponse response, @RequestBody JSONObject params) {
        sysRoleMenuPowerService.export(response, params);
    }

    /**
     * 获取数据角色功能授权详细信息
     */
    @RequiresPermissions("system:power:query")
    @GetMapping(value = "/{rmpId}")
    public AjaxResult getInfo(@PathVariable("rmpId") Long rmpId) {
        return success(sysRoleMenuPowerService.selectSysRoleMenuPowerByRmpId(rmpId));
    }

    /**
     * 获取数据角色功能授权详细信息(批量)
     */
    @RequiresPermissions("system:power:query")
    @GetMapping(value = "/in/{rmpIds}")
    public AjaxResult getInfos(@PathVariable(value = "rmpIds", required = true) Long[] rmpIds) {
        List<SysRoleMenuPower> list = sysRoleMenuPowerService.listByIds(Arrays.asList(rmpIds));
        return AjaxResult.success(list);
    }

    /**
     * 新增数据角色功能授权
     */
    @RequiresPermissions("system:power:add")
    @Log(title = "数据角色功能授权", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysRoleMenuPower sysRoleMenuPower) {
        return toAjax(sysRoleMenuPowerService.insertSysRoleMenuPower(sysRoleMenuPower));
    }

    /**
     * 新增数据角色功能授权(批量)
     */
    @RequiresPermissions("system:power:add")
    @Log(title = "数据角色功能授权", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saves"})
    public AjaxResult adds(@RequestBody JSONArray jsonArray) {
        if (!sysRoleMenuPowerService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleMenuPower.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 新增数据角色功能授权(批量)
     */
    @RequiresPermissions("system:power:add")
    @Log(title = "数据角色功能授权", businessType = BusinessType.INSERT)
    @PostMapping(value = {"/saveMenuIds"})
    public AjaxResult addMenuIds(@RequestBody JSONObject ajaxData) {
        SysRoleMenuPower sysRoleMenuPower = JSON.toJavaObject(ajaxData, SysRoleMenuPower.class);
        List<Long> menuIds = JSON.parseObject(String.valueOf(ajaxData.get("menuIds")), new TypeReference<ArrayList<Long>>() {
        });
        JSONArray jsonArray = new JSONArray();
        for (Long menuId : menuIds) {
            SysRoleMenuPower cloneSysRoleMenuPower = sysRoleMenuPower;
            cloneSysRoleMenuPower.setMenuId(menuId);
            jsonArray.add(cloneSysRoleMenuPower);
        }
        if (!sysRoleMenuPowerService.saveBatch(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleMenuPower.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 修改数据角色功能授权
     */
    @RequiresPermissions("system:power:edit")
    @Log(title = "数据角色功能授权", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysRoleMenuPower sysRoleMenuPower) {
        return toAjax(sysRoleMenuPowerService.updateSysRoleMenuPower(sysRoleMenuPower));
    }

    /**
     * 修改数据角色功能授权(批量)
     */
    @RequiresPermissions("system:power:edit")
    @Log(title = "数据角色功能授权", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifys"})
    public AjaxResult edits(@Validated @RequestBody JSONArray jsonArray) {
        if (!sysRoleMenuPowerService.updateBatchById(JSONObject.parseArray(jsonArray.toJSONString(), SysRoleMenuPower.class))) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 新增数据角色功能授权(批量)
     */
    @RequiresPermissions("system:power:edit")
    @Log(title = "数据角色功能授权", businessType = BusinessType.UPDATE)
    @PutMapping(value = {"/modifyMenuIds"})
    public AjaxResult editMenuIds(@RequestBody JSONObject ajaxData) {
        SysRoleMenuPower sysRoleMenuPower = JSON.toJavaObject(ajaxData, SysRoleMenuPower.class);
        List<Long> menuIds = JSON.parseObject(String.valueOf(ajaxData.get("menuIds")), new TypeReference<ArrayList<Long>>() {
        });
        int res = 0;
        //当前选中修改的菜单在修改的菜单数组中吗 在就修改，不在就删除
        if (menuIds.indexOf(sysRoleMenuPower.getMenuId()) < 0) {
            sysRoleMenuPowerService.deleteSysRoleMenuPowerByRmpId(sysRoleMenuPower.getRmpId());
        }
        for (Long menuId : menuIds) {
            SysRoleMenuPower cloneSysRoleMenuPower = ObjectUtil.cloneByStream(sysRoleMenuPower);
            cloneSysRoleMenuPower.setMenuId(menuId);
            if (menuId == sysRoleMenuPower.getMenuId()) {
                res += sysRoleMenuPowerService.updateSysRoleMenuPower(cloneSysRoleMenuPower);
            } else {
                cloneSysRoleMenuPower.setRmpId(null);
                res += sysRoleMenuPowerService.insertSysRoleMenuPower(cloneSysRoleMenuPower);
            }
        }
        if (res != menuIds.size()) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 状态修改
     */
    @RequiresPermissions("system:power:edit")
    @Log(title = "数据角色功能授权", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody SysRoleMenuPower sysRoleMenuPower) {
        LambdaQueryWrapper<SysRoleMenuPower> updateWrapper = new LambdaQueryWrapper<SysRoleMenuPower>().eq(SysRoleMenuPower::getRmpId, sysRoleMenuPower.getRmpId());
        return toAjax(sysRoleMenuPowerService.update(sysRoleMenuPower, updateWrapper));
    }

    /**
     * 删除数据角色功能授权
     */
    @RequiresPermissions("system:power:remove")
    @Log(title = "数据角色功能授权", businessType = BusinessType.DELETE)
    @DeleteMapping("/{rmpIds}")
    public AjaxResult removes(@PathVariable(value = "rmpIds", required = true) Long[] rmpIds) {
        return toAjax(sysRoleMenuPowerService.removeByIds(Arrays.asList(rmpIds)));
    }

    /**
     * 导入模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil.exportExcel(new ArrayList<>(), "数据角色功能授权模板", SysRoleMenuPower.class, response);
    }
}
