package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.excel.entity.Exceel;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysRoleMenuPowerMapper;
import com.ruoyi.system.api.domain.SysRoleMenuPower;
import com.ruoyi.system.service.ISysRoleMenuPowerService;

import javax.servlet.http.HttpServletResponse;

/**
 * 数据角色功能授权Service业务层处理
 *
 * @author zly
 * @date 2023-05-23
 */
@Service
public class SysRoleMenuPowerServiceImpl extends ServiceImpl<SysRoleMenuPowerMapper, SysRoleMenuPower> implements ISysRoleMenuPowerService {
    @Autowired
    private SysRoleMenuPowerMapper sysRoleMenuPowerMapper;

    /**
     * 查询数据角色功能授权
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 数据角色功能授权
     */
    @Override
    public SysRoleMenuPower selectSysRoleMenuPowerByRoleIdMenuId(SysRoleMenuPower sysRoleMenuPower) {
        return sysRoleMenuPowerMapper.selectSysRoleMenuPowerByRoleIdMenuId(sysRoleMenuPower);
    }

    /**
     * 查询数据角色功能授权
     *
     * @param rmpId 数据角色功能授权主键
     * @return 数据角色功能授权
     */
    @Override
    public SysRoleMenuPower selectSysRoleMenuPowerByRmpId(Long rmpId) {
        return sysRoleMenuPowerMapper.selectSysRoleMenuPowerByRmpId(rmpId);
    }

    /**
     * 查询数据角色功能授权列表
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 数据角色功能授权
     */
    @Override
    public List<SysRoleMenuPower> selectSysRoleMenuPowerList(SysRoleMenuPower sysRoleMenuPower) {
        return sysRoleMenuPowerMapper.selectSysRoleMenuPowerList(sysRoleMenuPower);
    }

    /**
     * 新增数据角色功能授权
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 结果
     */
    @Override
    public int insertSysRoleMenuPower(SysRoleMenuPower sysRoleMenuPower) {
        sysRoleMenuPower.setCreateTime(DateUtils.getNowDate());
        return sysRoleMenuPowerMapper.insertSysRoleMenuPower(sysRoleMenuPower);
    }

    /**
     * 修改数据角色功能授权
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 结果
     */
    @Override
    public int updateSysRoleMenuPower(SysRoleMenuPower sysRoleMenuPower) {
        sysRoleMenuPower.setUpdateTime(DateUtils.getNowDate());
        return sysRoleMenuPowerMapper.updateSysRoleMenuPower(sysRoleMenuPower);
    }

    /**
     * 批量删除数据角色功能授权
     *
     * @param rmpIds 需要删除的数据角色功能授权主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleMenuPowerByRmpIds(Long[] rmpIds) {
        return sysRoleMenuPowerMapper.deleteSysRoleMenuPowerByRmpIds(rmpIds);
    }

    /**
     * 删除数据角色功能授权信息
     *
     * @param rmpId 数据角色功能授权主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleMenuPowerByRmpId(Long rmpId) {
        return sysRoleMenuPowerMapper.deleteSysRoleMenuPowerByRmpId(rmpId);
    }

    @Override
    public TableDataInfo<SysRoleMenuPower> selectSysRoleMenuPowerPage(SysRoleMenuPower sysRoleMenuPower, PageQuery pageQuery) {
        IPage<SysRoleMenuPower> page = sysRoleMenuPowerMapper.selectSysRoleMenuPowerPage(pageQuery.build(), sysRoleMenuPower);
        return TableDataInfo.build(page);
    }

    /**
     * 数据角色功能授权查询条件
     *
     * @param sysRoleMenuPower
     * @return
     */
    @Override
    public LambdaQueryWrapper<SysRoleMenuPower> buildQueryWrapper(SysRoleMenuPower sysRoleMenuPower) {
        LambdaQueryWrapper<SysRoleMenuPower> lambdaQueryWrapper = new LambdaQueryWrapper<SysRoleMenuPower>()
                .eq(!StringUtils.isNull(sysRoleMenuPower.getRmpId()), SysRoleMenuPower::getRmpId, sysRoleMenuPower.getRmpId())
                .eq(!StringUtils.isNull(sysRoleMenuPower.getRoleId()), SysRoleMenuPower::getRoleId, sysRoleMenuPower.getRoleId())
                .eq(!StringUtils.isNull(sysRoleMenuPower.getMenuId()), SysRoleMenuPower::getMenuId, sysRoleMenuPower.getMenuId())
                .like(!StringUtils.isNull(sysRoleMenuPower.getPowerValue()), SysRoleMenuPower::getPowerValue, sysRoleMenuPower.getPowerValue())
                .eq(!StringUtils.isNull(sysRoleMenuPower.getSort()), SysRoleMenuPower::getSort, sysRoleMenuPower.getSort())
                .eq(!StringUtils.isNull(sysRoleMenuPower.getStatus()), SysRoleMenuPower::getStatus, sysRoleMenuPower.getStatus())
                .eq(!StringUtils.isNull(sysRoleMenuPower.getCreateBy()), SysRoleMenuPower::getCreateBy, sysRoleMenuPower.getCreateBy())
                .ge(sysRoleMenuPower.getParams().get("beginCreateTime") != null, SysRoleMenuPower::getCreateTime, sysRoleMenuPower.getParams().get("beginCreateTime"))
                .le(sysRoleMenuPower.getParams().get("endCreateTime") != null, SysRoleMenuPower::getCreateTime, sysRoleMenuPower.getParams().get("endCreateTime"))
                .eq(!StringUtils.isNull(sysRoleMenuPower.getUpdateBy()), SysRoleMenuPower::getUpdateBy, sysRoleMenuPower.getUpdateBy())
                .ge(sysRoleMenuPower.getParams().get("beginUpdateTime") != null, SysRoleMenuPower::getUpdateTime, sysRoleMenuPower.getParams().get("beginUpdateTime"))
                .le(sysRoleMenuPower.getParams().get("endUpdateTime") != null, SysRoleMenuPower::getUpdateTime, sysRoleMenuPower.getParams().get("endUpdateTime"))
                .like(!StringUtils.isNull(sysRoleMenuPower.getRemark()), SysRoleMenuPower::getRemark, sysRoleMenuPower.getRemark())
                .orderByDesc(SysRoleMenuPower::getRemark);
        return lambdaQueryWrapper;
    }

    /**
     * 导出
     *
     * @param response
     * @param jsonObject
     */
    @Override
    public void export(HttpServletResponse response, JSONObject jsonObject) {
        Exceel exceel = JSON.toJavaObject(jsonObject, Exceel.class);
        PageQuery pageQuery = JSON.toJavaObject(jsonObject, PageQuery.class);
        //导出方式（current 当前页, selected 选择, all 所有）
        List<SysRoleMenuPower> list = new ArrayList<>();
        if (exceel.getMode().equals("current")) {
            IPage<SysRoleMenuPower> iPage = sysRoleMenuPowerMapper.selectSysRoleMenuPowerPage(pageQuery.build(), new SysRoleMenuPower());
            list = iPage.getRecords();
        } else if (exceel.getMode().equals("selected") && !exceel.getIds().isEmpty()) { //选择导出（选择了选择导出并且选择了数据）
            list = sysRoleMenuPowerMapper.selectVoBatchIds(exceel.getIds());
        } else {
            SysRoleMenuPower sysRoleMenuPower = JSON.parseObject(String.valueOf(exceel.getQuery()), SysRoleMenuPower.class);
            list = sysRoleMenuPowerMapper.selectSysRoleMenuPowerList(sysRoleMenuPower);
        }
        String fileName = exceel.getFilename().equals(null) ? "数据角色功能授权数据" : exceel.getFilename();
        Set<String> column = Arrays.stream(exceel.getFields().stream().map(m -> m.getField()).collect(Collectors.toList()).stream().toArray(String[]::new)).collect(Collectors.toSet());
        ExcelUtil.exportExcel(list, fileName, SysRoleMenuPower.class, response, column);
    }
}
