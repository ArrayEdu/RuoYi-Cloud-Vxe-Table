package com.ruoyi.system.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.system.api.domain.SysRoleMenu;

/**
 * 角色和菜单关联表Service接口
 *
 * @author zly
 * @date 2023-06-07
 */
public interface ISysRoleMenuService {

    /**
     * 查询角色和菜单关联表
     *
     * @param menuId 菜单ID
     * @return 角色和菜单关联表
     */
    public List<SysRoleMenu> selectSysRoleMenuByMenuId(Long menuId);

    /**
     * 查询角色和菜单关联表
     *
     * @param roleId 角色ID
     * @return 角色和菜单关联表
     */
    public List<SysRoleMenu> selectSysRoleMenuByRoleId(Long roleId);

    /**
     * 查询角色和菜单关联表列表
     *
     * @param sysRoleMenu 角色和菜单关联表
     * @return 角色和菜单关联表集合
     */
    public List<SysRoleMenu> selectSysRoleMenuList(SysRoleMenu sysRoleMenu);

    /**
     * 角色和菜单关联表查询条件
     *
     * @return
     */
    public LambdaQueryWrapper<SysRoleMenu> queryWrapper(SysRoleMenu sysRoleMenu);
}
