package com.ruoyi.system.domain.vo;

import lombok.Data;

/**
 * @author zly
 */
@Data
public class RolePerm {
    private String perms;
    private Long roleId;
}
