package com.ruoyi.system.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.system.api.domain.SysRoleMenuPower;
import org.apache.ibatis.annotations.Param;

/**
 * 数据角色功能授权Mapper接口
 *
 * @author zly
 * @date 2023-05-23
 */
public interface SysRoleMenuPowerMapper extends BaseMapperPlus<SysRoleMenuPower> {
    /**
     * 根据条件分页查询角色数据
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 数据角色功能授权信息
     */
    public IPage<SysRoleMenuPower> selectSysRoleMenuPowerPage(@Param("page") IPage<SysRoleMenuPower> page, @Param(Constants.WRAPPER) SysRoleMenuPower sysRoleMenuPower);


    /**
     * 查询数据角色功能授权
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 数据角色功能授权
     */
    public SysRoleMenuPower selectSysRoleMenuPowerByRoleIdMenuId(SysRoleMenuPower sysRoleMenuPower);

    /**
     * 查询数据角色功能授权
     *
     * @param rmpId 数据角色功能授权主键
     * @return 数据角色功能授权
     */
    public SysRoleMenuPower selectSysRoleMenuPowerByRmpId(Long rmpId);

    /**
     * 查询数据角色功能授权列表
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 数据角色功能授权集合
     */
    public List<SysRoleMenuPower> selectSysRoleMenuPowerList(SysRoleMenuPower sysRoleMenuPower);

    /**
     * 新增数据角色功能授权
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 结果
     */
    public int insertSysRoleMenuPower(SysRoleMenuPower sysRoleMenuPower);

    /**
     * 修改数据角色功能授权
     *
     * @param sysRoleMenuPower 数据角色功能授权
     * @return 结果
     */
    public int updateSysRoleMenuPower(SysRoleMenuPower sysRoleMenuPower);

    /**
     * 删除数据角色功能授权
     *
     * @param rmpId 数据角色功能授权主键
     * @return 结果
     */
    public int deleteSysRoleMenuPowerByRmpId(Long rmpId);

    /**
     * 批量删除数据角色功能授权
     *
     * @param rmpIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysRoleMenuPowerByRmpIds(Long[] rmpIds);
}
