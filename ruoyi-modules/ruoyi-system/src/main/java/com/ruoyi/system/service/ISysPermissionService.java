package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.system.api.domain.SysUser;

/**
 * 权限信息 服务层
 *
 * @author ruoyi
 */
public interface ISysPermissionService {
    /**
     * 获取角色数据权限
     *
     * @param user 用户信息
     * @return 角色权限信息
     */
    public Set<String> getRolePermission(SysUser user);

    public List<SysRole> getRoleList(SysUser user);

    /**
     * 获取菜单数据权限
     *
     * @param user 用户信息
     * @return 菜单权限信息
     */
    public Set<String> getMenuPermission(SysUser user);

    /**
     * 获取用户的菜单角色权限
     *
     * @param user 用户信息
     * @return 菜单权限信息
     */
    public Map<String, Set<String>> getMenuRolePermission(SysUser user);

    /**
     * 获取用户拥有的使用权限权限
     *
     * @param user 用户信息
     * @return 权限信息
     */
    public Set<String> getuserAllPermission(SysUser user);

}
