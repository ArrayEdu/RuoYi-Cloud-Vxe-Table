package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.system.api.domain.SysRoleMenu;

/**
 * 角色与菜单关联表 数据层
 *
 * @author ruoyi
 */
public interface SysRoleMenuMapper extends BaseMapperPlus<SysRoleMenu> {

    /**
     * 查询角色和菜单关联表
     *
     * @param menuId 菜单ID
     * @return 角色和菜单关联表
     */
    public List<SysRoleMenu> selectSysRoleMenuByMenuId(Long menuId);

    /**
     * 查询角色和菜单关联表
     *
     * @param roleId 角色ID
     * @return 角色和菜单关联表
     */
    public List<SysRoleMenu> selectSysRoleMenuByRoleId(Long roleId);

    /**
     * 查询角色和菜单关联表列表
     *
     * @param sysRoleMenu 角色和菜单关联表
     * @return 角色和菜单关联表集合
     */
    public List<SysRoleMenu> selectSysRoleMenuList(SysRoleMenu sysRoleMenu);

    /**
     * 查询菜单使用数量
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    public int checkMenuExistRole(Long menuId);

    /**
     * 通过角色ID删除角色和菜单关联
     *
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleMenuByRoleId(Long roleId);

    /**
     * 批量删除角色菜单关联信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRoleMenu(Long[] ids);

    /**
     * 批量新增角色菜单信息
     *
     * @param roleMenuList 角色菜单列表
     * @return 结果
     */
    public int batchRoleMenu(List<SysRoleMenu> roleMenuList);
}
