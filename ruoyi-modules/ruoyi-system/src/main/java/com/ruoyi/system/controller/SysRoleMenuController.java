package com.ruoyi.system.controller;


import java.util.List;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.api.domain.SysRoleMenu;
import com.ruoyi.system.service.ISysRoleMenuService;
import com.ruoyi.common.mybatis.core.controller.BaseController;

/**
 * 角色和菜单关联表Controller
 *
 * @author zly
 * @date 2023-06-07
 */
@RestController
@RequestMapping("/roleMenu")
public class SysRoleMenuController extends BaseController {
    @Autowired
    private ISysRoleMenuService sysRoleMenuService;

    /**
     * 查询角色和菜单关联表列表
     */
    @RequiresPermissions("system:roleMenu:list")
    @GetMapping("/list")
    public TableDataInfo list(SysRoleMenu sysRoleMenu) {
        return TableDataInfo.build(sysRoleMenuService.selectSysRoleMenuList(sysRoleMenu));
    }

    /**
     * 获取角色和菜单关联表详细信息
     */
    @RequiresPermissions("system:roleMenu:query")
    @GetMapping(value = "/getByRoleId/{roleId}")
    public R<List<SysRoleMenu>> getInfoByRoleId(@PathVariable("roleId") Long roleId) {
        return R.ok(sysRoleMenuService.selectSysRoleMenuByRoleId(roleId));
    }

    /**
     * 获取角色和菜单关联表详细信息
     */
    @RequiresPermissions("system:roleMenu:query")
    @GetMapping(value = "/getByMenuId/{menuId}")
    public R<List<SysRoleMenu>> getInfoByMenuId(@PathVariable("menuId") Long menuId) {
        return R.ok(sysRoleMenuService.selectSysRoleMenuByMenuId(menuId));
    }
}
