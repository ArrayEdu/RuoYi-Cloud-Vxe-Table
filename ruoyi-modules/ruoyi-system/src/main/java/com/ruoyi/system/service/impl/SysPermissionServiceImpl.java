package com.ruoyi.system.service.impl;

import java.util.*;

import com.ruoyi.common.security.auth.AuthLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.service.ISysMenuService;
import com.ruoyi.system.service.ISysPermissionService;
import com.ruoyi.system.service.ISysRoleService;

/**
 * 用户权限处理
 *
 * @author ruoyi
 */
@Service
public class SysPermissionServiceImpl implements ISysPermissionService {
    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysMenuService menuService;

    /**
     * 获取角色数据权限
     *
     * @param user 用户信息
     * @return 角色权限信息
     */
    @Override
    public Set<String> getRolePermission(SysUser user) {
        Set<String> roles = new HashSet<String>();
        // 管理员拥有所有权限
        if (user.isAdmin()) {
            roles.add("admin");
        } else {
            roles.addAll(roleService.selectRolePermissionByUserId(user.getUserId()));
        }
        return roles;
    }

    @Override
    public List<SysRole> getRoleList(SysUser user) {
        Set<String> perms = new HashSet<String>();
        perms.add(AuthLogic.ALL_PERMISSION);
        // 管理员拥有所有权限
        if (user.isAdmin()) {
            List<SysRole> sysRoles = roleService.selectRoleAll();
            sysRoles.stream().forEach(m -> m.setPermissions(perms));
            return sysRoles;
        } else {
            List<SysRole> sysRoles = roleService.selectRoleByUserId(user.getUserId());
            return sysRoles;
        }
    }

    /**
     * 获取菜单数据权限
     *
     * @param user 用户信息
     * @return 菜单权限信息
     */
    @Override
    public Set<String> getMenuPermission(SysUser user) {
        Set<String> perms = new HashSet<String>();
        // 管理员拥有所有权限
        if (user.isAdmin()) {
            perms.add(AuthLogic.ALL_PERMISSION);
        } else {
            List<SysRole> roles = user.getRoles();
            if (!roles.isEmpty() && roles.size() > 1) {
                // 多角色设置permissions属性，以便数据权限匹配权限
                for (SysRole role : roles) {
                    Set<String> rolePerms = menuService.selectMenuPermsByRoleId(role.getRoleId());
                    role.setPermissions(rolePerms);
                    perms.addAll(rolePerms);
                }
            } else {
                perms.addAll(menuService.selectMenuPermsByUserId(user.getUserId()));
            }
        }
        return perms;
    }

    /**
     * 获取用户的菜单角色权限
     *
     * @param user 用户信息
     * @return 菜单权限信息
     * @author zly
     */
    @Override
    public Map<String, Set<String>> getMenuRolePermission(SysUser user) {
        Map<String, Set<String>> perms = new HashMap<String, Set<String>>();
        // 管理员拥有所有权限
        if (user.isAdmin()) {
            Set<String> admin = new HashSet<String>();
            admin.add(AuthLogic.ALL_PERMISSION);
            user.getRoles().stream().forEach(m -> perms.put(m.getRoleKey(), admin));
            //perms.put("admin", admin);
        } else {
            List<SysRole> roles = user.getRoles();
            if (!roles.isEmpty() && roles.size() > 1) {
                // 多角色设置permissions属性
                for (SysRole role : roles) {
                    Set<String> rolePerms = menuService.selectMenuPermsByRoleId(role.getRoleId());
                    role.setPermissions(rolePerms);
                    perms.put(role.getRoleKey(), rolePerms);
                }
            } else {
                List<SysRole> sysRoles = roleService.selectRolesByUserId(user.getUserId());
                for (SysRole role : sysRoles) {
                    if (role.isFlag()) {
                        Set<String> rolePerms = menuService.selectMenuPermsByRoleId(role.getRoleId());
                        role.setPermissions(rolePerms);
                        perms.put(role.getRoleKey(), rolePerms);
                    }
                }
            }
        }
        return perms;
    }

    /**
     * 获取用户拥有的使用权限权限
     *
     * @param user 用户信息
     * @return 菜单权限信息
     */
    @Override
    public Set<String> getuserAllPermission(SysUser user) {
        Set<String> perms = new HashSet<String>();
        // 管理员拥有所有权限
        if (user.isAdmin()) {
            perms.add(AuthLogic.ALL_PERMISSION);
        } else {
            List<SysRole> roles = user.getRoles();
            if (!roles.isEmpty()) {
                // 多角色设置permissions属性，以便数据权限匹配权限
                for (SysRole role : roles) {
                    perms.addAll(role.getPermissions());
                }
            }
        }
        return perms;
    }
}
