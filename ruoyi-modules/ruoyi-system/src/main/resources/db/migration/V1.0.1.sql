/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50739 (5.7.39-log)
 Source Host           : localhost:3306
 Source Schema         : ry-cloud

 Target Server Type    : MySQL
 Target Server Version : 50739 (5.7.39-log)
 File Encoding         : 65001

 Date: 01/09/2023 23:14:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (19, 'sys_table_column', '功能表格列配置', NULL, NULL, 'SysTableColumn', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'column', '功能表格列配置', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25', NULL);
INSERT INTO `gen_table` VALUES (20, 'sys_table_edit_config', '可编辑配置项', NULL, NULL, 'SysTableEditConfig', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'config', '可编辑配置项', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41', NULL);
INSERT INTO `gen_table` VALUES (21, 'sys_table_edit_rules', '校验规则配置项', NULL, NULL, 'SysTableEditRules', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'rules', '校验规则配置项', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:55', NULL);
INSERT INTO `gen_table` VALUES (22, 'sys_table_form', '功能表查询表', NULL, NULL, 'SysTableForm', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'form', '功能表查询', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44', NULL);
INSERT INTO `gen_table` VALUES (23, 'sys_table_form_item', '表单配置项列表', NULL, NULL, 'SysTableFormItem', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'item', '表单配置项列', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57', NULL);
INSERT INTO `gen_table` VALUES (24, 'sys_table_pager', '功能表分页配置', NULL, NULL, 'SysTablePager', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'pager', '功能表分页配置', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33', NULL);
INSERT INTO `gen_table` VALUES (25, 'sys_table_proxy', '功能表数据代理', NULL, NULL, 'SysTableProxy', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'proxy', '功能表数据代理', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51', NULL);
INSERT INTO `gen_table` VALUES (27, 'sys_table_toolbar_config', '工具栏配置', NULL, NULL, 'SysTableToolbarConfig', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'sysTableToolbarConfig', '工具栏配置', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25', NULL);
INSERT INTO `gen_table` VALUES (29, 'sys_table', '功能表格', NULL, NULL, 'SysTable', 'crud', 'com.ruoyi.system', 'system', 'table', '功能表格', 'zly', '0', '/', NULL, 1, '2023-05-15 23:37:31', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (30, 'sys_role_menu_power', '数据角色功能授权', NULL, NULL, 'SysRoleMenuPower', 'crud', 'com.ruoyi.system', 'system', 'power', '数据角色功能授权', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-05-23 11:22:56', NULL, '2023-05-23 11:24:25', NULL);
INSERT INTO `gen_table` VALUES (31, 'sys_role_table', '表格权限业务表', NULL, NULL, 'SysRoleTable', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'table', '表格权限业务表', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21', NULL);
INSERT INTO `gen_table` VALUES (32, 'sys_role_table_column', '表格权限列配置', NULL, NULL, 'SysRoleTableColumn', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'roleColumn', '表格权限列配置', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09', NULL);
INSERT INTO `gen_table` VALUES (33, 'sys_role_table_edit_config', '角色可编辑配置项', NULL, NULL, 'SysRoleTableEditConfig', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'config', '角色可编辑配置项', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57', NULL);
INSERT INTO `gen_table` VALUES (34, 'sys_role_table_edit_rules', '角色校验规则配置项', NULL, NULL, 'SysRoleTableEditRules', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'rules', '角色校验规则配置项', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08', NULL);
INSERT INTO `gen_table` VALUES (35, 'sys_role_table_form', '角色功能表查询表', NULL, NULL, 'SysRoleTableForm', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'form', '角色功能表查询表', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20', NULL);
INSERT INTO `gen_table` VALUES (36, 'sys_role_table_form_item', '角色表单配置项列表', NULL, NULL, 'SysRoleTableFormItem', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'item', '角色表单配置项列表', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35', NULL);
INSERT INTO `gen_table` VALUES (37, 'sys_role_table_pager', '角色功能表分页配置', NULL, NULL, 'SysRoleTablePager', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'pager', '角色功能表分页配置', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47', NULL);
INSERT INTO `gen_table` VALUES (38, 'sys_role_table_proxy', '角色功能表数据代理', NULL, NULL, 'SysRoleTableProxy', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'proxy', '角色功能表数据代理', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57', NULL);
INSERT INTO `gen_table` VALUES (39, 'sys_role_table_toolbar_config', '角色工具栏配置', NULL, NULL, 'SysRoleTableToolbarConfig', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'config', '角色工具栏配置', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14', NULL);
INSERT INTO `gen_table` VALUES (40, 'sys_role_menu', '角色和菜单关联表', NULL, NULL, 'SysRoleMenu', 'crud', 'com.ruoyi.system', 'system', 'roleMenu', '角色和菜单关联表', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-06-07 20:26:10', NULL, '2023-06-07 20:26:47', NULL);
INSERT INTO `gen_table` VALUES (41, 'sys_user_table_column', '用户表格列配置', NULL, NULL, 'SysUserTableColumn', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'userTableColumn', '用户表格列配置', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45', NULL);
INSERT INTO `gen_table` VALUES (42, 'sys_user_table_form_item', '用户表单配置项列表', NULL, NULL, 'SysUserTableFormItem', 'crud', 'com.ruoyi.tableConfig', 'tableConfig', 'userTableFormItem', '用户表单配置项列表', 'zly', '0', '/', '{\"parentMenuId\":\"\"}', 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 955 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (300, '19', 'column_id', '编号', 'int(11)', 'Long', 'columnId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (301, '19', 'table_id', '归属表编号', 'int(11)', 'Long', 'tableId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (302, '19', 'column_name', '列名称', 'varchar(200)', 'String', 'columnName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (303, '19', 'column_comment', '列描述', 'varchar(500)', 'String', 'columnComment', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 4, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (304, '19', 'field', '字段', 'varchar(200)', 'String', 'field', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (305, '19', 'title', '字段名', 'varchar(500)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 6, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (306, '19', 'is_ignore', '是否显示（1是', 'char(1)', 'String', 'isIgnore', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (307, '19', 'visible', '是否可视（1是', 'char(1)', 'String', 'visible', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (308, '19', 'width', '宽度', 'varchar(255)', 'String', 'width', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (309, '19', 'minWidth', '最小列宽度会自动将剩余空间按比例分配', 'double', 'Long', 'minwidth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (310, '19', 'resizable', '列宽是否可拖拽（1是', 'char(1)', 'String', 'resizable', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (311, '19', 'align', '列对齐方式：left（左对齐）,', 'varchar(5)', 'String', 'align', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 12, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (312, '19', 'type', '列的类型', 'varchar(255)', 'String', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 13, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (313, '19', 'fixed', '将列固定在左侧或者右侧（注意：固定列应该放在左右两侧的位置）', 'varchar(255)', 'String', 'fixed', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 14, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (314, '19', 'headerAlign', '表头列的对齐方式', 'varchar(255)', 'String', 'headeralign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 15, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (315, '19', 'footerAlign', '表尾列的对齐方式', 'varchar(255)', 'String', 'footeralign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 16, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (316, '19', 'showOverflow', '当内容过长时显示为省略号', 'varchar(255)', 'String', 'showoverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 17, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (317, '19', 'showHeaderOverflow', '当表头内容过长时显示为省略号', 'varchar(255)', 'String', 'showheaderoverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 18, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (318, '19', 'showFooterOverflow', '当表尾内容过长时显示为省略号', 'varchar(255)', 'String', 'showfooteroverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 19, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (319, '19', 'className', '给单元格附加', 'varchar(1000)', 'String', 'classname', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 20, 1, '2023-04-25 15:29:02', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (320, '19', 'headerClassName', '给表头的单元格附加', 'varchar(1000)', 'String', 'headerclassname', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 21, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (321, '19', 'footerClassName', '给表尾的单元格附加', 'varchar(1000)', 'String', 'footerclassname', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 22, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (322, '19', 'formatter', '格式化显示内容', 'varchar(1000)', 'String', 'formatter', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 23, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (323, '19', 'sortable', '是否允许列排序', 'tinyint(1)', 'Integer', 'sortable', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 24, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (324, '19', 'sortBy', '只对', 'varchar(1000)', 'String', 'sortby', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 25, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (325, '19', 'sortType', '排序的字段类型，比如字符串转数值等', 'varchar(255)', 'String', 'sorttype', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 26, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (326, '19', 'ext_params', '额外的参数（可以用来存放一些私有参数）', 'varchar(255)', 'String', 'extParams', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 27, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (327, '19', 'treeNode', '只对', 'tinyint(1)', 'Integer', 'treenode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 28, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (328, '19', 'slots', '插槽', 'varchar(1000)', 'String', 'slots', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 29, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (329, '19', 'filters', '配置筛选条件', 'varchar(1000)', 'String', 'filters', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 30, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (330, '19', 'filterMultiple', '只对', 'tinyint(1)', 'Integer', 'filtermultiple', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 31, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (331, '19', 'filterMethod', '只对', 'varchar(1000)', 'String', 'filtermethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 32, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (332, '19', 'filterResetMethod', '只对', 'varchar(1000)', 'String', 'filterresetmethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 33, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (333, '19', 'filterRecoverMethod', '只对', 'varchar(1000)', 'String', 'filterrecovermethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 34, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (334, '19', 'filterRender', '筛选渲染器配置项', 'varchar(1000)', 'String', 'filterrender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 35, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (335, '19', 'exportMethod', '自定义单元格数据导出方法，返回自定义的值', 'varchar(1000)', 'String', 'exportmethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 36, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (336, '19', 'footerExportMethod', '自定义表尾单元格数据导出方法，返回自定义的值', 'varchar(1000)', 'String', 'footerexportmethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 37, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (337, '19', 'titlePrefix', '标题前缀图标配置项', 'varchar(500)', 'String', 'titleprefix', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 38, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (338, '19', 'cellType', '只对特定功能有效，单元格值类型', 'varchar(255)', 'String', 'celltype', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 39, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (339, '19', 'cellRender', '默认的渲染器配置项', 'varchar(1000)', 'String', 'cellrender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 40, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (340, '19', 'editRender', '可编辑渲染器配置项', 'varchar(1000)', 'String', 'editrender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 41, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (341, '19', 'contentRender', '内容渲染配置项', 'varchar(1000)', 'String', 'contentrender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 42, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (342, '19', 'colId', '自定义列的唯一主键', 'varchar(255)', 'String', 'colid', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 43, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (343, '19', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', NULL, 44, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (344, '19', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 45, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (345, '19', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', NULL, 46, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (346, '19', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', NULL, 47, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (347, '19', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', NULL, 48, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:25');
INSERT INTO `gen_table_column` VALUES (348, '20', 'edit_config_id', '可编辑配置项id', 'int(11)', 'Long', 'editConfigId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (349, '20', 'table_id', '表id', 'int(11)', 'Long', 'tableId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (350, '20', 'trigger', '触发方式', 'varchar(255)', 'String', 'trigger', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (351, '20', 'enabled', '是否启用', 'tinyint(1)', 'Integer', 'enabled', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (352, '20', 'mode', '编辑模式', 'varchar(255)', 'String', 'mode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (353, '20', 'showIcon', '是否显示列头编辑图标', 'tinyint(1)', 'Integer', 'showicon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (354, '20', 'showStatus', '只对', 'tinyint(1)', 'Integer', 'showstatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', NULL, 7, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (355, '20', 'showUpdateStatus', '只对', 'tinyint(1)', 'Integer', 'showupdatestatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', NULL, 8, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (356, '20', 'showInsertStatus', '只对', 'tinyint(1)', 'Integer', 'showinsertstatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', NULL, 9, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (357, '20', 'showAsterisk', '是否显示必填字段的红色星号', 'tinyint(1)', 'Integer', 'showasterisk', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (358, '20', 'autoClear', '当点击非编辑列之后是否自动清除单元格的激活状态', 'tinyint(1)', 'Integer', 'autoclear', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (359, '20', 'beforeEditMethod', '该方法的返回值用来决定该单元格是否允许编辑', 'varchar(1000)', 'String', 'beforeeditmethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 12, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (360, '20', 'icon', '自定义可编辑列的状态图标', 'varchar(255)', 'String', 'icon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 13, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (361, '20', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 14, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (362, '20', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', NULL, 15, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (363, '20', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', NULL, 16, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (364, '20', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', NULL, 17, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (365, '20', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', NULL, 18, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:34:41');
INSERT INTO `gen_table_column` VALUES (366, '21', 'edit_rules_id', '校验规则配置id', 'int(11)', 'Long', 'editRulesId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-04-25 15:29:03', NULL, '2023-06-01 22:57:23');
INSERT INTO `gen_table_column` VALUES (367, '21', 'table_id', '表id', 'int(11)', 'Long', 'tableId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-04-25 15:29:03', NULL, '2023-06-01 22:57:23');
INSERT INTO `gen_table_column` VALUES (370, '21', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-04-25 15:29:03', NULL, '2023-06-01 22:57:23');
INSERT INTO `gen_table_column` VALUES (371, '21', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'datetime', NULL, 11, 1, '2023-04-25 15:29:03', NULL, '2023-06-01 22:57:23');
INSERT INTO `gen_table_column` VALUES (372, '21', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 12, 1, '2023-04-25 15:29:03', NULL, '2023-06-01 22:57:23');
INSERT INTO `gen_table_column` VALUES (373, '21', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', NULL, 13, 1, '2023-04-25 15:29:03', NULL, '2023-06-01 22:57:23');
INSERT INTO `gen_table_column` VALUES (374, '21', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 14, 1, '2023-04-25 15:29:03', NULL, '2023-06-01 22:57:23');
INSERT INTO `gen_table_column` VALUES (375, '22', 'form_id', '查询表id', 'int(11)', 'Long', 'formId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 1, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (376, '22', 'table_id', '表格id', 'int(11)', 'Long', 'tableId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (377, '22', 'span', '所有项的栅格占据的列数（共', 'int(3)', 'Integer', 'span', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (378, '22', 'align', '所有项的内容对齐方式', 'varchar(255)', 'String', 'align', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (379, '22', 'size', '尺寸', 'varchar(255)', 'String', 'size', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (380, '22', 'titleAlign', '所有项的标题对齐方式', 'varchar(255)', 'String', 'titlealign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (381, '22', 'titleWidth', '所有项的标题宽度', 'varchar(255)', 'String', 'titlewidth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (382, '22', 'titleColon', '是否显示标题冒号', 'tinyint(1)', 'Integer', 'titlecolon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (383, '22', 'titleAsterisk', '是否显示必填字段的红色星号', 'tinyint(1)', 'Integer', 'titleasterisk', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (384, '22', 'titleOverflow', '所有设置标题内容过长时显示为省略号', 'varchar(255)', 'String', 'titleoverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (385, '22', 'className', '给表单附加', 'varchar(1000)', 'String', 'classname', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 11, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (386, '22', 'collapseStatus', 'v-model', 'tinyint(1)', 'Integer', 'collapsestatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', NULL, 12, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (387, '22', 'customLayout', '是否使用自定义布局', 'tinyint(1)', 'Integer', 'customlayout', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 13, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (388, '22', 'preventSubmit', '是否禁用默认的回车提交方式禁用后配合', 'tinyint(1)', 'Integer', 'preventsubmit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 14, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (389, '22', 'enabled', '是否启用', 'tinyint(1)', 'Integer', 'enabled', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 15, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (390, '22', 'rules', '校验规则配置项', 'varchar(1000)', 'String', 'rules', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 16, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (391, '22', 'validConfig', '检验配置项', 'varchar(1000)', 'String', 'validconfig', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 17, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (392, '22', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 18, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (393, '22', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', NULL, 19, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (394, '22', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', NULL, 20, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (395, '22', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', NULL, 21, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (396, '22', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', NULL, 22, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:35:44');
INSERT INTO `gen_table_column` VALUES (397, '23', 'item_id', '表单项id', 'int(11)', 'Long', 'itemId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (398, '23', 'form_id', '表单id', 'int(11)', 'Long', 'formId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (399, '23', 'field', '字段名', 'varchar(255)', 'String', 'field', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (400, '23', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (401, '23', 'span', '栅格占据的列数（共', 'int(2)', 'Integer', 'span', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (402, '23', 'align', '内容对齐方式', 'varchar(255)', 'String', 'align', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (409, '23', 'visible', '默认是否显示', 'tinyint(1)', 'Boolean', 'visible', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 16, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (411, '23', 'folding', '默认收起', 'tinyint(1)', 'Boolean', 'folding', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 18, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (414, '23', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 24, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (415, '23', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'datetime', NULL, 25, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (416, '23', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 26, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (417, '23', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', NULL, 27, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (418, '23', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 28, 1, '2023-04-25 15:29:03', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (419, '24', 'pager_id', '分页配置id', 'int(11)', 'Long', 'pagerId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 1, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (420, '24', 'table_id', '表格id', 'int(11)', 'Long', 'tableId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (421, '24', 'layouts', '自定义布局', 'varchar(255)', 'String', 'layouts', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (422, '24', 'currentPage', '当前页', 'int(11)', 'Long', 'currentpage', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (423, '24', 'pageSize', '每页大小', 'int(11)', 'Long', 'pagesize', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (424, '24', 'pagerCount', '显示页码按钮的数量', 'int(11)', 'Long', 'pagercount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (425, '24', 'pageSizes', '每页大小选项列表', 'varchar(255)', 'String', 'pagesizes', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (426, '24', 'align', '对齐方式', 'varchar(255)', 'String', 'align', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (427, '24', 'border', '带边框', 'tinyint(1)', 'Integer', 'border', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (428, '24', 'background', '带背景颜色', 'tinyint(1)', 'Integer', 'background', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (429, '24', 'perfect', '配套的样式', 'tinyint(1)', 'Integer', 'perfect', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (430, '24', 'className', '给分页附加', 'varchar(1000)', 'String', 'classname', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 12, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (431, '24', 'autoHidden', '当只有一页时自动隐藏', 'tinyint(1)', 'Integer', 'autohidden', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 13, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (432, '24', 'iconPrevPage', '自定义上一页图标', 'varchar(255)', 'String', 'iconprevpage', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 14, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (433, '24', 'iconJumpPrev', '自定义向上跳页图标', 'varchar(255)', 'String', 'iconjumpprev', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 15, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (434, '24', 'iconJumpNext', '自定义向下跳页图标', 'varchar(255)', 'String', 'iconjumpnext', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 16, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (435, '24', 'iconnextPage', '自定义下一页图标', 'varchar(255)', 'String', 'iconnextpage', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 17, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (436, '24', 'iconJumpMore', '自定义跳页显示图标', 'varchar(255)', 'String', 'iconjumpmore', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 18, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (437, '24', 'enabled', '是否启用', 'tinyint(1)', 'Integer', 'enabled', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 19, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (438, '24', 'slots', '插槽', 'varchar(500)', 'String', 'slots', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 20, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (439, '24', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 21, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (440, '24', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', NULL, 22, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (441, '24', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', NULL, 23, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (442, '24', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', NULL, 24, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (443, '24', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', NULL, 25, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:33');
INSERT INTO `gen_table_column` VALUES (444, '25', 'proxy_id', '代理表id', 'int(11)', 'Long', 'proxyId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 1, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (445, '25', 'table_id', '表格id', 'int(11)', 'Long', 'tableId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (446, '25', 'enabled', '是否启用', 'tinyint(1)', 'Integer', 'enabled', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (447, '25', 'autoLoad', '是否自动加载查询数据', 'tinyint(1)', 'Integer', 'autoload', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (448, '25', 'message', '是否显示内置的消息提示（可以设为', 'tinyint(1)', 'Integer', 'message', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (449, '25', 'seq', '存在', 'tinyint(1)', 'Integer', 'seq', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (450, '25', 'sort', '是否代理排序', 'tinyint(1)', 'Integer', 'sort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (451, '25', 'filter', '是否代理筛选', 'tinyint(1)', 'Integer', 'filter', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (452, '25', 'form', '是否代理表单', 'tinyint(1)', 'Integer', 'form', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (453, '25', 'props', '获取的属性配置', 'varchar(500)', 'String', 'props', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 10, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (454, '25', 'query', '查询配置', 'varchar(1000)', 'String', 'query', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 11, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (455, '25', 'queryAll', '全量查询配置', 'varchar(1000)', 'String', 'queryall', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 12, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (456, '25', 'delete', '删除配置', 'varchar(1000)', 'String', 'delete', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 13, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (457, '25', 'insert', '插入配置', 'varchar(1000)', 'String', 'insert', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 14, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (458, '25', 'update', '更新配置', 'varchar(1000)', 'String', 'update', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 15, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (459, '25', 'save', '保存', 'varchar(1000)', 'String', 'save', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 16, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (460, '25', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 17, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (461, '25', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', NULL, 18, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (462, '25', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', NULL, 19, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (463, '25', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', NULL, 20, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (464, '25', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', NULL, 21, 1, '2023-04-25 15:29:03', NULL, '2023-04-25 15:36:51');
INSERT INTO `gen_table_column` VALUES (475, '27', 'toolbar_config_id', '工具栏配置id', 'int(11)', 'Long', 'toolbarConfigId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (476, '27', 'table_id', '表格id', 'int(11)', 'Long', 'tableId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (477, '27', 'size', '尺寸(medium,', 'varchar(255)', 'String', 'size', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (478, '27', 'loading', '是否加载中', 'tinyint(1)', 'Integer', 'loading', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (479, '27', 'perfect', '配套的样式', 'tinyint(1)', 'Integer', 'perfect', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (480, '27', 'className', '给工具栏', 'varchar(1000)', 'String', 'classname', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 6, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (481, '27', 'import', '导入按钮配置（需要设置', 'varchar(255)', 'String', 'import', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (482, '27', 'export', '导出按钮配置（需要设置', 'varchar(255)', 'String', 'export', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (483, '27', 'print', '打印按钮配置', 'varchar(255)', 'String', 'print', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (484, '27', 'refresh', '刷新按钮配置', 'varchar(255)', 'String', 'refresh', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (485, '27', 'custom', '自定义列配置', 'varchar(255)', 'String', 'custom', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (486, '27', 'buttons', '左侧按钮列表', 'varchar(3000)', 'String', 'buttons', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 12, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (487, '27', 'tools', '右侧工具列表', 'varchar(3000)', 'String', 'tools', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 13, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (488, '27', 'enabled', '是否启用', 'tinyint(1)', 'Integer', 'enabled', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 14, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (489, '27', 'printDesign', '自定义的打印配置', 'varchar(2000)', 'String', 'printdesign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 15, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (490, '27', 'print_data_query_port', '打印时查询数据接口', 'varchar(255)', 'String', 'printDataQueryPort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 16, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (491, '27', 'print_data_query_before', '查询打印数据前处理函数', 'varchar(1000)', 'String', 'printDataQueryBefore', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 17, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (492, '27', 'print_data_query_after', '查询打印数据后处理函数', 'varchar(1000)', 'String', 'printDataQueryAfter', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 18, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (493, '27', 'zoom', '是否允许最大化显示', 'varchar(255)', 'String', 'zoom', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 19, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (494, '27', 'slots', '插槽', 'varchar(500)', 'String', 'slots', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 20, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (495, '27', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 21, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (496, '27', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', NULL, 22, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (497, '27', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', NULL, 23, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (498, '27', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', NULL, 24, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (499, '27', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', NULL, 25, 1, '2023-05-15 21:11:25', NULL, '2023-05-15 21:12:25');
INSERT INTO `gen_table_column` VALUES (540, '29', 'table_id', '表id', 'int(11)', 'Long', 'tableId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (541, '29', 'table_name', '表名称', 'varchar(255)', 'String', 'tableName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (542, '29', 'table_comment', '表描述', 'varchar(255)', 'String', 'tableComment', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (543, '29', 'menu_id', '功能id', 'int(11)', 'Long', 'menuId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (544, '29', 'id', '唯一标识', 'varchar(255)', 'String', 'id', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (545, '29', 'height', '表格的高度；支持铺满父容器或者固定高度，如果设置', 'int(4)', 'Integer', 'height', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (546, '29', 'unique_id', '表格主键字段', 'varchar(255)', 'String', 'uniqueId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (547, '29', 'row_id', '表格行id字段', 'varchar(255)', 'String', 'rowId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (548, '29', 'max_height', '表格的最大高度', 'int(4)', 'Integer', 'maxHeight', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (549, '29', 'auto_resize', '自动监听父元素的变化去重新计算表格（对于父元素可能存在动态变化、显示隐藏的容器中、列宽异常等场景中的可能会用到）', 'tinyint(1)', 'Integer', 'autoResize', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (550, '29', 'sync_resize', '自动跟随某个属性的变化去重新计算表格，和手动调用', 'tinyint(1)', 'Integer', 'syncResize', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (551, '29', 'stripe', '是否带有斑马纹（需要注意的是，在可编辑表格场景下，临时插入的数据不会有斑马纹样式）', 'tinyint(1)', 'Integer', 'stripe', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 12, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (552, '29', 'border', '是否带有边框', 'tinyint(1)', 'Integer', 'border', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 13, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (553, '29', 'round', '是否为圆角边框', 'tinyint(1)', 'Integer', 'round', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 14, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (554, '29', 'size', '表格的尺寸', 'varchar(255)', 'String', 'size', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 15, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (555, '29', 'align', '所有的列对齐方式', 'varchar(255)', 'String', 'align', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 16, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (556, '29', 'header_align', '所有的表头列的对齐方式', 'varchar(255)', 'String', 'headerAlign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 17, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (557, '29', 'footer_align', '所有的表尾列的对齐方式', 'varchar(255)', 'String', 'footerAlign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 18, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (558, '29', 'show_header', '是否显示表头', 'tinyint(1)', 'Integer', 'showHeader', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 19, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (559, '29', 'row_class_name', '给行附加', 'varchar(1000)', 'String', 'rowClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 20, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (560, '29', 'cell_class_name', '给单元格附加', 'varchar(1000)', 'String', 'cellClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 21, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (561, '29', 'header_row_class_name', '给表头的行附加', 'varchar(1000)', 'String', 'headerRowClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 22, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (562, '29', 'header_cell_class_name', '给表头的单元格附加', 'varchar(1000)', 'String', 'headerCellClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 23, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (563, '29', 'footer_row_class_name', '给表尾的行附加', 'varchar(1000)', 'String', 'footerRowClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 24, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (564, '29', 'footer_cell_class_name', '给表尾的单元格附加', 'varchar(1000)', 'String', 'footerCellClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 25, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (565, '29', 'show_footer', '是否显示表尾', 'tinyint(1)', 'Integer', 'showFooter', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 26, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (566, '29', 'footer_method', '表尾的数据获取方法，返回一个二维数组', 'varchar(1000)', 'String', 'footerMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 27, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (567, '29', 'show_overflow', '设置所有内容过长时显示为省略号（如果是固定列建议设置该值，提升渲染速度）', 'varchar(255)', 'String', 'showOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 28, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (568, '29', 'show_header_overflow', '设置表头所有内容过长时显示为省略号', 'varchar(255)', 'String', 'showHeaderOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 29, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (569, '29', 'show_footer_overflow', '设置表尾所有内容过长时显示为省略号', 'varchar(255)', 'String', 'showFooterOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 30, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (570, '29', 'keep_source', '保持原始值的状态，被某些功能所依赖，比如编辑状态、还原数据等（开启后影响性能，具体取决于数据量）', 'tinyint(1)', 'Integer', 'keepSource', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 31, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (571, '29', 'empty_text', '空数据时显示的内容', 'varchar(255)', 'String', 'emptyText', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 32, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (572, '29', 'ext_params', '自定义参数（可以用来存放一些自定义的数据）', 'varchar(255)', 'String', 'extParams', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 33, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (573, '29', 'extra_config', '额外配置（json）', 'longtext', 'String', 'extraConfig', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 34, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (574, '29', 'hand_config', '操作配置', 'varchar(1000)', 'String', 'handConfig', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 35, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (575, '29', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 36, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (576, '29', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 37, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (577, '29', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 38, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (578, '29', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'datetime', NULL, 39, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (579, '29', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 40, 1, '2023-05-15 23:37:31', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (580, '30', 'rmp_id', '数据角色功能授权ID', 'int(11)', 'Long', 'rmpId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-05-23 11:22:56', NULL, '2023-06-06 21:12:29');
INSERT INTO `gen_table_column` VALUES (581, '30', 'role_id', '角色ID', 'int(11)', 'Long', 'roleId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-05-23 11:22:56', NULL, '2023-06-06 21:12:29');
INSERT INTO `gen_table_column` VALUES (582, '30', 'menu_id', '菜单id', 'int(11)', 'Long', 'menuId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-05-23 11:22:56', NULL, '2023-06-06 21:12:29');
INSERT INTO `gen_table_column` VALUES (583, '30', 'power_value', '授权值', 'varchar(255)', 'String', 'powerValue', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1, '2023-05-23 11:22:56', NULL, '2023-06-06 21:12:29');
INSERT INTO `gen_table_column` VALUES (584, '30', 'sort', '排序', 'int(3)', 'Integer', 'sort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-05-23 11:22:56', NULL, '2023-06-06 21:12:29');
INSERT INTO `gen_table_column` VALUES (585, '30', 'status', '状态（0启用', 'int(3)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', NULL, 6, 1, '2023-05-23 11:22:56', NULL, '2023-06-06 21:12:29');
INSERT INTO `gen_table_column` VALUES (588, '30', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-05-23 11:22:56', NULL, '2023-06-06 21:12:29');
INSERT INTO `gen_table_column` VALUES (589, '30', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 8, 1, '2023-05-23 11:22:56', NULL, '2023-06-06 21:12:29');
INSERT INTO `gen_table_column` VALUES (590, '30', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-05-23 11:22:56', NULL, '2023-06-06 21:12:29');
INSERT INTO `gen_table_column` VALUES (591, '30', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 10, 1, '2023-05-23 11:22:56', NULL, '2023-06-06 21:12:29');
INSERT INTO `gen_table_column` VALUES (592, '30', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 11, 1, '2023-05-23 11:22:56', NULL, '2023-06-06 21:12:29');
INSERT INTO `gen_table_column` VALUES (593, '21', 'required', '是否必填', 'tinyint(1)', 'Integer', 'required', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, NULL, '2023-06-01 22:57:23', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (594, '21', 'min', '校验值最小长度', 'int(11)', 'Long', 'min', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, NULL, '2023-06-01 22:57:23', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (595, '21', 'max', '校验值最大长度', 'int(11)', 'Long', 'max', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, NULL, '2023-06-01 22:57:23', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (596, '21', 'type', '数据校验的类型', 'varchar(255)', 'String', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 6, NULL, '2023-06-01 22:57:23', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (597, '21', 'pattern', '正则校验', 'varchar(1000)', 'String', 'pattern', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 7, NULL, '2023-06-01 22:57:23', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (598, '21', 'validator', '自定义校验方法', 'varchar(1000)', 'String', 'validator', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 8, NULL, '2023-06-01 22:57:23', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (599, '21', 'message', '校验提示内容', 'varchar(500)', 'String', 'message', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 9, NULL, '2023-06-01 22:57:23', NULL, NULL);
INSERT INTO `gen_table_column` VALUES (600, '23', 'title_align', '标题对齐方式', 'varchar(255)', 'String', 'titleAlign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (601, '23', 'title_width', '标题宽度', 'int(3)', 'Integer', 'titleWidth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (602, '23', 'title_colon', '是否显示标题冒号', 'tinyint(1)', 'Boolean', 'titleColon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (603, '23', 'title_asterisk', '是否显示必填字段的红色星号', 'tinyint(1)', 'Boolean', 'titleAsterisk', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (604, '23', 'title_overflow', '标题内容过长时显示为省略号', 'varchar(255)', 'String', 'titleOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (605, '23', 'show_title', '是否显示标题', 'tinyint(1)', 'Boolean', 'showTitle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 12, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (606, '23', 'class_name', '给表单项附加', 'varchar(1000)', 'String', 'className', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 13, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (607, '23', 'content_class_name', '给表单项内容附加', 'varchar(500)', 'String', 'contentClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 14, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (608, '23', 'content_style', '给表单项内容附加样式', 'varchar(500)', 'String', 'contentStyle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 15, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (609, '23', 'visible_method', '该方法的返回值用来决定该项是否显示', 'varchar(1000)', 'String', 'visibleMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 17, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (610, '23', 'collapse_node', '折叠节点', 'tinyint(1)', 'Boolean', 'collapseNode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 19, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (611, '23', 'reset_value', '重置时的默认值', 'varchar(255)', 'String', 'resetValue', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 20, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (612, '23', 'item_render', '项渲染器配置项', 'varchar(1000)', 'String', 'itemRender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 21, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (613, '23', 'children', '项集合', 'varchar(1000)', 'String', 'children', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 22, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (614, '23', 'slots', '插槽', 'varchar(500)', 'String', 'slots', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 23, NULL, '2023-06-02 11:22:50', NULL, '2023-06-02 11:25:57');
INSERT INTO `gen_table_column` VALUES (615, '31', 'role_table_id', '表id', 'int(11)', 'Long', 'roleTableId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (616, '31', 'table_name', '表名称', 'varchar(255)', 'String', 'tableName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (617, '31', 'table_comment', '表描述', 'varchar(255)', 'String', 'tableComment', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (618, '31', 'menu_id', '功能id', 'int(11)', 'Long', 'menuId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (619, '31', 'id', '唯一标识', 'varchar(255)', 'String', 'id', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (620, '31', 'height', '表格的高度；支持铺满父容器或者固定高度，如果设置', 'varchar(255)', 'String', 'height', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (621, '31', 'unique_id', '表格主键字段', 'varchar(255)', 'String', 'uniqueId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (622, '31', 'row_id', '表格行id字段', 'varchar(255)', 'String', 'rowId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (623, '31', 'max_height', '表格的最大高度', 'int(4)', 'Integer', 'maxHeight', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (624, '31', 'auto_resize', '自动监听父元素的变化去重新计算表格（对于父元素可能存在动态变化、显示隐藏的容器中、列宽异常等场景中的可能会用到）', 'tinyint(1)', 'Integer', 'autoResize', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (625, '31', 'sync_resize', '自动跟随某个属性的变化去重新计算表格，和手动调用', 'tinyint(1)', 'Integer', 'syncResize', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (626, '31', 'stripe', '是否带有斑马纹（需要注意的是，在可编辑表格场景下，临时插入的数据不会有斑马纹样式）', 'tinyint(1)', 'Integer', 'stripe', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 12, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (627, '31', 'border', '是否带有边框', 'tinyint(1)', 'Integer', 'border', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 13, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (628, '31', 'round', '是否为圆角边框', 'tinyint(1)', 'Integer', 'round', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 14, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (629, '31', 'size', '表格的尺寸', 'varchar(255)', 'String', 'size', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 15, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (630, '31', 'align', '所有的列对齐方式', 'varchar(255)', 'String', 'align', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 16, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (631, '31', 'header_align', '所有的表头列的对齐方式', 'varchar(255)', 'String', 'headerAlign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 17, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (632, '31', 'footer_align', '所有的表尾列的对齐方式', 'varchar(255)', 'String', 'footerAlign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 18, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (633, '31', 'show_header', '是否显示表头', 'tinyint(1)', 'Integer', 'showHeader', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 19, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (634, '31', 'row_class_name', '给行附加', 'varchar(1000)', 'String', 'rowClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 20, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (635, '31', 'cell_class_name', '给单元格附加', 'varchar(1000)', 'String', 'cellClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 21, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (636, '31', 'header_row_class_name', '给表头的行附加', 'varchar(1000)', 'String', 'headerRowClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 22, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (637, '31', 'header_cell_class_name', '给表头的单元格附加', 'varchar(1000)', 'String', 'headerCellClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 23, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (638, '31', 'footer_row_class_name', '给表尾的行附加', 'varchar(1000)', 'String', 'footerRowClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 24, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (639, '31', 'footer_cell_class_name', '给表尾的单元格附加', 'varchar(1000)', 'String', 'footerCellClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 25, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (640, '31', 'show_footer', '是否显示表尾', 'tinyint(1)', 'Integer', 'showFooter', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 26, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (641, '31', 'footer_method', '表尾的数据获取方法，返回一个二维数组', 'varchar(1000)', 'String', 'footerMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 27, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (642, '31', 'show_overflow', '设置所有内容过长时显示为省略号（如果是固定列建议设置该值，提升渲染速度）', 'varchar(255)', 'String', 'showOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 28, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (643, '31', 'show_header_overflow', '设置表头所有内容过长时显示为省略号', 'varchar(255)', 'String', 'showHeaderOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 29, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (644, '31', 'show_footer_overflow', '设置表尾所有内容过长时显示为省略号', 'varchar(255)', 'String', 'showFooterOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 30, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (645, '31', 'keep_source', '保持原始值的状态，被某些功能所依赖，比如编辑状态、还原数据等（开启后影响性能，具体取决于数据量）', 'tinyint(1)', 'Integer', 'keepSource', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 31, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (646, '31', 'empty_text', '空数据时显示的内容', 'varchar(255)', 'String', 'emptyText', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 32, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (647, '31', 'ext_params', '自定义参数（可以用来存放一些自定义的数据）', 'varchar(255)', 'String', 'extParams', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 33, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (648, '31', 'extra_config', '额外配置（json）', 'longtext', 'String', 'extraConfig', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 34, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (649, '31', 'hand_config', '操作配置', 'varchar(1000)', 'String', 'handConfig', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 35, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (650, '31', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 36, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (651, '31', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 37, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (652, '31', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 38, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (653, '31', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 39, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (654, '31', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 40, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:21');
INSERT INTO `gen_table_column` VALUES (655, '32', 'column_id', '编号', 'int(11)', 'Long', 'columnId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (656, '32', 'role_table_id', '归属表编号', 'int(11)', 'Long', 'roleTableId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (657, '32', 'column_name', '列名称', 'varchar(200)', 'String', 'columnName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (658, '32', 'column_comment', '列描述', 'varchar(500)', 'String', 'columnComment', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 4, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (659, '32', 'field', '字段', 'varchar(200)', 'String', 'field', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (660, '32', 'title', '字段名', 'varchar(500)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 6, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (661, '32', 'is_ignore', '是否显示（1是', 'tinyint(1)', 'Integer', 'isIgnore', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (662, '32', 'visible', '是否可视（1是', 'tinyint(1)', 'Integer', 'visible', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (663, '32', 'width', '宽度', 'varchar(255)', 'String', 'width', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (664, '32', 'min_width', '最小列宽度会自动将剩余空间按比例分配', 'double', 'Long', 'minWidth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (665, '32', 'resizable', '列宽是否可拖拽（1是', 'tinyint(1)', 'Integer', 'resizable', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (666, '32', 'align', '列对齐方式：left（左对齐）,', 'varchar(10)', 'String', 'align', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 12, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (667, '32', 'type', '列的类型', 'varchar(255)', 'String', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 13, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (668, '32', 'fixed', '将列固定在左侧或者右侧（注意：固定列应该放在左右两侧的位置）', 'varchar(255)', 'String', 'fixed', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 14, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (669, '32', 'header_align', '表头列的对齐方式', 'varchar(255)', 'String', 'headerAlign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 15, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (670, '32', 'footer_align', '表尾列的对齐方式', 'varchar(255)', 'String', 'footerAlign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 16, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (671, '32', 'show_overflow', '当内容过长时显示为省略号', 'varchar(255)', 'String', 'showOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 17, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (672, '32', 'show_header_overflow', '当表头内容过长时显示为省略号', 'varchar(255)', 'String', 'showHeaderOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 18, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (673, '32', 'show_footer_overflow', '当表尾内容过长时显示为省略号', 'varchar(255)', 'String', 'showFooterOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 19, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (674, '32', 'class_name', '给单元格附加', 'varchar(1000)', 'String', 'className', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 20, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (675, '32', 'header_class_name', '给表头的单元格附加', 'varchar(1000)', 'String', 'headerClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 21, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (676, '32', 'footer_class_name', '给表尾的单元格附加', 'varchar(1000)', 'String', 'footerClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 22, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (677, '32', 'formatter', '格式化显示内容', 'varchar(1000)', 'String', 'formatter', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 23, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (678, '32', 'sortable', '是否允许列排序', 'tinyint(1)', 'Integer', 'sortable', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 24, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (679, '32', 'sort_by', '只对', 'varchar(1000)', 'String', 'sortBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 25, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (680, '32', 'sort_type', '排序的字段类型，比如字符串转数值等', 'varchar(255)', 'String', 'sortType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 26, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (681, '32', 'ext_params', '额外的参数（可以用来存放一些私有参数）', 'varchar(255)', 'String', 'extParams', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 27, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (682, '32', 'tree_node', '只对', 'tinyint(1)', 'Integer', 'treeNode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 28, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (683, '32', 'slots', '插槽', 'varchar(1000)', 'String', 'slots', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 29, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (684, '32', 'filters', '配置筛选条件', 'varchar(1000)', 'String', 'filters', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 30, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (685, '32', 'filter_multiple', '只对', 'tinyint(1)', 'Integer', 'filterMultiple', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 31, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (686, '32', 'filter_method', '只对', 'varchar(1000)', 'String', 'filterMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 32, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (687, '32', 'filter_reset_method', '只对', 'varchar(1000)', 'String', 'filterResetMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 33, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (688, '32', 'filter_recover_method', '只对', 'varchar(1000)', 'String', 'filterRecoverMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 34, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (689, '32', 'filter_render', '筛选渲染器配置项', 'varchar(1000)', 'String', 'filterRender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 35, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (690, '32', 'export_method', '自定义单元格数据导出方法，返回自定义的值', 'varchar(1000)', 'String', 'exportMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 36, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (691, '32', 'footer_export_method', '自定义表尾单元格数据导出方法，返回自定义的值', 'varchar(1000)', 'String', 'footerExportMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 37, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (692, '32', 'title_prefix', '标题前缀图标配置项', 'varchar(500)', 'String', 'titlePrefix', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 38, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (693, '32', 'cell_type', '只对特定功能有效，单元格值类型', 'varchar(255)', 'String', 'cellType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 39, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (694, '32', 'cell_render', '默认的渲染器配置项', 'varchar(1000)', 'String', 'cellRender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 40, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (695, '32', 'edit_render', '可编辑渲染器配置项', 'varchar(1000)', 'String', 'editRender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 41, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (696, '32', 'content_render', '内容渲染配置项', 'varchar(1000)', 'String', 'contentRender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 42, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (697, '32', 'col_id', '自定义列的唯一主键', 'varchar(255)', 'String', 'colId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 43, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (698, '32', 'sort_no', '排序', 'int(11)', 'Long', 'sortNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 44, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (699, '32', 'dict_type', '字典类型', 'text', 'String', 'dictType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 45, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (700, '32', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 46, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (701, '32', 'create_by', '创建者', 'varchar(100)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 47, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (702, '32', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 48, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (703, '32', 'update_by', '更新者', 'varchar(100)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 49, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (704, '32', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 50, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:51:09');
INSERT INTO `gen_table_column` VALUES (705, '33', 'edit_config_id', '可编辑配置项id', 'int(11)', 'Long', 'editConfigId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (706, '33', 'role_table_id', '表id', 'int(11)', 'Long', 'roleTableId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (707, '33', 'triggerbase', '触发方式', 'varchar(255)', 'String', 'triggerbase', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (708, '33', 'enabled', '是否启用', 'tinyint(1)', 'Integer', 'enabled', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (709, '33', 'mode', '编辑模式', 'varchar(255)', 'String', 'mode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (710, '33', 'show_icon', '是否显示列头编辑图标', 'tinyint(1)', 'Integer', 'showIcon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (711, '33', 'show_status', '只对', 'tinyint(1)', 'Integer', 'showStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', NULL, 7, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (712, '33', 'show_update_status', '只对', 'tinyint(1)', 'Integer', 'showUpdateStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', NULL, 8, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (713, '33', 'show_insert_status', '只对', 'tinyint(1)', 'Integer', 'showInsertStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', NULL, 9, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (714, '33', 'show_asterisk', '是否显示必填字段的红色星号', 'tinyint(1)', 'Integer', 'showAsterisk', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (715, '33', 'auto_clear', '当点击非编辑列之后是否自动清除单元格的激活状态', 'tinyint(1)', 'Integer', 'autoClear', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (716, '33', 'before_edit_method', '该方法的返回值用来决定该单元格是否允许编辑', 'varchar(1000)', 'String', 'beforeEditMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 12, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (717, '33', 'icon', '自定义可编辑列的状态图标', 'varchar(255)', 'String', 'icon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 13, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (718, '33', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 14, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (719, '33', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 15, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (720, '33', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 16, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (721, '33', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 17, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (722, '33', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 18, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:18:57');
INSERT INTO `gen_table_column` VALUES (723, '34', 'edit_rules_id', '校验规则配置id', 'int(11)', 'Long', 'editRulesId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (724, '34', 'role_table_id', '表id', 'int(11)', 'Long', 'roleTableId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (725, '34', 'required', '是否必填', 'tinyint(1)', 'Integer', 'required', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (726, '34', 'min', '校验值最小长度', 'int(11)', 'Long', 'min', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (727, '34', 'max', '校验值最大长度', 'int(11)', 'Long', 'max', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (728, '34', 'type', '数据校验的类型', 'varchar(255)', 'String', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 6, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (729, '34', 'pattern', '正则校验', 'varchar(1000)', 'String', 'pattern', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 7, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (730, '34', 'validator', '自定义校验方法', 'varchar(1000)', 'String', 'validator', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 8, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (731, '34', 'message', '校验提示内容', 'varchar(500)', 'String', 'message', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 9, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (732, '34', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (733, '34', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 11, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (734, '34', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 12, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (735, '34', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 13, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (736, '34', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 14, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:08');
INSERT INTO `gen_table_column` VALUES (737, '35', 'form_id', '查询表id', 'int(11)', 'Long', 'formId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (738, '35', 'role_table_id', '表格id', 'int(11)', 'Long', 'roleTableId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (739, '35', 'span', '所有项的栅格占据的列数（共', 'int(3)', 'Integer', 'span', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (740, '35', 'align', '所有项的内容对齐方式', 'varchar(255)', 'String', 'align', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (741, '35', 'size', '尺寸', 'varchar(255)', 'String', 'size', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (742, '35', 'title_align', '所有项的标题对齐方式', 'varchar(255)', 'String', 'titleAlign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (743, '35', 'title_width', '所有项的标题宽度', 'varchar(255)', 'String', 'titleWidth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (744, '35', 'title_colon', '是否显示标题冒号', 'tinyint(1)', 'Integer', 'titleColon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (745, '35', 'title_asterisk', '是否显示必填字段的红色星号', 'tinyint(1)', 'Integer', 'titleAsterisk', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (746, '35', 'title_overflow', '所有设置标题内容过长时显示为省略号', 'varchar(255)', 'String', 'titleOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (747, '35', 'class_name', '给表单附加', 'varchar(1000)', 'String', 'className', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 11, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (748, '35', 'collapse_status', 'v-model', 'tinyint(1)', 'Integer', 'collapseStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', NULL, 12, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (749, '35', 'custom_layout', '是否使用自定义布局', 'tinyint(1)', 'Integer', 'customLayout', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 13, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (750, '35', 'prevent_submit', '是否禁用默认的回车提交方式禁用后配合', 'tinyint(1)', 'Integer', 'preventSubmit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 14, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (751, '35', 'enabled', '是否启用', 'tinyint(1)', 'Integer', 'enabled', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 15, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (752, '35', 'rules', '校验规则配置项', 'varchar(1000)', 'String', 'rules', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 16, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (753, '35', 'valid_config', '检验配置项', 'varchar(1000)', 'String', 'validConfig', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 17, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (754, '35', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 18, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (755, '35', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 19, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (756, '35', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 20, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (757, '35', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 21, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (758, '35', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 22, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:20');
INSERT INTO `gen_table_column` VALUES (759, '36', 'role_item_id', '表单项id', 'int(11)', 'Long', 'roleItemId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (760, '36', 'role_form_id', '表单id', 'int(11)', 'Long', 'roleFormId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (761, '36', 'field', '字段名', 'varchar(255)', 'String', 'field', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (762, '36', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (763, '36', 'span', '栅格占据的列数（共', 'int(2)', 'Integer', 'span', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (764, '36', 'align', '内容对齐方式', 'varchar(255)', 'String', 'align', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (765, '36', 'title_align', '标题对齐方式', 'varchar(255)', 'String', 'titleAlign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (766, '36', 'title_width', '标题宽度', 'int(3)', 'Integer', 'titleWidth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (767, '36', 'title_colon', '是否显示标题冒号', 'tinyint(1)', 'Integer', 'titleColon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (768, '36', 'title_asterisk', '是否显示必填字段的红色星号', 'tinyint(1)', 'Integer', 'titleAsterisk', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (769, '36', 'title_overflow', '标题内容过长时显示为省略号', 'varchar(255)', 'String', 'titleOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (770, '36', 'show_title', '是否显示标题', 'tinyint(1)', 'Integer', 'showTitle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 12, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (771, '36', 'class_name', '给表单项附加', 'varchar(1000)', 'String', 'className', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 13, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (772, '36', 'content_class_name', '给表单项内容附加', 'varchar(500)', 'String', 'contentClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 14, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (773, '36', 'content_style', '给表单项内容附加样式', 'varchar(500)', 'String', 'contentStyle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 15, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (774, '36', 'visible', '默认是否显示', 'tinyint(1)', 'Integer', 'visible', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 16, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (775, '36', 'visible_method', '该方法的返回值用来决定该项是否显示', 'varchar(1000)', 'String', 'visibleMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 17, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (776, '36', 'form_filter', '是否显示删除按钮', 'tinyint(1)', 'Integer', 'formFilter', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 18, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (777, '36', 'folding', '默认收起', 'tinyint(1)', 'Integer', 'folding', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 19, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (778, '36', 'collapse_node', '折叠节点', 'tinyint(1)', 'Integer', 'collapseNode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 20, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (779, '36', 'reset_value', '重置时的默认值', 'varchar(255)', 'String', 'resetValue', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 21, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (780, '36', 'item_render', '项渲染器配置项', 'varchar(1000)', 'String', 'itemRender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 22, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (781, '36', 'children', '项集合', 'varchar(1000)', 'String', 'children', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 23, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (782, '36', 'slots', '插槽', 'varchar(500)', 'String', 'slots', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 24, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (783, '36', 'sort_no', '排序', 'int(11)', 'Long', 'sortNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 25, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (784, '36', 'dict_type', '字典类型', 'varchar(255)', 'String', 'dictType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 26, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (785, '36', 'search_handle', '是否为查询操作项', 'tinyint(1)', 'Integer', 'searchHandle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 27, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (786, '36', 'search_no', '查询排序', 'int(11)', 'Long', 'searchNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 28, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (787, '36', 'search_visible', '是否是查询项', 'tinyint(1)', 'Integer', 'searchVisible', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 29, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (788, '36', 'search_fixed', '是否为固定查询', 'tinyint(1)', 'Integer', 'searchFixed', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 30, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (789, '36', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 31, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (790, '36', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 32, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (791, '36', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 33, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (792, '36', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 34, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (793, '36', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 35, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:35');
INSERT INTO `gen_table_column` VALUES (794, '37', 'pager_id', '分页配置id', 'int(11)', 'Long', 'pagerId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (795, '37', 'role_table_id', '表格id', 'int(11)', 'Long', 'roleTableId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (796, '37', 'layouts', '自定义布局', 'varchar(255)', 'String', 'layouts', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (797, '37', 'current_page', '当前页', 'int(11)', 'Long', 'currentPage', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (798, '37', 'page_size', '每页大小', 'int(11)', 'Long', 'pageSize', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (799, '37', 'pager_count', '显示页码按钮的数量', 'int(11)', 'Long', 'pagerCount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (800, '37', 'page_sizes', '每页大小选项列表', 'varchar(255)', 'String', 'pageSizes', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (801, '37', 'align', '对齐方式', 'varchar(255)', 'String', 'align', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (802, '37', 'border', '带边框', 'tinyint(1)', 'Integer', 'border', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (803, '37', 'background', '带背景颜色', 'tinyint(1)', 'Integer', 'background', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (804, '37', 'perfect', '配套的样式', 'tinyint(1)', 'Integer', 'perfect', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (805, '37', 'class_name', '给分页附加', 'varchar(1000)', 'String', 'className', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 12, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (806, '37', 'auto_hidden', '当只有一页时自动隐藏', 'tinyint(1)', 'Integer', 'autoHidden', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 13, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (807, '37', 'icon_prev_page', '自定义上一页图标', 'varchar(255)', 'String', 'iconPrevPage', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 14, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (808, '37', 'icon_jump_prev', '自定义向上跳页图标', 'varchar(255)', 'String', 'iconJumpPrev', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 15, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (809, '37', 'icon_jump_next', '自定义向下跳页图标', 'varchar(255)', 'String', 'iconJumpNext', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 16, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (810, '37', 'iconnext_page', '自定义下一页图标', 'varchar(255)', 'String', 'iconnextPage', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 17, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (811, '37', 'icon_jump_more', '自定义跳页显示图标', 'varchar(255)', 'String', 'iconJumpMore', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 18, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (812, '37', 'enabled', '是否启用', 'tinyint(1)', 'Integer', 'enabled', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 19, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (813, '37', 'slots', '插槽', 'varchar(500)', 'String', 'slots', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 20, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (814, '37', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 21, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (815, '37', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 22, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (816, '37', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 23, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (817, '37', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 24, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (818, '37', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 25, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:47');
INSERT INTO `gen_table_column` VALUES (819, '38', 'proxy_id', '代理表id', 'int(11)', 'Long', 'proxyId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (820, '38', 'role_table_id', '表格id', 'int(11)', 'Long', 'roleTableId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (821, '38', 'enabled', '是否启用', 'tinyint(1)', 'Integer', 'enabled', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (822, '38', 'auto_load', '是否自动加载查询数据', 'tinyint(1)', 'Integer', 'autoLoad', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (823, '38', 'message', '是否显示内置的消息提示（可以设为', 'tinyint(1)', 'Integer', 'message', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (824, '38', 'seq', '存在', 'tinyint(1)', 'Integer', 'seq', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (825, '38', 'sort', '是否代理排序', 'tinyint(1)', 'Integer', 'sort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (826, '38', 'filter', '是否代理筛选', 'tinyint(1)', 'Integer', 'filter', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (827, '38', 'form', '是否代理表单', 'tinyint(1)', 'Integer', 'form', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (828, '38', 'props', '获取的属性配置', 'varchar(500)', 'String', 'props', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 10, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (829, '38', 'querybase', '查询配置', 'varchar(1000)', 'String', 'querybase', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 11, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (830, '38', 'query_all', '全量查询配置', 'varchar(1000)', 'String', 'queryAll', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 12, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (831, '38', 'deletebase', '删除配置', 'varchar(1000)', 'String', 'deletebase', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 13, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (832, '38', 'insertbase', '插入配置', 'varchar(1000)', 'String', 'insertbase', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 14, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (833, '38', 'updatebase', '更新配置', 'varchar(1000)', 'String', 'updatebase', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 15, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (834, '38', 'save', '保存', 'varchar(1000)', 'String', 'save', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 16, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (835, '38', 'params_change_load', '额外的请求参数改变时是否重新请求数据', 'tinyint(1)', 'Integer', 'paramsChangeLoad', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 17, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (836, '38', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 18, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (837, '38', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 19, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (838, '38', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 20, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (839, '38', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 21, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (840, '38', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 22, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:19:57');
INSERT INTO `gen_table_column` VALUES (841, '39', 'toolbar_config_id', '工具栏配置id', 'int(11)', 'Long', 'toolbarConfigId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (842, '39', 'role_table_id', '表格id', 'int(11)', 'Long', 'roleTableId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (843, '39', 'ext_size', '尺寸(medium,', 'varchar(255)', 'String', 'extSize', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (844, '39', 'loading', '是否加载中', 'tinyint(1)', 'Integer', 'loading', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (845, '39', 'perfect', '配套的样式', 'tinyint(1)', 'Integer', 'perfect', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (846, '39', 'class_name', '给工具栏', 'varchar(1000)', 'String', 'className', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 6, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (847, '39', 'ext_import', '导入按钮配置（需要设置', 'varchar(255)', 'String', 'extImport', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (848, '39', 'ext_export', '导出按钮配置（需要设置', 'varchar(255)', 'String', 'extExport', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (849, '39', 'print', '打印按钮配置', 'varchar(255)', 'String', 'print', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (850, '39', 'refresh', '刷新按钮配置', 'varchar(255)', 'String', 'refresh', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (851, '39', 'custom', '自定义列配置', 'varchar(255)', 'String', 'custom', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (852, '39', 'buttons', '左侧按钮列表', 'varchar(3000)', 'String', 'buttons', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 12, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (853, '39', 'tools', '右侧工具列表', 'varchar(3000)', 'String', 'tools', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 13, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (854, '39', 'enabled', '是否启用', 'tinyint(1)', 'Integer', 'enabled', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 14, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (855, '39', 'print_design', '自定义的打印配置', 'varchar(2000)', 'String', 'printDesign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 15, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (856, '39', 'print_data_query_port', '打印时查询数据接口', 'varchar(255)', 'String', 'printDataQueryPort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 16, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (857, '39', 'print_data_query_before', '查询打印数据前处理函数', 'varchar(1000)', 'String', 'printDataQueryBefore', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 17, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (858, '39', 'print_data_query_after', '查询打印数据后处理函数', 'varchar(1000)', 'String', 'printDataQueryAfter', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 18, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (859, '39', 'zoom', '是否允许最大化显示', 'varchar(255)', 'String', 'zoom', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 19, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (860, '39', 'slots', '插槽', 'varchar(500)', 'String', 'slots', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 20, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (861, '39', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 21, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (862, '39', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 22, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (863, '39', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 23, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (864, '39', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 24, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (865, '39', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 25, 1, '2023-06-06 21:11:48', NULL, '2023-06-06 21:20:14');
INSERT INTO `gen_table_column` VALUES (866, '40', 'role_id', '角色ID', 'bigint(20)', 'Long', 'roleId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-06-07 20:26:10', NULL, '2023-06-07 20:26:47');
INSERT INTO `gen_table_column` VALUES (867, '40', 'menu_id', '菜单ID', 'bigint(20)', 'Long', 'menuId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 2, 1, '2023-06-07 20:26:10', NULL, '2023-06-07 20:26:47');
INSERT INTO `gen_table_column` VALUES (868, '41', 'user_column_id', '用户表格列配置id', 'int(11)', 'Long', 'userColumnId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (869, '41', 'column_id', '编号', 'int(11)', 'Long', 'columnId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (870, '41', 'role_table_id', '角色归属表编号', 'int(11)', 'Long', 'roleTableId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (871, '41', 'column_name', '列名称', 'varchar(200)', 'String', 'columnName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (872, '41', 'column_comment', '列描述', 'varchar(500)', 'String', 'columnComment', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 5, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (873, '41', 'field', '字段', 'varchar(200)', 'String', 'field', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (874, '41', 'title', '字段名', 'varchar(500)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 7, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (875, '41', 'is_ignore', '是否显示（1是', 'tinyint(1)', 'Boolean', 'isIgnore', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (876, '41', 'visible', '是否可视（1是', 'tinyint(1)', 'Boolean', 'visible', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (877, '41', 'width', '宽度', 'varchar(255)', 'String', 'width', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (878, '41', 'min_width', '最小列宽度会自动将剩余空间按比例分配', 'double', 'Long', 'minWidth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (879, '41', 'resizable', '列宽是否可拖拽（1是', 'tinyint(1)', 'Boolean', 'resizable', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 12, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (880, '41', 'align', '列对齐方式：left（左对齐）,', 'varchar(10)', 'String', 'align', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 13, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (881, '41', 'type', '列的类型', 'varchar(255)', 'String', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 14, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (882, '41', 'fixed', '将列固定在左侧或者右侧（注意：固定列应该放在左右两侧的位置）', 'varchar(255)', 'String', 'fixed', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 15, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (883, '41', 'header_align', '表头列的对齐方式', 'varchar(255)', 'String', 'headerAlign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 16, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (884, '41', 'footer_align', '表尾列的对齐方式', 'varchar(255)', 'String', 'footerAlign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 17, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (885, '41', 'show_overflow', '当内容过长时显示为省略号', 'varchar(255)', 'String', 'showOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 18, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (886, '41', 'show_header_overflow', '当表头内容过长时显示为省略号', 'varchar(255)', 'String', 'showHeaderOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 19, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (887, '41', 'show_footer_overflow', '当表尾内容过长时显示为省略号', 'varchar(255)', 'String', 'showFooterOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 20, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (888, '41', 'class_name', '给单元格附加', 'varchar(1000)', 'String', 'className', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 21, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (889, '41', 'header_class_name', '给表头的单元格附加', 'varchar(1000)', 'String', 'headerClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 22, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (890, '41', 'footer_class_name', '给表尾的单元格附加', 'varchar(1000)', 'String', 'footerClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 23, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (891, '41', 'formatter', '格式化显示内容', 'varchar(1000)', 'String', 'formatter', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 24, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (892, '41', 'sortable', '是否允许列排序', 'tinyint(1)', 'Boolean', 'sortable', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 25, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (893, '41', 'sort_by', '只对', 'varchar(1000)', 'String', 'sortBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 26, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (894, '41', 'sort_type', '排序的字段类型，比如字符串转数值等', 'varchar(255)', 'String', 'sortType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 27, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (895, '41', 'ext_params', '额外的参数（可以用来存放一些私有参数）', 'varchar(255)', 'String', 'extParams', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 28, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (896, '41', 'tree_node', '只对', 'tinyint(1)', 'Boolean', 'treeNode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 29, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (897, '41', 'slots', '插槽', 'varchar(1000)', 'String', 'slots', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 30, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (898, '41', 'filters', '配置筛选条件', 'varchar(1000)', 'String', 'filters', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 31, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (899, '41', 'filter_multiple', '只对', 'tinyint(1)', 'Boolean', 'filterMultiple', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 32, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (900, '41', 'filter_method', '只对', 'varchar(1000)', 'String', 'filterMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 33, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (901, '41', 'filter_reset_method', '只对', 'varchar(1000)', 'String', 'filterResetMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 34, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (902, '41', 'filter_recover_method', '只对', 'varchar(1000)', 'String', 'filterRecoverMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 35, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (903, '41', 'filter_render', '筛选渲染器配置项', 'varchar(1000)', 'String', 'filterRender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 36, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (904, '41', 'export_method', '自定义单元格数据导出方法，返回自定义的值', 'varchar(1000)', 'String', 'exportMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 37, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (905, '41', 'footer_export_method', '自定义表尾单元格数据导出方法，返回自定义的值', 'varchar(1000)', 'String', 'footerExportMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 38, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (906, '41', 'title_prefix', '标题前缀图标配置项', 'varchar(500)', 'String', 'titlePrefix', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 39, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (907, '41', 'cell_type', '只对特定功能有效，单元格值类型', 'varchar(255)', 'String', 'cellType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 40, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (908, '41', 'cell_render', '默认的渲染器配置项', 'varchar(1000)', 'String', 'cellRender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 41, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (909, '41', 'edit_render', '可编辑渲染器配置项', 'varchar(1000)', 'String', 'editRender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 42, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (910, '41', 'content_render', '内容渲染配置项', 'varchar(1000)', 'String', 'contentRender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 43, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (911, '41', 'col_id', '自定义列的唯一主键', 'varchar(255)', 'String', 'colId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 44, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (912, '41', 'sort_no', '排序', 'int(11)', 'Long', 'sortNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 45, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (913, '41', 'dict_type', '字典类型', 'text', 'String', 'dictType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 46, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (914, '41', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 47, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (915, '41', 'create_by', '创建者', 'varchar(100)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 48, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (916, '41', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 49, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (917, '41', 'update_by', '更新者', 'varchar(100)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 50, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (918, '41', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 51, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:05:45');
INSERT INTO `gen_table_column` VALUES (919, '42', 'user_item_id', '用户表单项id', 'int(11)', 'Long', 'userItemId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (920, '42', 'item_id', '表单项id', 'int(11)', 'Long', 'itemId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (921, '42', 'role_form_id', '角色表单id', 'int(11)', 'Long', 'roleFormId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (922, '42', 'field', '字段名', 'varchar(255)', 'String', 'field', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (923, '42', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 5, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (924, '42', 'span', '栅格占据的列数（共', 'int(2)', 'Integer', 'span', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 6, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (925, '42', 'align', '内容对齐方式', 'varchar(255)', 'String', 'align', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 7, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (926, '42', 'title_align', '标题对齐方式', 'varchar(255)', 'String', 'titleAlign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 8, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (927, '42', 'title_width', '标题宽度', 'int(3)', 'Integer', 'titleWidth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 9, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (928, '42', 'title_colon', '是否显示标题冒号', 'tinyint(1)', 'Boolean', 'titleColon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 10, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (929, '42', 'title_asterisk', '是否显示必填字段的红色星号', 'tinyint(1)', 'Boolean', 'titleAsterisk', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 11, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (930, '42', 'title_overflow', '标题内容过长时显示为省略号', 'varchar(255)', 'String', 'titleOverflow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 12, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (931, '42', 'show_title', '是否显示标题', 'tinyint(1)', 'Boolean', 'showTitle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 13, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (932, '42', 'class_name', '给表单项附加', 'varchar(1000)', 'String', 'className', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 14, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (933, '42', 'content_class_name', '给表单项内容附加', 'varchar(500)', 'String', 'contentClassName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 15, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (934, '42', 'content_style', '给表单项内容附加样式', 'varchar(500)', 'String', 'contentStyle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 16, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (935, '42', 'visible', '默认是否显示', 'tinyint(1)', 'Boolean', 'visible', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 17, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (936, '42', 'visible_method', '该方法的返回值用来决定该项是否显示', 'varchar(1000)', 'String', 'visibleMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 18, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (937, '42', 'form_filter', '是否显示删除按钮', 'tinyint(1)', 'Boolean', 'formFilter', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 19, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (938, '42', 'folding', '默认收起', 'tinyint(1)', 'Boolean', 'folding', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 20, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (939, '42', 'collapse_node', '折叠节点', 'tinyint(1)', 'Boolean', 'collapseNode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 21, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (940, '42', 'reset_value', '重置时的默认值', 'varchar(255)', 'String', 'resetValue', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 22, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (941, '42', 'item_render', '项渲染器配置项', 'varchar(1000)', 'String', 'itemRender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 23, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (942, '42', 'children', '项集合', 'varchar(1000)', 'String', 'children', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 24, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (943, '42', 'slots', '插槽', 'varchar(500)', 'String', 'slots', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 25, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (944, '42', 'sort_no', '排序', 'int(11)', 'Long', 'sortNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 26, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (945, '42', 'dict_type', '字典类型', 'varchar(255)', 'String', 'dictType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 27, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (946, '42', 'search_handle', '是否为查询操作项', 'tinyint(1)', 'Boolean', 'searchHandle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 28, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (947, '42', 'search_no', '查询排序', 'int(11)', 'Long', 'searchNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 29, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (948, '42', 'search_visible', '是否是查询项', 'tinyint(1)', 'Boolean', 'searchVisible', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 30, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (949, '42', 'search_fixed', '是否为固定查询', 'tinyint(1)', 'Boolean', 'searchFixed', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 31, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (950, '42', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', NULL, 32, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (951, '42', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 33, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (952, '42', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 34, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (953, '42', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', NULL, 35, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');
INSERT INTO `gen_table_column` VALUES (954, '42', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', NULL, 36, 1, '2023-06-13 21:30:16', NULL, '2023-06-13 22:04:36');

-- ----------------------------
-- Table structure for my_flyway_schema_history
-- ----------------------------
DROP TABLE IF EXISTS `my_flyway_schema_history`;
CREATE TABLE `my_flyway_schema_history`  (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `script` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `checksum` int(11) NULL DEFAULT NULL,
  `installed_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`) USING BTREE,
  INDEX `my_flyway_schema_history_s_idx`(`success`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_flyway_schema_history
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_param` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', NULL, 'Y', 1, '2023-01-31 20:38:15', 1, NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', NULL, 'Y', 1, '2023-01-31 20:38:15', 1, NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', NULL, 'Y', 1, '2023-01-31 20:38:15', 1, '2023-02-15 13:12:11', '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'true', NULL, 'Y', 1, '2023-01-31 20:38:15', 1, '2023-02-15 14:53:34', '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '灰色模式', 'sys.config.grayMode', 'false', NULL, 'Y', NULL, NULL, 1, '2023-08-08 20:59:49', '键值为true时开启灰色模式');
INSERT INTO `sys_config` VALUES (6, '侧边栏标题内容', 'sys.sidebar.container', 'true', '{\"sidebarTitle\":\"ruoyi-vxeTable\",\"sidebarLogo\":true}', 'Y', NULL, NULL, 1, '2023-08-08 22:11:08', '键值为true时启用该配置；sidebarTitle：项目名称，sidebarLogo：是否显示logo，sidebarLogoUrl：logo地址');
INSERT INTO `sys_config` VALUES (7, '初始化所有字典', 'sys.init.dict', 'false', NULL, 'Y', NULL, NULL, 1, '2023-08-08 22:09:33', '键值为true时在登录之后初始化所有字典');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 1, '2023-01-31 20:38:15', NULL, NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 1, '2023-01-31 20:38:15', NULL, NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 1, '2023-01-31 20:38:15', NULL, NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 1, '2023-01-31 20:38:15', NULL, NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 1, '2023-01-31 20:38:15', NULL, NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 1, '2023-01-31 20:38:15', NULL, NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 1, '2023-01-31 20:38:15', NULL, NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 1, '2023-01-31 20:38:15', NULL, NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 1, '2023-01-31 20:38:15', NULL, NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 1, '2023-01-31 20:38:15', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', NULL, NULL, 'Y', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', NULL, NULL, 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', NULL, NULL, 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', NULL, 'primary', 'Y', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', NULL, 'danger', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', NULL, 'primary', 'Y', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', NULL, 'danger', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', NULL, 'primary', 'Y', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', NULL, 'danger', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', NULL, NULL, 'Y', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', NULL, NULL, 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', NULL, 'primary', 'Y', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', NULL, 'danger', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', NULL, 'warning', 'Y', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', NULL, 'success', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', NULL, 'primary', 'Y', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', NULL, 'danger', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', NULL, 'info', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', NULL, 'info', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', NULL, 'info', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', NULL, 'danger', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', NULL, 'primary', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', NULL, 'warning', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', NULL, 'warning', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', NULL, 'danger', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', NULL, 'warning', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', NULL, 'danger', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', NULL, 'primary', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', NULL, 'danger', 'N', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `dict_describe` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '用户性别【sys_user_sex】', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '菜单状态【sys_show_hide】', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '系统开关【sys_normal_disable】', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '任务状态【sys_job_status】', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '任务分组【sys_job_group】', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '系统是否【sys_yes_no】', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '通知类型【sys_notice_type】', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '通知状态【sys_notice_status】', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '操作类型【sys_oper_type】', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '系统状态【sys_common_status】', '0', 1, '2023-01-31 20:38:15', NULL, NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '1', '1', '1', 1, '2023-01-31 20:38:15', 1, '2023-02-26 19:37:24', NULL);
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 1, '2023-01-31 20:38:15', NULL, '2023-02-03 14:29:11', NULL);
INSERT INTO `sys_job` VALUES (4, '测试', 'DEFAULT', 'ryTask.ryNoParams', '* 1 * * * ?', '3', '1', '1', 1, '2023-02-26 19:48:49', 1, '2023-02-26 20:19:14', NULL);
INSERT INTO `sys_job` VALUES (5, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '* * * * * ?', '1', '1', '1', 1, '2023-06-18 17:20:03', 1, '2023-07-19 20:29:53', NULL);

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
INSERT INTO `sys_job_log` VALUES (1, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable', '同步用户表格 总共耗时：10045毫秒', '0', '', '2023-07-16 17:59:41');
INSERT INTO `sys_job_log` VALUES (2, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable', '同步用户表格 总共耗时：10036毫秒', '0', '', '2023-07-18 20:57:38');
INSERT INTO `sys_job_log` VALUES (3, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable', '同步用户表格 总共耗时：8838毫秒', '0', '', '2023-07-18 21:43:49');
INSERT INTO `sys_job_log` VALUES (4, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：20508毫秒', '0', '', '2023-07-18 22:02:52');
INSERT INTO `sys_job_log` VALUES (5, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：5640毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:26:23');
INSERT INTO `sys_job_log` VALUES (6, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：1毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:26:44');
INSERT INTO `sys_job_log` VALUES (7, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：1毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:27:14');
INSERT INTO `sys_job_log` VALUES (8, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：1毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:27:26');
INSERT INTO `sys_job_log` VALUES (9, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：2毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:28:11');
INSERT INTO `sys_job_log` VALUES (10, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：2毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:29:30');
INSERT INTO `sys_job_log` VALUES (11, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：31898毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:30:33');
INSERT INTO `sys_job_log` VALUES (12, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：1毫秒', '1', 'java.lang.NoSuchMethodException: com.ruoyi.job.task.RoleTask$$M$_jr_4CF4D528713D02F8_3.asyncUserTable(java.lang.Boolean)\r\n	at java.lang.Class.getMethod(Class.java:1786)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:49)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', '2023-07-18 22:32:53');
INSERT INTO `sys_job_log` VALUES (13, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：2毫秒', '1', 'java.lang.NoSuchMethodException: com.ruoyi.job.task.RoleTask$$M$_jr_4CF4D528713D02F8_3.asyncUserTable(java.lang.Boolean)\r\n	at java.lang.Class.getMethod(Class.java:1786)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:49)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', '2023-07-18 22:33:23');
INSERT INTO `sys_job_log` VALUES (14, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：0毫秒', '1', 'java.lang.NoSuchMethodException: com.ruoyi.job.task.RoleTask$$M$_jr_4CF4D528713D02F8_3.asyncUserTable(java.lang.Boolean)\r\n	at java.lang.Class.getMethod(Class.java:1786)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:49)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', '2023-07-18 22:33:33');
INSERT INTO `sys_job_log` VALUES (15, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：1毫秒', '1', 'java.lang.NoSuchMethodException: com.ruoyi.job.task.RoleTask$$M$_jr_4CF4D528713D02F8_3.asyncUserTable(java.lang.Boolean)\r\n	at java.lang.Class.getMethod(Class.java:1786)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:49)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', '2023-07-18 22:36:07');
INSERT INTO `sys_job_log` VALUES (16, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：52422毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:40:13');
INSERT INTO `sys_job_log` VALUES (17, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：14毫秒', '0', '', '2023-07-18 22:41:28');
INSERT INTO `sys_job_log` VALUES (18, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：20048毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:42:22');
INSERT INTO `sys_job_log` VALUES (19, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：18526毫秒', '0', '', '2023-07-18 22:43:38');
INSERT INTO `sys_job_log` VALUES (20, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：3265毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:46:08');
INSERT INTO `sys_job_log` VALUES (21, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：10806毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:47:41');
INSERT INTO `sys_job_log` VALUES (22, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：5797毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:51:20');
INSERT INTO `sys_job_log` VALUES (23, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：109931毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:53:53');
INSERT INTO `sys_job_log` VALUES (24, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：28043毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:55:04');
INSERT INTO `sys_job_log` VALUES (25, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：18565毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:55:36');
INSERT INTO `sys_job_log` VALUES (26, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：169941毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 22:59:32');
INSERT INTO `sys_job_log` VALUES (27, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：64807毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 23:00:53');
INSERT INTO `sys_job_log` VALUES (28, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：73679毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:50)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 23:03:17');
INSERT INTO `sys_job_log` VALUES (29, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable()', '同步用户表格 总共耗时：12486毫秒', '1', 'java.lang.NoSuchMethodException: com.ruoyi.job.task.RoleTask$$M$_jr_4CF4D528713D02F8_8.asyncUserTable()\r\n	at java.lang.Class.getMethod(Class.java:1786)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:52)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', '2023-07-18 23:03:47');
INSERT INTO `sys_job_log` VALUES (30, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable()', '同步用户表格 总共耗时：27111毫秒', '1', 'java.lang.NoSuchMethodException: com.ruoyi.job.task.RoleTask$$M$_jr_4CF4D528713D02F8_8.asyncUserTable()\r\n	at java.lang.Class.getMethod(Class.java:1786)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:52)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', '2023-07-18 23:04:36');
INSERT INTO `sys_job_log` VALUES (31, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable()', '同步用户表格 总共耗时：9823毫秒', '1', 'java.lang.NoSuchMethodException: com.ruoyi.job.task.RoleTask$$M$_jr_4CF4D528713D02F8_8.asyncUserTable()\r\n	at java.lang.Class.getMethod(Class.java:1786)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:52)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', '2023-07-18 23:04:48');
INSERT INTO `sys_job_log` VALUES (32, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable()', '同步用户表格 总共耗时：70560毫秒', '1', 'java.lang.NoSuchMethodException: com.ruoyi.job.task.RoleTask$$M$_jr_4CF4D528713D02F8_8.asyncUserTable()\r\n	at java.lang.Class.getMethod(Class.java:1786)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:52)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', '2023-07-18 23:06:08');
INSERT INTO `sys_job_log` VALUES (33, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable()', '同步用户表格 总共耗时：9455毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:53)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 23:07:41');
INSERT INTO `sys_job_log` VALUES (34, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable()', '同步用户表格 总共耗时：4277毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:53)\r\n	at com.ruoyi.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:31)\r\n	at com.ruoyi.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:17)\r\n	at com.ruoyi.job.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:39)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: java.lang.NullPointerException\r\n	at com.alibaba.cloud.sentinel.feign.SentinelInvocationHandler.invoke(SentinelInvocationHandler.java:100)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory$1.proceed(FeignCachingInvocationHandlerFactory.java:66)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.lambda$invoke$0(CacheInterceptor.java:54)\r\n	at org.springframework.cache.interceptor.CacheAspectSupport.execute(CacheAspectSupport.java:351)\r\n	at org.springframework.cache.interceptor.CacheInterceptor.invoke(CacheInterceptor.java:64)\r\n	at org.springframework.cloud.openfeign.FeignCachingInvocationHandlerFactory.lambda$create$1(FeignCachingInvocationHandlerFactory.java:53)\r\n	at com.sun.proxy.$Proxy169.asyncUserTable(Unknown Source)\r\n	at com.ruoyi.job.task.RoleTask.asyncUserTable(RoleTask.java:21)\r\n	... 10 more\r\n', '2023-07-18 23:08:30');
INSERT INTO `sys_job_log` VALUES (35, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable()', '同步用户表格 总共耗时：8337毫秒', '0', '', '2023-07-18 23:11:47');
INSERT INTO `sys_job_log` VALUES (36, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable()', '同步用户表格 总共耗时：3118毫秒', '0', '', '2023-07-18 23:14:12');
INSERT INTO `sys_job_log` VALUES (37, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable()', '同步用户表格 总共耗时：11217毫秒', '0', '', '2023-07-18 23:14:55');
INSERT INTO `sys_job_log` VALUES (38, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable()', '同步用户表格 总共耗时：6025毫秒', '0', '', '2023-07-18 23:19:01');
INSERT INTO `sys_job_log` VALUES (39, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable()', '同步用户表格 总共耗时：17474毫秒', '0', '', '2023-07-18 23:21:24');
INSERT INTO `sys_job_log` VALUES (40, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：12400毫秒', '0', '', '2023-07-19 20:31:06');
INSERT INTO `sys_job_log` VALUES (41, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：12268毫秒', '0', '', '2023-07-19 20:31:33');
INSERT INTO `sys_job_log` VALUES (42, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：3408毫秒', '0', '', '2023-07-19 22:30:14');
INSERT INTO `sys_job_log` VALUES (43, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：8092毫秒', '0', '', '2023-07-19 22:31:15');
INSERT INTO `sys_job_log` VALUES (44, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：5508毫秒', '0', '', '2023-07-19 22:36:09');
INSERT INTO `sys_job_log` VALUES (45, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：4872毫秒', '0', '', '2023-07-19 22:39:06');
INSERT INTO `sys_job_log` VALUES (46, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：12819毫秒', '0', '', '2023-07-19 22:40:21');
INSERT INTO `sys_job_log` VALUES (47, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：13074毫秒', '0', '', '2023-07-19 22:44:06');
INSERT INTO `sys_job_log` VALUES (48, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：12471毫秒', '0', '', '2023-07-19 22:45:39');
INSERT INTO `sys_job_log` VALUES (49, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：14628毫秒', '0', '', '2023-07-19 22:46:17');
INSERT INTO `sys_job_log` VALUES (50, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：10025毫秒', '0', '', '2023-07-19 22:47:58');
INSERT INTO `sys_job_log` VALUES (51, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：135毫秒', '0', '', '2023-07-19 22:50:21');
INSERT INTO `sys_job_log` VALUES (52, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：10017毫秒', '0', '', '2023-07-19 23:03:26');
INSERT INTO `sys_job_log` VALUES (53, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：1421毫秒', '0', '', '2023-07-21 19:44:20');
INSERT INTO `sys_job_log` VALUES (54, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：1187毫秒', '0', '', '2023-07-26 21:57:33');
INSERT INTO `sys_job_log` VALUES (55, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：1030毫秒', '0', '', '2023-07-26 22:04:24');
INSERT INTO `sys_job_log` VALUES (56, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：1010毫秒', '0', '', '2023-07-26 22:08:11');
INSERT INTO `sys_job_log` VALUES (57, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：891毫秒', '0', '', '2023-07-26 22:10:43');
INSERT INTO `sys_job_log` VALUES (58, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：897毫秒', '0', '', '2023-07-26 22:19:26');
INSERT INTO `sys_job_log` VALUES (59, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：648毫秒', '0', '', '2023-07-26 22:28:53');
INSERT INTO `sys_job_log` VALUES (60, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：501毫秒', '0', '', '2023-07-26 22:34:33');
INSERT INTO `sys_job_log` VALUES (61, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：650毫秒', '0', '', '2023-07-26 22:37:49');
INSERT INTO `sys_job_log` VALUES (62, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：601毫秒', '0', '', '2023-07-26 22:41:42');
INSERT INTO `sys_job_log` VALUES (63, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：642毫秒', '0', '', '2023-07-26 22:41:49');
INSERT INTO `sys_job_log` VALUES (64, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：471毫秒', '0', '', '2023-07-26 22:43:10');
INSERT INTO `sys_job_log` VALUES (65, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：600毫秒', '0', '', '2023-07-26 22:44:51');
INSERT INTO `sys_job_log` VALUES (66, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：843毫秒', '0', '', '2023-07-26 22:46:27');
INSERT INTO `sys_job_log` VALUES (67, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：578毫秒', '0', '', '2023-07-26 22:46:36');
INSERT INTO `sys_job_log` VALUES (68, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：647毫秒', '0', '', '2023-07-26 22:46:58');
INSERT INTO `sys_job_log` VALUES (69, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：424毫秒', '0', '', '2023-07-26 22:48:30');
INSERT INTO `sys_job_log` VALUES (70, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：1566毫秒', '0', '', '2023-07-26 22:51:29');
INSERT INTO `sys_job_log` VALUES (71, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：563毫秒', '0', '', '2023-07-26 22:51:38');
INSERT INTO `sys_job_log` VALUES (72, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：559毫秒', '0', '', '2023-07-26 22:52:10');
INSERT INTO `sys_job_log` VALUES (73, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：967毫秒', '0', '', '2023-07-26 22:52:26');
INSERT INTO `sys_job_log` VALUES (74, '同步用户表格', 'DEFAULT', 'roleTask.asyncUserTable(true)', '同步用户表格 总共耗时：1348毫秒', '0', '', '2023-07-27 20:32:26');

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示信息',
  `access_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 504 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (1, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-04-25 12:02:13');
INSERT INTO `sys_logininfor` VALUES (2, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-04-25 13:39:12');
INSERT INTO `sys_logininfor` VALUES (3, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-04-26 19:57:18');
INSERT INTO `sys_logininfor` VALUES (4, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-04-26 19:58:14');
INSERT INTO `sys_logininfor` VALUES (5, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-04-26 19:58:24');
INSERT INTO `sys_logininfor` VALUES (6, '', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-04 16:49:50');
INSERT INTO `sys_logininfor` VALUES (7, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-04 16:50:00');
INSERT INTO `sys_logininfor` VALUES (8, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-04 17:51:25');
INSERT INTO `sys_logininfor` VALUES (9, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-04 17:51:32');
INSERT INTO `sys_logininfor` VALUES (10, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-04 18:05:54');
INSERT INTO `sys_logininfor` VALUES (11, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-04 18:06:01');
INSERT INTO `sys_logininfor` VALUES (12, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-04 18:15:42');
INSERT INTO `sys_logininfor` VALUES (13, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-06 20:41:11');
INSERT INTO `sys_logininfor` VALUES (14, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-07 10:37:55');
INSERT INTO `sys_logininfor` VALUES (15, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-07 10:39:08');
INSERT INTO `sys_logininfor` VALUES (16, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-08 09:33:03');
INSERT INTO `sys_logininfor` VALUES (17, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-08 20:02:37');
INSERT INTO `sys_logininfor` VALUES (18, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-09 20:24:27');
INSERT INTO `sys_logininfor` VALUES (19, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-11 20:38:26');
INSERT INTO `sys_logininfor` VALUES (20, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-11 22:46:21');
INSERT INTO `sys_logininfor` VALUES (21, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-12 19:09:28');
INSERT INTO `sys_logininfor` VALUES (22, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-12 19:09:43');
INSERT INTO `sys_logininfor` VALUES (23, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-13 20:11:19');
INSERT INTO `sys_logininfor` VALUES (24, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-13 20:17:19');
INSERT INTO `sys_logininfor` VALUES (25, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-14 11:49:12');
INSERT INTO `sys_logininfor` VALUES (26, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-14 12:03:04');
INSERT INTO `sys_logininfor` VALUES (27, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-14 15:20:47');
INSERT INTO `sys_logininfor` VALUES (28, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-15 20:19:21');
INSERT INTO `sys_logininfor` VALUES (29, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-16 20:42:18');
INSERT INTO `sys_logininfor` VALUES (30, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-17 19:35:32');
INSERT INTO `sys_logininfor` VALUES (31, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-17 19:37:10');
INSERT INTO `sys_logininfor` VALUES (32, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-18 20:26:28');
INSERT INTO `sys_logininfor` VALUES (33, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 09:11:31');
INSERT INTO `sys_logininfor` VALUES (34, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 09:14:57');
INSERT INTO `sys_logininfor` VALUES (35, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 09:50:38');
INSERT INTO `sys_logininfor` VALUES (36, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 09:50:38');
INSERT INTO `sys_logininfor` VALUES (37, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 09:50:52');
INSERT INTO `sys_logininfor` VALUES (38, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 10:04:26');
INSERT INTO `sys_logininfor` VALUES (39, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 10:04:31');
INSERT INTO `sys_logininfor` VALUES (40, 'ry', '127.0.0.1', '', '', '', '1', '密码输入错误1次', '2023-05-22 10:04:37');
INSERT INTO `sys_logininfor` VALUES (41, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 10:04:48');
INSERT INTO `sys_logininfor` VALUES (42, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 10:05:10');
INSERT INTO `sys_logininfor` VALUES (43, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 10:05:22');
INSERT INTO `sys_logininfor` VALUES (44, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 10:19:27');
INSERT INTO `sys_logininfor` VALUES (45, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 10:19:33');
INSERT INTO `sys_logininfor` VALUES (46, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 10:39:03');
INSERT INTO `sys_logininfor` VALUES (47, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 10:39:14');
INSERT INTO `sys_logininfor` VALUES (48, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 10:40:36');
INSERT INTO `sys_logininfor` VALUES (49, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 10:40:40');
INSERT INTO `sys_logininfor` VALUES (50, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 11:11:01');
INSERT INTO `sys_logininfor` VALUES (51, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 11:11:18');
INSERT INTO `sys_logininfor` VALUES (52, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 11:45:38');
INSERT INTO `sys_logininfor` VALUES (53, 'ry', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-05-22 12:00:30');
INSERT INTO `sys_logininfor` VALUES (54, 'ry', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-05-22 12:04:28');
INSERT INTO `sys_logininfor` VALUES (55, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 12:04:33');
INSERT INTO `sys_logininfor` VALUES (56, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 13:18:48');
INSERT INTO `sys_logininfor` VALUES (57, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 13:19:37');
INSERT INTO `sys_logininfor` VALUES (58, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 13:20:25');
INSERT INTO `sys_logininfor` VALUES (59, 'ry', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-05-22 13:25:09');
INSERT INTO `sys_logininfor` VALUES (60, 'ry', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-05-22 13:26:38');
INSERT INTO `sys_logininfor` VALUES (61, 'ry', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-05-22 13:36:08');
INSERT INTO `sys_logininfor` VALUES (62, 'ry', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-05-22 13:40:56');
INSERT INTO `sys_logininfor` VALUES (63, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 13:41:06');
INSERT INTO `sys_logininfor` VALUES (64, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 14:24:28');
INSERT INTO `sys_logininfor` VALUES (65, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 14:25:02');
INSERT INTO `sys_logininfor` VALUES (66, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 14:25:37');
INSERT INTO `sys_logininfor` VALUES (67, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 14:26:06');
INSERT INTO `sys_logininfor` VALUES (68, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 14:57:05');
INSERT INTO `sys_logininfor` VALUES (69, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 14:57:17');
INSERT INTO `sys_logininfor` VALUES (70, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 19:49:39');
INSERT INTO `sys_logininfor` VALUES (71, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 19:50:30');
INSERT INTO `sys_logininfor` VALUES (72, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 19:50:35');
INSERT INTO `sys_logininfor` VALUES (73, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 21:13:11');
INSERT INTO `sys_logininfor` VALUES (74, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 21:13:22');
INSERT INTO `sys_logininfor` VALUES (75, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 21:34:37');
INSERT INTO `sys_logininfor` VALUES (76, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 21:40:58');
INSERT INTO `sys_logininfor` VALUES (77, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 21:41:09');
INSERT INTO `sys_logininfor` VALUES (78, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 21:44:54');
INSERT INTO `sys_logininfor` VALUES (79, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 21:45:06');
INSERT INTO `sys_logininfor` VALUES (80, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-22 21:45:30');
INSERT INTO `sys_logininfor` VALUES (81, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-22 21:45:41');
INSERT INTO `sys_logininfor` VALUES (82, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-23 10:33:13');
INSERT INTO `sys_logininfor` VALUES (83, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-23 10:39:31');
INSERT INTO `sys_logininfor` VALUES (84, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-23 10:39:53');
INSERT INTO `sys_logininfor` VALUES (85, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-23 10:40:01');
INSERT INTO `sys_logininfor` VALUES (86, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-23 10:48:13');
INSERT INTO `sys_logininfor` VALUES (87, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-23 10:48:25');
INSERT INTO `sys_logininfor` VALUES (88, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-23 11:22:44');
INSERT INTO `sys_logininfor` VALUES (89, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-23 11:22:48');
INSERT INTO `sys_logininfor` VALUES (90, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-23 17:21:15');
INSERT INTO `sys_logininfor` VALUES (91, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-23 17:21:20');
INSERT INTO `sys_logininfor` VALUES (92, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-25 20:16:02');
INSERT INTO `sys_logininfor` VALUES (93, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-25 20:17:55');
INSERT INTO `sys_logininfor` VALUES (94, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-25 21:23:36');
INSERT INTO `sys_logininfor` VALUES (95, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-27 19:28:15');
INSERT INTO `sys_logininfor` VALUES (96, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-27 20:53:34');
INSERT INTO `sys_logininfor` VALUES (97, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-28 19:53:49');
INSERT INTO `sys_logininfor` VALUES (98, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-28 19:54:11');
INSERT INTO `sys_logininfor` VALUES (99, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-29 11:31:52');
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 11:53:30');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-29 11:55:07');
INSERT INTO `sys_logininfor` VALUES (102, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 11:55:29');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 16:51:18');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 16:51:24');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-29 16:52:08');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 16:56:17');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-29 16:57:42');
INSERT INTO `sys_logininfor` VALUES (108, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 16:57:52');
INSERT INTO `sys_logininfor` VALUES (109, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-29 17:33:42');
INSERT INTO `sys_logininfor` VALUES (110, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 17:36:10');
INSERT INTO `sys_logininfor` VALUES (111, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 17:46:31');
INSERT INTO `sys_logininfor` VALUES (112, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 17:52:18');
INSERT INTO `sys_logininfor` VALUES (113, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 17:58:30');
INSERT INTO `sys_logininfor` VALUES (114, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 17:59:39');
INSERT INTO `sys_logininfor` VALUES (115, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 18:05:10');
INSERT INTO `sys_logininfor` VALUES (116, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-29 20:19:58');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 20:20:16');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-29 20:20:48');
INSERT INTO `sys_logininfor` VALUES (119, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 20:21:01');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 21:07:38');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-29 21:09:19');
INSERT INTO `sys_logininfor` VALUES (122, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 21:09:30');
INSERT INTO `sys_logininfor` VALUES (123, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 21:30:56');
INSERT INTO `sys_logininfor` VALUES (124, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 21:47:20');
INSERT INTO `sys_logininfor` VALUES (125, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 22:04:22');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-29 22:28:19');
INSERT INTO `sys_logininfor` VALUES (127, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-30 20:12:16');
INSERT INTO `sys_logininfor` VALUES (128, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-30 20:12:45');
INSERT INTO `sys_logininfor` VALUES (129, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-05-31 15:50:36');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-05-31 15:50:41');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-01 09:28:01');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-01 09:28:30');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-01 19:29:14');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-02 11:08:51');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-02 11:08:59');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-03 13:19:23');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-03 13:20:02');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-04 11:03:46');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-04 11:17:08');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-05 11:13:15');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-05 11:13:28');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-06 13:38:06');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-06 14:49:46');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-06 20:16:20');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-07 20:11:52');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-07 20:11:57');
INSERT INTO `sys_logininfor` VALUES (147, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-08 09:17:07');
INSERT INTO `sys_logininfor` VALUES (148, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-08 09:17:13');
INSERT INTO `sys_logininfor` VALUES (149, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-10 13:40:36');
INSERT INTO `sys_logininfor` VALUES (150, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-11 10:26:21');
INSERT INTO `sys_logininfor` VALUES (151, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-11 21:51:58');
INSERT INTO `sys_logininfor` VALUES (152, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-12 10:47:51');
INSERT INTO `sys_logininfor` VALUES (153, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-12 11:28:10');
INSERT INTO `sys_logininfor` VALUES (154, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-12 11:28:10');
INSERT INTO `sys_logininfor` VALUES (155, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-12 11:28:16');
INSERT INTO `sys_logininfor` VALUES (156, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-12 11:30:21');
INSERT INTO `sys_logininfor` VALUES (157, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-12 11:42:55');
INSERT INTO `sys_logininfor` VALUES (158, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-12 11:43:13');
INSERT INTO `sys_logininfor` VALUES (159, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-12 11:45:36');
INSERT INTO `sys_logininfor` VALUES (160, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-12 11:45:51');
INSERT INTO `sys_logininfor` VALUES (161, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-12 11:46:53');
INSERT INTO `sys_logininfor` VALUES (162, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-12 11:51:38');
INSERT INTO `sys_logininfor` VALUES (163, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-12 11:57:11');
INSERT INTO `sys_logininfor` VALUES (164, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-12 11:57:16');
INSERT INTO `sys_logininfor` VALUES (165, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-12 12:02:56');
INSERT INTO `sys_logininfor` VALUES (166, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-12 12:03:00');
INSERT INTO `sys_logininfor` VALUES (167, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-12 19:43:53');
INSERT INTO `sys_logininfor` VALUES (168, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-13 14:13:55');
INSERT INTO `sys_logininfor` VALUES (169, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-13 14:23:19');
INSERT INTO `sys_logininfor` VALUES (170, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-14 20:16:04');
INSERT INTO `sys_logininfor` VALUES (171, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-14 20:16:38');
INSERT INTO `sys_logininfor` VALUES (172, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-15 17:04:28');
INSERT INTO `sys_logininfor` VALUES (173, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-15 19:48:08');
INSERT INTO `sys_logininfor` VALUES (174, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-15 19:49:27');
INSERT INTO `sys_logininfor` VALUES (175, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-15 20:56:34');
INSERT INTO `sys_logininfor` VALUES (176, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-16 19:32:48');
INSERT INTO `sys_logininfor` VALUES (177, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-16 19:37:41');
INSERT INTO `sys_logininfor` VALUES (178, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-17 11:11:39');
INSERT INTO `sys_logininfor` VALUES (179, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-17 11:30:47');
INSERT INTO `sys_logininfor` VALUES (180, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-18 10:04:32');
INSERT INTO `sys_logininfor` VALUES (181, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-18 10:29:20');
INSERT INTO `sys_logininfor` VALUES (182, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-21 17:35:20');
INSERT INTO `sys_logininfor` VALUES (183, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-25 19:43:06');
INSERT INTO `sys_logininfor` VALUES (184, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-25 19:43:22');
INSERT INTO `sys_logininfor` VALUES (185, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:27:19');
INSERT INTO `sys_logininfor` VALUES (186, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:28:36');
INSERT INTO `sys_logininfor` VALUES (187, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:29:02');
INSERT INTO `sys_logininfor` VALUES (188, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:30:28');
INSERT INTO `sys_logininfor` VALUES (189, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:30:53');
INSERT INTO `sys_logininfor` VALUES (190, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:38:31');
INSERT INTO `sys_logininfor` VALUES (191, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:42:05');
INSERT INTO `sys_logininfor` VALUES (192, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:42:05');
INSERT INTO `sys_logininfor` VALUES (193, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:42:14');
INSERT INTO `sys_logininfor` VALUES (194, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:43:13');
INSERT INTO `sys_logininfor` VALUES (195, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:44:35');
INSERT INTO `sys_logininfor` VALUES (196, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:44:35');
INSERT INTO `sys_logininfor` VALUES (197, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:45:37');
INSERT INTO `sys_logininfor` VALUES (198, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:45:52');
INSERT INTO `sys_logininfor` VALUES (199, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 21:54:33');
INSERT INTO `sys_logininfor` VALUES (200, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-27 21:56:51');
INSERT INTO `sys_logininfor` VALUES (201, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-27 21:59:16');
INSERT INTO `sys_logininfor` VALUES (202, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-27 22:00:24');
INSERT INTO `sys_logininfor` VALUES (203, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-27 22:14:38');
INSERT INTO `sys_logininfor` VALUES (204, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-27 22:14:46');
INSERT INTO `sys_logininfor` VALUES (205, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 21:45:53');
INSERT INTO `sys_logininfor` VALUES (206, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 22:05:38');
INSERT INTO `sys_logininfor` VALUES (207, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 22:13:38');
INSERT INTO `sys_logininfor` VALUES (208, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 22:15:35');
INSERT INTO `sys_logininfor` VALUES (209, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 22:18:44');
INSERT INTO `sys_logininfor` VALUES (210, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 22:26:12');
INSERT INTO `sys_logininfor` VALUES (211, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 22:27:19');
INSERT INTO `sys_logininfor` VALUES (212, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 22:49:34');
INSERT INTO `sys_logininfor` VALUES (213, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 22:54:29');
INSERT INTO `sys_logininfor` VALUES (214, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 22:54:43');
INSERT INTO `sys_logininfor` VALUES (215, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 22:57:07');
INSERT INTO `sys_logininfor` VALUES (216, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 23:04:49');
INSERT INTO `sys_logininfor` VALUES (217, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 23:09:28');
INSERT INTO `sys_logininfor` VALUES (218, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-29 23:10:25');
INSERT INTO `sys_logininfor` VALUES (219, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-30 09:19:52');
INSERT INTO `sys_logininfor` VALUES (220, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-30 10:01:42');
INSERT INTO `sys_logininfor` VALUES (221, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-06-30 10:02:35');
INSERT INTO `sys_logininfor` VALUES (222, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-30 20:14:48');
INSERT INTO `sys_logininfor` VALUES (223, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-30 20:47:25');
INSERT INTO `sys_logininfor` VALUES (224, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-30 20:48:05');
INSERT INTO `sys_logininfor` VALUES (225, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-30 20:48:16');
INSERT INTO `sys_logininfor` VALUES (226, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-06-30 20:48:50');
INSERT INTO `sys_logininfor` VALUES (227, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-30 20:48:53');
INSERT INTO `sys_logininfor` VALUES (228, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-06-30 20:52:19');
INSERT INTO `sys_logininfor` VALUES (229, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-03 19:25:47');
INSERT INTO `sys_logininfor` VALUES (230, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-03 19:26:20');
INSERT INTO `sys_logininfor` VALUES (231, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-03 19:29:41');
INSERT INTO `sys_logininfor` VALUES (232, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-03 19:33:54');
INSERT INTO `sys_logininfor` VALUES (233, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-03 19:34:03');
INSERT INTO `sys_logininfor` VALUES (234, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-03 19:40:00');
INSERT INTO `sys_logininfor` VALUES (235, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-03 19:40:17');
INSERT INTO `sys_logininfor` VALUES (236, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-04 19:35:16');
INSERT INTO `sys_logininfor` VALUES (237, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-04 21:15:10');
INSERT INTO `sys_logininfor` VALUES (238, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-05 20:01:29');
INSERT INTO `sys_logininfor` VALUES (239, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-05 20:01:53');
INSERT INTO `sys_logininfor` VALUES (240, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-05 20:02:30');
INSERT INTO `sys_logininfor` VALUES (241, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-05 20:02:40');
INSERT INTO `sys_logininfor` VALUES (242, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-05 21:49:24');
INSERT INTO `sys_logininfor` VALUES (243, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-05 21:49:34');
INSERT INTO `sys_logininfor` VALUES (244, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-05 21:57:36');
INSERT INTO `sys_logininfor` VALUES (245, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-05 21:58:53');
INSERT INTO `sys_logininfor` VALUES (246, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-05 22:07:10');
INSERT INTO `sys_logininfor` VALUES (247, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-05 22:10:02');
INSERT INTO `sys_logininfor` VALUES (248, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-05 22:10:39');
INSERT INTO `sys_logininfor` VALUES (249, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 19:02:55');
INSERT INTO `sys_logininfor` VALUES (250, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 19:03:02');
INSERT INTO `sys_logininfor` VALUES (251, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-06 19:16:06');
INSERT INTO `sys_logininfor` VALUES (252, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-06 19:16:09');
INSERT INTO `sys_logininfor` VALUES (253, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 19:16:45');
INSERT INTO `sys_logininfor` VALUES (254, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 19:16:56');
INSERT INTO `sys_logininfor` VALUES (255, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-06 19:37:50');
INSERT INTO `sys_logininfor` VALUES (256, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-06 19:37:51');
INSERT INTO `sys_logininfor` VALUES (257, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 19:38:01');
INSERT INTO `sys_logininfor` VALUES (258, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-06 20:22:30');
INSERT INTO `sys_logininfor` VALUES (259, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 20:22:58');
INSERT INTO `sys_logininfor` VALUES (260, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 20:32:57');
INSERT INTO `sys_logininfor` VALUES (261, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-06 20:32:58');
INSERT INTO `sys_logininfor` VALUES (262, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 20:33:38');
INSERT INTO `sys_logininfor` VALUES (263, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-06 20:33:41');
INSERT INTO `sys_logininfor` VALUES (264, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 20:34:05');
INSERT INTO `sys_logininfor` VALUES (265, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-06 20:38:15');
INSERT INTO `sys_logininfor` VALUES (266, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 20:38:43');
INSERT INTO `sys_logininfor` VALUES (267, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-06 20:51:43');
INSERT INTO `sys_logininfor` VALUES (268, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-06 20:51:58');
INSERT INTO `sys_logininfor` VALUES (269, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 20:52:19');
INSERT INTO `sys_logininfor` VALUES (270, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 21:06:24');
INSERT INTO `sys_logininfor` VALUES (271, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-06 21:09:21');
INSERT INTO `sys_logininfor` VALUES (272, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 21:09:33');
INSERT INTO `sys_logininfor` VALUES (273, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-06 21:09:34');
INSERT INTO `sys_logininfor` VALUES (274, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 21:10:01');
INSERT INTO `sys_logininfor` VALUES (275, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 21:11:04');
INSERT INTO `sys_logininfor` VALUES (276, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 21:14:32');
INSERT INTO `sys_logininfor` VALUES (277, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 21:15:38');
INSERT INTO `sys_logininfor` VALUES (278, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-06 21:34:03');
INSERT INTO `sys_logininfor` VALUES (279, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-07 19:16:04');
INSERT INTO `sys_logininfor` VALUES (280, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-07 20:52:37');
INSERT INTO `sys_logininfor` VALUES (281, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-07 20:56:23');
INSERT INTO `sys_logininfor` VALUES (282, 'ry', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-07 20:56:50');
INSERT INTO `sys_logininfor` VALUES (283, 'ry', '127.0.0.1', '', '', '', '1', '密码输入错误1次', '2023-07-07 20:57:08');
INSERT INTO `sys_logininfor` VALUES (284, 'ry', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-07 20:58:11');
INSERT INTO `sys_logininfor` VALUES (285, 'ry', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-08 13:16:44');
INSERT INTO `sys_logininfor` VALUES (286, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-08 13:16:58');
INSERT INTO `sys_logininfor` VALUES (287, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-08 13:17:18');
INSERT INTO `sys_logininfor` VALUES (288, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-09 12:41:46');
INSERT INTO `sys_logininfor` VALUES (289, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-09 12:46:34');
INSERT INTO `sys_logininfor` VALUES (290, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-09 12:46:46');
INSERT INTO `sys_logininfor` VALUES (291, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-09 18:30:03');
INSERT INTO `sys_logininfor` VALUES (292, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 18:30:34');
INSERT INTO `sys_logininfor` VALUES (293, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-09 18:30:48');
INSERT INTO `sys_logininfor` VALUES (294, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-09 18:31:01');
INSERT INTO `sys_logininfor` VALUES (295, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 18:31:45');
INSERT INTO `sys_logininfor` VALUES (296, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 18:32:20');
INSERT INTO `sys_logininfor` VALUES (297, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 18:33:13');
INSERT INTO `sys_logininfor` VALUES (298, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 18:33:45');
INSERT INTO `sys_logininfor` VALUES (299, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 18:36:48');
INSERT INTO `sys_logininfor` VALUES (300, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 18:51:17');
INSERT INTO `sys_logininfor` VALUES (301, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 19:01:51');
INSERT INTO `sys_logininfor` VALUES (302, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 19:06:21');
INSERT INTO `sys_logininfor` VALUES (303, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 20:20:45');
INSERT INTO `sys_logininfor` VALUES (304, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-09 20:50:16');
INSERT INTO `sys_logininfor` VALUES (305, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-09 21:06:23');
INSERT INTO `sys_logininfor` VALUES (306, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 21:06:50');
INSERT INTO `sys_logininfor` VALUES (307, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 21:07:33');
INSERT INTO `sys_logininfor` VALUES (308, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 21:08:49');
INSERT INTO `sys_logininfor` VALUES (309, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-09 21:09:47');
INSERT INTO `sys_logininfor` VALUES (310, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 21:20:16');
INSERT INTO `sys_logininfor` VALUES (311, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 21:29:25');
INSERT INTO `sys_logininfor` VALUES (312, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 21:33:31');
INSERT INTO `sys_logininfor` VALUES (313, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 21:34:56');
INSERT INTO `sys_logininfor` VALUES (314, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-09 21:39:32');
INSERT INTO `sys_logininfor` VALUES (315, 'admin', '127.0.0.1', '', '', '', '0', '退出成功', '2023-07-09 21:41:01');
INSERT INTO `sys_logininfor` VALUES (316, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-09 21:41:49');
INSERT INTO `sys_logininfor` VALUES (317, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-09 21:43:02');
INSERT INTO `sys_logininfor` VALUES (318, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-09 21:44:23');
INSERT INTO `sys_logininfor` VALUES (319, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 21:48:35');
INSERT INTO `sys_logininfor` VALUES (320, 'admin', '127.0.0.1', '', '', '', '1', '登录用户不存在', '2023-07-09 21:52:11');
INSERT INTO `sys_logininfor` VALUES (321, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-09 21:52:36');
INSERT INTO `sys_logininfor` VALUES (322, 'admin', '127.0.0.1', '', '', '', '0', '登录成功', '2023-07-09 21:52:53');
INSERT INTO `sys_logininfor` VALUES (323, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-10 19:39:12');
INSERT INTO `sys_logininfor` VALUES (324, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-10 19:39:21');
INSERT INTO `sys_logininfor` VALUES (325, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-11 13:21:16');
INSERT INTO `sys_logininfor` VALUES (326, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-12 20:50:44');
INSERT INTO `sys_logininfor` VALUES (327, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-12 20:50:52');
INSERT INTO `sys_logininfor` VALUES (328, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-13 18:40:22');
INSERT INTO `sys_logininfor` VALUES (329, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-13 18:40:37');
INSERT INTO `sys_logininfor` VALUES (330, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-14 13:18:33');
INSERT INTO `sys_logininfor` VALUES (331, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-14 13:19:00');
INSERT INTO `sys_logininfor` VALUES (332, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-15 02:28:02');
INSERT INTO `sys_logininfor` VALUES (333, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-15 17:10:42');
INSERT INTO `sys_logininfor` VALUES (334, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-16 12:03:36');
INSERT INTO `sys_logininfor` VALUES (335, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-16 14:04:48');
INSERT INTO `sys_logininfor` VALUES (336, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-18 20:47:11');
INSERT INTO `sys_logininfor` VALUES (337, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-18 20:47:23');
INSERT INTO `sys_logininfor` VALUES (338, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-19 20:28:32');
INSERT INTO `sys_logininfor` VALUES (339, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-19 20:28:44');
INSERT INTO `sys_logininfor` VALUES (340, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-20 20:45:27');
INSERT INTO `sys_logininfor` VALUES (341, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-20 20:45:48');
INSERT INTO `sys_logininfor` VALUES (342, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-20 20:45:52');
INSERT INTO `sys_logininfor` VALUES (343, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-21 19:07:30');
INSERT INTO `sys_logininfor` VALUES (344, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-21 19:12:45');
INSERT INTO `sys_logininfor` VALUES (345, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-21 19:42:22');
INSERT INTO `sys_logininfor` VALUES (346, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-22 13:46:03');
INSERT INTO `sys_logininfor` VALUES (347, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-22 14:30:07');
INSERT INTO `sys_logininfor` VALUES (348, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-23 21:02:38');
INSERT INTO `sys_logininfor` VALUES (349, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-23 21:02:57');
INSERT INTO `sys_logininfor` VALUES (350, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-24 09:34:42');
INSERT INTO `sys_logininfor` VALUES (351, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-24 09:34:45');
INSERT INTO `sys_logininfor` VALUES (352, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-25 10:33:30');
INSERT INTO `sys_logininfor` VALUES (353, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-26 21:20:37');
INSERT INTO `sys_logininfor` VALUES (354, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-26 21:21:52');
INSERT INTO `sys_logininfor` VALUES (355, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-26 21:22:02');
INSERT INTO `sys_logininfor` VALUES (356, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-27 20:20:18');
INSERT INTO `sys_logininfor` VALUES (357, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-27 20:21:13');
INSERT INTO `sys_logininfor` VALUES (358, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 15:35:36');
INSERT INTO `sys_logininfor` VALUES (359, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 15:36:15');
INSERT INTO `sys_logininfor` VALUES (360, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 18:13:49');
INSERT INTO `sys_logininfor` VALUES (361, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 18:46:43');
INSERT INTO `sys_logininfor` VALUES (362, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 18:48:35');
INSERT INTO `sys_logininfor` VALUES (363, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 18:51:26');
INSERT INTO `sys_logininfor` VALUES (364, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 18:55:21');
INSERT INTO `sys_logininfor` VALUES (365, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 19:00:06');
INSERT INTO `sys_logininfor` VALUES (366, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 19:18:38');
INSERT INTO `sys_logininfor` VALUES (367, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 19:36:27');
INSERT INTO `sys_logininfor` VALUES (368, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 19:39:43');
INSERT INTO `sys_logininfor` VALUES (369, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 19:40:59');
INSERT INTO `sys_logininfor` VALUES (370, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 19:42:47');
INSERT INTO `sys_logininfor` VALUES (371, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-29 20:38:45');
INSERT INTO `sys_logininfor` VALUES (372, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '密码输入错误1次', '2023-07-29 20:39:00');
INSERT INTO `sys_logininfor` VALUES (373, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '密码输入错误2次', '2023-07-29 20:39:20');
INSERT INTO `sys_logininfor` VALUES (374, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '密码输入错误3次', '2023-07-29 20:39:36');
INSERT INTO `sys_logininfor` VALUES (375, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 20:39:53');
INSERT INTO `sys_logininfor` VALUES (376, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 20:43:00');
INSERT INTO `sys_logininfor` VALUES (377, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 20:44:40');
INSERT INTO `sys_logininfor` VALUES (378, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 20:46:57');
INSERT INTO `sys_logininfor` VALUES (379, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 20:59:08');
INSERT INTO `sys_logininfor` VALUES (380, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 20:59:17');
INSERT INTO `sys_logininfor` VALUES (381, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 20:59:32');
INSERT INTO `sys_logininfor` VALUES (382, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 21:37:17');
INSERT INTO `sys_logininfor` VALUES (383, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 21:38:04');
INSERT INTO `sys_logininfor` VALUES (384, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 21:45:28');
INSERT INTO `sys_logininfor` VALUES (385, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 21:47:55');
INSERT INTO `sys_logininfor` VALUES (386, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 21:48:10');
INSERT INTO `sys_logininfor` VALUES (387, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 21:52:57');
INSERT INTO `sys_logininfor` VALUES (388, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:15:04');
INSERT INTO `sys_logininfor` VALUES (389, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:16:40');
INSERT INTO `sys_logininfor` VALUES (390, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:16:40');
INSERT INTO `sys_logininfor` VALUES (391, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:17:02');
INSERT INTO `sys_logininfor` VALUES (392, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:18:23');
INSERT INTO `sys_logininfor` VALUES (393, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:19:19');
INSERT INTO `sys_logininfor` VALUES (394, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:20:17');
INSERT INTO `sys_logininfor` VALUES (395, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:21:55');
INSERT INTO `sys_logininfor` VALUES (396, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:22:22');
INSERT INTO `sys_logininfor` VALUES (397, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:23:20');
INSERT INTO `sys_logininfor` VALUES (398, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:23:38');
INSERT INTO `sys_logininfor` VALUES (399, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:23:42');
INSERT INTO `sys_logininfor` VALUES (400, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:24:41');
INSERT INTO `sys_logininfor` VALUES (401, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:31:08');
INSERT INTO `sys_logininfor` VALUES (402, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:31:13');
INSERT INTO `sys_logininfor` VALUES (403, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:32:44');
INSERT INTO `sys_logininfor` VALUES (404, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:38:10');
INSERT INTO `sys_logininfor` VALUES (405, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:40:49');
INSERT INTO `sys_logininfor` VALUES (406, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:40:56');
INSERT INTO `sys_logininfor` VALUES (407, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:44:29');
INSERT INTO `sys_logininfor` VALUES (408, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:44:32');
INSERT INTO `sys_logininfor` VALUES (409, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:45:08');
INSERT INTO `sys_logininfor` VALUES (410, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:51:20');
INSERT INTO `sys_logininfor` VALUES (411, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:51:29');
INSERT INTO `sys_logininfor` VALUES (412, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:53:39');
INSERT INTO `sys_logininfor` VALUES (413, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:53:59');
INSERT INTO `sys_logininfor` VALUES (414, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:56:21');
INSERT INTO `sys_logininfor` VALUES (415, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 22:58:02');
INSERT INTO `sys_logininfor` VALUES (416, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 22:58:13');
INSERT INTO `sys_logininfor` VALUES (417, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:04:31');
INSERT INTO `sys_logininfor` VALUES (418, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:16:00');
INSERT INTO `sys_logininfor` VALUES (419, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:16:10');
INSERT INTO `sys_logininfor` VALUES (420, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:18:44');
INSERT INTO `sys_logininfor` VALUES (421, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:18:47');
INSERT INTO `sys_logininfor` VALUES (422, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:18:51');
INSERT INTO `sys_logininfor` VALUES (423, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:19:06');
INSERT INTO `sys_logininfor` VALUES (424, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:19:09');
INSERT INTO `sys_logininfor` VALUES (425, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:20:06');
INSERT INTO `sys_logininfor` VALUES (426, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:20:11');
INSERT INTO `sys_logininfor` VALUES (427, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:20:46');
INSERT INTO `sys_logininfor` VALUES (428, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:26:34');
INSERT INTO `sys_logininfor` VALUES (429, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:26:40');
INSERT INTO `sys_logininfor` VALUES (430, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:28:10');
INSERT INTO `sys_logininfor` VALUES (431, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:28:37');
INSERT INTO `sys_logininfor` VALUES (432, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:28:39');
INSERT INTO `sys_logininfor` VALUES (433, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:28:58');
INSERT INTO `sys_logininfor` VALUES (434, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:29:01');
INSERT INTO `sys_logininfor` VALUES (435, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:30:08');
INSERT INTO `sys_logininfor` VALUES (436, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:32:10');
INSERT INTO `sys_logininfor` VALUES (437, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:32:19');
INSERT INTO `sys_logininfor` VALUES (438, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:34:01');
INSERT INTO `sys_logininfor` VALUES (439, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:35:04');
INSERT INTO `sys_logininfor` VALUES (440, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:35:17');
INSERT INTO `sys_logininfor` VALUES (441, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:38:20');
INSERT INTO `sys_logininfor` VALUES (442, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:40:23');
INSERT INTO `sys_logininfor` VALUES (443, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:40:26');
INSERT INTO `sys_logininfor` VALUES (444, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:41:06');
INSERT INTO `sys_logininfor` VALUES (445, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-29 23:43:05');
INSERT INTO `sys_logininfor` VALUES (446, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-29 23:43:09');
INSERT INTO `sys_logininfor` VALUES (447, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-30 13:07:54');
INSERT INTO `sys_logininfor` VALUES (448, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 15:50:05');
INSERT INTO `sys_logininfor` VALUES (449, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-30 17:42:54');
INSERT INTO `sys_logininfor` VALUES (450, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 17:43:02');
INSERT INTO `sys_logininfor` VALUES (451, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 17:43:35');
INSERT INTO `sys_logininfor` VALUES (452, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 17:58:23');
INSERT INTO `sys_logininfor` VALUES (453, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-30 18:14:49');
INSERT INTO `sys_logininfor` VALUES (454, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 18:14:54');
INSERT INTO `sys_logininfor` VALUES (455, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-30 19:21:46');
INSERT INTO `sys_logininfor` VALUES (456, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 20:04:59');
INSERT INTO `sys_logininfor` VALUES (457, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 20:08:39');
INSERT INTO `sys_logininfor` VALUES (458, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 20:11:09');
INSERT INTO `sys_logininfor` VALUES (459, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 20:16:41');
INSERT INTO `sys_logininfor` VALUES (460, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 20:20:45');
INSERT INTO `sys_logininfor` VALUES (461, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 20:36:35');
INSERT INTO `sys_logininfor` VALUES (462, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-30 21:23:11');
INSERT INTO `sys_logininfor` VALUES (463, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 21:23:49');
INSERT INTO `sys_logininfor` VALUES (464, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-30 21:23:53');
INSERT INTO `sys_logininfor` VALUES (465, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 21:24:25');
INSERT INTO `sys_logininfor` VALUES (466, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-30 21:24:33');
INSERT INTO `sys_logininfor` VALUES (467, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 21:25:05');
INSERT INTO `sys_logininfor` VALUES (468, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-30 21:25:38');
INSERT INTO `sys_logininfor` VALUES (469, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-30 21:35:20');
INSERT INTO `sys_logininfor` VALUES (470, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 21:35:24');
INSERT INTO `sys_logininfor` VALUES (471, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-30 21:47:40');
INSERT INTO `sys_logininfor` VALUES (472, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 21:47:48');
INSERT INTO `sys_logininfor` VALUES (473, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-30 21:48:59');
INSERT INTO `sys_logininfor` VALUES (474, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 21:49:06');
INSERT INTO `sys_logininfor` VALUES (475, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-30 21:52:59');
INSERT INTO `sys_logininfor` VALUES (476, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-30 22:00:42');
INSERT INTO `sys_logininfor` VALUES (477, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-07-30 22:02:59');
INSERT INTO `sys_logininfor` VALUES (478, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-30 22:03:36');
INSERT INTO `sys_logininfor` VALUES (479, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-07-31 20:20:19');
INSERT INTO `sys_logininfor` VALUES (480, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-07-31 20:22:22');
INSERT INTO `sys_logininfor` VALUES (481, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-08-01 20:35:47');
INSERT INTO `sys_logininfor` VALUES (482, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-08-02 19:57:13');
INSERT INTO `sys_logininfor` VALUES (483, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-08-02 19:57:34');
INSERT INTO `sys_logininfor` VALUES (484, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-08-03 19:50:59');
INSERT INTO `sys_logininfor` VALUES (485, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-08-03 19:52:21');
INSERT INTO `sys_logininfor` VALUES (486, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-08-03 19:53:22');
INSERT INTO `sys_logininfor` VALUES (487, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-08-07 18:43:03');
INSERT INTO `sys_logininfor` VALUES (488, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-08-07 18:44:24');
INSERT INTO `sys_logininfor` VALUES (489, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-08-07 18:44:48');
INSERT INTO `sys_logininfor` VALUES (490, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-08-07 18:44:57');
INSERT INTO `sys_logininfor` VALUES (491, 'ry', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-08-07 21:25:54');
INSERT INTO `sys_logininfor` VALUES (492, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-08-07 21:26:01');
INSERT INTO `sys_logininfor` VALUES (493, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-08-08 19:29:11');
INSERT INTO `sys_logininfor` VALUES (494, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-08-08 19:29:42');
INSERT INTO `sys_logininfor` VALUES (495, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-08-08 19:30:13');
INSERT INTO `sys_logininfor` VALUES (496, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-08-08 19:30:13');
INSERT INTO `sys_logininfor` VALUES (497, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-08-08 19:33:25');
INSERT INTO `sys_logininfor` VALUES (498, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '登录用户不存在', '2023-08-08 19:54:09');
INSERT INTO `sys_logininfor` VALUES (499, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-08-08 20:09:49');
INSERT INTO `sys_logininfor` VALUES (500, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-08-08 20:13:55');
INSERT INTO `sys_logininfor` VALUES (501, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-08-08 20:14:37');
INSERT INTO `sys_logininfor` VALUES (502, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2023-08-09 20:06:30');
INSERT INTO `sys_logininfor` VALUES (503, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2023-08-09 20:08:21');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2052 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'system', 1, '2023-01-31 20:38:15', NULL, NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'monitor', 1, '2023-01-31 20:38:15', NULL, NULL, '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'tool', 1, '2023-01-31 20:38:15', NULL, NULL, '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', NULL, 1, 0, 'C', '0', '0', 'system:user:page', 'user', 1, '2023-01-31 20:38:15', 1, '2023-02-05 21:20:44', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', NULL, 1, 0, 'C', '0', '0', 'system:role:page', 'peoples', 1, '2023-01-31 20:38:15', 1, '2023-02-05 21:21:44', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', NULL, 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 1, '2023-01-31 20:38:15', NULL, NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', NULL, 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 1, '2023-01-31 20:38:15', NULL, NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', NULL, 1, 1, 'C', '0', '0', 'system:post:page', 'post', 1, '2023-01-31 20:38:15', 1, '2023-06-01 10:28:11', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', NULL, 1, 0, 'C', '0', '0', 'system:dict:page', 'dict', 1, '2023-01-31 20:38:15', 2, '2023-02-05 13:14:45', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', NULL, 1, 0, 'C', '0', '0', 'system:config:page', 'edit', 1, '2023-01-31 20:38:15', 1, '2023-02-05 13:32:58', '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', NULL, 1, 0, 'C', '0', '0', 'system:notice:page', 'message', 1, '2023-01-31 20:38:15', 1, '2023-02-05 21:24:41', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'log', 1, '2023-01-31 20:38:15', NULL, NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', NULL, 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 1, '2023-01-31 20:38:15', NULL, NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', NULL, 1, 0, 'C', '0', '0', 'monitor:job:page', 'job', 1, '2023-01-31 20:38:15', 1, '2023-02-05 21:06:26', '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, 'Sentinel控制台', 2, 3, 'http://localhost:8718', NULL, NULL, 0, 0, 'C', '0', '0', 'monitor:sentinel:list', 'sentinel', 1, '2023-01-31 20:38:15', NULL, NULL, '流量控制菜单');
INSERT INTO `sys_menu` VALUES (112, 'Nacos控制台', 2, 4, 'http://localhost:8848/nacos', NULL, NULL, 0, 0, 'C', '0', '0', 'monitor:nacos:list', 'nacos', 1, '2023-01-31 20:38:15', NULL, NULL, '服务治理菜单');
INSERT INTO `sys_menu` VALUES (113, 'Admin控制台', 2, 5, 'http://localhost:9100/login', NULL, NULL, 0, 0, 'C', '0', '0', 'monitor:server:list', 'server', 1, '2023-01-31 20:38:15', NULL, NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', NULL, 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 1, '2023-01-31 20:38:15', NULL, NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', NULL, 1, 0, 'C', '0', '0', 'tool:gen:page', 'code', 1, '2023-01-31 20:38:15', 1, '2023-02-05 20:50:04', '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'http://localhost:8080/swagger-ui/index.html', NULL, NULL, 0, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 1, '2023-01-31 20:38:15', NULL, NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'system/operlog/index', NULL, 1, 0, 'C', '0', '0', 'system:operlog:page', 'form', 1, '2023-01-31 20:38:15', 1, '2023-02-05 15:06:12', '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'system/logininfor/index', NULL, 1, 0, 'C', '0', '0', 'system:logininfor:page', 'logininfor', 1, '2023-01-31 20:38:15', 1, '2023-02-05 13:55:28', '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:user:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:user:add', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:user:edit', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:user:remove', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:user:export', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:user:import', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:role:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:role:add', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:role:edit', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:role:remove', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:role:export', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:menu:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:menu:add', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:dept:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:dept:add', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:post:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:post:add', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:post:edit', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:post:remove', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:post:export', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:dict:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:dict:add', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:dict:export', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:config:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:config:add', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:config:edit', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:config:remove', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:config:export', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:notice:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:notice:add', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:operlog:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:operlog:remove', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1041, '日志导出', 500, 3, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:operlog:export', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1042, '登录查询', 501, 1, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:logininfor:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1043, '登录删除', 501, 2, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:logininfor:remove', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1044, '日志导出', 501, 3, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:logininfor:export', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1045, '账户解锁', 501, 4, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'system:logininfor:unlock', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 6, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', NULL, NULL, 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 1, '2023-01-31 20:38:15', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2010, '打印设置', 1, 10, 'print', 'system/print/index', NULL, 1, 0, 'C', '0', '0', 'system:printPanel:page', 'documentation', 1, '2023-02-06 17:18:30', 1, '2023-02-06 17:19:57', NULL);
INSERT INTO `sys_menu` VALUES (2011, '模板查询', 2010, 1, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printPanel:query', '#', 1, '2023-02-06 17:40:11', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2012, '模板新增', 2010, 2, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printPanel:add', '#', 1, '2023-02-06 17:40:38', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2013, '模板修改', 2010, 3, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printPanel:edit', '#', 1, '2023-02-06 17:41:04', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2014, '模板删除', 2010, 4, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printPanel:remove', '#', 1, '2023-02-06 17:41:26', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2015, '模板导出', 2010, 5, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printPanel:export', '#', 1, '2023-02-06 17:41:57', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2016, '模板导入', 2010, 6, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printPanel:import', '#', 1, '2023-02-06 17:42:18', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2017, '面板查询', 2010, 7, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printProvider:query', '#', 1, '2023-02-06 18:26:49', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2018, '面板', 2010, 8, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printProvider:page', '#', 1, '2023-02-06 18:27:20', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2019, '面板新增', 2010, 9, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printProvider:add', '#', 1, '2023-02-06 18:27:46', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2020, '面板修改', 2010, 10, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printProvider:edit', '#', 1, '2023-02-06 18:28:07', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2021, '面板删除', 2010, 11, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printProvider:remove', '#', 1, '2023-02-06 18:28:29', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2022, '面板导出', 2010, 12, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printProvider:export', '#', 1, '2023-02-06 18:29:51', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2023, '面板导入', 2010, 13, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:printProvider:import', '#', 1, '2023-02-06 18:30:13', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2024, '功能表格配置', 102, 6, 'menuTable', NULL, NULL, 1, 0, 'M', '1', '0', NULL, '#', 1, '2023-04-09 13:21:02', 1, '2023-05-22 10:20:49', NULL);
INSERT INTO `sys_menu` VALUES (2025, '表格', 2024, 1, 'table', NULL, NULL, 1, 0, 'M', '1', '0', NULL, '#', 1, '2023-04-09 13:22:03', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2026, '列', 2024, 2, 'column', NULL, NULL, 1, 0, 'M', '1', '0', NULL, '#', 1, '2023-04-09 13:22:40', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2027, '查询列表', 2025, 1, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuTable:list', '#', 1, '2023-04-09 13:23:21', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2028, '查询分页', 2025, 2, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuTable:page', '#', 1, '2023-04-09 13:23:45', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2029, '导出', 2025, 3, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuTable:export', '#', 1, '2023-04-09 13:24:34', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2030, '查询', 2025, 4, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuTable:query', '#', 1, '2023-04-09 13:25:11', 1, '2023-05-22 10:49:37', NULL);
INSERT INTO `sys_menu` VALUES (2031, '添加', 2025, 5, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuTable:add', '#', 1, '2023-04-09 13:25:55', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2032, '修改', 2025, 6, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuTable:edit', '#', 1, '2023-04-09 13:26:18', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2033, '删除', 2025, 7, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuTable:remove', '#', 1, '2023-04-09 13:26:41', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2034, '查询列表', 2026, 1, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuColumn:list', '#', 1, '2023-04-09 13:27:10', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2035, '查询分页', 2026, 2, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuColumn:page', '#', 1, '2023-04-09 13:27:37', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2036, '导出', 2026, 3, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuColumn:export', '#', 1, '2023-04-09 13:28:07', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2037, '查询', 2026, 4, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuColumn:query', '#', 1, '2023-04-09 13:28:33', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2038, '添加', 2026, 5, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuColumn:add', '#', 1, '2023-04-09 13:29:02', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2039, '修改', 2026, 6, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuColumn:edit', '#', 1, '2023-04-09 13:29:21', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2040, '删除', 2026, 7, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuColumn:remove', '#', 1, '2023-04-09 13:29:39', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2041, '缓存监控', 2, 6, 'cache', 'monitor/cache/index', NULL, 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 1, '2023-05-04 17:16:06', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2042, '缓存列表', 2, 7, 'cacheList', 'monitor/cache/list', NULL, 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis-list', 1, '2023-05-04 17:18:05', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2045, '数据库表查询', 2025, 8, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:menuTable:dbList', '#', 1, '2023-05-22 10:23:55', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2046, '导入数据库表结构', 2025, 9, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuTable:importDb', '#', 1, '2023-05-22 10:25:12', 1, '2023-05-22 10:27:50', NULL);
INSERT INTO `sys_menu` VALUES (2047, '同步数据库表结构', 2025, 10, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'tableConfig:menuTable:syncDb', '#', 1, '2023-05-22 10:28:31', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2048, '调度日志', 2050, 1, 'index/:jobId', 'monitor/job/log', NULL, 1, 0, 'C', '1', '0', 'monitor:job:list', '#', 1, '2023-05-22 20:05:31', 1, '2023-05-22 21:05:08', NULL);
INSERT INTO `sys_menu` VALUES (2050, '日志', 2, 8, 'job-log', NULL, NULL, 1, 0, 'M', '1', '0', NULL, '#', 1, '2023-05-22 20:48:15', 1, '2023-05-22 21:05:54', NULL);
INSERT INTO `sys_menu` VALUES (2051, '字典列表', 105, 6, NULL, NULL, NULL, 1, 0, 'F', '0', '0', 'system:dict:list', '#', 1, '2023-05-22 21:09:35', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '1', 1, '2023-01-31 20:38:15', 2, '2023-02-26 16:54:55', '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 1, '2023-01-31 20:38:15', NULL, NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  `cost_time` bigint(20) NULL DEFAULT 0 COMMENT '消耗时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 171 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '字典类型', 9, 'com.ruoyi.system.controller.SysDictTypeController.refreshCache()', 'DELETE', 1, 'admin', NULL, '/dict/type/refreshCache', '127.0.0.1', '', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-12 22:02:35', 91);
INSERT INTO `sys_oper_log` VALUES (2, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"mode\":\"current\",\"filename\":\"角色数据\",\"original\":false,\"sheetName\":\"参数导出\",\"pager\":{\"size\":10,\"current\":1,\"total\":3},\"isHeader\":true,\"query\":{},\"ids\":[],\"sort\":{},\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}]}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-12 22:05:54', 1208);
INSERT INTO `sys_oper_log` VALUES (3, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"mode\":\"current\",\"filename\":\"角色数据\",\"original\":false,\"sheetName\":\"参数导出\",\"pager\":{\"size\":10,\"current\":1,\"total\":3},\"isHeader\":true,\"query\":{},\"ids\":[],\"sort\":{},\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}]}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-12 22:18:59', 654143);
INSERT INTO `sys_oper_log` VALUES (4, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"original\":false,\"sheetName\":\"参数导出\",\"isHeader\":true,\"query\":{\"roleName\":\"开发\"},\"pageSize\":10,\"ids\":[],\"sort\":{},\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}]}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-12 22:22:20', 158799);
INSERT INTO `sys_oper_log` VALUES (5, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"mode\":\"all\",\"total\":3,\"filename\":\"角色数据\",\"original\":false,\"sheetName\":\"参数导出\",\"isHeader\":true,\"query\":{\"roleName\":\"开发\"},\"pageSize\":10,\"ids\":[],\"sort\":{},\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}]}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-12 22:26:10', 219765);
INSERT INTO `sys_oper_log` VALUES (6, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"参数导出\",\"query\":{\"roleName\":\"开发\"},\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"mode\":\"current\",\"total\":1,\"filename\":\"角色数据\",\"isHeader\":true,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}]}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-12 22:27:49', 26939);
INSERT INTO `sys_oper_log` VALUES (7, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"参数\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-12 23:00:26', 119822);
INSERT INTO `sys_oper_log` VALUES (8, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"参数\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-12 23:04:49', 18662);
INSERT INTO `sys_oper_log` VALUES (9, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-13 19:45:11', 1612);
INSERT INTO `sys_oper_log` VALUES (10, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-13 19:46:21', 49214);
INSERT INTO `sys_oper_log` VALUES (11, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-13 21:56:37', 201093);
INSERT INTO `sys_oper_log` VALUES (12, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-13 21:59:04', 33595);
INSERT INTO `sys_oper_log` VALUES (13, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-13 22:01:02', 30784);
INSERT INTO `sys_oper_log` VALUES (14, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-13 22:04:10', 128743);
INSERT INTO `sys_oper_log` VALUES (15, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-13 22:05:57', 21236);
INSERT INTO `sys_oper_log` VALUES (16, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Can not find \'Converter\' support class Map.', '2023-07-13 22:07:20', 37191);
INSERT INTO `sys_oper_log` VALUES (17, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-13 22:12:41', 80639);
INSERT INTO `sys_oper_log` VALUES (18, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-13 22:15:17', 41139);
INSERT INTO `sys_oper_log` VALUES (19, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-13 22:20:11', 83815);
INSERT INTO `sys_oper_log` VALUES (20, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-13 22:22:33', 27864);
INSERT INTO `sys_oper_log` VALUES (21, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-13 22:22:47', 115);
INSERT INTO `sys_oper_log` VALUES (22, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-13 22:24:36', 934);
INSERT INTO `sys_oper_log` VALUES (23, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'java.lang.ExceptionInInitializerError', '2023-07-13 22:25:43', 974);
INSERT INTO `sys_oper_log` VALUES (24, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'java.lang.NoClassDefFoundError: Could not initialize class com.ruoyi.common.excel.convert.ExcelDictConvert', '2023-07-13 22:26:40', 18782);
INSERT INTO `sys_oper_log` VALUES (25, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'java.lang.NoClassDefFoundError: Could not initialize class com.ruoyi.common.excel.convert.ExcelDictConvert', '2023-07-13 22:27:11', 23809);
INSERT INTO `sys_oper_log` VALUES (26, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'java.lang.NoClassDefFoundError: Could not initialize class com.ruoyi.common.excel.convert.ExcelDictConvert', '2023-07-13 22:28:02', 28819);
INSERT INTO `sys_oper_log` VALUES (27, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'java.lang.ExceptionInInitializerError', '2023-07-13 22:34:56', 775);
INSERT INTO `sys_oper_log` VALUES (28, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'java.lang.NoClassDefFoundError: Could not initialize class com.ruoyi.common.excel.convert.ExcelDictConvert', '2023-07-13 22:35:49', 20545);
INSERT INTO `sys_oper_log` VALUES (29, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-13 22:43:24', 57053);
INSERT INTO `sys_oper_log` VALUES (30, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'java.lang.ExceptionInInitializerError', '2023-07-13 23:09:58', 1103);
INSERT INTO `sys_oper_log` VALUES (31, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 1, 'Convert data:0 error, at row:1', '2023-07-13 23:14:20', 66371);
INSERT INTO `sys_oper_log` VALUES (32, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-13 23:21:59', 65806);
INSERT INTO `sys_oper_log` VALUES (33, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-13 23:22:07', 112);
INSERT INTO `sys_oper_log` VALUES (34, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"若依管理系统\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-14 13:19:34', 6894);
INSERT INTO `sys_oper_log` VALUES (35, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"测试\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-14 13:21:59', 125);
INSERT INTO `sys_oper_log` VALUES (36, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"测试\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-14 13:23:01', 125);
INSERT INTO `sys_oper_log` VALUES (37, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"测试\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-14 13:31:57', 610);
INSERT INTO `sys_oper_log` VALUES (38, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"测试\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-14 13:33:45', 242);
INSERT INTO `sys_oper_log` VALUES (39, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"测试\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-14 13:35:51', 219);
INSERT INTO `sys_oper_log` VALUES (40, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"测试\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleId\",\"title\":\"角色编号\"},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-14 13:36:25', 755);
INSERT INTO `sys_oper_log` VALUES (41, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"测试\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"},{\"field\":\"roleId\",\"title\":\"角色编号\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-14 13:47:16', 291611);
INSERT INTO `sys_oper_log` VALUES (42, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"测试\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"},{\"field\":\"roleId\",\"title\":\"角色编号\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-14 13:49:07', 92166);
INSERT INTO `sys_oper_log` VALUES (43, '用户管理', 5, 'com.ruoyi.system.controller.SysRoleController.exportVxe()', 'POST', 1, 'admin', NULL, '/role/exportVxe', '127.0.0.1', '', '{\"original\":false,\"sheetName\":\"测试\",\"isColgroup\":false,\"query\":{},\"isAllExpand\":false,\"pageSize\":10,\"sort\":{},\"orderByColumn\":\"\",\"isFooter\":false,\"mode\":\"current\",\"total\":3,\"filename\":\"角色数据\",\"isHeader\":true,\"isMerge\":false,\"ids\":[],\"isAsc\":\"\",\"currentPage\":1,\"fields\":[{},{\"field\":\"roleName\",\"title\":\"角色名称\"},{\"field\":\"roleKey\",\"title\":\"权限字符\"},{\"field\":\"roleSort\",\"title\":\"显示顺序\"},{\"field\":\"status\",\"title\":\"状态\"},{\"field\":\"createTime\",\"title\":\"创建时间\"},{\"field\":\"roleId\",\"title\":\"角色编号\"}],\"useStyle\":true}', NULL, 0, NULL, '2023-07-14 13:49:23', 154);
INSERT INTO `sys_oper_log` VALUES (44, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"24\",\"roleTableId\":\"48\"}', NULL, 1, 'nested exception is org.apache.ibatis.exceptions.TooManyResultsException: Expected one result (or null) to be returned by selectOne(), but found: 2', '2023-07-16 16:45:37', 386);
INSERT INTO `sys_oper_log` VALUES (45, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"24\",\"roleTableId\":\"48\"}', NULL, 1, 'nested exception is org.apache.ibatis.exceptions.TooManyResultsException: Expected one result (or null) to be returned by selectOne(), but found: 2', '2023-07-16 16:58:03', 495141);
INSERT INTO `sys_oper_log` VALUES (46, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"24\",\"roleTableId\":\"48\"}', NULL, 1, 'nested exception is org.apache.ibatis.exceptions.TooManyResultsException: Expected one result (or null) to be returned by selectOne(), but found: 2', '2023-07-16 16:58:14', 4048);
INSERT INTO `sys_oper_log` VALUES (47, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"24\",\"roleTableId\":\"48\"}', NULL, 1, 'nested exception is org.apache.ibatis.exceptions.TooManyResultsException: Expected one result (or null) to be returned by selectOne(), but found: 2', '2023-07-16 16:58:39', 211);
INSERT INTO `sys_oper_log` VALUES (48, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"24\",\"roleTableId\":\"48\"}', NULL, 1, 'nested exception is org.apache.ibatis.exceptions.TooManyResultsException: Expected one result (or null) to be returned by selectOne(), but found: 2', '2023-07-16 16:59:52', 506);
INSERT INTO `sys_oper_log` VALUES (49, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"24\",\"roleTableId\":\"48\"}', NULL, 1, 'nested exception is org.apache.ibatis.exceptions.TooManyResultsException: Expected one result (or null) to be returned by selectOne(), but found: 2', '2023-07-16 17:00:04', 242);
INSERT INTO `sys_oper_log` VALUES (50, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"24\",\"roleTableId\":\"48\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-16 17:00:31', 603);
INSERT INTO `sys_oper_log` VALUES (51, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.synchDb()', 'GET', 1, 'admin', NULL, '/roleTable/synchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"24\",\"roleTableId\":\"48\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-16 17:23:44', 737);
INSERT INTO `sys_oper_log` VALUES (52, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"24\",\"roleTableId\":\"48\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-16 17:25:52', 632);
INSERT INTO `sys_oper_log` VALUES (53, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.dataList()', 'GET', 1, 'admin', NULL, '/roleTable/syncMenu/2', '127.0.0.1', '', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-16 17:47:00', 3992);
INSERT INTO `sys_oper_log` VALUES (54, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.synchDb()', 'GET', 1, 'admin', NULL, '/roleTable/synchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"24\",\"roleTableId\":\"48\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-16 17:47:36', 728);
INSERT INTO `sys_oper_log` VALUES (55, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-16 17:59:31', 265);
INSERT INTO `sys_oper_log` VALUES (56, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 20:57:28', 250);
INSERT INTO `sys_oper_log` VALUES (57, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 21:43:40', 34);
INSERT INTO `sys_oper_log` VALUES (58, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.edit()', 'PUT', 1, 'admin', NULL, '/job', '127.0.0.1', '', '{\"concurrent\":\"1\",\"createBy\":1,\"createByName\":\"admin\",\"createTime\":\"2023-06-18 17:20:03\",\"cronExpression\":\"* * * * * ?\",\"invokeTarget\":\"roleTask.asyncUserTable(true)\",\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"jobName\":\"同步用户表格\",\"misfirePolicy\":\"1\",\"nextValidTime\":\"2023-07-18 22:01:57\",\"params\":{},\"status\":\"1\",\"updateBy\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:01:56', 85);
INSERT INTO `sys_oper_log` VALUES (59, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:02:32', 15);
INSERT INTO `sys_oper_log` VALUES (60, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.dataList()', 'GET', 1, 'admin', NULL, '/roleTable/syncMenu/2', '127.0.0.1', '', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:13:56', 1795);
INSERT INTO `sys_oper_log` VALUES (61, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:26:19', 21);
INSERT INTO `sys_oper_log` VALUES (62, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:26:44', 20);
INSERT INTO `sys_oper_log` VALUES (63, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:27:14', 13);
INSERT INTO `sys_oper_log` VALUES (64, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:27:26', 17);
INSERT INTO `sys_oper_log` VALUES (65, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:28:12', 30);
INSERT INTO `sys_oper_log` VALUES (66, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:29:31', 12);
INSERT INTO `sys_oper_log` VALUES (67, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:30:22', 16);
INSERT INTO `sys_oper_log` VALUES (68, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:32:53', 8);
INSERT INTO `sys_oper_log` VALUES (69, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:33:23', 14);
INSERT INTO `sys_oper_log` VALUES (70, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:33:33', 10);
INSERT INTO `sys_oper_log` VALUES (71, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:36:08', 11);
INSERT INTO `sys_oper_log` VALUES (72, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:39:21', 10);
INSERT INTO `sys_oper_log` VALUES (73, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":3,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:41:28', 19);
INSERT INTO `sys_oper_log` VALUES (74, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:42:22', 15);
INSERT INTO `sys_oper_log` VALUES (75, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:43:20', 7);
INSERT INTO `sys_oper_log` VALUES (76, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:46:05', 10);
INSERT INTO `sys_oper_log` VALUES (77, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:47:31', 8);
INSERT INTO `sys_oper_log` VALUES (78, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:51:14', 11);
INSERT INTO `sys_oper_log` VALUES (79, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:53:12', 14);
INSERT INTO `sys_oper_log` VALUES (80, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:54:36', 8);
INSERT INTO `sys_oper_log` VALUES (81, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:55:36', 18);
INSERT INTO `sys_oper_log` VALUES (82, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:56:42', 24);
INSERT INTO `sys_oper_log` VALUES (83, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 22:59:48', 12);
INSERT INTO `sys_oper_log` VALUES (84, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:02:21', 11);
INSERT INTO `sys_oper_log` VALUES (85, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.edit()', 'PUT', 1, 'admin', NULL, '/job', '127.0.0.1', '', '{\"concurrent\":\"1\",\"createBy\":1,\"createByName\":\"admin\",\"createTime\":\"2023-06-18 17:20:03\",\"cronExpression\":\"* * * * * ?\",\"invokeTarget\":\"roleTask.asyncUserTable()\",\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"jobName\":\"同步用户表格\",\"misfirePolicy\":\"1\",\"nextValidTime\":\"2023-07-18 23:03:27\",\"params\":{},\"status\":\"1\",\"updateBy\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:03:26', 21);
INSERT INTO `sys_oper_log` VALUES (86, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:03:37', 14);
INSERT INTO `sys_oper_log` VALUES (87, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:04:11', 7352);
INSERT INTO `sys_oper_log` VALUES (88, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:04:41', 14);
INSERT INTO `sys_oper_log` VALUES (89, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:04:59', 13);
INSERT INTO `sys_oper_log` VALUES (90, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:07:23', 7);
INSERT INTO `sys_oper_log` VALUES (91, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:08:30', 11);
INSERT INTO `sys_oper_log` VALUES (92, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:11:47', 8257);
INSERT INTO `sys_oper_log` VALUES (93, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:14:12', 2925);
INSERT INTO `sys_oper_log` VALUES (94, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:14:55', 19);
INSERT INTO `sys_oper_log` VALUES (95, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:19:01', 5747);
INSERT INTO `sys_oper_log` VALUES (96, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-18 23:21:32', 7490);
INSERT INTO `sys_oper_log` VALUES (97, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.edit()', 'PUT', 1, 'admin', NULL, '/job', '127.0.0.1', '', '{\"concurrent\":\"1\",\"createBy\":1,\"createByName\":\"admin\",\"createTime\":\"2023-06-18 17:20:03\",\"cronExpression\":\"* * * * * ?\",\"invokeTarget\":\"roleTask.asyncUserTable(true)\",\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"jobName\":\"同步用户表格\",\"misfirePolicy\":\"1\",\"nextValidTime\":\"2023-07-19 20:29:54\",\"params\":{},\"status\":\"1\",\"updateBy\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 20:29:54', 397);
INSERT INTO `sys_oper_log` VALUES (98, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 20:30:58', 2377);
INSERT INTO `sys_oper_log` VALUES (99, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 20:31:31', 15);
INSERT INTO `sys_oper_log` VALUES (100, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 22:30:14', 35);
INSERT INTO `sys_oper_log` VALUES (101, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 22:31:15', 7739);
INSERT INTO `sys_oper_log` VALUES (102, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 22:36:08', 4855);
INSERT INTO `sys_oper_log` VALUES (103, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 22:39:02', 21);
INSERT INTO `sys_oper_log` VALUES (104, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 22:40:12', 26);
INSERT INTO `sys_oper_log` VALUES (105, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 22:43:56', 15);
INSERT INTO `sys_oper_log` VALUES (106, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 22:45:29', 18);
INSERT INTO `sys_oper_log` VALUES (107, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 22:46:07', 21);
INSERT INTO `sys_oper_log` VALUES (108, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 22:47:48', 8);
INSERT INTO `sys_oper_log` VALUES (109, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 22:50:21', 126);
INSERT INTO `sys_oper_log` VALUES (110, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-19 23:03:16', 26);
INSERT INTO `sys_oper_log` VALUES (111, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.synchDb()', 'GET', 1, 'admin', NULL, '/roleTable/synchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"104\",\"tableId\":\"21\",\"roleTableId\":\"47\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-20 21:57:00', 267);
INSERT INTO `sys_oper_log` VALUES (112, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.synchDb()', 'GET', 1, 'admin', NULL, '/roleTable/synchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"24\",\"roleTableId\":\"48\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-21 19:41:28', 493);
INSERT INTO `sys_oper_log` VALUES (113, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"24\",\"roleTableId\":\"48\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-21 19:41:31', 345);
INSERT INTO `sys_oper_log` VALUES (114, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-21 19:44:19', 288);
INSERT INTO `sys_oper_log` VALUES (115, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_dept\",\"menuId\":\"103\"}', NULL, 1, '导入失败：nested exception is org.apache.ibatis.reflection.ReflectionException: There is no getter for property named \'tableName\' in \'class java.lang.String\'', '2023-07-25 20:30:26', 30214);
INSERT INTO `sys_oper_log` VALUES (116, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_dept\",\"menuId\":\"103\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-25 20:32:45', 44031);
INSERT INTO `sys_oper_log` VALUES (117, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_dict_type,sys_dict_data\",\"menuId\":\"105\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-25 20:35:59', 119);
INSERT INTO `sys_oper_log` VALUES (118, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_config\",\"menuId\":\"106\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-25 20:36:34', 53);
INSERT INTO `sys_oper_log` VALUES (119, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_notice\",\"menuId\":\"107\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-25 20:36:51', 39);
INSERT INTO `sys_oper_log` VALUES (120, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_oper_log\",\"menuId\":\"500\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-25 20:37:27', 75);
INSERT INTO `sys_oper_log` VALUES (121, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_logininfor\",\"menuId\":\"501\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-25 20:40:19', 52);
INSERT INTO `sys_oper_log` VALUES (122, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_print_provider,sys_print_panel\",\"menuId\":\"2010\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-25 20:40:57', 72);
INSERT INTO `sys_oper_log` VALUES (123, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_job_log,sys_job\",\"menuId\":\"110\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-25 20:42:15', 69);
INSERT INTO `sys_oper_log` VALUES (124, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_user\",\"menuId\":\"100\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 21:32:51', 8565);
INSERT INTO `sys_oper_log` VALUES (125, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.dataList()', 'GET', 1, 'admin', NULL, '/roleTable/syncMenu/2', '127.0.0.1', '', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 21:51:38', 2711);
INSERT INTO `sys_oper_log` VALUES (126, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.dataList()', 'GET', 1, 'admin', NULL, '/roleTable/syncMenu/2', '127.0.0.1', '', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 21:52:56', 1724);
INSERT INTO `sys_oper_log` VALUES (127, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.dataList()', 'GET', 1, 'admin', NULL, '/roleTable/syncMenu/2', '127.0.0.1', '', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 21:55:38', 1800);
INSERT INTO `sys_oper_log` VALUES (128, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.dataList()', 'GET', 1, 'admin', NULL, '/roleTable/syncMenu/2', '127.0.0.1', '', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 21:55:55', 1948);
INSERT INTO `sys_oper_log` VALUES (129, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"1\",\"roleTableId\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 21:56:55', 8590);
INSERT INTO `sys_oper_log` VALUES (130, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 21:57:32', 294);
INSERT INTO `sys_oper_log` VALUES (131, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.synchDb()', 'GET', 1, 'admin', NULL, '/roleTable/synchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"1\",\"roleTableId\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:04:00', 108117);
INSERT INTO `sys_oper_log` VALUES (132, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:04:23', 30);
INSERT INTO `sys_oper_log` VALUES (133, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:08:10', 13);
INSERT INTO `sys_oper_log` VALUES (134, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:10:42', 14);
INSERT INTO `sys_oper_log` VALUES (135, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.synchDb()', 'GET', 1, 'admin', NULL, '/roleTable/synchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"1\",\"roleTableId\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:14:29', 4557);
INSERT INTO `sys_oper_log` VALUES (136, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"1\",\"roleTableId\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:15:10', 512);
INSERT INTO `sys_oper_log` VALUES (137, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:19:25', 27);
INSERT INTO `sys_oper_log` VALUES (138, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:28:53', 17);
INSERT INTO `sys_oper_log` VALUES (139, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:34:33', 13);
INSERT INTO `sys_oper_log` VALUES (140, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"1\",\"roleTableId\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:37:38', 301);
INSERT INTO `sys_oper_log` VALUES (141, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:37:48', 14);
INSERT INTO `sys_oper_log` VALUES (142, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:41:42', 11);
INSERT INTO `sys_oper_log` VALUES (143, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:41:49', 11);
INSERT INTO `sys_oper_log` VALUES (144, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:43:10', 13);
INSERT INTO `sys_oper_log` VALUES (145, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:44:50', 12);
INSERT INTO `sys_oper_log` VALUES (146, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:46:26', 15);
INSERT INTO `sys_oper_log` VALUES (147, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:46:36', 14);
INSERT INTO `sys_oper_log` VALUES (148, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:46:57', 14);
INSERT INTO `sys_oper_log` VALUES (149, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:48:30', 17);
INSERT INTO `sys_oper_log` VALUES (150, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:51:28', 9);
INSERT INTO `sys_oper_log` VALUES (151, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:51:38', 8);
INSERT INTO `sys_oper_log` VALUES (152, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:52:10', 11);
INSERT INTO `sys_oper_log` VALUES (153, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-26 22:52:25', 15);
INSERT INTO `sys_oper_log` VALUES (154, '表格权限业务', 2, 'com.ruoyi.tableConfig.controller.SysRoleTableController.forceSynchDb()', 'GET', 1, 'admin', NULL, '/roleTable/forceSynchDb', '127.0.0.1', '', '{\"roleId\":\"2\",\"menuId\":\"100\",\"tableId\":\"1\",\"roleTableId\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-27 20:31:12', 655);
INSERT INTO `sys_oper_log` VALUES (155, '定时任务', 2, 'com.ruoyi.job.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/job/run', '127.0.0.1', '', '{\"jobGroup\":\"DEFAULT\",\"jobId\":5,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-27 20:32:25', 256);
INSERT INTO `sys_oper_log` VALUES (156, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_dept\",\"menuId\":\"103\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-27 20:50:13', 77);
INSERT INTO `sys_oper_log` VALUES (157, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_post\",\"menuId\":\"104\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-27 21:14:17', 71);
INSERT INTO `sys_oper_log` VALUES (158, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_dict_type,sys_dict_data\",\"menuId\":\"105\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-27 21:24:03', 118);
INSERT INTO `sys_oper_log` VALUES (159, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_config\",\"menuId\":\"106\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-27 21:38:18', 59);
INSERT INTO `sys_oper_log` VALUES (160, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_notice\",\"menuId\":\"107\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-27 21:45:50', 63);
INSERT INTO `sys_oper_log` VALUES (161, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_oper_log\",\"menuId\":\"500\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-27 21:52:43', 105);
INSERT INTO `sys_oper_log` VALUES (162, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_logininfor\",\"menuId\":\"501\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-27 22:00:03', 59);
INSERT INTO `sys_oper_log` VALUES (163, '表格权限业务', 6, 'com.ruoyi.tableConfig.controller.SysTableController.importTableSave()', 'POST', 1, 'admin', NULL, '/menuTable/importTable', '127.0.0.1', '', '{\"tables\":\"sys_print_provider,sys_print_panel\",\"menuId\":\"2010\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-07-27 22:06:57', 125);
INSERT INTO `sys_oper_log` VALUES (164, '参数管理', 2, 'com.ruoyi.system.controller.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/config', '127.0.0.1', '', '{\"configId\":5,\"configKey\":\"sys.config.grayMode\",\"configName\":\"灰色模式\",\"configType\":\"Y\",\"configValue\":\"true\",\"params\":{},\"remark\":\"键值为true时开启灰色模式\",\"updateBy\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-08 20:59:17', 171);
INSERT INTO `sys_oper_log` VALUES (165, '参数管理', 2, 'com.ruoyi.system.controller.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/config', '127.0.0.1', '', '{\"configId\":6,\"configKey\":\"sys.sidebar.container\",\"configName\":\"侧边栏标题内容\",\"configType\":\"Y\",\"configValue\":\"111111111111\",\"params\":{},\"updateBy\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-08 20:59:31', 27);
INSERT INTO `sys_oper_log` VALUES (166, '参数管理', 2, 'com.ruoyi.system.controller.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/config', '127.0.0.1', '', '{\"configId\":5,\"configKey\":\"sys.config.grayMode\",\"configName\":\"灰色模式\",\"configType\":\"Y\",\"configValue\":\"false\",\"params\":{},\"remark\":\"键值为true时开启灰色模式\",\"updateBy\":1,\"updateByName\":\"admin\",\"updateTime\":\"2023-08-08 20:59:17\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-08 20:59:49', 24);
INSERT INTO `sys_oper_log` VALUES (167, '参数管理', 2, 'com.ruoyi.system.controller.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/config', '127.0.0.1', '', '{\"configId\":6,\"configKey\":\"sys.sidebar.container\",\"configName\":\"侧边栏标题内容\",\"configParam\":\"{sidebarTitle:\\\"222222222222222222\\\"}\",\"configType\":\"Y\",\"configValue\":\"true\",\"params\":{},\"updateBy\":1,\"updateByName\":\"admin\",\"updateTime\":\"2023-08-08 20:59:31\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-08 21:03:21', 25);
INSERT INTO `sys_oper_log` VALUES (168, '参数管理', 2, 'com.ruoyi.system.controller.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/config', '127.0.0.1', '', '{\"configId\":6,\"configKey\":\"sys.sidebar.container\",\"configName\":\"侧边栏标题内容\",\"configParam\":\"{\\\"sidebarTitle\\\":\\\"ruoyi-vxeTable\\\",\\\"sidebarLogo\\\":false}\",\"configType\":\"Y\",\"configValue\":\"true\",\"params\":{},\"remark\":\"sidebarTitle：项目名称，sidebarLogo：是否显示logo，sidebarLogoUrl：logo地址\",\"updateBy\":1,\"updateByName\":\"admin\",\"updateTime\":\"2023-08-08 21:03:21\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-08 22:08:46', 21);
INSERT INTO `sys_oper_log` VALUES (169, '参数管理', 2, 'com.ruoyi.system.controller.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/config', '127.0.0.1', '', '{\"configId\":7,\"configKey\":\"sys.init.dict\",\"configName\":\"初始化所有字典\",\"configType\":\"Y\",\"configValue\":\"false\",\"params\":{},\"remark\":\"键值为true时在登录之后初始化所有字典\",\"updateBy\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-08 22:09:33', 23);
INSERT INTO `sys_oper_log` VALUES (170, '参数管理', 2, 'com.ruoyi.system.controller.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/config', '127.0.0.1', '', '{\"configId\":6,\"configKey\":\"sys.sidebar.container\",\"configName\":\"侧边栏标题内容\",\"configParam\":\"{\\\"sidebarTitle\\\":\\\"ruoyi-vxeTable\\\",\\\"sidebarLogo\\\":false}\",\"configType\":\"Y\",\"configValue\":\"true\",\"params\":{},\"remark\":\"键值为true时启用该配置；sidebarTitle：项目名称，sidebarLogo：是否显示logo，sidebarLogoUrl：logo地址\",\"updateBy\":1,\"updateByName\":\"admin\",\"updateTime\":\"2023-08-08 22:08:46\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-08 22:11:08', 23);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', '1', '2023-01-31 20:38:15', NULL, NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', '1', '2023-01-31 20:38:15', NULL, NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', '1', '2023-01-31 20:38:15', NULL, NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '1', '1', '2023-01-31 20:38:15', '1', '2023-02-24 21:53:42', '');

-- ----------------------------
-- Table structure for sys_print_panel
-- ----------------------------
DROP TABLE IF EXISTS `sys_print_panel`;
CREATE TABLE `sys_print_panel`  (
  `panel_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '模板id',
  `provider_id` int(11) NULL DEFAULT NULL COMMENT '绑定的面板id',
  `panel_paper_width` double NULL DEFAULT NULL COMMENT '纸张宽度',
  `panel_paper_height` double NULL DEFAULT NULL COMMENT '纸张高度',
  `panel_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板名称',
  `panel_data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '模板数据（json）',
  `panel_paper_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '纸张类型',
  `rotate` tinyint(1) NULL DEFAULT NULL COMMENT '纸张是否旋转',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`panel_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '打印模板' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_print_panel
-- ----------------------------
INSERT INTO `sys_print_panel` VALUES (1, 1, 210, 270, '测试', '{\"panels\":[{\"index\":0,\"paperType\":\"A4\",\"height\":297,\"width\":210,\"paperHeader\":4.5,\"paperFooter\":814.5,\"printElements\":[{\"options\":{\"left\":408,\"top\":16.5,\"height\":21,\"width\":142.5,\"field\":\"recimprestno\",\"testData\":\"函证编号\",\"fontSize\":9,\"fontWeight\":\"700\",\"textContentVerticalAlign\":\"middle\",\"title\":\"函证编号\",\"right\":541.5,\"bottom\":37.5,\"vCenter\":474.75,\"hCenter\":27,\"coordinateSync\":false,\"widthHeightSync\":false,\"qrCodeLevel\":0},\"printElementType\":{\"title\":\"函证编号\",\"type\":\"text\"}},{\"options\":{\"left\":175.5,\"top\":51,\"height\":27,\"width\":259,\"title\":\"对账函\",\"fontSize\":19,\"fontWeight\":\"600\",\"textAlign\":\"center\",\"lineHeight\":26,\"coordinateSync\":false,\"widthHeightSync\":false,\"qrCodeLevel\":0,\"right\":432.25,\"bottom\":77.25,\"vCenter\":302.75,\"hCenter\":63.75,\"letterSpacing\":12,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"自定义文本\",\"type\":\"text\"}},{\"options\":{\"left\":57,\"top\":103.5,\"height\":18,\"width\":460.5,\"field\":\"customname\",\"testData\":\"客户名称\",\"fontSize\":14.25,\"textContentVerticalAlign\":\"middle\",\"title\":\"客户名称\",\"coordinateSync\":false,\"widthHeightSync\":false,\"hideTitle\":true,\"qrCodeLevel\":0,\"right\":517.5,\"bottom\":121.5,\"vCenter\":287.25,\"hCenter\":112.5,\"fontFamily\":\"SimSun\",\"textDecoration\":\"underline\"},\"printElementType\":{\"title\":\"客户名称\",\"type\":\"text\"}},{\"options\":{\"left\":40.5,\"top\":103.5,\"height\":18,\"width\":16.5,\"title\":\"致\",\"coordinateSync\":false,\"widthHeightSync\":false,\"fontSize\":14.25,\"textContentVerticalAlign\":\"middle\",\"qrCodeLevel\":0,\"right\":138.75,\"bottom\":64.5,\"vCenter\":78.75,\"hCenter\":59.625,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":39,\"top\":135,\"height\":216,\"width\":511.5,\"field\":\"letterMessage\",\"testData\":\"&emsp;&emsp;感谢贵单位长期以来对我公司业务的支持和配合，为保障贵单位与本公司的贸易业务开展顺畅，往来账务记录清晰，特向贵单位核对与本公司的往来账账项。下列数据出自本公司账簿记录，如与贵单位记录相符，请在本函下端“信息证明无误”处签章证明；如有不符，请在“信息不符”处列明不符金额，并加盖公章或财务专用章；关于“信息不符”的成因，请贵单位提供相关说明附件。<br/>&emsp;&emsp;本函仅为复核账目之用，并非催款结算；本公司对贵单位的支持配合表示衷心的感谢。<br/><br/>通讯地址：云南省昆明市盘龙区新迎路182号新迎新城写字楼1栋33A楼<br/>联系人：李昌杰<br/>联系电话：0871-63610552、0871-65709609 &emsp;&emsp;传真：0871- 63613074\",\"fontSize\":14.25,\"fontWeight\":\"500\",\"textContentVerticalAlign\":\"middle\",\"title\":\"致辞\",\"right\":549,\"bottom\":319.5,\"vCenter\":293.25,\"hCenter\":227.25,\"coordinateSync\":false,\"widthHeightSync\":false,\"hideTitle\":true,\"letterSpacing\":0.75,\"lineHeight\":19.5,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"致辞\",\"type\":\"longText\"}},{\"options\":{\"left\":39,\"top\":370.5,\"height\":16.5,\"width\":294,\"title\":\"本公司与贵医院的往来账项如下\",\"field\":\"empty\",\"coordinateSync\":false,\"widthHeightSync\":false,\"fontSize\":14.25,\"textContentVerticalAlign\":\"middle\",\"qrCodeLevel\":0,\"right\":223.5,\"bottom\":346.5,\"vCenter\":129.75,\"hCenter\":338.25,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":40.5,\"top\":393,\"height\":33,\"width\":168,\"title\":\"截止时间\",\"right\":179.25,\"bottom\":309,\"vCenter\":108.75,\"hCenter\":292.5,\"coordinateSync\":false,\"widthHeightSync\":false,\"fontSize\":14.25,\"textAlign\":\"center\",\"textContentVerticalAlign\":\"middle\",\"borderLeft\":\"solid\",\"borderTop\":\"solid\",\"borderRight\":\"solid\",\"qrCodeLevel\":0,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":375,\"top\":393,\"height\":33,\"width\":166.5,\"title\":\"贵单位应付账款余额\",\"right\":534.75,\"bottom\":308.25,\"vCenter\":451.5,\"hCenter\":291.75,\"coordinateSync\":false,\"widthHeightSync\":false,\"fontSize\":14.25,\"textAlign\":\"center\",\"textContentVerticalAlign\":\"middle\",\"borderTop\":\"solid\",\"borderRight\":\"solid\",\"qrCodeLevel\":0,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":208.5,\"top\":393,\"height\":33,\"width\":166.5,\"title\":\"我公司应收账款余额\",\"right\":369,\"bottom\":310.5,\"vCenter\":285.75,\"hCenter\":294,\"coordinateSync\":false,\"widthHeightSync\":false,\"fontSize\":14.25,\"textAlign\":\"center\",\"textContentVerticalAlign\":\"middle\",\"borderTop\":\"solid\",\"borderRight\":\"solid\",\"qrCodeLevel\":0,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":208.5,\"top\":426,\"height\":33,\"width\":166.5,\"field\":\"recamount\",\"testData\":\"对账金额\",\"fontSize\":14.25,\"fontWeight\":\"500\",\"textAlign\":\"center\",\"textContentVerticalAlign\":\"middle\",\"title\":\"对账金额\",\"right\":321.75,\"bottom\":341.25,\"vCenter\":261.75,\"hCenter\":324.75,\"coordinateSync\":false,\"widthHeightSync\":false,\"hideTitle\":true,\"borderTop\":\"solid\",\"borderRight\":\"solid\",\"borderBottom\":\"solid\",\"qrCodeLevel\":0,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"对账金额\",\"type\":\"text\"}},{\"options\":{\"left\":375,\"top\":426,\"height\":33,\"width\":166.5,\"title\":\"文本\",\"right\":534.75,\"bottom\":370.5,\"vCenter\":451.5,\"hCenter\":354,\"coordinateSync\":false,\"widthHeightSync\":false,\"borderTop\":\"solid\",\"borderRight\":\"solid\",\"borderBottom\":\"solid\",\"qrCodeLevel\":0,\"textAlign\":\"center\",\"textContentVerticalAlign\":\"middle\",\"field\":\"empty\",\"hideTitle\":true},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":40.5,\"top\":426,\"height\":33,\"width\":168,\"field\":\"recmonth\",\"testData\":\"对账月\",\"fontSize\":14.25,\"fontWeight\":\"500\",\"textAlign\":\"center\",\"textContentVerticalAlign\":\"middle\",\"title\":\"对账月\",\"right\":157.5,\"bottom\":444.25,\"vCenter\":97.5,\"hCenter\":436.25,\"coordinateSync\":false,\"widthHeightSync\":false,\"hideTitle\":true,\"qrCodeLevel\":0,\"fontFamily\":\"SimSun\",\"borderLeft\":\"solid\",\"borderTop\":\"solid\",\"borderRight\":\"solid\",\"borderBottom\":\"solid\"},\"printElementType\":{\"title\":\"对账月\",\"type\":\"text\"}},{\"options\":{\"left\":394.5,\"top\":495,\"height\":19.5,\"width\":145.5,\"title\":\"国药控股云南有限公司\",\"right\":541.5,\"bottom\":514.5,\"vCenter\":468.75,\"hCenter\":504.75,\"coordinateSync\":false,\"widthHeightSync\":false,\"fontSize\":14.25,\"textContentVerticalAlign\":\"middle\",\"qrCodeLevel\":0,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":420,\"top\":526.5,\"height\":19.5,\"width\":120,\"field\":\"nowdate\",\"testData\":\"系统时间\",\"fontSize\":13.5,\"textContentVerticalAlign\":\"middle\",\"title\":\"系统时间\",\"right\":543,\"bottom\":546,\"vCenter\":483,\"hCenter\":536.25,\"coordinateSync\":false,\"widthHeightSync\":false,\"hideTitle\":true,\"qrCodeLevel\":0,\"fontFamily\":\"SimSun\",\"letterSpacing\":2.25},\"printElementType\":{\"title\":\"系统时间\",\"type\":\"text\"}},{\"options\":{\"left\":40.5,\"top\":541.5,\"height\":19.5,\"width\":31.5,\"title\":\"回函\",\"coordinateSync\":false,\"widthHeightSync\":false,\"fontSize\":14.25,\"fontWeight\":\"bold\",\"textContentVerticalAlign\":\"middle\",\"qrCodeLevel\":0,\"right\":67.5,\"bottom\":560.25,\"vCenter\":51.75,\"hCenter\":550.5,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":357,\"top\":568.5,\"height\":18,\"width\":144,\"title\":\"信息不符请加以说明：\",\"right\":476.25,\"bottom\":586.5,\"vCenter\":414.75,\"hCenter\":577.5,\"coordinateSync\":false,\"widthHeightSync\":false,\"textContentVerticalAlign\":\"middle\",\"qrCodeLevel\":0,\"fontSize\":14.25,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":40.5,\"top\":568.5,\"height\":18,\"width\":120,\"title\":\"信息证明无误\",\"right\":155.25,\"bottom\":585.75,\"vCenter\":95.25,\"hCenter\":576.75,\"coordinateSync\":false,\"widthHeightSync\":false,\"textContentVerticalAlign\":\"middle\",\"qrCodeLevel\":0,\"fontSize\":14.25,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":40.5,\"top\":705,\"height\":19.5,\"width\":102,\"title\":\"签章：\",\"right\":145.5,\"bottom\":712.5,\"vCenter\":94.5,\"hCenter\":702.75,\"coordinateSync\":false,\"widthHeightSync\":false,\"fontSize\":14.25,\"textContentVerticalAlign\":\"middle\",\"qrCodeLevel\":0,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":357,\"top\":705,\"height\":19.5,\"width\":102,\"title\":\"签章：\",\"right\":458.25,\"bottom\":714.75,\"vCenter\":407.25,\"hCenter\":705,\"coordinateSync\":false,\"widthHeightSync\":false,\"fontSize\":14.25,\"textContentVerticalAlign\":\"middle\",\"qrCodeLevel\":0,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":40.5,\"top\":753,\"height\":16.5,\"width\":102,\"title\":\"日期：\",\"right\":138,\"bottom\":757.5,\"vCenter\":87,\"hCenter\":749.25,\"coordinateSync\":false,\"widthHeightSync\":false,\"fontSize\":14.25,\"textContentVerticalAlign\":\"middle\",\"qrCodeLevel\":0,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}},{\"options\":{\"left\":357,\"top\":753,\"height\":16.5,\"width\":102,\"title\":\"日期：\",\"right\":454.5,\"bottom\":758.25,\"vCenter\":403.5,\"hCenter\":750,\"coordinateSync\":false,\"widthHeightSync\":false,\"fontSize\":14.25,\"textContentVerticalAlign\":\"middle\",\"qrCodeLevel\":0,\"fontFamily\":\"SimSun\"},\"printElementType\":{\"title\":\"文本\",\"type\":\"text\"}}],\"paperNumberLeft\":565.5,\"paperNumberTop\":819,\"paperNumberDisabled\":true}]}', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_print_panel` VALUES (2, 1, NULL, NULL, '123', '{\"panels\":[{\"index\":0,\"paperType\":\"A4\",\"height\":297,\"width\":210,\"paperHeader\":4.5,\"paperFooter\":814.5,\"printElements\":[{\"options\":{\"left\":408,\"top\":16.5,\"height\":21,\"width\":142.5,\"field\":\"recimprestno\",\"testData\":\"函证编号\",\"fontSize\":9,\"fontWeight\":\"700\",\"textContentVerticalAlign\":\"middle\",\"title\":\"函证编号\",\"right\":541.5,\"bottom\":37.5,\"vCenter\":474.75,\"hCenter\":27,\"coordinateSync\":false,\"widthHeightSync\":false,\"qrCodeLevel\":0},\"printElementType\":{\"title\":\"函证编号\",\"type\":\"text\"}},{\"options\":{\"left\":264,\"top\":115.5,\"height\":16,\"width\":120,\"field\":\"imprestno\",\"testData\":\"单号\",\"fontSize\":6.75,\"fontWeight\":\"700\",\"textAlign\":\"left\",\"textContentVerticalAlign\":\"middle\",\"title\":\"单号\"},\"printElementType\":{\"title\":\"单号\",\"type\":\"text\"}},{\"options\":{\"left\":144,\"top\":147,\"height\":16,\"width\":120,\"field\":\"createtime\",\"testData\":\"创建时间\",\"fontSize\":6.75,\"fontWeight\":\"700\",\"textAlign\":\"left\",\"textContentVerticalAlign\":\"middle\",\"title\":\"创建时间\"},\"printElementType\":{\"title\":\"创建时间\",\"type\":\"text\"}}],\"paperNumberLeft\":565.5,\"paperNumberTop\":819,\"paperNumberDisabled\":true}]}', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_print_panel` VALUES (3, NULL, NULL, NULL, '修改保存', '{vg}', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_print_panel` VALUES (4, NULL, NULL, NULL, '保存', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_print_provider
-- ----------------------------
DROP TABLE IF EXISTS `sys_print_provider`;
CREATE TABLE `sys_print_provider`  (
  `provider_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '面板id',
  `provider_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表名称',
  `provider_data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '面板数据',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`provider_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '打印模板面板' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_print_provider
-- ----------------------------
INSERT INTO `sys_print_provider` VALUES (1, '测试', '{\n  \"modalName\": \"recimprestModal\",\n  \"printProvider\": [\n    {\n      \"groupName\": \"总单\",\n      \"groupModal\": [\n        {\n          \"tid\": \"recimprestModal.imprestno\",\n          \"title\": \"单号\",\n          \"field\": \"imprestno\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"imprestno\",\n            \"testData\": \"单号\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.recimprestid\",\n          \"title\": \"对账函ID\",\n          \"field\": \"recimprestid\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"recimprestid\",\n            \"testData\": \"对账函ID\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.createtime\",\n          \"title\": \"创建时间\",\n          \"field\": \"createtime\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"createtime\",\n            \"testData\": \"创建时间\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.createuserid\",\n          \"title\": \"创建人id\",\n          \"field\": \"createuserid\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"createuserid\",\n            \"testData\": \"创建人id\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.createusername\",\n          \"title\": \"创建人\",\n          \"field\": \"createusername\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"createusername\",\n            \"testData\": \"创建人\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.modifytime\",\n          \"title\": \"修改时间\",\n          \"field\": \"modifytime\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"modifytime\",\n            \"testData\": \"修改时间\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.modifyuserid\",\n          \"title\": \"修改人id\",\n          \"field\": \"modifyuserid\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"modifyuserid\",\n            \"testData\": \"修改人id\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.modifyuser\",\n          \"title\": \"修改人\",\n          \"field\": \"modifyuser\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"modifyuser\",\n            \"testData\": \"修改人\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.supplyid\",\n          \"title\": \"供应商id\",\n          \"field\": \"supplyid\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"supplyid\",\n            \"testData\": \"供应商id\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.supplycode\",\n          \"title\": \"供应商代码\",\n          \"field\": \"supplycode\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"supplycode\",\n            \"testData\": \"供应商代码\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.supplyname\",\n          \"title\": \"供应商名称\",\n          \"field\": \"supplyname\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"supplyname\",\n            \"testData\": \"供应商名称\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.middlemanid\",\n          \"title\": \"代理商id\",\n          \"field\": \"middlemanid\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"middlemanid\",\n            \"testData\": \"代理商id\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.middlemancode\",\n          \"title\": \"代理商代码\",\n          \"field\": \"middlemancode\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"middlemancode\",\n            \"testData\": \"代理商代码\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.middleman\",\n          \"title\": \"代理商名称\",\n          \"field\": \"middleman\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"middleman\",\n            \"testData\": \"代理商名称\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.ownerid\",\n          \"title\": \"公司id\",\n          \"field\": \"ownerid\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"ownerid\",\n            \"testData\": \"公司id\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.ownername\",\n          \"title\": \"公司名称\",\n          \"field\": \"ownername\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"ownername\",\n            \"testData\": \"公司名称\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.buyerid\",\n          \"title\": \"采购员id\",\n          \"field\": \"buyerid\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"buyerid\",\n            \"testData\": \"采购员id\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.buyer\",\n          \"title\": \"采购员\",\n          \"field\": \"buyer\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"buyer\",\n            \"testData\": \"采购员\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.finreceiverid\",\n          \"title\": \"财务对接人id\",\n          \"field\": \"finreceiverid\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"finreceiverid\",\n            \"testData\": \"财务对接人id\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.finreceiver\",\n          \"title\": \"财务对接人\",\n          \"field\": \"finreceiver\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"finreceiver\",\n            \"testData\": \"财务对接人\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.companyrecamount\",\n          \"title\": \"应付余额\",\n          \"field\": \"companyrecamount\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"companyrecamount\",\n            \"testData\": \"应付余额\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.syscompanyrecamount\",\n          \"title\": \"系统应付余额\",\n          \"field\": \"syscompanyrecamount\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"syscompanyrecamount\",\n            \"testData\": \"系统应付余额\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.prepaybalance\",\n          \"title\": \"预付款余额\",\n          \"field\": \"prepaybalance\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"prepaybalance\",\n            \"testData\": \"预付款余额\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.recmonth\",\n          \"title\": \"对账月\",\n          \"field\": \"recmonth\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"recmonth\",\n            \"testData\": \"对账月\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.rectype\",\n          \"title\": \"对账类型\",\n          \"field\": \"rectype\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"rectype\",\n            \"testData\": \"对账类型\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.recamount\",\n          \"title\": \"对账金额\",\n          \"field\": \"recamount\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"recamount\",\n            \"testData\": \"对账金额\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.replyamount\",\n          \"title\": \"回函金额\",\n          \"field\": \"replyamount\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"replyamount\",\n            \"testData\": \"回函金额\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.differenceamount\",\n          \"title\": \"差异金额\",\n          \"field\": \"differenceamount\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"differenceamount\",\n            \"testData\": \"差异金额\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.status\",\n          \"title\": \"状态\",\n          \"field\": \"status\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"status\",\n            \"testData\": \"状态\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.adjtotalamount\",\n          \"title\": \"调整总金额\",\n          \"field\": \"adjtotalamount\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"adjtotalamount\",\n            \"testData\": \"调整总金额\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.storageuninvoice\",\n          \"title\": \"已入库未开票\",\n          \"field\": \"storageuninvoice\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"storageuninvoice\",\n            \"testData\": \"已入库未开票\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.onorder\",\n          \"title\": \"在途\",\n          \"field\": \"onorder\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"onorder\",\n            \"testData\": \"在途\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.purbackamount\",\n          \"title\": \"采退未处理\",\n          \"field\": \"purbackamount\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"purbackamount\",\n            \"testData\": \"采退未处理\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.spreadrebate\",\n          \"title\": \"价差返利\",\n          \"field\": \"spreadrebate\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"spreadrebate\",\n            \"testData\": \"价差返利\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.other\",\n          \"title\": \"其他\",\n          \"field\": \"other\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"other\",\n            \"testData\": \"其他\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.otherreason\",\n          \"title\": \"其他差异说明\",\n          \"field\": \"otherreason\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"otherreason\",\n            \"testData\": \"其他差异说明\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.adjreplyamount\",\n          \"title\": \"调整后回函金额\",\n          \"field\": \"adjreplyamount\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"adjreplyamount\",\n            \"testData\": \"调整后回函金额\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.adjdifferenceamount\",\n          \"title\": \"调整后差异金额\",\n          \"field\": \"adjdifferenceamount\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"adjdifferenceamount\",\n            \"testData\": \"调整后差异金额\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.adjdifferencereason\",\n          \"title\": \"调整后差异原因\",\n          \"field\": \"adjdifferencereason\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"adjdifferencereason\",\n            \"testData\": \"调整后差异原因\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.memo\",\n          \"title\": \"备注\",\n          \"field\": \"memo\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"memo\",\n            \"testData\": \"备注\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.printnum\",\n          \"title\": \"打印次数\",\n          \"field\": \"printnum\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"printnum\",\n            \"testData\": \"打印次数\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.isprint\",\n          \"title\": \"是否打印\",\n          \"field\": \"isprint\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"isprint\",\n            \"testData\": \"是否打印\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.replynum\",\n          \"title\": \"回函次数\",\n          \"field\": \"replynum\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"replynum\",\n            \"testData\": \"回函次数\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.reviewerid\",\n          \"title\": \"复核人id\",\n          \"field\": \"reviewerid\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"reviewerid\",\n            \"testData\": \"复核人id\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.reviewer\",\n          \"title\": \"复核人\",\n          \"field\": \"reviewer\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"reviewer\",\n            \"testData\": \"复核人\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.replyinfo\",\n          \"title\": \"复核信息\",\n          \"field\": \"replyinfo\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"replyinfo\",\n            \"testData\": \"复核信息\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.replyresult\",\n          \"title\": \"回函结果\",\n          \"field\": \"replyresult\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"replyresult\",\n            \"testData\": \"回函结果\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.replycompletememo\",\n          \"title\": \"回函完成说明\",\n          \"field\": \"replycompletememo\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"replycompletememo\",\n            \"testData\": \"回函完成说明\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.auditmanid\",\n          \"title\": \"确认人id\",\n          \"field\": \"auditmanid\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"auditmanid\",\n            \"testData\": \"确认人id\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.auditman\",\n          \"title\": \"确认人\",\n          \"field\": \"auditman\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"auditman\",\n            \"testData\": \"确认人\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.audittime\",\n          \"title\": \"确认时间\",\n          \"field\": \"audittime\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"audittime\",\n            \"testData\": \"确认时间\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.isreview\",\n          \"title\": \"是否复核\",\n          \"field\": \"isreview\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"isreview\",\n            \"testData\": \"是否复核\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.sendcompany\",\n          \"title\": \"名称\",\n          \"field\": \"sendcompany\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"sendcompany\",\n            \"testData\": \"名称\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.letterMessage\",\n          \"title\": \"致辞\",\n          \"field\": \"letterMessage\",\n          \"data\": \"  感谢贵公司长期以来对我公司业务的支持和配合，为保障贵公司与本公司的贸易业务开展顺畅，往来账务记录清晰，特向贵公司核对与本公司的往来账账项。下列数据出自本公司账簿记录，如与贵公司记录相符，请在本函下端“信息证明无误”处签章证明，并按确认后的金额向我公司退款或发货；如有不符，请在“信息不符”处列明不符金额，并加盖公章或财务专用章，并先就无争议部份的金额向我公司退款或发货。关于“信息不符”的成因，请贵公司提供相关说明附件，本公司对贵公司的支持配合表示衷心的感谢。 \\n\\r通讯地址：云南省昆明市盘龙区新迎路182号新迎新城写字楼1栋33A楼\",\n          \"type\": \"longText\",\n          \"options\": {\n            \"field\": \"letterMessage\",\n            \"testData\": \"  感谢贵公司长期以来对我公司业务的支持和配合，为保障贵公司与本公司的贸易业务开展顺畅，往来账务记录清晰，特向贵公司核对与本公司的往来账账项。下列数据出自本公司账簿记录，如与贵公司记录相符，请在本函下端“信息证明无误”处签章证明，并按确认后的金额向我公司退款或发货；如有不符，请在“信息不符”处列明不符金额，并加盖公章或财务专用章，并先就无争议部份的金额向我公司退款或发货。关于“信息不符”的成因，请贵公司提供相关说明附件，本公司对贵公司的支持配合表示衷心的感谢。\\n\\r通讯地址：云南省昆明市盘龙区新迎路182号新迎新城写字楼1栋33A楼\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.nowdate\",\n          \"title\": \"系统时间\",\n          \"field\": \"nowdate\",\n          \"data\": \"系统时间\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"nowdate\",\n            \"testData\": \"系统时间\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        },\n        {\n          \"tid\": \"recimprestModal.createdate\",\n          \"title\": \"创建时间(转换)\",\n          \"field\": \"createdate\",\n          \"data\": \"\",\n          \"type\": \"text\",\n          \"options\": {\n            \"field\": \"createdate\",\n            \"testData\": \"创建时间(转换)\",\n            \"height\": 16,\n            \"fontSize\": 6.75,\n            \"fontWeight\": \"700\",\n            \"textAlign\": \"left\",\n            \"textContentVerticalAlign\": \"middle\"\n          }\n        }\n      ]\n    }\n  ]\n}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_print_provider` VALUES (2, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限 5:仅本人数据权限 6:自定义语句）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `custom_condition` tinyint(1) NULL DEFAULT 0 COMMENT '自定义语句（需要配合数据授权使用）0：不控制 1：控制',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, 0, '0', '0', 1, '2023-01-31 20:38:15', 1, NULL, NULL);
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 0, 1, '0', '0', 1, '2023-01-31 20:38:15', 1, '2023-06-14 21:33:32', '普通角色');
INSERT INTO `sys_role` VALUES (3, '开发', 'dev', 3, '1', 1, 1, 1, '0', '0', 1, NULL, 1, '2023-06-13 20:35:03', NULL);
INSERT INTO `sys_role` VALUES (4, '测试', 'test', 4, '1', 1, 1, 0, '0', '0', 1, '2023-02-25 18:06:20', 1, '2023-05-22 15:26:13', NULL);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 2010);
INSERT INTO `sys_role_menu` VALUES (2, 2011);
INSERT INTO `sys_role_menu` VALUES (2, 2012);
INSERT INTO `sys_role_menu` VALUES (2, 2013);
INSERT INTO `sys_role_menu` VALUES (2, 2014);
INSERT INTO `sys_role_menu` VALUES (2, 2015);
INSERT INTO `sys_role_menu` VALUES (2, 2016);
INSERT INTO `sys_role_menu` VALUES (2, 2017);
INSERT INTO `sys_role_menu` VALUES (2, 2018);
INSERT INTO `sys_role_menu` VALUES (2, 2019);
INSERT INTO `sys_role_menu` VALUES (2, 2020);
INSERT INTO `sys_role_menu` VALUES (2, 2021);
INSERT INTO `sys_role_menu` VALUES (2, 2022);
INSERT INTO `sys_role_menu` VALUES (2, 2023);
INSERT INTO `sys_role_menu` VALUES (2, 2024);
INSERT INTO `sys_role_menu` VALUES (2, 2025);
INSERT INTO `sys_role_menu` VALUES (2, 2026);
INSERT INTO `sys_role_menu` VALUES (2, 2027);
INSERT INTO `sys_role_menu` VALUES (2, 2028);
INSERT INTO `sys_role_menu` VALUES (2, 2029);
INSERT INTO `sys_role_menu` VALUES (2, 2030);
INSERT INTO `sys_role_menu` VALUES (2, 2031);
INSERT INTO `sys_role_menu` VALUES (2, 2032);
INSERT INTO `sys_role_menu` VALUES (2, 2033);
INSERT INTO `sys_role_menu` VALUES (2, 2034);
INSERT INTO `sys_role_menu` VALUES (2, 2035);
INSERT INTO `sys_role_menu` VALUES (2, 2036);
INSERT INTO `sys_role_menu` VALUES (2, 2037);
INSERT INTO `sys_role_menu` VALUES (2, 2038);
INSERT INTO `sys_role_menu` VALUES (2, 2039);
INSERT INTO `sys_role_menu` VALUES (2, 2040);
INSERT INTO `sys_role_menu` VALUES (2, 2045);
INSERT INTO `sys_role_menu` VALUES (2, 2046);
INSERT INTO `sys_role_menu` VALUES (2, 2047);
INSERT INTO `sys_role_menu` VALUES (2, 2048);
INSERT INTO `sys_role_menu` VALUES (2, 2050);
INSERT INTO `sys_role_menu` VALUES (3, 1);
INSERT INTO `sys_role_menu` VALUES (3, 2);
INSERT INTO `sys_role_menu` VALUES (3, 100);
INSERT INTO `sys_role_menu` VALUES (3, 101);
INSERT INTO `sys_role_menu` VALUES (3, 102);
INSERT INTO `sys_role_menu` VALUES (3, 105);
INSERT INTO `sys_role_menu` VALUES (3, 106);
INSERT INTO `sys_role_menu` VALUES (3, 107);
INSERT INTO `sys_role_menu` VALUES (3, 108);
INSERT INTO `sys_role_menu` VALUES (3, 109);
INSERT INTO `sys_role_menu` VALUES (3, 110);
INSERT INTO `sys_role_menu` VALUES (3, 111);
INSERT INTO `sys_role_menu` VALUES (3, 112);
INSERT INTO `sys_role_menu` VALUES (3, 113);
INSERT INTO `sys_role_menu` VALUES (3, 500);
INSERT INTO `sys_role_menu` VALUES (3, 501);
INSERT INTO `sys_role_menu` VALUES (3, 1000);
INSERT INTO `sys_role_menu` VALUES (3, 1001);
INSERT INTO `sys_role_menu` VALUES (3, 1002);
INSERT INTO `sys_role_menu` VALUES (3, 1003);
INSERT INTO `sys_role_menu` VALUES (3, 1004);
INSERT INTO `sys_role_menu` VALUES (3, 1005);
INSERT INTO `sys_role_menu` VALUES (3, 1006);
INSERT INTO `sys_role_menu` VALUES (3, 1007);
INSERT INTO `sys_role_menu` VALUES (3, 1008);
INSERT INTO `sys_role_menu` VALUES (3, 1009);
INSERT INTO `sys_role_menu` VALUES (3, 1010);
INSERT INTO `sys_role_menu` VALUES (3, 1011);
INSERT INTO `sys_role_menu` VALUES (3, 1012);
INSERT INTO `sys_role_menu` VALUES (3, 1013);
INSERT INTO `sys_role_menu` VALUES (3, 1014);
INSERT INTO `sys_role_menu` VALUES (3, 1015);
INSERT INTO `sys_role_menu` VALUES (3, 1025);
INSERT INTO `sys_role_menu` VALUES (3, 1026);
INSERT INTO `sys_role_menu` VALUES (3, 1027);
INSERT INTO `sys_role_menu` VALUES (3, 1028);
INSERT INTO `sys_role_menu` VALUES (3, 1029);
INSERT INTO `sys_role_menu` VALUES (3, 1030);
INSERT INTO `sys_role_menu` VALUES (3, 1031);
INSERT INTO `sys_role_menu` VALUES (3, 1032);
INSERT INTO `sys_role_menu` VALUES (3, 1033);
INSERT INTO `sys_role_menu` VALUES (3, 1034);
INSERT INTO `sys_role_menu` VALUES (3, 1035);
INSERT INTO `sys_role_menu` VALUES (3, 1036);
INSERT INTO `sys_role_menu` VALUES (3, 1037);
INSERT INTO `sys_role_menu` VALUES (3, 1038);
INSERT INTO `sys_role_menu` VALUES (3, 1039);
INSERT INTO `sys_role_menu` VALUES (3, 1040);
INSERT INTO `sys_role_menu` VALUES (3, 1041);
INSERT INTO `sys_role_menu` VALUES (3, 1042);
INSERT INTO `sys_role_menu` VALUES (3, 1043);
INSERT INTO `sys_role_menu` VALUES (3, 1044);
INSERT INTO `sys_role_menu` VALUES (3, 1045);
INSERT INTO `sys_role_menu` VALUES (3, 1046);
INSERT INTO `sys_role_menu` VALUES (3, 1047);
INSERT INTO `sys_role_menu` VALUES (3, 1048);
INSERT INTO `sys_role_menu` VALUES (3, 1049);
INSERT INTO `sys_role_menu` VALUES (3, 1050);
INSERT INTO `sys_role_menu` VALUES (3, 1051);
INSERT INTO `sys_role_menu` VALUES (3, 1052);
INSERT INTO `sys_role_menu` VALUES (3, 1053);
INSERT INTO `sys_role_menu` VALUES (3, 1054);
INSERT INTO `sys_role_menu` VALUES (3, 2010);
INSERT INTO `sys_role_menu` VALUES (3, 2011);
INSERT INTO `sys_role_menu` VALUES (3, 2012);
INSERT INTO `sys_role_menu` VALUES (3, 2013);
INSERT INTO `sys_role_menu` VALUES (3, 2014);
INSERT INTO `sys_role_menu` VALUES (3, 2015);
INSERT INTO `sys_role_menu` VALUES (3, 2016);
INSERT INTO `sys_role_menu` VALUES (3, 2017);
INSERT INTO `sys_role_menu` VALUES (3, 2018);
INSERT INTO `sys_role_menu` VALUES (3, 2019);
INSERT INTO `sys_role_menu` VALUES (3, 2020);
INSERT INTO `sys_role_menu` VALUES (3, 2021);
INSERT INTO `sys_role_menu` VALUES (3, 2022);
INSERT INTO `sys_role_menu` VALUES (3, 2023);
INSERT INTO `sys_role_menu` VALUES (3, 2024);
INSERT INTO `sys_role_menu` VALUES (3, 2025);
INSERT INTO `sys_role_menu` VALUES (3, 2026);
INSERT INTO `sys_role_menu` VALUES (3, 2027);
INSERT INTO `sys_role_menu` VALUES (3, 2028);
INSERT INTO `sys_role_menu` VALUES (3, 2029);
INSERT INTO `sys_role_menu` VALUES (3, 2030);
INSERT INTO `sys_role_menu` VALUES (3, 2031);
INSERT INTO `sys_role_menu` VALUES (3, 2032);
INSERT INTO `sys_role_menu` VALUES (3, 2033);
INSERT INTO `sys_role_menu` VALUES (3, 2034);
INSERT INTO `sys_role_menu` VALUES (3, 2035);
INSERT INTO `sys_role_menu` VALUES (3, 2036);
INSERT INTO `sys_role_menu` VALUES (3, 2037);
INSERT INTO `sys_role_menu` VALUES (3, 2038);
INSERT INTO `sys_role_menu` VALUES (3, 2039);
INSERT INTO `sys_role_menu` VALUES (3, 2040);
INSERT INTO `sys_role_menu` VALUES (3, 2045);
INSERT INTO `sys_role_menu` VALUES (3, 2046);
INSERT INTO `sys_role_menu` VALUES (3, 2047);
INSERT INTO `sys_role_menu` VALUES (3, 2051);
INSERT INTO `sys_role_menu` VALUES (4, 1);
INSERT INTO `sys_role_menu` VALUES (4, 104);
INSERT INTO `sys_role_menu` VALUES (4, 1020);
INSERT INTO `sys_role_menu` VALUES (4, 1021);
INSERT INTO `sys_role_menu` VALUES (4, 1022);
INSERT INTO `sys_role_menu` VALUES (4, 1023);
INSERT INTO `sys_role_menu` VALUES (4, 1024);

-- ----------------------------
-- Table structure for sys_role_menu_power
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu_power`;
CREATE TABLE `sys_role_menu_power`  (
  `rmp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '数据角色功能授权ID',
  `role_id` int(20) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` int(11) NULL DEFAULT NULL COMMENT '菜单id',
  `power_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权值',
  `sort` int(3) NULL DEFAULT NULL COMMENT '排序',
  `status` int(3) NULL DEFAULT NULL COMMENT '状态（0启用 1禁用）',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`rmp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据角色功能授权' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu_power
-- ----------------------------
INSERT INTO `sys_role_menu_power` VALUES (4, 2, 101, 'OR u.user_id= 1', 0, 0, NULL, '2023-05-23 18:15:53', NULL, '2023-05-23 17:42:51', NULL);
INSERT INTO `sys_role_menu_power` VALUES (5, 2, 103, 'OR u.user_id= 1', 0, 0, NULL, '2023-05-23 18:15:53', NULL, '2023-05-23 17:42:51', NULL);

-- ----------------------------
-- Table structure for sys_role_table
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_table`;
CREATE TABLE `sys_role_table`  (
  `role_table_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表id',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `menu_id` int(11) NOT NULL COMMENT '功能id',
  `table_id` int(11) NULL DEFAULT NULL COMMENT '表格id',
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表名称',
  `table_comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表描述',
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '唯一标识',
  `height` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格的高度；支持铺满父容器或者固定高度，如果设置 auto 为铺满父容器（如果设置为 auto，则必须确保存在父节点且不允许存在相邻元素）',
  `unique_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格主键字段',
  `row_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格行id字段',
  `max_height` int(4) NULL DEFAULT NULL COMMENT '表格的最大高度',
  `auto_resize` tinyint(1) NULL DEFAULT NULL COMMENT '自动监听父元素的变化去重新计算表格（对于父元素可能存在动态变化、显示隐藏的容器中、列宽异常等场景中的可能会用到）',
  `sync_resize` tinyint(1) NULL DEFAULT NULL COMMENT '自动跟随某个属性的变化去重新计算表格，和手动调用 recalculate 方法是一样的效果（对于通过某个属性来控制显示/隐藏切换时可能会用到）',
  `stripe` tinyint(1) NULL DEFAULT NULL COMMENT '是否带有斑马纹（需要注意的是，在可编辑表格场景下，临时插入的数据不会有斑马纹样式）',
  `border` tinyint(1) NULL DEFAULT NULL COMMENT '是否带有边框',
  `round` tinyint(1) NULL DEFAULT NULL COMMENT '是否为圆角边框',
  `size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格的尺寸',
  `align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有的列对齐方式',
  `header_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有的表头列的对齐方式',
  `footer_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有的表尾列的对齐方式',
  `show_header` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示表头',
  `row_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给行附加 className',
  `cell_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给单元格附加 className',
  `header_row_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表头的行附加 className',
  `header_cell_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表头的单元格附加 className',
  `footer_row_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表尾的行附加 className',
  `footer_cell_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表尾的单元格附加 className',
  `show_footer` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示表尾',
  `footer_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表尾的数据获取方法，返回一个二维数组',
  `show_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设置所有内容过长时显示为省略号（如果是固定列建议设置该值，提升渲染速度）',
  `show_header_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设置表头所有内容过长时显示为省略号',
  `show_footer_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设置表尾所有内容过长时显示为省略号',
  `keep_source` tinyint(1) NULL DEFAULT NULL COMMENT '保持原始值的状态，被某些功能所依赖，比如编辑状态、还原数据等（开启后影响性能，具体取决于数据量）',
  `empty_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '空数据时显示的内容',
  `ext_params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义参数（可以用来存放一些自定义的数据）',
  `extra_config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '额外配置（json）',
  `hand_config` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作配置',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表格权限业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_table
-- ----------------------------
INSERT INTO `sys_role_table` VALUES (1, 2, 100, 1, 'sys_user', '用户信息表', 'sys_user_1_2', 'auto', NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"save\":false,\"add\":false,\"modify\":false,\"delete\":false}', 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12', NULL);

-- ----------------------------
-- Table structure for sys_role_table_column
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_table_column`;
CREATE TABLE `sys_role_table_column`  (
  `role_column_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色表格列配置',
  `column_id` int(11) NOT NULL COMMENT '编号',
  `role_table_id` int(11) NOT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段',
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `is_ignore` tinyint(1) NULL DEFAULT 1 COMMENT '是否显示（1是 0否）',
  `visible` tinyint(1) NULL DEFAULT 1 COMMENT '是否可视（1是 0否）',
  `width` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '宽度',
  `min_width` double NULL DEFAULT NULL COMMENT '最小列宽度会自动将剩余空间按比例分配',
  `resizable` tinyint(1) NULL DEFAULT 1 COMMENT '列宽是否可拖拽（1是 0否）',
  `align` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'left' COMMENT '列对齐方式：left（左对齐）, center（居中对齐）, right（右对齐）',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列的类型',
  `fixed` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '将列固定在左侧或者右侧（注意：固定列应该放在左右两侧的位置）',
  `header_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表头列的对齐方式',
  `footer_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表尾列的对齐方式',
  `show_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当内容过长时显示为省略号',
  `show_header_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当表头内容过长时显示为省略号',
  `show_footer_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当表尾内容过长时显示为省略号',
  `class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给单元格附加 className',
  `header_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表头的单元格附加 className',
  `footer_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表尾的单元格附加 className',
  `formatter` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '格式化显示内容',
  `sortable` tinyint(1) NULL DEFAULT 1 COMMENT '是否允许列排序',
  `sort_by` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对 sortable 有效，指定排序的字段（当值 formatter 格式化后，可以设置该字段，使用值进行排序）',
  `sort_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排序的字段类型，比如字符串转数值等',
  `ext_params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '额外的参数（可以用来存放一些私有参数）',
  `tree_node` tinyint(1) NULL DEFAULT NULL COMMENT '只对 tree-config 配置时有效，指定为树节点',
  `slots` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插槽',
  `filters` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置筛选条件',
  `filter_multiple` tinyint(1) NULL DEFAULT 1 COMMENT '只对 filters 有效，筛选是否允许多选',
  `filter_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对 filters 有效，列的筛选方法，该方法的返回值用来决定该行是否显示',
  `filter_reset_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对 filters 有效，自定义筛选重置方法',
  `filter_recover_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对 filters 有效，自定义筛选复原方法（使用自定义筛选时可能会用到）',
  `filter_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '筛选渲染器配置项',
  `export_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义单元格数据导出方法，返回自定义的值',
  `footer_export_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义表尾单元格数据导出方法，返回自定义的值',
  `title_prefix` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题前缀图标配置项',
  `cell_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对特定功能有效，单元格值类型',
  `cell_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认的渲染器配置项',
  `edit_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '可编辑渲染器配置项',
  `content_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容渲染配置项',
  `col_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义列的唯一主键',
  `sort_no` int(11) NULL DEFAULT NULL COMMENT '排序',
  `dict_type` text CHARACTER SET ujis COLLATE ujis_japanese_ci NULL COMMENT '字典类型',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`role_column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表格权限列配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_table_column
-- ----------------------------
INSERT INTO `sys_role_table_column` VALUES (1, 1, 1, 'user_id', '用户ID', 'userId', '用户ID', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (2, 2, 1, 'dept_id', '部门ID', 'deptId', '部门ID', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (3, 3, 1, 'user_name', '用户账号', 'userName', '用户账号', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (4, 4, 1, 'nick_name', '用户昵称', 'nickName', '用户昵称', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (5, 5, 1, 'user_type', '用户类型（00系统用户）', 'userType', '用户类型', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (6, 6, 1, 'email', '用户邮箱', 'email', '用户邮箱', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (7, 7, 1, 'phonenumber', '手机号码', 'phonenumber', '手机号码', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (8, 8, 1, 'sex', '用户性别（0男 1女 2未知）', 'sex', '用户性别', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_user_sex\"\r\n  }\r\n}', NULL, NULL, NULL, 8, '[\"sys_user_sex\"]', NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (9, 9, 1, 'avatar', '头像地址', 'avatar', '头像地址', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (10, 11, 1, 'status', '帐号状态（0正常 1停用）', 'status', '帐号状态', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\":\"ElDictTag\",\"props\":{\"options\":\"sys_normal_disable\"}}', NULL, NULL, NULL, 11, '[\"sys_normal_disable\"]', NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (11, 13, 1, 'login_ip', '最后登录IP', 'loginIp', '最后登录IP', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (12, 14, 1, 'login_date', '最后登录时间', 'loginDate', '最后登录时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (13, 15, 1, 'create_by', '创建者', 'createBy', '创建者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (14, 16, 1, 'create_time', '创建时间', 'createTime', '创建时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (15, 17, 1, 'update_by', '更新者', 'updateBy', '更新者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (16, 18, 1, 'update_time', '更新时间', 'updateTime', '更新时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (17, 19, 1, 'remark', '备注', 'remark', '备注', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_role_table_column` VALUES (18, 20, 1, NULL, NULL, 'createByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_role_table_column` VALUES (19, 21, 1, NULL, NULL, 'updateByName', '修改人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, 1, '2023-07-27 20:31:12', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_table_edit_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_table_edit_config`;
CREATE TABLE `sys_role_table_edit_config`  (
  `role_edit_config_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色可编辑配置项id',
  `edit_config_id` int(11) NOT NULL COMMENT '可编辑配置项id',
  `role_table_id` int(11) NOT NULL COMMENT '表id',
  `triggerbase` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '触发方式',
  `enabled` tinyint(1) NULL DEFAULT NULL COMMENT '是否启用',
  `mode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编辑模式',
  `show_icon` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示列头编辑图标',
  `show_status` tinyint(1) NULL DEFAULT NULL COMMENT '只对 keep-source 开启有效是否显示单元格新增与修改状态',
  `show_update_status` tinyint(1) NULL DEFAULT NULL COMMENT '只对 keep-source 开启有效是否显示单元格修改状态',
  `show_insert_status` tinyint(1) NULL DEFAULT NULL COMMENT '只对 keep-source 开启有效是否显示单元格新增状态',
  `show_asterisk` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示必填字段的红色星号',
  `auto_clear` tinyint(1) NULL DEFAULT NULL COMMENT '当点击非编辑列之后是否自动清除单元格的激活状态',
  `before_edit_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '该方法的返回值用来决定该单元格是否允许编辑',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义可编辑列的状态图标',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_edit_config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色可编辑配置项' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_table_edit_config
-- ----------------------------
INSERT INTO `sys_role_table_edit_config` VALUES (1, 1, 1, NULL, 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', 1, '2023-07-26 21:51:36', 1, '2023-07-27 20:31:12', '');

-- ----------------------------
-- Table structure for sys_role_table_edit_rules
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_table_edit_rules`;
CREATE TABLE `sys_role_table_edit_rules`  (
  `role_edit_rules_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色校验规则配置id',
  `edit_rules_id` int(11) NOT NULL COMMENT '校验规则配置id',
  `role_table_id` int(11) NOT NULL COMMENT '表id',
  `required` tinyint(1) NULL DEFAULT NULL COMMENT '是否必填',
  `min` int(11) NULL DEFAULT NULL COMMENT '校验值最小长度',
  `max` int(11) NULL DEFAULT NULL COMMENT '校验值最大长度',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据校验的类型',
  `pattern` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '正则校验',
  `validator` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义校验方法',
  `message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '校验提示内容',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_edit_rules_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色校验规则配置项' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_table_edit_rules
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_table_form
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_table_form`;
CREATE TABLE `sys_role_table_form`  (
  `role_form_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色查询表id',
  `form_id` int(11) NOT NULL COMMENT '查询表id',
  `role_table_id` int(11) NOT NULL COMMENT '表格id',
  `span` int(3) NULL DEFAULT NULL COMMENT '所有项的栅格占据的列数（共 24 分栏）',
  `align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有项的内容对齐方式',
  `size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '尺寸',
  `title_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有项的标题对齐方式',
  `title_width` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有项的标题宽度',
  `title_colon` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示标题冒号',
  `title_asterisk` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示必填字段的红色星号',
  `title_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有设置标题内容过长时显示为省略号',
  `class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表单附加 className',
  `collapse_status` tinyint(1) NULL DEFAULT NULL COMMENT 'v-model 绑定值，折叠状态',
  `custom_layout` tinyint(1) NULL DEFAULT NULL COMMENT '是否使用自定义布局',
  `prevent_submit` tinyint(1) NULL DEFAULT NULL COMMENT '是否禁用默认的回车提交方式禁用后配合 validate() 方法可以更加自由的控制提交逻辑',
  `enabled` tinyint(1) NULL DEFAULT NULL COMMENT '是否启用',
  `rules` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '校验规则配置项',
  `valid_config` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '检验配置项',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_form_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色功能表查询表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_table_form
-- ----------------------------
INSERT INTO `sys_role_table_form` VALUES (1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2023-07-26 21:51:36', 1, '2023-07-27 20:31:12', NULL);

-- ----------------------------
-- Table structure for sys_role_table_form_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_table_form_item`;
CREATE TABLE `sys_role_table_form_item`  (
  `role_item_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色表单项id',
  `item_id` int(11) NOT NULL COMMENT '表单项id',
  `role_form_id` int(11) NOT NULL COMMENT '表单id',
  `field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `span` int(2) NULL DEFAULT NULL COMMENT '栅格占据的列数（共 24 分栏）',
  `align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容对齐方式',
  `title_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题对齐方式',
  `title_width` int(3) NULL DEFAULT NULL COMMENT '标题宽度',
  `title_colon` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示标题冒号',
  `title_asterisk` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示必填字段的红色星号',
  `title_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题内容过长时显示为省略号',
  `show_title` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示标题',
  `class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表单项附加 className',
  `content_class_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表单项内容附加 className',
  `content_style` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表单项内容附加样式',
  `visible` tinyint(1) NULL DEFAULT 1 COMMENT '默认是否显示',
  `visible_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '该方法的返回值用来决定该项是否显示',
  `form_filter` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示删除按钮',
  `folding` tinyint(1) NULL DEFAULT 0 COMMENT '默认收起',
  `collapse_node` tinyint(1) NULL DEFAULT 0 COMMENT '折叠节点',
  `reset_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重置时的默认值',
  `item_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项渲染器配置项',
  `children` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项集合',
  `slots` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插槽',
  `sort_no` int(11) NULL DEFAULT NULL COMMENT '排序',
  `dict_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类型',
  `search_handle` tinyint(1) NULL DEFAULT 0 COMMENT '是否为查询操作项（查询，重置）',
  `search_no` int(11) NULL DEFAULT NULL COMMENT '查询排序',
  `search_visible` tinyint(1) NULL DEFAULT 1 COMMENT '是否是查询项',
  `search_fixed` tinyint(1) NULL DEFAULT NULL COMMENT '是否为固定查询',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_item_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表单配置项列表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_table_form_item
-- ----------------------------
INSERT INTO `sys_role_table_form_item` VALUES (1, 1, 1, 'userId', '用户ID', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户ID\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (2, 2, 1, 'deptId', '部门ID', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入部门ID\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (3, 3, 1, 'userName', '用户账号', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户账号\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (4, 4, 1, 'nickName', '用户昵称', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户昵称\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (5, 5, 1, 'userType', '用户类型', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户类型（00系统用户）\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (6, 6, 1, 'email', '用户邮箱', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户邮箱\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (7, 7, 1, 'phonenumber', '手机号码', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入手机号码\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (8, 8, 1, 'sex', '用户性别', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_user_sex\"\r\n}', NULL, NULL, NULL, '[\"sys_user_sex\"]', 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (9, 9, 1, 'avatar', '头像地址', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入头像地址\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (10, 10, 1, 'password', '密码', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入密码\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (11, 11, 1, 'status', '帐号状态', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_normal_disable\"\r\n}', NULL, NULL, NULL, '[\"sys_normal_disable\"]', 0, NULL, 1, 1, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (12, 12, 1, 'delFlag', '删除标志', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入删除标志（0代表存在 2代表删除）\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (13, 13, 1, 'loginIp', '最后登录IP', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入最后登录IP\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (14, 14, 1, 'loginDate', '最后登录时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入最后登录时间\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (15, 15, 1, 'createBy', '创建者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建者\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (16, 16, 1, 'createTime', '创建时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建时间\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (17, 17, 1, 'updateBy', '更新者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新者\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (18, 18, 1, 'updateTime', '更新时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新时间\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (19, 19, 1, 'remark', '备注', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入备注\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_role_table_form_item` VALUES (20, 20, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_table_pager
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_table_pager`;
CREATE TABLE `sys_role_table_pager`  (
  `role_pager_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色分页配置id',
  `pager_id` int(11) NOT NULL COMMENT '分页配置id',
  `role_table_id` int(11) NOT NULL COMMENT '表格id',
  `layouts` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total' COMMENT '自定义布局',
  `current_page` int(11) NULL DEFAULT NULL COMMENT '当前页',
  `page_size` int(11) NULL DEFAULT NULL COMMENT '每页大小',
  `pager_count` int(11) NULL DEFAULT NULL COMMENT '显示页码按钮的数量',
  `page_sizes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '每页大小选项列表',
  `align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'right' COMMENT '对齐方式',
  `border` tinyint(1) NULL DEFAULT NULL COMMENT '带边框',
  `background` tinyint(1) NULL DEFAULT NULL COMMENT '带背景颜色',
  `perfect` tinyint(1) NULL DEFAULT NULL COMMENT '配套的样式',
  `class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给分页附加 className',
  `auto_hidden` tinyint(1) NULL DEFAULT NULL COMMENT '当只有一页时自动隐藏',
  `icon_prev_page` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义上一页图标',
  `icon_jump_prev` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义向上跳页图标',
  `icon_jump_next` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义向下跳页图标',
  `iconnext_page` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义下一页图标',
  `icon_jump_more` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义跳页显示图标',
  `enabled` tinyint(1) NULL DEFAULT NULL COMMENT '是否启用',
  `slots` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插槽',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_pager_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色功能表分页配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_table_pager
-- ----------------------------
INSERT INTO `sys_role_table_pager` VALUES (1, 1, 1, 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total', NULL, 10, NULL, '5,10,15,20,50,100,200,500,1000', 'right', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2023-07-26 21:51:36', 1, '2023-07-27 20:31:12', NULL);

-- ----------------------------
-- Table structure for sys_role_table_proxy
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_table_proxy`;
CREATE TABLE `sys_role_table_proxy`  (
  `role_proxy_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色代理表id',
  `proxy_id` int(11) NOT NULL COMMENT '代理表id',
  `role_table_id` int(11) NOT NULL COMMENT '表格id',
  `enabled` tinyint(1) NULL DEFAULT NULL COMMENT '是否启用',
  `auto_load` tinyint(1) NULL DEFAULT NULL COMMENT '是否自动加载查询数据',
  `message` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示内置的消息提示（可以设为 false 关闭内置的消息提示）',
  `seq` tinyint(1) NULL DEFAULT 1 COMMENT '存在 type=index 列时有效是否代理动态序号（根据分页动态变化）',
  `sort` tinyint(1) NULL DEFAULT 1 COMMENT '是否代理排序',
  `filter` tinyint(1) NULL DEFAULT 1 COMMENT '是否代理筛选',
  `form` tinyint(1) NULL DEFAULT 1 COMMENT '是否代理表单',
  `props` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '获取的属性配置',
  `querybase` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询配置',
  `query_all` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '全量查询配置',
  `deletebase` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除配置',
  `insertbase` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插入配置',
  `updatebase` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新配置',
  `save` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存',
  `params_change_load` tinyint(1) NULL DEFAULT 0 COMMENT '额外的请求参数改变时是否重新请求数据',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_proxy_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色功能表数据代理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_table_proxy
-- ----------------------------
INSERT INTO `sys_role_table_proxy` VALUES (1, 1, 1, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"method\":\"post\",\"api\":\"/system/user/page\"}', '{}', '{}', '{}', '{}', '{}', 1, 1, '2023-07-26 21:51:36', 1, '2023-07-27 20:31:12', NULL);

-- ----------------------------
-- Table structure for sys_role_table_toolbar_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_table_toolbar_config`;
CREATE TABLE `sys_role_table_toolbar_config`  (
  `role_toolbar_config_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色工具栏配置id',
  `toolbar_config_id` int(11) NOT NULL COMMENT '工具栏配置id',
  `role_table_id` int(11) NOT NULL COMMENT '表格id',
  `ext_size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '尺寸(medium, small, mini)',
  `loading` tinyint(1) NULL DEFAULT 0 COMMENT '是否加载中',
  `perfect` tinyint(1) NULL DEFAULT 0 COMMENT '配套的样式',
  `class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给工具栏 className',
  `ext_import` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '导入按钮配置（需要设置 \"import-config\"）',
  `ext_export` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '导出按钮配置（需要设置 \"export-config\"）',
  `print` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印按钮配置',
  `refresh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '刷新按钮配置',
  `custom` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义列配置',
  `buttons` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '左侧按钮列表',
  `tools` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '右侧工具列表',
  `enabled` tinyint(1) NULL DEFAULT 1 COMMENT '是否启用',
  `print_design` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义的打印配置',
  `print_data_query_port` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印时查询数据接口',
  `print_data_query_before` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询打印数据前处理函数',
  `print_data_query_after` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询打印数据后处理函数',
  `zoom` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否允许最大化显示',
  `slots` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插槽',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_toolbar_config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色工具栏配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_table_toolbar_config
-- ----------------------------
INSERT INTO `sys_role_table_toolbar_config` VALUES (1, 1, 1, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"directPrintConfig\":{\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"size\":\"medium\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{},\"directPrint\":true,\"setPrintTemplate\":true,\"previewPrintConfig\":{\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{}', 1, '2023-07-26 21:51:36', 1, '2023-07-27 20:31:12', NULL);

-- ----------------------------
-- Table structure for sys_table
-- ----------------------------
DROP TABLE IF EXISTS `sys_table`;
CREATE TABLE `sys_table`  (
  `table_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表id',
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表名称',
  `table_comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表描述',
  `menu_id` int(11) NULL DEFAULT NULL COMMENT '功能id',
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '唯一标识',
  `height` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格的高度；支持铺满父容器或者固定高度，如果设置 auto 为铺满父容器（如果设置为 auto，则必须确保存在父节点且不允许存在相邻元素）',
  `unique_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格主键字段',
  `row_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格行id字段',
  `max_height` int(4) NULL DEFAULT NULL COMMENT '表格的最大高度',
  `auto_resize` tinyint(1) NULL DEFAULT NULL COMMENT '自动监听父元素的变化去重新计算表格（对于父元素可能存在动态变化、显示隐藏的容器中、列宽异常等场景中的可能会用到）',
  `sync_resize` tinyint(1) NULL DEFAULT NULL COMMENT '自动跟随某个属性的变化去重新计算表格，和手动调用 recalculate 方法是一样的效果（对于通过某个属性来控制显示/隐藏切换时可能会用到）',
  `stripe` tinyint(1) NULL DEFAULT NULL COMMENT '是否带有斑马纹（需要注意的是，在可编辑表格场景下，临时插入的数据不会有斑马纹样式）',
  `border` tinyint(1) NULL DEFAULT NULL COMMENT '是否带有边框',
  `round` tinyint(1) NULL DEFAULT NULL COMMENT '是否为圆角边框',
  `size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格的尺寸',
  `align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有的列对齐方式',
  `header_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有的表头列的对齐方式',
  `footer_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有的表尾列的对齐方式',
  `show_header` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示表头',
  `row_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给行附加 className',
  `cell_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给单元格附加 className',
  `header_row_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表头的行附加 className',
  `header_cell_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表头的单元格附加 className',
  `footer_row_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表尾的行附加 className',
  `footer_cell_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表尾的单元格附加 className',
  `show_footer` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示表尾',
  `footer_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表尾的数据获取方法，返回一个二维数组',
  `show_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设置所有内容过长时显示为省略号（如果是固定列建议设置该值，提升渲染速度）',
  `show_header_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设置表头所有内容过长时显示为省略号',
  `show_footer_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设置表尾所有内容过长时显示为省略号',
  `keep_source` tinyint(1) NULL DEFAULT NULL COMMENT '保持原始值的状态，被某些功能所依赖，比如编辑状态、还原数据等（开启后影响性能，具体取决于数据量）',
  `empty_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '空数据时显示的内容',
  `ext_params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义参数（可以用来存放一些自定义的数据）',
  `extra_config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '额外配置（json）',
  `hand_config` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作配置',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '功能表格' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_table
-- ----------------------------
INSERT INTO `sys_table` VALUES (1, 'sys_user', '用户信息表', 100, NULL, 'auto', NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"save\":false,\"add\":false,\"modify\":false,\"delete\":false}', 1, '2023-07-26 21:32:50', NULL, NULL, NULL);
INSERT INTO `sys_table` VALUES (2, 'sys_dept', '部门表', 103, NULL, 'auto', NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"treeConfig\":{\"transform\":false,\"children\":\"children\",\"rowField\":\"deptId\",\"parentField\":\"parentId\",\"expandAll\":true}}', NULL, '{\"save\":false,\"add\":false,\"modify\":false,\"delete\":false}', 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table` VALUES (3, 'sys_post', '岗位信息表', 104, NULL, 'auto', NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"save\":false,\"add\":false,\"modify\":false,\"delete\":false}', 1, '2023-07-27 21:14:18', NULL, NULL, NULL);
INSERT INTO `sys_table` VALUES (4, 'sys_dict_data', '字典数据表', 105, NULL, 'auto', NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"save\":false,\"add\":false,\"modify\":false,\"delete\":false}', 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table` VALUES (5, 'sys_dict_type', '字典类型表', 105, NULL, 'auto', NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"save\":false,\"add\":false,\"modify\":false,\"delete\":false}', 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table` VALUES (6, 'sys_config', '参数配置表', 106, NULL, 'auto', NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"save\":false,\"add\":false,\"modify\":false,\"delete\":false}', 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table` VALUES (7, 'sys_notice', '通知公告表', 107, NULL, 'auto', NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"save\":false,\"add\":false,\"modify\":false,\"delete\":false}', 1, '2023-07-27 21:45:51', NULL, NULL, NULL);
INSERT INTO `sys_table` VALUES (8, 'sys_oper_log', '操作日志记录', 500, NULL, 'auto', NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"save\":false,\"add\":false,\"modify\":false,\"delete\":false}', 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:45:50', NULL);
INSERT INTO `sys_table` VALUES (9, 'sys_logininfor', '系统访问记录', 501, NULL, 'auto', NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"save\":false,\"add\":false,\"modify\":false,\"delete\":false}', 1, '2023-07-27 22:00:03', NULL, '2023-07-27 20:21:13', NULL);
INSERT INTO `sys_table` VALUES (10, 'sys_print_panel', '打印模板', 2010, NULL, 'auto', NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"save\":false,\"add\":false,\"modify\":false,\"delete\":false}', 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table` VALUES (11, 'sys_print_provider', '打印模板面板', 2010, NULL, 'auto', NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"save\":false,\"add\":false,\"modify\":false,\"delete\":false}', 1, '2023-07-27 22:06:58', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_table_column
-- ----------------------------
DROP TABLE IF EXISTS `sys_table_column`;
CREATE TABLE `sys_table_column`  (
  `column_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` int(11) NOT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段',
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `is_ignore` tinyint(1) NULL DEFAULT 1 COMMENT '是否显示（1是 0否）',
  `visible` tinyint(1) NULL DEFAULT 1 COMMENT '是否可视（1是 0否）',
  `width` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '宽度',
  `min_width` double NULL DEFAULT NULL COMMENT '最小列宽度会自动将剩余空间按比例分配',
  `resizable` tinyint(1) NULL DEFAULT 1 COMMENT '列宽是否可拖拽（1是 0否）',
  `align` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'left' COMMENT '列对齐方式：left（左对齐）, center（居中对齐）, right（右对齐）',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列的类型',
  `fixed` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '将列固定在左侧或者右侧（注意：固定列应该放在左右两侧的位置）',
  `header_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表头列的对齐方式',
  `footer_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表尾列的对齐方式',
  `show_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当内容过长时显示为省略号',
  `show_header_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当表头内容过长时显示为省略号',
  `show_footer_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当表尾内容过长时显示为省略号',
  `class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给单元格附加 className',
  `header_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表头的单元格附加 className',
  `footer_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表尾的单元格附加 className',
  `formatter` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '格式化显示内容',
  `sortable` tinyint(1) NULL DEFAULT 1 COMMENT '是否允许列排序',
  `sort_by` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对 sortable 有效，指定排序的字段（当值 formatter 格式化后，可以设置该字段，使用值进行排序）',
  `sort_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排序的字段类型，比如字符串转数值等',
  `ext_params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '额外的参数（可以用来存放一些私有参数）',
  `tree_node` tinyint(1) NULL DEFAULT NULL COMMENT '只对 tree-config 配置时有效，指定为树节点',
  `slots` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插槽',
  `filters` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置筛选条件',
  `filter_multiple` tinyint(1) NULL DEFAULT 1 COMMENT '只对 filters 有效，筛选是否允许多选',
  `filter_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对 filters 有效，列的筛选方法，该方法的返回值用来决定该行是否显示',
  `filter_reset_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对 filters 有效，自定义筛选重置方法',
  `filter_recover_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对 filters 有效，自定义筛选复原方法（使用自定义筛选时可能会用到）',
  `filter_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '筛选渲染器配置项',
  `export_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义单元格数据导出方法，返回自定义的值',
  `footer_export_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义表尾单元格数据导出方法，返回自定义的值',
  `title_prefix` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题前缀图标配置项',
  `cell_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对特定功能有效，单元格值类型',
  `cell_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认的渲染器配置项',
  `edit_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '可编辑渲染器配置项',
  `content_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容渲染配置项',
  `col_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义列的唯一主键',
  `sort_no` int(11) NULL DEFAULT NULL COMMENT '排序',
  `dict_type` text CHARACTER SET ujis COLLATE ujis_japanese_ci NULL COMMENT '字典类型',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 158 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '功能表格列配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_table_column
-- ----------------------------
INSERT INTO `sys_table_column` VALUES (1, 1, 'user_id', '用户ID', 'userId', '用户ID', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (2, 1, 'dept_id', '部门ID', 'deptId', '部门ID', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (3, 1, 'user_name', '用户账号', 'userName', '用户账号', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (4, 1, 'nick_name', '用户昵称', 'nickName', '用户昵称', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (5, 1, 'user_type', '用户类型（00系统用户）', 'userType', '用户类型', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (6, 1, 'email', '用户邮箱', 'email', '用户邮箱', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (7, 1, 'phonenumber', '手机号码', 'phonenumber', '手机号码', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (8, 1, 'sex', '用户性别（0男 1女 2未知）', 'sex', '用户性别', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_user_sex\"\r\n  }\r\n}', NULL, NULL, NULL, 8, '[\"sys_user_sex\"]', NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (9, 1, 'avatar', '头像地址', 'avatar', '头像地址', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (10, 1, 'password', '密码', 'password', '密码', 0, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-26 21:43:16');
INSERT INTO `sys_table_column` VALUES (11, 1, 'status', '帐号状态（0正常 1停用）', 'status', '帐号状态', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\":\"ElDictTag\",\"props\":{\"options\":\"sys_normal_disable\"}}', NULL, NULL, NULL, 11, '[\"sys_normal_disable\"]', NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (12, 1, 'del_flag', '删除标志（0代表存在 2代表删除）', 'delFlag', '删除标志（0代表存在 2代表删除）', 0, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-26 21:43:16');
INSERT INTO `sys_table_column` VALUES (13, 1, 'login_ip', '最后登录IP', 'loginIp', '最后登录IP', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (14, 1, 'login_date', '最后登录时间', 'loginDate', '最后登录时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (15, 1, 'create_by', '创建者', 'createBy', '创建者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (16, 1, 'create_time', '创建时间', 'createTime', '创建时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (17, 1, 'update_by', '更新者', 'updateBy', '更新者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (18, 1, 'update_time', '更新时间', 'updateTime', '更新时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (19, 1, 'remark', '备注', 'remark', '备注', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19, NULL, NULL, 1, '2023-07-26 21:32:50', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (20, 1, NULL, NULL, 'createByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_table_column` VALUES (21, 1, NULL, NULL, 'updateByName', '修改人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (22, 2, 'dept_id', '部门id', 'deptId', '部门id', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (23, 2, 'parent_id', '父部门id', 'parentId', '父部门id', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (24, 2, 'ancestors', '祖级列表', 'ancestors', '祖级列表', 0, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:07:45');
INSERT INTO `sys_table_column` VALUES (25, 2, 'dept_name', '部门名称', 'deptName', '部门名称', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (26, 2, 'order_num', '显示顺序', 'orderNum', '显示顺序', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (27, 2, 'leader', '负责人', 'leader', '负责人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (28, 2, 'phone', '联系电话', 'phone', '联系电话', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (29, 2, 'email', '邮箱', 'email', '邮箱', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (30, 2, 'status', '部门状态（0正常 1停用）', 'status', '部门状态（0正常 1停用）', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_normal_disable\"\r\n  }\r\n}', NULL, NULL, NULL, 9, '[]', NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (31, 2, 'del_flag', '删除标志（0代表存在 2代表删除）', 'delFlag', '删除标志（0代表存在 2代表删除）', 0, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:07:45');
INSERT INTO `sys_table_column` VALUES (32, 2, 'create_by', '创建者', 'createBy', '创建者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (33, 2, 'create_time', '创建时间', 'createTime', '创建时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (34, 2, 'update_by', '更新者', 'updateBy', '更新者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (35, 2, 'update_time', '更新时间', 'updateTime', '更新时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14, NULL, NULL, 1, '2023-07-27 20:50:13', NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (36, 2, NULL, NULL, 'updateByName', '更新人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (37, 2, NULL, NULL, 'createByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-07-27 21:13:31');
INSERT INTO `sys_table_column` VALUES (38, 3, 'post_id', '岗位ID', 'postId', '岗位ID', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-27 21:14:18', NULL, '2023-07-27 21:23:21');
INSERT INTO `sys_table_column` VALUES (39, 3, 'post_code', '岗位编码', 'postCode', '岗位编码', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-27 21:14:18', NULL, '2023-07-27 21:23:21');
INSERT INTO `sys_table_column` VALUES (40, 3, 'post_name', '岗位名称', 'postName', '岗位名称', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 1, '2023-07-27 21:14:18', NULL, '2023-07-27 21:23:21');
INSERT INTO `sys_table_column` VALUES (41, 3, 'post_sort', '显示顺序', 'postSort', '显示顺序', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-27 21:14:18', NULL, '2023-07-27 21:23:21');
INSERT INTO `sys_table_column` VALUES (42, 3, 'status', '状态（0正常 1停用）', 'status', '状态（0正常 1停用）', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_normal_disable\"\r\n  }\r\n}', '', NULL, NULL, 5, '[\"sys_normal_disable\"]', NULL, 1, '2023-07-27 21:14:18', NULL, '2023-07-27 21:23:21');
INSERT INTO `sys_table_column` VALUES (43, 3, 'create_by', '创建者', 'createBy', '创建者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-27 21:14:18', NULL, '2023-07-27 21:23:21');
INSERT INTO `sys_table_column` VALUES (44, 3, 'create_time', '创建时间', 'createTime', '创建时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-27 21:14:18', NULL, '2023-07-27 21:23:21');
INSERT INTO `sys_table_column` VALUES (45, 3, 'update_by', '更新者', 'updateBy', '更新者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, 1, '2023-07-27 21:14:18', NULL, '2023-07-27 21:23:21');
INSERT INTO `sys_table_column` VALUES (46, 3, 'update_time', '更新时间', 'updateTime', '更新时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, 1, '2023-07-27 21:14:18', NULL, '2023-07-27 21:23:21');
INSERT INTO `sys_table_column` VALUES (47, 3, 'remark', '备注', 'remark', '备注', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, 1, '2023-07-27 21:14:18', NULL, '2023-07-27 21:23:21');
INSERT INTO `sys_table_column` VALUES (48, 3, NULL, NULL, 'updateByName', '更新人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (49, 3, NULL, NULL, 'createByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (50, 4, 'dict_code', '字典编码', 'dictCode', '字典编码', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (51, 4, 'dict_sort', '字典排序', 'dictSort', '字典排序', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (52, 4, 'dict_label', '字典标签', 'dictLabel', '字典标签', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (53, 4, 'dict_value', '字典键值', 'dictValue', '字典键值', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (54, 4, 'dict_type', '字典类型', 'dictType', '字典类型', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (55, 4, 'css_class', '样式属性（其他样式扩展）', 'cssClass', '样式属性', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (56, 4, 'list_class', '表格回显样式', 'listClass', '表格回显样式', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (57, 4, 'is_default', '是否默认（Y是 N否）', 'isDefault', '是否默认', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (58, 4, 'status', '状态（0正常 1停用）', 'status', '状态', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_normal_disable\"\r\n  }\r\n}', NULL, NULL, NULL, 9, '[\"sys_normal_disable\"]', NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (59, 4, 'create_by', '创建者', 'createBy', '创建者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (60, 4, 'create_time', '创建时间', 'createTime', '创建时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (61, 4, 'update_by', '更新者', 'updateBy', '更新者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (62, 4, 'update_time', '更新时间', 'updateTime', '更新时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (63, 4, 'remark', '备注', 'remark', '备注', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:37:48');
INSERT INTO `sys_table_column` VALUES (64, 5, 'dict_id', '字典主键', 'dictId', '字典ID', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-27 21:24:03', NULL, '2023-07-27 21:31:06');
INSERT INTO `sys_table_column` VALUES (65, 5, 'dict_name', '字典名称', 'dictName', '字典名称', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-27 21:24:04', NULL, '2023-07-27 21:31:06');
INSERT INTO `sys_table_column` VALUES (66, 5, 'dict_type', '字典类型', 'dictType', '字典类型', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 1, '2023-07-27 21:24:04', NULL, '2023-07-27 21:31:06');
INSERT INTO `sys_table_column` VALUES (67, 5, 'dict_describe', '描述', 'dictDescribe', '描述', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-27 21:24:04', NULL, '2023-07-27 21:31:06');
INSERT INTO `sys_table_column` VALUES (68, 5, 'status', '状态（0正常 1停用）', 'status', '状态', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_normal_disable\"\r\n  }\r\n}', NULL, NULL, NULL, 5, '[\"sys_normal_disable\"]', NULL, 1, '2023-07-27 21:24:04', NULL, '2023-07-27 21:31:06');
INSERT INTO `sys_table_column` VALUES (69, 5, 'create_by', '创建者', 'createBy', '创建者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-27 21:24:04', NULL, '2023-07-27 21:31:06');
INSERT INTO `sys_table_column` VALUES (70, 5, 'create_time', '创建时间', 'createTime', '创建时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-27 21:24:04', NULL, '2023-07-27 21:31:06');
INSERT INTO `sys_table_column` VALUES (71, 5, 'update_by', '更新者', 'updateBy', '更新者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, 1, '2023-07-27 21:24:04', NULL, '2023-07-27 21:31:06');
INSERT INTO `sys_table_column` VALUES (72, 5, 'update_time', '更新时间', 'updateTime', '更新时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, 1, '2023-07-27 21:24:04', NULL, '2023-07-27 21:31:06');
INSERT INTO `sys_table_column` VALUES (73, 5, 'remark', '备注', 'remark', '备注', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, 1, '2023-07-27 21:24:04', NULL, '2023-07-27 21:31:06');
INSERT INTO `sys_table_column` VALUES (74, 5, NULL, NULL, 'updateByName', '更新人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (75, 5, NULL, NULL, 'createByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (76, 4, NULL, NULL, 'updateByName', '更新人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (77, 4, NULL, NULL, 'createByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (78, 6, 'config_id', '参数主键', 'configId', '参数主键', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-27 21:38:18', NULL, '2023-07-27 21:43:53');
INSERT INTO `sys_table_column` VALUES (79, 6, 'config_name', '参数名称', 'configName', '参数名称', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-27 21:38:18', NULL, '2023-07-27 21:43:53');
INSERT INTO `sys_table_column` VALUES (80, 6, 'config_key', '参数键名', 'configKey', '参数键名', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 1, '2023-07-27 21:38:18', NULL, '2023-07-27 21:43:53');
INSERT INTO `sys_table_column` VALUES (81, 6, 'config_value', '参数键值', 'configValue', '参数键值', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-27 21:38:18', NULL, '2023-07-27 21:43:53');
INSERT INTO `sys_table_column` VALUES (82, 6, 'config_param', '参数', 'configParam', '参数', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, '2023-07-27 21:38:18', NULL, '2023-07-27 21:43:53');
INSERT INTO `sys_table_column` VALUES (83, 6, 'config_type', '系统内置（Y是 N否）', 'configType', '系统内置', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_yes_no\"\r\n  }\r\n}', NULL, NULL, NULL, 6, '[\"sys_yes_no\"]', NULL, 1, '2023-07-27 21:38:18', NULL, '2023-07-27 21:43:53');
INSERT INTO `sys_table_column` VALUES (84, 6, 'create_by', '创建者', 'createBy', '创建者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-27 21:38:18', NULL, '2023-07-27 21:43:53');
INSERT INTO `sys_table_column` VALUES (85, 6, 'create_time', '创建时间', 'createTime', '创建时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, 1, '2023-07-27 21:38:18', NULL, '2023-07-27 21:43:53');
INSERT INTO `sys_table_column` VALUES (86, 6, 'update_by', '更新者', 'updateBy', '更新者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, 1, '2023-07-27 21:38:18', NULL, '2023-07-27 21:43:53');
INSERT INTO `sys_table_column` VALUES (87, 6, 'update_time', '更新时间', 'updateTime', '更新时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, 1, '2023-07-27 21:38:18', NULL, '2023-07-27 21:43:53');
INSERT INTO `sys_table_column` VALUES (88, 6, 'remark', '备注', 'remark', '备注', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, 1, '2023-07-27 21:38:18', NULL, '2023-07-27 21:43:53');
INSERT INTO `sys_table_column` VALUES (89, 6, NULL, NULL, 'updateByName', '更新人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (90, 6, NULL, NULL, 'createByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (91, 7, 'notice_id', '公告ID', 'noticeId', '公告ID', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-27 21:45:51', NULL, '2023-07-27 21:52:14');
INSERT INTO `sys_table_column` VALUES (92, 7, 'notice_title', '公告标题', 'noticeTitle', '公告标题', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-27 21:45:51', NULL, '2023-07-27 21:52:14');
INSERT INTO `sys_table_column` VALUES (93, 7, 'notice_type', '公告类型（1通知 2公告）', 'noticeType', '公告类型', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_notice_type\"\r\n  }\r\n}', NULL, NULL, NULL, 3, '[\"sys_notice_type\"]', NULL, 1, '2023-07-27 21:45:51', NULL, '2023-07-27 21:52:14');
INSERT INTO `sys_table_column` VALUES (94, 7, 'notice_content', '公告内容', 'noticeContent', '公告内容', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-27 21:45:51', NULL, '2023-07-27 21:52:14');
INSERT INTO `sys_table_column` VALUES (95, 7, 'status', '公告状态（0正常 1关闭）', 'status', '公告状态', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_notice_status\"\r\n  }\r\n}', NULL, NULL, NULL, 5, '[\"sys_notice_status\"]', NULL, 1, '2023-07-27 21:45:51', NULL, '2023-07-27 21:52:14');
INSERT INTO `sys_table_column` VALUES (96, 7, 'create_by', '创建者', 'createBy', '创建者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-27 21:45:51', NULL, '2023-07-27 21:52:14');
INSERT INTO `sys_table_column` VALUES (97, 7, 'create_time', '创建时间', 'createTime', '创建时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-27 21:45:51', NULL, '2023-07-27 21:52:14');
INSERT INTO `sys_table_column` VALUES (98, 7, 'update_by', '更新者', 'updateBy', '更新者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, 1, '2023-07-27 21:45:51', NULL, '2023-07-27 21:52:14');
INSERT INTO `sys_table_column` VALUES (99, 7, 'update_time', '更新时间', 'updateTime', '更新时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, 1, '2023-07-27 21:45:51', NULL, '2023-07-27 21:52:14');
INSERT INTO `sys_table_column` VALUES (100, 7, 'remark', '备注', 'remark', '备注', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, 1, '2023-07-27 21:45:51', NULL, '2023-07-27 21:52:14');
INSERT INTO `sys_table_column` VALUES (101, 7, NULL, NULL, 'updateByName', '更新人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (102, 7, NULL, NULL, 'createByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (103, 7, NULL, NULL, 'updateByName', '更新人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (104, 7, NULL, NULL, 'createByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (105, 8, 'oper_id', '日志主键', 'operId', '日志主键', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (106, 8, 'title', '模块标题', 'title', '模块标题', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (107, 8, 'business_type', '业务类型（0其它 1新增 2修改 3删除）', 'businessType', '业务类型', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_oper_type\"\r\n  }\r\n}', NULL, NULL, NULL, 3, '[\"sys_oper_type\"]', NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (108, 8, 'method', '方法名称', 'method', '方法名称', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (109, 8, 'request_method', '请求方式', 'requestMethod', '请求方式', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (110, 8, 'operator_type', '操作类别（0其它 1后台用户 2手机端用户）', 'operatorType', '操作类别', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (111, 8, 'oper_name', '操作人员', 'operName', '操作人员', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (112, 8, 'dept_name', '部门名称', 'deptName', '部门名称', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (113, 8, 'oper_url', '请求URL', 'operUrl', '请求URL', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (114, 8, 'oper_ip', '主机地址', 'operIp', '主机地址', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (115, 8, 'oper_location', '操作地点', 'operLocation', '操作地点', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (116, 8, 'oper_param', '请求参数', 'operParam', '请求参数', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (117, 8, 'json_result', '返回参数', 'jsonResult', '返回参数', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (118, 8, 'status', '操作状态（0正常 1异常）', 'status', '操作状态', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_common_status\"\r\n  }\r\n}', NULL, NULL, NULL, 14, '[\"sys_common_status\"]', NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (119, 8, 'error_msg', '错误消息', 'errorMsg', '错误消息', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (120, 8, 'oper_time', '操作时间', 'operTime', '操作时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (121, 8, 'cost_time', '消耗时间', 'costTime', '消耗时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17, NULL, NULL, 1, '2023-07-27 21:52:43', NULL, '2023-07-27 21:59:46');
INSERT INTO `sys_table_column` VALUES (122, 9, 'info_id', '访问ID', 'infoId', '访问ID', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-27 22:00:03', NULL, '2023-07-27 22:06:19');
INSERT INTO `sys_table_column` VALUES (123, 9, 'user_name', '用户账号', 'userName', '用户账号', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-27 22:00:03', NULL, '2023-07-27 22:06:19');
INSERT INTO `sys_table_column` VALUES (124, 9, 'ipaddr', '登录IP地址', 'ipaddr', '登录IP地址', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 1, '2023-07-27 22:00:03', NULL, '2023-07-27 22:06:19');
INSERT INTO `sys_table_column` VALUES (125, 9, 'login_location', '登录地点', 'loginLocation', '登录地点', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-27 22:00:03', NULL, '2023-07-27 22:06:19');
INSERT INTO `sys_table_column` VALUES (126, 9, 'browser', '浏览器类型', 'browser', '浏览器类型', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, '2023-07-27 22:00:03', NULL, '2023-07-27 22:06:19');
INSERT INTO `sys_table_column` VALUES (127, 9, 'os', '操作系统', 'os', '操作系统', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-27 22:00:03', NULL, '2023-07-27 22:06:19');
INSERT INTO `sys_table_column` VALUES (128, 9, 'status', '登录状态（0成功 1失败）', 'status', '登录状态', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_common_status\"\r\n  }\r\n}', NULL, NULL, NULL, 7, '[\"sys_common_status\"]', NULL, 1, '2023-07-27 22:00:03', NULL, '2023-07-27 22:06:19');
INSERT INTO `sys_table_column` VALUES (129, 9, 'msg', '提示信息', 'msg', '提示信息', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, 1, '2023-07-27 22:00:03', NULL, '2023-07-27 22:06:19');
INSERT INTO `sys_table_column` VALUES (130, 9, 'access_time', '访问时间', 'accessTime', '访问时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, 1, '2023-07-27 22:00:03', NULL, '2023-07-27 22:06:19');
INSERT INTO `sys_table_column` VALUES (131, 10, 'panel_id', '模板id', 'panelId', '模板id', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (132, 10, 'provider_id', '绑定的面板id', 'providerId', '绑定的面板id', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (133, 10, 'panel_paper_width', '纸张宽度', 'panelPaperWidth', '纸张宽度', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (134, 10, 'panel_paper_height', '纸张高度', 'panelPaperHeight', '纸张高度', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (135, 10, 'panel_title', '模板名称', 'panelTitle', '模板名称', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (136, 10, 'panel_data', '模板数据（json）', 'panelData', '模板数据（json）', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (137, 10, 'panel_paper_type', '纸张类型', 'panelPaperType', '纸张类型', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (138, 10, 'rotate', '纸张是否旋转', 'rotate', '纸张是否旋转', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (139, 10, 'create_by', '创建者', 'createBy', '创建者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (140, 10, 'create_time', '创建时间', 'createTime', '创建时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (141, 10, 'update_by', '更新者', 'updateBy', '更新者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (142, 10, 'update_time', '更新时间', 'updateTime', '更新时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (143, 10, 'remark', '备注', 'remark', '备注', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:10:29');
INSERT INTO `sys_table_column` VALUES (144, 11, 'provider_id', '面板id', 'providerId', '面板id', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:12:03');
INSERT INTO `sys_table_column` VALUES (145, 11, 'provider_title', '表名称', 'providerTitle', '表名称', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:12:03');
INSERT INTO `sys_table_column` VALUES (146, 11, 'provider_data', '面板数据', 'providerData', '面板数据', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:12:03');
INSERT INTO `sys_table_column` VALUES (147, 11, 'create_by', '创建者', 'createBy', '创建者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:12:03');
INSERT INTO `sys_table_column` VALUES (148, 11, 'create_time', '创建时间', 'createTime', '创建时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:12:03');
INSERT INTO `sys_table_column` VALUES (149, 11, 'update_by', '更新者', 'updateBy', '更新者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:12:03');
INSERT INTO `sys_table_column` VALUES (150, 11, 'update_time', '更新时间', 'updateTime', '更新时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:12:03');
INSERT INTO `sys_table_column` VALUES (151, 11, 'remark', '备注', 'remark', '备注', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, 1, '2023-07-27 22:06:58', NULL, '2023-07-27 22:12:03');
INSERT INTO `sys_table_column` VALUES (152, 10, NULL, NULL, 'updateByName', '更新人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (153, 10, NULL, NULL, 'createByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (154, 10, NULL, NULL, 'updateByName', '更新人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (155, 10, NULL, NULL, 'createByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (156, 11, NULL, NULL, 'updateByName', '更新人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_column` VALUES (157, 11, NULL, NULL, 'ctrateByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_table_edit_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_table_edit_config`;
CREATE TABLE `sys_table_edit_config`  (
  `edit_config_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '可编辑配置项id',
  `table_id` int(11) NULL DEFAULT NULL COMMENT '表id',
  `triggerbase` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '触发方式',
  `enabled` tinyint(1) NULL DEFAULT NULL COMMENT '是否启用',
  `mode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编辑模式',
  `show_icon` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示列头编辑图标',
  `show_status` tinyint(1) NULL DEFAULT NULL COMMENT '只对 keep-source 开启有效是否显示单元格新增与修改状态',
  `show_update_status` tinyint(1) NULL DEFAULT NULL COMMENT '只对 keep-source 开启有效是否显示单元格修改状态',
  `show_insert_status` tinyint(1) NULL DEFAULT NULL COMMENT '只对 keep-source 开启有效是否显示单元格新增状态',
  `show_asterisk` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示必填字段的红色星号',
  `auto_clear` tinyint(1) NULL DEFAULT NULL COMMENT '当点击非编辑列之后是否自动清除单元格的激活状态',
  `before_edit_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '该方法的返回值用来决定该单元格是否允许编辑',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义可编辑列的状态图标',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`edit_config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '可编辑配置项' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_table_edit_config
-- ----------------------------
INSERT INTO `sys_table_edit_config` VALUES (1, 1, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');
INSERT INTO `sys_table_edit_config` VALUES (2, 2, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');
INSERT INTO `sys_table_edit_config` VALUES (3, 3, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');
INSERT INTO `sys_table_edit_config` VALUES (4, 5, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');
INSERT INTO `sys_table_edit_config` VALUES (5, 4, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');
INSERT INTO `sys_table_edit_config` VALUES (6, 6, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');
INSERT INTO `sys_table_edit_config` VALUES (7, 7, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');
INSERT INTO `sys_table_edit_config` VALUES (8, 7, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');
INSERT INTO `sys_table_edit_config` VALUES (9, 8, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');
INSERT INTO `sys_table_edit_config` VALUES (10, 9, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');
INSERT INTO `sys_table_edit_config` VALUES (11, 10, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');
INSERT INTO `sys_table_edit_config` VALUES (12, 10, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');
INSERT INTO `sys_table_edit_config` VALUES (13, 11, '', 1, 'cell', 1, 0, 0, 0, 1, 1, '', '', NULL, NULL, NULL, NULL, '');

-- ----------------------------
-- Table structure for sys_table_edit_rules
-- ----------------------------
DROP TABLE IF EXISTS `sys_table_edit_rules`;
CREATE TABLE `sys_table_edit_rules`  (
  `edit_rules_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '校验规则配置id',
  `table_id` int(11) NULL DEFAULT NULL COMMENT '表id',
  `required` tinyint(1) NULL DEFAULT NULL COMMENT '是否必填',
  `min` int(11) NULL DEFAULT NULL COMMENT '校验值最小长度',
  `max` int(11) NULL DEFAULT NULL COMMENT '校验值最大长度',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据校验的类型',
  `pattern` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '正则校验',
  `validator` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义校验方法',
  `message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '校验提示内容',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`edit_rules_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '校验规则配置项' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_table_edit_rules
-- ----------------------------

-- ----------------------------
-- Table structure for sys_table_form
-- ----------------------------
DROP TABLE IF EXISTS `sys_table_form`;
CREATE TABLE `sys_table_form`  (
  `form_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '查询表id',
  `table_id` int(11) NULL DEFAULT NULL COMMENT '表格id',
  `span` int(3) NULL DEFAULT NULL COMMENT '所有项的栅格占据的列数（共 24 分栏）',
  `align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有项的内容对齐方式',
  `size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '尺寸',
  `title_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有项的标题对齐方式',
  `title_width` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有项的标题宽度',
  `title_colon` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示标题冒号',
  `title_asterisk` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示必填字段的红色星号',
  `title_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所有设置标题内容过长时显示为省略号',
  `class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表单附加 className',
  `collapse_status` tinyint(1) NULL DEFAULT NULL COMMENT 'v-model 绑定值，折叠状态',
  `custom_layout` tinyint(1) NULL DEFAULT NULL COMMENT '是否使用自定义布局',
  `prevent_submit` tinyint(1) NULL DEFAULT NULL COMMENT '是否禁用默认的回车提交方式禁用后配合 validate() 方法可以更加自由的控制提交逻辑',
  `enabled` tinyint(1) NULL DEFAULT NULL COMMENT '是否启用',
  `rules` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '校验规则配置项',
  `valid_config` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '检验配置项',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`form_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '功能表查询表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_table_form
-- ----------------------------
INSERT INTO `sys_table_form` VALUES (1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_form` VALUES (2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_form` VALUES (3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_form` VALUES (4, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_form` VALUES (5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_form` VALUES (6, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_form` VALUES (7, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_form` VALUES (8, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_form` VALUES (9, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_form` VALUES (10, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_form` VALUES (11, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_table_form_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_table_form_item`;
CREATE TABLE `sys_table_form_item`  (
  `item_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表单项id',
  `form_id` int(11) NULL DEFAULT NULL COMMENT '表单id',
  `field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `span` int(2) NULL DEFAULT NULL COMMENT '栅格占据的列数（共 24 分栏）',
  `align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容对齐方式',
  `title_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题对齐方式',
  `title_width` int(3) NULL DEFAULT NULL COMMENT '标题宽度',
  `title_colon` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示标题冒号',
  `title_asterisk` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示必填字段的红色星号',
  `title_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题内容过长时显示为省略号',
  `show_title` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示标题',
  `class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表单项附加 className',
  `content_class_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表单项内容附加 className',
  `content_style` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表单项内容附加样式',
  `visible` tinyint(1) NULL DEFAULT 1 COMMENT '默认是否显示',
  `visible_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '该方法的返回值用来决定该项是否显示',
  `form_filter` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示删除按钮',
  `folding` tinyint(1) NULL DEFAULT 0 COMMENT '默认收起',
  `collapse_node` tinyint(1) NULL DEFAULT 0 COMMENT '折叠节点',
  `reset_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重置时的默认值',
  `item_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项渲染器配置项',
  `children` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项集合',
  `slots` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插槽',
  `sort_no` int(11) NULL DEFAULT NULL COMMENT '排序',
  `dict_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类型',
  `search_handle` tinyint(1) NULL DEFAULT 0 COMMENT '是否为查询操作项（查询，重置）',
  `search_no` int(11) NULL DEFAULT NULL COMMENT '查询排序',
  `search_visible` tinyint(1) NULL DEFAULT 1 COMMENT '是否是查询项',
  `search_fixed` tinyint(1) NULL DEFAULT NULL COMMENT '是否为固定查询',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`item_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 147 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单配置项列表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_table_form_item
-- ----------------------------
INSERT INTO `sys_table_form_item` VALUES (1, 1, 'userId', '用户ID', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户ID\"}}', NULL, NULL, 1, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (2, 1, 'deptId', '部门ID', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入部门ID\"}}', NULL, NULL, 2, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (3, 1, 'userName', '用户账号', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户账号\"}}', NULL, NULL, 3, NULL, 0, NULL, 1, 1, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (4, 1, 'nickName', '用户昵称', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户昵称\"}}', NULL, NULL, 4, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (5, 1, 'userType', '用户类型', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户类型（00系统用户）\"}}', NULL, NULL, 5, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (6, 1, 'email', '用户邮箱', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户邮箱\"}}', NULL, NULL, 6, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (7, 1, 'phonenumber', '手机号码', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入手机号码\"}}', NULL, NULL, 7, NULL, 0, NULL, 1, 1, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (8, 1, 'sex', '用户性别', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_user_sex\"\r\n}', NULL, NULL, 8, '[\"sys_user_sex\"]', 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (9, 1, 'avatar', '头像地址', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入头像地址\"}}', NULL, NULL, 9, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (10, 1, 'password', '密码', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入密码\"}}', NULL, NULL, 10, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (11, 1, 'status', '帐号状态', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_normal_disable\"\r\n}', NULL, NULL, 11, '[\"sys_normal_disable\"]', 0, NULL, 1, 1, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (12, 1, 'delFlag', '删除标志', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入删除标志（0代表存在 2代表删除）\"}}', NULL, NULL, 12, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (13, 1, 'loginIp', '最后登录IP', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入最后登录IP\"}}', NULL, NULL, 13, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (14, 1, 'loginDate', '最后登录时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入最后登录时间\"}}', NULL, NULL, 14, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (15, 1, 'createBy', '创建者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建者\"}}', NULL, NULL, 15, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (16, 1, 'createTime', '创建时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建时间\"}}', NULL, NULL, 16, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (17, 1, 'updateBy', '更新者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新者\"}}', NULL, NULL, 17, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (18, 1, 'updateTime', '更新时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新时间\"}}', NULL, NULL, 18, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (19, 1, 'remark', '备注', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入备注\"}}', NULL, NULL, 19, NULL, 0, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (20, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, 19, NULL, 1, NULL, 1, NULL, 1, '2023-07-26 21:32:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (21, 2, 'deptId', '部门id', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入部门id\"}}', NULL, NULL, 1, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (22, 2, 'parentId', '父部门id', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入父部门id\"}}', NULL, NULL, 2, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (23, 2, 'ancestors', '祖级列表', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入祖级列表\"}}', NULL, NULL, 3, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (24, 2, 'deptName', '部门名称', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入部门名称\"}}', NULL, NULL, 4, NULL, 0, NULL, 1, 1, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (25, 2, 'orderNum', '显示顺序', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入显示顺序\"}}', NULL, NULL, 5, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (26, 2, 'leader', '负责人', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入负责人\"}}', NULL, NULL, 6, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (27, 2, 'phone', '联系电话', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入联系电话\"}}', NULL, NULL, 7, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (28, 2, 'email', '邮箱', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入邮箱\"}}', NULL, NULL, 8, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (29, 2, 'status', '部门状态（0正常 1停用）', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_normal_disable\"\r\n}', NULL, NULL, 9, '[\"sys_normal_disable\"]', 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (30, 2, 'delFlag', '删除标志（0代表存在 2代表删除）', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入删除标志（0代表存在 2代表删除）\"}}', NULL, NULL, 10, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (31, 2, 'createBy', '创建者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建者\"}}', NULL, NULL, 11, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (32, 2, 'createTime', '创建时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建时间\"}}', NULL, NULL, 12, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (33, 2, 'updateBy', '更新者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新者\"}}', NULL, NULL, 13, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (34, 2, 'updateTime', '更新时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新时间\"}}', NULL, NULL, 14, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (35, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, 14, NULL, 1, NULL, 1, NULL, 1, '2023-07-27 20:50:13', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (36, 3, 'postId', '岗位ID', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入岗位ID\"}}', NULL, NULL, 1, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:14:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (37, 3, 'postCode', '岗位编码', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入岗位编码\"}}', NULL, NULL, 2, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:14:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (38, 3, 'postName', '岗位名称', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入岗位名称\"}}', NULL, NULL, 3, NULL, 0, NULL, 1, 1, 1, '2023-07-27 21:14:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (39, 3, 'postSort', '显示顺序', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入显示顺序\"}}', NULL, NULL, 4, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:14:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (40, 3, 'status', '状态（0正常 1停用）', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_normal_disable\"\r\n}', NULL, NULL, 5, '[\"sys_normal_disable\"]', 0, NULL, 1, NULL, 1, '2023-07-27 21:14:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (41, 3, 'createBy', '创建者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建者\"}}', NULL, NULL, 6, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:14:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (42, 3, 'createTime', '创建时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建时间\"}}', NULL, NULL, 7, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:14:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (43, 3, 'updateBy', '更新者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新者\"}}', NULL, NULL, 8, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:14:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (44, 3, 'updateTime', '更新时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新时间\"}}', NULL, NULL, 9, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:14:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (45, 3, 'remark', '备注', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入备注\"}}', NULL, NULL, 10, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:14:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (46, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, 10, NULL, 1, NULL, 1, NULL, 1, '2023-07-27 21:14:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (47, 4, 'dictCode', '字典编码', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入字典编码\"}}', NULL, NULL, 1, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (48, 4, 'dictSort', '字典排序', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入字典排序\"}}', NULL, NULL, 2, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (49, 4, 'dictLabel', '字典标签', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入字典标签\"}}', NULL, NULL, 3, NULL, 0, NULL, 1, 1, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (50, 4, 'dictValue', '字典键值', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入字典键值\"}}', NULL, NULL, 4, NULL, 0, NULL, 1, 1, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (51, 4, 'dictType', '字典类型', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入字典类型\"}}', NULL, NULL, 5, NULL, 0, NULL, 1, 1, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (52, 4, 'cssClass', '样式属性', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入样式属性（其他样式扩展）\"}}', NULL, NULL, 6, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (53, 4, 'listClass', '表格回显样式', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入表格回显样式\"}}', NULL, NULL, 7, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (54, 4, 'isDefault', '是否默认', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入是否默认（Y是 N否）\"}}', NULL, NULL, 8, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (55, 4, 'status', '状态', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_normal_disable\"\r\n}', NULL, NULL, 9, '[\"sys_normal_disable\"]', 0, NULL, 1, NULL, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (56, 4, 'createBy', '创建者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建者\"}}', NULL, NULL, 10, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (57, 4, 'createTime', '创建时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建时间\"}}', NULL, NULL, 11, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (58, 4, 'updateBy', '更新者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新者\"}}', NULL, NULL, 12, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (59, 4, 'updateTime', '更新时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新时间\"}}', NULL, NULL, 13, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (60, 4, 'remark', '备注', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入备注\"}}', NULL, NULL, 14, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (61, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, 14, NULL, 1, NULL, 1, NULL, 1, '2023-07-27 21:24:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (62, 5, 'dictId', '字典ID', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入字典主键\"}}', NULL, NULL, 1, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:04', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (63, 5, 'dictName', '字典名称', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入字典名称\"}}', NULL, NULL, 2, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:04', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (64, 5, 'dictType', '字典类型', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入字典类型\"}}', NULL, NULL, 3, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:04', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (65, 5, 'dictDescribe', '描述', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入描述\"}}', NULL, NULL, 4, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:04', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (66, 5, 'status', '状态', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_normal_disable\"\r\n}', NULL, NULL, 5, '[\"sys_normal_disable\"]', 0, NULL, 1, NULL, 1, '2023-07-27 21:24:04', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (67, 5, 'createBy', '创建者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建者\"}}', NULL, NULL, 6, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:04', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (68, 5, 'createTime', '创建时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建时间\"}}', NULL, NULL, 7, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:04', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (69, 5, 'updateBy', '更新者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新者\"}}', NULL, NULL, 8, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:04', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (70, 5, 'updateTime', '更新时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新时间\"}}', NULL, NULL, 9, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:04', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (71, 5, 'remark', '备注', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入备注\"}}', NULL, NULL, 10, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:24:04', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (72, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, 10, NULL, 1, NULL, 1, NULL, 1, '2023-07-27 21:24:04', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (73, 6, 'configId', '参数主键', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入参数主键\"}}', NULL, NULL, 1, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (74, 6, 'configName', '参数名称', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入参数名称\"}}', NULL, NULL, 2, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (75, 6, 'configKey', '参数键名', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入参数键名\"}}', NULL, NULL, 3, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (76, 6, 'configValue', '参数键值', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入参数键值\"}}', NULL, NULL, 4, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (77, 6, 'configParam', '参数', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入参数\"}}', NULL, NULL, 5, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (78, 6, 'configType', '系统内置', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_yes_no\"\r\n}', NULL, NULL, 6, '[\"sys_yes_no\"]', 0, NULL, 1, NULL, 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (79, 6, 'createBy', '创建者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建者\"}}', NULL, NULL, 7, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (80, 6, 'createTime', '创建时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建时间\"}}', NULL, NULL, 8, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (81, 6, 'updateBy', '更新者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新者\"}}', NULL, NULL, 9, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (82, 6, 'updateTime', '更新时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新时间\"}}', NULL, NULL, 10, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (83, 6, 'remark', '备注', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入备注\"}}', NULL, NULL, 11, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (84, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, 11, NULL, 1, NULL, 1, NULL, 1, '2023-07-27 21:38:18', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (85, 7, 'noticeId', '公告ID', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入公告ID\"}}', NULL, NULL, 1, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:45:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (86, 7, 'noticeTitle', '公告标题', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入公告标题\"}}', NULL, NULL, 2, NULL, 0, NULL, 1, 1, 1, '2023-07-27 21:45:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (87, 7, 'noticeType', '公告类型', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_notice_type\"\r\n}', NULL, NULL, 3, '[\"sys_notice_type\"]', 0, NULL, 1, 1, 1, '2023-07-27 21:45:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (88, 7, 'noticeContent', '公告内容', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入公告内容\"}}', NULL, NULL, 4, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:45:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (89, 7, 'status', '公告状态', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_notice_status\"\r\n}', NULL, NULL, 5, '[\"sys_notice_status\"]', 0, NULL, 1, 1, 1, '2023-07-27 21:45:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (90, 7, 'createBy', '创建者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建者\"}}', NULL, NULL, 6, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:45:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (91, 7, 'createTime', '创建时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建时间\"}}', NULL, NULL, 7, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:45:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (92, 7, 'updateBy', '更新者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新者\"}}', NULL, NULL, 8, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:45:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (93, 7, 'updateTime', '更新时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新时间\"}}', NULL, NULL, 9, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:45:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (94, 7, 'remark', '备注', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入备注\"}}', NULL, NULL, 10, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:45:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (95, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, 10, NULL, 1, NULL, 1, NULL, 1, '2023-07-27 21:45:51', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (96, 8, 'operId', '日志主键', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入日志主键\"}}', NULL, NULL, 1, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (97, 8, 'title', '模块标题', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入模块标题\"}}', NULL, NULL, 2, NULL, 0, NULL, 1, 1, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (98, 8, 'businessType', '业务类型', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_oper_type\"\r\n}', NULL, NULL, 3, '[\"sys_oper_type\"]', 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (99, 8, 'method', '方法名称', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入方法名称\"}}', NULL, NULL, 4, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (100, 8, 'requestMethod', '请求方式', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入请求方式\"}}', NULL, NULL, 5, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (101, 8, 'operatorType', '操作类别', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入操作类别（0其它 1后台用户 2手机端用户）\"}}', NULL, NULL, 6, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (102, 8, 'operName', '操作人员', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入操作人员\"}}', NULL, NULL, 7, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (103, 8, 'deptName', '部门名称', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入部门名称\"}}', NULL, NULL, 8, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (104, 8, 'operUrl', '请求URL', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入请求URL\"}}', NULL, NULL, 9, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (105, 8, 'operIp', '主机地址', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入主机地址\"}}', NULL, NULL, 10, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (106, 8, 'operLocation', '操作地点', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入操作地点\"}}', NULL, NULL, 11, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (107, 8, 'operParam', '请求参数', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入请求参数\"}}', NULL, NULL, 12, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (108, 8, 'jsonResult', '返回参数', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入返回参数\"}}', NULL, NULL, 13, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (109, 8, 'status', '操作状态', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_common_status\"\r\n}', NULL, NULL, 14, '[\"sys_common_status\"]', 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (110, 8, 'errorMsg', '错误消息', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入错误消息\"}}', NULL, NULL, 15, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (111, 8, 'operTime', '操作时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入操作时间\"}}', NULL, NULL, 16, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (112, 8, 'costTime', '消耗时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入消耗时间\"}}', NULL, NULL, 17, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (113, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, 17, NULL, 1, NULL, 1, NULL, 1, '2023-07-27 21:52:43', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (114, 9, 'infoId', '访问ID', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入访问ID\"}}', NULL, NULL, 1, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:00:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (115, 9, 'userName', '用户账号', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户账号\"}}', NULL, NULL, 2, NULL, 0, NULL, 1, 1, 1, '2023-07-27 22:00:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (116, 9, 'ipaddr', '登录IP地址', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入登录IP地址\"}}', NULL, NULL, 3, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:00:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (117, 9, 'loginLocation', '登录地点', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入登录地点\"}}', NULL, NULL, 4, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:00:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (118, 9, 'browser', '浏览器类型', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入浏览器类型\"}}', NULL, NULL, 5, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:00:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (119, 9, 'os', '操作系统', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入操作系统\"}}', NULL, NULL, 6, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:00:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (120, 9, 'status', '登录状态', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_common_status\"\r\n}', NULL, NULL, 7, '[\"sys_common_status\"]', 0, NULL, 1, 1, 1, '2023-07-27 22:00:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (121, 9, 'msg', '提示信息', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入提示信息\"}}', NULL, NULL, 8, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:00:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (122, 9, 'accessTime', '访问时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入访问时间\"}}', NULL, NULL, 9, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:00:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (123, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, 9, NULL, 1, NULL, 1, NULL, 1, '2023-07-27 22:00:03', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (124, 10, 'panelId', '模板id', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入模板id\"}}', NULL, NULL, 1, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (125, 10, 'providerId', '绑定的面板id', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入绑定的面板id\"}}', NULL, NULL, 2, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (126, 10, 'panelPaperWidth', '纸张宽度', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入纸张宽度\"}}', NULL, NULL, 3, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (127, 10, 'panelPaperHeight', '纸张高度', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入纸张高度\"}}', NULL, NULL, 4, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (128, 10, 'panelTitle', '模板名称', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入模板名称\"}}', NULL, NULL, 5, NULL, 0, NULL, 1, 1, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (129, 10, 'panelData', '模板数据（json）', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入模板数据（json）\"}}', NULL, NULL, 6, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (130, 10, 'panelPaperType', '纸张类型', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入纸张类型\"}}', NULL, NULL, 7, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (131, 10, 'rotate', '纸张是否旋转', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入纸张是否旋转\"}}', NULL, NULL, 8, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (132, 10, 'createBy', '创建者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建者\"}}', NULL, NULL, 9, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (133, 10, 'createTime', '创建时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建时间\"}}', NULL, NULL, 10, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (134, 10, 'updateBy', '更新者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新者\"}}', NULL, NULL, 11, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (135, 10, 'updateTime', '更新时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新时间\"}}', NULL, NULL, 12, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (136, 10, 'remark', '备注', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入备注\"}}', NULL, NULL, 13, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (137, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, 13, NULL, 1, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (138, 11, 'providerId', '面板id', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入面板id\"}}', NULL, NULL, 1, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (139, 11, 'providerTitle', '表名称', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入表名称\"}}', NULL, NULL, 2, NULL, 0, NULL, 1, 1, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (140, 11, 'providerData', '面板数据', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入面板数据\"}}', NULL, NULL, 3, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (141, 11, 'createBy', '创建者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建者\"}}', NULL, NULL, 4, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (142, 11, 'createTime', '创建时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建时间\"}}', NULL, NULL, 5, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (143, 11, 'updateBy', '更新者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新者\"}}', NULL, NULL, 6, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (144, 11, 'updateTime', '更新时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新时间\"}}', NULL, NULL, 7, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (145, 11, 'remark', '备注', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入备注\"}}', NULL, NULL, 8, NULL, 0, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);
INSERT INTO `sys_table_form_item` VALUES (146, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, 8, NULL, 1, NULL, 1, NULL, 1, '2023-07-27 22:06:58', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_table_pager
-- ----------------------------
DROP TABLE IF EXISTS `sys_table_pager`;
CREATE TABLE `sys_table_pager`  (
  `pager_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分页配置id',
  `table_id` int(11) NULL DEFAULT NULL COMMENT '表格id',
  `layouts` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total' COMMENT '自定义布局',
  `current_page` int(11) NULL DEFAULT NULL COMMENT '当前页',
  `page_size` int(11) NULL DEFAULT NULL COMMENT '每页大小',
  `pager_count` int(11) NULL DEFAULT NULL COMMENT '显示页码按钮的数量',
  `page_sizes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '每页大小选项列表',
  `align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'right' COMMENT '对齐方式',
  `border` tinyint(1) NULL DEFAULT NULL COMMENT '带边框',
  `background` tinyint(1) NULL DEFAULT NULL COMMENT '带背景颜色',
  `perfect` tinyint(1) NULL DEFAULT NULL COMMENT '配套的样式',
  `class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给分页附加 className',
  `auto_hidden` tinyint(1) NULL DEFAULT NULL COMMENT '当只有一页时自动隐藏',
  `icon_prev_page` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义上一页图标',
  `icon_jump_prev` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义向上跳页图标',
  `icon_jump_next` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义向下跳页图标',
  `iconnext_page` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义下一页图标',
  `icon_jump_more` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义跳页显示图标',
  `enabled` tinyint(1) NULL DEFAULT NULL COMMENT '是否启用',
  `slots` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插槽',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`pager_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '功能表分页配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_table_pager
-- ----------------------------
INSERT INTO `sys_table_pager` VALUES (1, 1, 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total', NULL, 10, NULL, '5,10,15,20,50,100,200,500,1000', 'right', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_pager` VALUES (2, 2, 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total', NULL, 10, NULL, '5,10,15,20,50,100,200,500,1000', 'right', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_pager` VALUES (3, 3, 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total', NULL, 10, NULL, '5,10,15,20,50,100,200,500,1000', 'right', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_pager` VALUES (4, 4, 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total', NULL, 10, NULL, '5,10,15,20,50,100,200,500,1000', 'right', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_pager` VALUES (5, 5, 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total', NULL, 10, NULL, '5,10,15,20,50,100,200,500,1000', 'right', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_pager` VALUES (6, 6, 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total', NULL, 10, NULL, '5,10,15,20,50,100,200,500,1000', 'right', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_pager` VALUES (7, 7, 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total', NULL, 10, NULL, '5,10,15,20,50,100,200,500,1000', 'right', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_pager` VALUES (8, 8, 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total', NULL, 10, NULL, '5,10,15,20,50,100,200,500,1000', 'right', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_pager` VALUES (9, 9, 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total', NULL, 10, NULL, '5,10,15,20,50,100,200,500,1000', 'right', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_pager` VALUES (10, 10, 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total', NULL, 10, NULL, '5,10,15,20,50,100,200,500,1000', 'right', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_pager` VALUES (11, 11, 'PrevJump,PrevPage,Jump,PageCount,NextPage,NextJump,Sizes,Total', NULL, 10, NULL, '5,10,15,20,50,100,200,500,1000', 'right', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_table_proxy
-- ----------------------------
DROP TABLE IF EXISTS `sys_table_proxy`;
CREATE TABLE `sys_table_proxy`  (
  `proxy_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '代理表id',
  `table_id` int(11) NULL DEFAULT NULL COMMENT '表格id',
  `enabled` tinyint(1) NULL DEFAULT NULL COMMENT '是否启用',
  `auto_load` tinyint(1) NULL DEFAULT NULL COMMENT '是否自动加载查询数据',
  `message` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示内置的消息提示（可以设为 false 关闭内置的消息提示）',
  `seq` tinyint(1) NULL DEFAULT 1 COMMENT '存在 type=index 列时有效是否代理动态序号（根据分页动态变化）',
  `sort` tinyint(1) NULL DEFAULT 1 COMMENT '是否代理排序',
  `filter` tinyint(1) NULL DEFAULT 1 COMMENT '是否代理筛选',
  `form` tinyint(1) NULL DEFAULT 1 COMMENT '是否代理表单',
  `props` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '获取的属性配置',
  `querybase` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询配置',
  `query_all` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '全量查询配置',
  `deletebase` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除配置',
  `insertbase` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插入配置',
  `updatebase` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新配置',
  `save` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存',
  `params_change_load` tinyint(1) NULL DEFAULT 0 COMMENT '额外的请求参数改变时是否重新请求数据',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`proxy_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '功能表数据代理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_table_proxy
-- ----------------------------
INSERT INTO `sys_table_proxy` VALUES (1, 1, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"method\":\"post\",\"api\":\"/system/user/page\"}', '{}', '{}', '{}', '{}', '{}', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_proxy` VALUES (2, 2, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"method\":\"get\",\"api\":\"/system/dept/deptTree\"}', '{}', '{}', '{}', '{}', '{}', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_proxy` VALUES (3, 3, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"beforeQuery\":\"\",\"method\":\"post\",\"afterQuery\":\"\",\"api\":\"/system/post/page\"}', '{\"afterQueryAll\":\"\",\"method\":\"\",\"beforeQueryAll\":\"\",\"api\":\"\"}', '{\"afterDelete\":\"\",\"method\":\"\",\"api\":\"\",\"beforeDelete\":\"\"}', '{\"beforeInsert\":\"\",\"method\":\"\",\"afterInsert\":\"\",\"api\":\"\"}', '{\"beforeUpdate\":\"\",\"method\":\"\",\"afterUpdate\":\"\",\"api\":\"\"}', '{\"afterSave\":\"\"}', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_proxy` VALUES (4, 5, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"beforeQuery\":\"\",\"method\":\"post\",\"afterQuery\":\"\",\"api\":\"/system/dict/type/page\"}', '{\"afterQueryAll\":\"\",\"method\":\"\",\"beforeQueryAll\":\"\",\"api\":\"\"}', '{\"afterDelete\":\"\",\"method\":\"\",\"api\":\"\",\"beforeDelete\":\"\"}', '{\"beforeInsert\":\"\",\"method\":\"\",\"afterInsert\":\"\",\"api\":\"\"}', '{\"beforeUpdate\":\"\",\"method\":\"\",\"afterUpdate\":\"\",\"api\":\"\"}', '{\"afterSave\":\"\"}', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_proxy` VALUES (5, 4, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"beforeQuery\":\"\",\"method\":\"post\",\"afterQuery\":\"\",\"api\":\"/system/dict/data/page\"}', '{\"afterQueryAll\":\"\",\"method\":\"\",\"beforeQueryAll\":\"\",\"api\":\"\"}', '{\"afterDelete\":\"\",\"method\":\"\",\"api\":\"\",\"beforeDelete\":\"\"}', '{\"beforeInsert\":\"\",\"method\":\"\",\"afterInsert\":\"\",\"api\":\"\"}', '{\"beforeUpdate\":\"\",\"method\":\"\",\"afterUpdate\":\"\",\"api\":\"\"}', '{\"afterSave\":\"\"}', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_proxy` VALUES (6, 6, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"beforeQuery\":\"\",\"method\":\"post\",\"afterQuery\":\"\",\"api\":\"/system/config/page\"}', '{\"afterQueryAll\":\"\",\"method\":\"\",\"beforeQueryAll\":\"\",\"api\":\"\"}', '{\"afterDelete\":\"\",\"method\":\"\",\"api\":\"\",\"beforeDelete\":\"\"}', '{\"beforeInsert\":\"\",\"method\":\"\",\"afterInsert\":\"\",\"api\":\"\"}', '{\"beforeUpdate\":\"\",\"method\":\"\",\"afterUpdate\":\"\",\"api\":\"\"}', '{\"afterSave\":\"\"}', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_proxy` VALUES (7, 7, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"beforeQuery\":\"\",\"method\":\"post\",\"afterQuery\":\"\",\"api\":\"/system/notice/page\"}', '{\"afterQueryAll\":\"\",\"method\":\"\",\"beforeQueryAll\":\"\",\"api\":\"\"}', '{\"afterDelete\":\"\",\"method\":\"\",\"api\":\"\",\"beforeDelete\":\"\"}', '{\"beforeInsert\":\"\",\"method\":\"\",\"afterInsert\":\"\",\"api\":\"\"}', '{\"beforeUpdate\":\"\",\"method\":\"\",\"afterUpdate\":\"\",\"api\":\"\"}', '{\"afterSave\":\"\"}', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_proxy` VALUES (8, 7, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"beforeQuery\":\"\",\"method\":\"post\",\"afterQuery\":\"\",\"api\":\"/system/notice/page\"}', '{\"afterQueryAll\":\"\",\"method\":\"\",\"beforeQueryAll\":\"\",\"api\":\"\"}', '{\"afterDelete\":\"\",\"method\":\"\",\"api\":\"\",\"beforeDelete\":\"\"}', '{\"beforeInsert\":\"\",\"method\":\"\",\"afterInsert\":\"\",\"api\":\"\"}', '{\"beforeUpdate\":\"\",\"method\":\"\",\"afterUpdate\":\"\",\"api\":\"\"}', '{\"afterSave\":\"\"}', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_proxy` VALUES (9, 8, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"beforeQuery\":\"\",\"method\":\"post\",\"afterQuery\":\"\",\"api\":\"/system/operlog/page\"}', '{\"afterQueryAll\":\"\",\"method\":\"\",\"beforeQueryAll\":\"\",\"api\":\"\"}', '{\"afterDelete\":\"\",\"method\":\"\",\"api\":\"\",\"beforeDelete\":\"\"}', '{\"beforeInsert\":\"\",\"method\":\"\",\"afterInsert\":\"\",\"api\":\"\"}', '{\"beforeUpdate\":\"\",\"method\":\"\",\"afterUpdate\":\"\",\"api\":\"\"}', '{\"afterSave\":\"\"}', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_proxy` VALUES (10, 9, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"beforeQuery\":\"\",\"method\":\"get\",\"afterQuery\":\"\",\"api\":\"/system/online/list\"}', '{\"afterQueryAll\":\"\",\"method\":\"\",\"beforeQueryAll\":\"\",\"api\":\"\"}', '{\"afterDelete\":\"\",\"method\":\"\",\"api\":\"\",\"beforeDelete\":\"\"}', '{\"beforeInsert\":\"\",\"method\":\"\",\"afterInsert\":\"\",\"api\":\"\"}', '{\"beforeUpdate\":\"\",\"method\":\"\",\"afterUpdate\":\"\",\"api\":\"\"}', '{\"afterSave\":\"\"}', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_proxy` VALUES (11, 10, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"beforeQuery\":\"\",\"method\":\"post\",\"afterQuery\":\"\",\"api\":\"/print/printPanel/page\"}', '{\"afterQueryAll\":\"\",\"method\":\"\",\"beforeQueryAll\":\"\",\"api\":\"\"}', '{\"afterDelete\":\"\",\"method\":\"\",\"api\":\"\",\"beforeDelete\":\"\"}', '{\"beforeInsert\":\"\",\"method\":\"\",\"afterInsert\":\"\",\"api\":\"\"}', '{\"beforeUpdate\":\"\",\"method\":\"\",\"afterUpdate\":\"\",\"api\":\"\"}', '{\"afterSave\":\"\"}', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_proxy` VALUES (12, 10, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"beforeQuery\":\"\",\"method\":\"post\",\"afterQuery\":\"\",\"api\":\"/print/printPanel/page\"}', '{\"afterQueryAll\":\"\",\"method\":\"\",\"beforeQueryAll\":\"\",\"api\":\"\"}', '{\"afterDelete\":\"\",\"method\":\"\",\"api\":\"\",\"beforeDelete\":\"\"}', '{\"beforeInsert\":\"\",\"method\":\"\",\"afterInsert\":\"\",\"api\":\"\"}', '{\"beforeUpdate\":\"\",\"method\":\"\",\"afterUpdate\":\"\",\"api\":\"\"}', '{\"afterSave\":\"\"}', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_proxy` VALUES (13, 11, 1, 1, 1, 0, 0, 0, 0, '{\"result\":\"rows\",\"total\":\"total\",\"list\":\"list\",\"message\":\"message\"}', '{\"beforeQuery\":\"\",\"method\":\"post\",\"afterQuery\":\"\",\"api\":\"/print/printProvider/page\"}', '{\"afterQueryAll\":\"\",\"method\":\"\",\"beforeQueryAll\":\"\",\"api\":\"\"}', '{\"afterDelete\":\"\",\"method\":\"\",\"api\":\"\",\"beforeDelete\":\"\"}', '{\"beforeInsert\":\"\",\"method\":\"\",\"afterInsert\":\"\",\"api\":\"\"}', '{\"beforeUpdate\":\"\",\"method\":\"\",\"afterUpdate\":\"\",\"api\":\"\"}', '{\"afterSave\":\"\"}', 1, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_table_toolbar_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_table_toolbar_config`;
CREATE TABLE `sys_table_toolbar_config`  (
  `toolbar_config_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '工具栏配置id',
  `table_id` int(11) NULL DEFAULT NULL COMMENT '表格id',
  `ext_size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '尺寸(medium, small, mini)',
  `loading` tinyint(1) NULL DEFAULT 0 COMMENT '是否加载中',
  `perfect` tinyint(1) NULL DEFAULT 0 COMMENT '配套的样式',
  `class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给工具栏 className',
  `ext_import` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '导入按钮配置（需要设置 \"import-config\"）',
  `ext_export` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '导出按钮配置（需要设置 \"export-config\"）',
  `print` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印按钮配置',
  `refresh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '刷新按钮配置',
  `custom` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义列配置',
  `buttons` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '左侧按钮列表',
  `tools` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '右侧工具列表',
  `enabled` tinyint(1) NULL DEFAULT 1 COMMENT '是否启用',
  `print_design` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义的打印配置',
  `print_data_query_port` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印时查询数据接口',
  `print_data_query_before` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询打印数据前处理函数',
  `print_data_query_after` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询打印数据后处理函数',
  `zoom` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否允许最大化显示',
  `slots` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插槽',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`toolbar_config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工具栏配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_table_toolbar_config
-- ----------------------------
INSERT INTO `sys_table_toolbar_config` VALUES (1, 1, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"directPrintConfig\":{\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"size\":\"medium\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{},\"directPrint\":true,\"setPrintTemplate\":true,\"previewPrintConfig\":{\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_toolbar_config` VALUES (2, 2, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"directPrintConfig\":{\"extConfig\":\"\\\"\\\"\",\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"size\":\"medium\",\"extConfig\":\"\\\"\\\"\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{},\"directPrint\":true,\"setPrintTemplate\":true,\"previewPrintConfig\":{\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_toolbar_config` VALUES (3, 3, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"printDataQueryAfter\":\"\",\"printDataQueryPort\":\"\",\"directPrintConfig\":{\"queryPanel\":\"\",\"extConfig\":\"\\\"\\\\\\\"\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"\\\\\\\"\\\"\",\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"queryPanel\":\"\",\"size\":\"medium\",\"queryProvider\":\"\",\"beforeClick\":\"\",\"extConfig\":\"\\\"\\\\\\\"\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"\\\\\\\"\\\"\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{\"queryPanel\":\"\"},\"directPrint\":true,\"setPrintTemplate\":true,\"printDataQueryBefore\":\"\",\"previewPrintConfig\":{\"queryPanel\":\"\",\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{\"buttons\":\"\",\"tools\":\"\"}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_toolbar_config` VALUES (4, 5, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"printDataQueryAfter\":\"\",\"printDataQueryPort\":\"\",\"directPrintConfig\":{\"queryPanel\":\"\",\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"queryPanel\":\"\",\"size\":\"medium\",\"queryProvider\":\"\",\"beforeClick\":\"\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{\"queryPanel\":\"\"},\"directPrint\":true,\"setPrintTemplate\":true,\"printDataQueryBefore\":\"\",\"previewPrintConfig\":{\"queryPanel\":\"\",\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{\"buttons\":\"\",\"tools\":\"\"}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_toolbar_config` VALUES (5, 4, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"printDataQueryAfter\":\"\",\"printDataQueryPort\":\"\",\"directPrintConfig\":{\"queryPanel\":\"\",\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"queryPanel\":\"\",\"size\":\"medium\",\"queryProvider\":\"\",\"beforeClick\":\"\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{\"queryPanel\":\"\"},\"directPrint\":true,\"setPrintTemplate\":true,\"printDataQueryBefore\":\"\",\"previewPrintConfig\":{\"queryPanel\":\"\",\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{\"buttons\":\"\",\"tools\":\"\"}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_toolbar_config` VALUES (6, 6, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"printDataQueryAfter\":\"\",\"printDataQueryPort\":\"\",\"directPrintConfig\":{\"queryPanel\":\"\",\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"queryPanel\":\"\",\"size\":\"medium\",\"queryProvider\":\"\",\"beforeClick\":\"\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{\"queryPanel\":\"\"},\"directPrint\":true,\"setPrintTemplate\":true,\"printDataQueryBefore\":\"\",\"previewPrintConfig\":{\"queryPanel\":\"\",\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{\"buttons\":\"\",\"tools\":\"\"}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_toolbar_config` VALUES (7, 7, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"printDataQueryAfter\":\"\",\"printDataQueryPort\":\"\",\"directPrintConfig\":{\"queryPanel\":\"\",\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"queryPanel\":\"\",\"size\":\"medium\",\"queryProvider\":\"\",\"beforeClick\":\"\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{\"queryPanel\":\"\"},\"directPrint\":true,\"setPrintTemplate\":true,\"printDataQueryBefore\":\"\",\"previewPrintConfig\":{\"queryPanel\":\"\",\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{\"buttons\":\"\",\"tools\":\"\"}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_toolbar_config` VALUES (8, 7, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"printDataQueryAfter\":\"\",\"printDataQueryPort\":\"\",\"directPrintConfig\":{\"queryPanel\":\"\",\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"queryPanel\":\"\",\"size\":\"medium\",\"queryProvider\":\"\",\"beforeClick\":\"\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{\"queryPanel\":\"\"},\"directPrint\":true,\"setPrintTemplate\":true,\"printDataQueryBefore\":\"\",\"previewPrintConfig\":{\"queryPanel\":\"\",\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{\"buttons\":\"\",\"tools\":\"\"}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_toolbar_config` VALUES (9, 8, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"printDataQueryAfter\":\"\",\"printDataQueryPort\":\"\",\"directPrintConfig\":{\"queryPanel\":\"\",\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"queryPanel\":\"\",\"size\":\"medium\",\"queryProvider\":\"\",\"beforeClick\":\"\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{\"queryPanel\":\"\"},\"directPrint\":true,\"setPrintTemplate\":true,\"printDataQueryBefore\":\"\",\"previewPrintConfig\":{\"queryPanel\":\"\",\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{\"buttons\":\"\",\"tools\":\"\"}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_toolbar_config` VALUES (10, 9, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"printDataQueryAfter\":\"\",\"printDataQueryPort\":\"\",\"directPrintConfig\":{\"queryPanel\":\"\",\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"queryPanel\":\"\",\"size\":\"medium\",\"queryProvider\":\"\",\"beforeClick\":\"\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{\"queryPanel\":\"\"},\"directPrint\":true,\"setPrintTemplate\":true,\"printDataQueryBefore\":\"\",\"previewPrintConfig\":{\"queryPanel\":\"\",\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{\"buttons\":\"\",\"tools\":\"\"}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_toolbar_config` VALUES (11, 10, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"printDataQueryAfter\":\"\",\"printDataQueryPort\":\"\",\"directPrintConfig\":{\"queryPanel\":\"\",\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"queryPanel\":\"\",\"size\":\"medium\",\"queryProvider\":\"\",\"beforeClick\":\"\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{\"queryPanel\":\"\"},\"directPrint\":true,\"setPrintTemplate\":true,\"printDataQueryBefore\":\"\",\"previewPrintConfig\":{\"queryPanel\":\"\",\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{\"buttons\":\"\",\"tools\":\"\"}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_toolbar_config` VALUES (12, 10, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"printDataQueryAfter\":\"\",\"printDataQueryPort\":\"\",\"directPrintConfig\":{\"queryPanel\":\"\",\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"queryPanel\":\"\",\"size\":\"medium\",\"queryProvider\":\"\",\"beforeClick\":\"\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{\"queryPanel\":\"\"},\"directPrint\":true,\"setPrintTemplate\":true,\"printDataQueryBefore\":\"\",\"previewPrintConfig\":{\"queryPanel\":\"\",\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{\"buttons\":\"\",\"tools\":\"\"}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_table_toolbar_config` VALUES (13, 11, NULL, 0, 0, '', NULL, NULL, 'false', 'true', 'true', '', '', 1, '{\"printDataQueryAfter\":\"\",\"printDataQueryPort\":\"\",\"directPrintConfig\":{\"queryPanel\":\"\",\"content\":\"直接打印\"},\"previewPrint\":true,\"setPrintTemplateConfig\":{\"queryPanel\":\"\",\"size\":\"medium\",\"queryProvider\":\"\",\"beforeClick\":\"\",\"disabled\":true,\"content\":\"设置模板\"},\"printApi\":{\"queryPanel\":\"\"},\"directPrint\":true,\"setPrintTemplate\":true,\"printDataQueryBefore\":\"\",\"previewPrintConfig\":{\"queryPanel\":\"\",\"params\":{\"printerName\":\"测试\",\"printTitle\":\"测试\"},\"content\":\"预览打印\"}}', NULL, NULL, NULL, 'true', '{\"buttons\":\"\",\"tools\":\"\"}', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2023-01-31 10:18:57', 1, '2023-01-31 10:18:57', NULL, NULL, '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '0', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2023-01-31 10:18:57', 1, '2023-01-31 10:18:57', NULL, NULL, '测试员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (1, 2);
INSERT INTO `sys_user_role` VALUES (1, 3);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (2, 3);
INSERT INTO `sys_user_role` VALUES (2, 4);

-- ----------------------------
-- Table structure for sys_user_table_column
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_table_column`;
CREATE TABLE `sys_user_table_column`  (
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `user_column_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户表格列配置id',
  `column_id` int(11) NOT NULL COMMENT '编号',
  `role_table_id` int(11) NOT NULL COMMENT '角色归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段',
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `is_ignore` tinyint(1) NULL DEFAULT 1 COMMENT '是否显示（1是 0否）',
  `visible` tinyint(1) NULL DEFAULT 1 COMMENT '是否可视（1是 0否）',
  `width` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '宽度',
  `min_width` double NULL DEFAULT NULL COMMENT '最小列宽度会自动将剩余空间按比例分配',
  `resizable` tinyint(1) NULL DEFAULT 1 COMMENT '列宽是否可拖拽（1是 0否）',
  `align` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'left' COMMENT '列对齐方式：left（左对齐）, center（居中对齐）, right（右对齐）',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列的类型',
  `fixed` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '将列固定在左侧或者右侧（注意：固定列应该放在左右两侧的位置）',
  `header_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表头列的对齐方式',
  `footer_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表尾列的对齐方式',
  `show_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当内容过长时显示为省略号',
  `show_header_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当表头内容过长时显示为省略号',
  `show_footer_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当表尾内容过长时显示为省略号',
  `class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给单元格附加 className',
  `header_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表头的单元格附加 className',
  `footer_class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表尾的单元格附加 className',
  `formatter` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '格式化显示内容',
  `sortable` tinyint(1) NULL DEFAULT 1 COMMENT '是否允许列排序',
  `sort_by` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对 sortable 有效，指定排序的字段（当值 formatter 格式化后，可以设置该字段，使用值进行排序）',
  `sort_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排序的字段类型，比如字符串转数值等',
  `ext_params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '额外的参数（可以用来存放一些私有参数）',
  `tree_node` tinyint(1) NULL DEFAULT NULL COMMENT '只对 tree-config 配置时有效，指定为树节点',
  `slots` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插槽',
  `filters` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置筛选条件',
  `filter_multiple` tinyint(1) NULL DEFAULT 1 COMMENT '只对 filters 有效，筛选是否允许多选',
  `filter_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对 filters 有效，列的筛选方法，该方法的返回值用来决定该行是否显示',
  `filter_reset_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对 filters 有效，自定义筛选重置方法',
  `filter_recover_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对 filters 有效，自定义筛选复原方法（使用自定义筛选时可能会用到）',
  `filter_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '筛选渲染器配置项',
  `export_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义单元格数据导出方法，返回自定义的值',
  `footer_export_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义表尾单元格数据导出方法，返回自定义的值',
  `title_prefix` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题前缀图标配置项',
  `cell_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '只对特定功能有效，单元格值类型',
  `cell_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认的渲染器配置项',
  `edit_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '可编辑渲染器配置项',
  `content_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容渲染配置项',
  `col_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义列的唯一主键',
  `sort_no` int(11) NULL DEFAULT NULL COMMENT '排序',
  `dict_type` text CHARACTER SET ujis COLLATE ujis_japanese_ci NULL COMMENT '字典类型',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表格列配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_table_column
-- ----------------------------
INSERT INTO `sys_user_table_column` VALUES (1, 1, 1, 1, 'user_id', '用户ID', 'userId', '用户ID', 1, 1, '91', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 2, 2, 1, 'dept_id', '部门ID', 'deptId', '部门ID', 1, 1, '90', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 3, 3, 1, 'user_name', '用户账号', 'userName', '用户账号', 1, 1, '103', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 4, 4, 1, 'nick_name', '用户昵称', 'nickName', '用户昵称', 1, 1, '107', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 5, 5, 1, 'user_type', '用户类型（00系统用户）', 'userType', '用户类型', 1, 1, '88', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 6, 6, 1, 'email', '用户邮箱', 'email', '用户邮箱', 1, 1, '108', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 7, 7, 1, 'phonenumber', '手机号码', 'phonenumber', '手机号码', 1, 1, '110', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:34:33');
INSERT INTO `sys_user_table_column` VALUES (1, 8, 8, 1, 'sex', '用户性别（0男 1女 2未知）', 'sex', '用户性别', 1, 1, '82', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_user_sex\"\r\n  }\r\n}', NULL, NULL, NULL, 8, '[\"sys_user_sex\"]', NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 9, 9, 1, 'avatar', '头像地址', 'avatar', '头像地址', 1, 1, '111', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 10, 10, 1, 'status', '帐号状态（0正常 1停用）', 'status', '帐号状态', 1, 1, '85', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\":\"ElDictTag\",\"props\":{\"options\":\"sys_normal_disable\"}}', NULL, NULL, NULL, 10, '[\"sys_normal_disable\"]', NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 11, 11, 1, 'login_ip', '最后登录IP', 'loginIp', '最后登录IP', 1, 1, '125', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 12, 12, 1, 'login_date', '最后登录时间', 'loginDate', '最后登录时间', 1, 1, '155', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 13, 13, 1, 'create_by', '创建者', 'createBy', '创建者', 1, 0, '97', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 14, 14, 1, 'create_time', '创建时间', 'createTime', '创建时间', 1, 1, '159', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 15, 15, 1, 'update_by', '更新者', 'updateBy', '更新者', 1, 0, '96', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 16, 16, 1, 'update_time', '更新时间', 'updateTime', '更新时间', 1, 1, '132', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 17, 17, 1, 'remark', '备注', 'remark', '备注', 1, 1, '114', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (2, 18, 1, 1, 'user_id', '用户ID', 'userId', '用户ID', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 19, 2, 1, 'dept_id', '部门ID', 'deptId', '部门ID', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 20, 3, 1, 'user_name', '用户账号', 'userName', '用户账号', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 21, 4, 1, 'nick_name', '用户昵称', 'nickName', '用户昵称', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 22, 5, 1, 'user_type', '用户类型（00系统用户）', 'userType', '用户类型', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 23, 6, 1, 'email', '用户邮箱', 'email', '用户邮箱', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 24, 7, 1, 'phonenumber', '手机号码', 'phonenumber', '手机号码', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 25, 8, 1, 'sex', '用户性别（0男 1女 2未知）', 'sex', '用户性别', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\r\n  \"name\": \"ElDictTag\",\r\n  \"props\": {\r\n    \"options\": \"sys_user_sex\"\r\n  }\r\n}', NULL, NULL, NULL, 8, '[\"sys_user_sex\"]', NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 26, 9, 1, 'avatar', '头像地址', 'avatar', '头像地址', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 27, 10, 1, 'status', '帐号状态（0正常 1停用）', 'status', '帐号状态', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\":\"ElDictTag\",\"props\":{\"options\":\"sys_normal_disable\"}}', NULL, NULL, NULL, 11, '[\"sys_normal_disable\"]', NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 28, 11, 1, 'login_ip', '最后登录IP', 'loginIp', '最后登录IP', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 29, 12, 1, 'login_date', '最后登录时间', 'loginDate', '最后登录时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 30, 13, 1, 'create_by', '创建者', 'createBy', '创建者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 31, 14, 1, 'create_time', '创建时间', 'createTime', '创建时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 32, 15, 1, 'update_by', '更新者', 'updateBy', '更新者', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 33, 16, 1, 'update_time', '更新时间', 'updateTime', '更新时间', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (2, 34, 17, 1, 'remark', '备注', 'remark', '备注', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19, NULL, NULL, 1, '2023-07-26 21:32:50', 1, '2023-07-27 20:31:12');
INSERT INTO `sys_user_table_column` VALUES (1, 35, 18, 1, NULL, NULL, 'createByName', '创建人', 1, 1, '102', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, 14, NULL, NULL, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (1, 36, 19, 1, NULL, NULL, 'updateByName', '修改人', 1, 1, '88', NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, 17, NULL, NULL, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:33:51');
INSERT INTO `sys_user_table_column` VALUES (2, 37, 18, 1, NULL, NULL, 'createByName', '创建人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:27:07');
INSERT INTO `sys_user_table_column` VALUES (2, 38, 19, 1, NULL, NULL, 'updateByName', '修改人', 1, 1, NULL, NULL, 1, 'left', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"name\": \"$input\",\"enabled\": false,\"props\": {},\"options\": [],\"optionProps\": [],\"optionGroups\": [],\"optionGroupProps\": \"\",\"events\": \"\",\"content\": \"\",\"autofocus\": \"\",\"autoselect\": \"\",\"defaultValue\": \"\",\"immediate\": \"\",\"placeholder\": \"\"}', NULL, NULL, NULL, NULL, NULL, 1, '2023-07-27 20:31:12', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_table_form_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_table_form_item`;
CREATE TABLE `sys_user_table_form_item`  (
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `user_item_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户表单项id',
  `item_id` int(11) NOT NULL COMMENT '表单项id',
  `role_form_id` int(11) NOT NULL COMMENT '角色表单id',
  `field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `span` int(2) NULL DEFAULT NULL COMMENT '栅格占据的列数（共 24 分栏）',
  `align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容对齐方式',
  `title_align` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题对齐方式',
  `title_width` int(3) NULL DEFAULT NULL COMMENT '标题宽度',
  `title_colon` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示标题冒号',
  `title_asterisk` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示必填字段的红色星号',
  `title_overflow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题内容过长时显示为省略号',
  `show_title` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示标题',
  `class_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表单项附加 className',
  `content_class_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表单项内容附加 className',
  `content_style` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '给表单项内容附加样式',
  `visible` tinyint(1) NULL DEFAULT 1 COMMENT '默认是否显示',
  `visible_method` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '该方法的返回值用来决定该项是否显示',
  `form_filter` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示删除按钮',
  `folding` tinyint(1) NULL DEFAULT 0 COMMENT '默认收起',
  `collapse_node` tinyint(1) NULL DEFAULT 0 COMMENT '折叠节点',
  `reset_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重置时的默认值',
  `item_render` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项渲染器配置项',
  `children` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项集合',
  `slots` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插槽',
  `sort_no` int(11) NULL DEFAULT NULL COMMENT '排序',
  `dict_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类型',
  `search_handle` tinyint(1) NULL DEFAULT 0 COMMENT '是否为查询操作项（查询，重置）',
  `search_no` int(11) NULL DEFAULT NULL COMMENT '查询排序',
  `search_visible` tinyint(1) NULL DEFAULT 1 COMMENT '是否是查询项（1:显示在查询项，0:显示在添加项）',
  `search_fixed` tinyint(1) NULL DEFAULT 0 COMMENT '是否为固定查询',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_item_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表单配置项列表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_table_form_item
-- ----------------------------
INSERT INTO `sys_user_table_form_item` VALUES (1, 1, 1, 1, 'userId', '用户ID', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户ID\"}}', NULL, NULL, NULL, NULL, 0, 12, 0, 0, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:48:10', NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 2, 2, 1, 'deptId', '部门ID', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入部门ID\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 3, 3, 1, 'userName', '用户账号', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户账号\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 4, 4, 1, 'nickName', '用户昵称', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户昵称\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 5, 5, 1, 'userType', '用户类型', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户类型（00系统用户）\"}}', NULL, NULL, NULL, NULL, 0, 13, 0, 0, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:34:19', NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 6, 6, 1, 'email', '用户邮箱', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户邮箱\"}}', NULL, NULL, NULL, NULL, 0, 12, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 7, 7, 1, 'phonenumber', '手机号码', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入手机号码\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 8, 8, 1, 'sex', '用户性别', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_user_sex\"\r\n}', NULL, NULL, NULL, '[\"sys_user_sex\"]', 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 9, 9, 1, 'avatar', '头像地址', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入头像地址\"}}', NULL, NULL, NULL, NULL, 0, 18, 0, 0, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:34:06', NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 10, 10, 1, 'password', '密码', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入密码\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 11, 11, 1, 'status', '帐号状态', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_normal_disable\"\r\n}', NULL, NULL, NULL, '[\"sys_normal_disable\"]', 0, NULL, 1, 1, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 12, 12, 1, 'delFlag', '删除标志', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入删除标志（0代表存在 2代表删除）\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 13, 13, 1, 'loginIp', '最后登录IP', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入最后登录IP\"}}', NULL, NULL, NULL, NULL, 0, 16, 0, 0, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:34:13', NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 14, 14, 1, 'loginDate', '最后登录时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入最后登录时间\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 15, 15, 1, 'createBy', '创建者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建者\"}}', NULL, NULL, NULL, NULL, 0, 20, 0, 0, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:33:59', NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 16, 16, 1, 'createTime', '创建时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建时间\"}}', NULL, NULL, NULL, NULL, 0, 14, 0, 0, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:34:17', NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 17, 17, 1, 'updateBy', '更新者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新者\"}}', NULL, NULL, NULL, NULL, 0, 17, 0, 0, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:34:08', NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 18, 18, 1, 'updateTime', '更新时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新时间\"}}', NULL, NULL, NULL, NULL, 0, 15, 0, 0, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:34:15', NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 19, 19, 1, 'remark', '备注', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入备注\"}}', NULL, NULL, NULL, NULL, 0, 19, 0, 0, 1, '2023-07-27 20:31:12', NULL, '2023-07-27 20:34:03', NULL);
INSERT INTO `sys_user_table_form_item` VALUES (1, 20, 20, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 21, 1, 1, 'userId', '用户ID', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户ID\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 22, 2, 1, 'deptId', '部门ID', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入部门ID\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 23, 3, 1, 'userName', '用户账号', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户账号\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 24, 4, 1, 'nickName', '用户昵称', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户昵称\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 25, 5, 1, 'userType', '用户类型', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户类型（00系统用户）\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 26, 6, 1, 'email', '用户邮箱', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入用户邮箱\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 27, 7, 1, 'phonenumber', '手机号码', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入手机号码\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 28, 8, 1, 'sex', '用户性别', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_user_sex\"\r\n}', NULL, NULL, NULL, '[\"sys_user_sex\"]', 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 29, 9, 1, 'avatar', '头像地址', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入头像地址\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 30, 10, 1, 'password', '密码', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入密码\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 31, 11, 1, 'status', '帐号状态', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\r\n  \"name\": \"$select\",\r\n  \"options\": \"sys_normal_disable\"\r\n}', NULL, NULL, NULL, '[\"sys_normal_disable\"]', 0, NULL, 1, 1, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 32, 12, 1, 'delFlag', '删除标志', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入删除标志（0代表存在 2代表删除）\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 33, 13, 1, 'loginIp', '最后登录IP', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入最后登录IP\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 34, 14, 1, 'loginDate', '最后登录时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入最后登录时间\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 35, 15, 1, 'createBy', '创建者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建者\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 36, 16, 1, 'createTime', '创建时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入创建时间\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 37, 17, 1, 'updateBy', '更新者', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新者\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 38, 18, 1, 'updateTime', '更新时间', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入更新时间\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 39, 19, 1, 'remark', '备注', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, NULL, '{\"name\":\"$input\",\"props\":{\"placeholder\": \"请输入备注\"}}', NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);
INSERT INTO `sys_user_table_form_item` VALUES (2, 40, 20, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, '{\"name\":\"$buttonSearchs\",\"submit\":{\"props\":{\"type\":\"submit\",\"content\":\"查询\",\"status\":\"primary\"}},\"reset\":{\"props\":{\"type\":\"reset\",\"content\":\"重置\"}},\"filter\":{\"buttonProps\":{\"type\":\"button\",\"icon\":\"vxe-icon-add\"},\"listProps\":{}}}', NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 1, '2023-07-27 20:31:12', NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
