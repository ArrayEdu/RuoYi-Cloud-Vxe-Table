package com.ruoyi.auth.service;

import java.time.Duration;

import com.ruoyi.common.core.utils.SpringUtils;
import com.ruoyi.common.redis.utils.RedisUtils;
import com.ruoyi.common.core.properties.CacheTokenConfig;
import com.ruoyi.system.api.domain.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ruoyi.common.core.constant.CacheConstants;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.security.utils.SecurityUtils;

/**
 * 登录密码方法
 *
 * @author ruoyi
 */
@Component
public class SysPasswordService {

    //    private int maxRetryCount = CacheConstants.PASSWORD_MAX_RETRY_COUNT;
    //    private Long lockTime = CacheConstants.PASSWORD_LOCK_TIME;
    private int maxRetryCount = SpringUtils.getBean(CacheTokenConfig.class).getPassword_max_retry_count();
    private Long lockTime = SpringUtils.getBean(CacheTokenConfig.class).getPassword_lock_time();

    @Autowired
    private SysRecordLogService recordLogService;

    /**
     * 登录账户密码错误次数缓存键名
     *
     * @param username 用户名
     * @return 缓存键key
     */
    private String getCacheKey(String username) {
        return CacheConstants.PWD_ERR_CNT_KEY + username;
    }

    public void validate(SysUser user, String password) {
        String username = user.getUserName();

        Integer retryCount = RedisUtils.getCacheObject(getCacheKey(username));

        if (retryCount == null) {
            retryCount = 0;
        }

        if (retryCount >= Integer.valueOf(maxRetryCount).intValue()) {
            String errMsg = String.format("密码输入错误%s次，帐户锁定%s分钟", maxRetryCount, lockTime);
            recordLogService.recordLogininfor(username, Constants.LOGIN_FAIL, errMsg);
            throw new ServiceException(errMsg);
        }

        if (!matches(user, password)) {
            retryCount = retryCount + 1;
            recordLogService.recordLogininfor(username, Constants.LOGIN_FAIL, String.format("密码输入错误%s次", retryCount));
            RedisUtils.setCacheObject(getCacheKey(username), retryCount, Duration.ofMinutes(lockTime));
            throw new ServiceException("用户不存在/密码错误");
        } else {
            clearLoginRecordCache(username);
        }
    }

    public boolean matches(SysUser user, String rawPassword) {
        return SecurityUtils.matchesPassword(rawPassword, user.getPassword());
    }

    public void clearLoginRecordCache(String loginName) {
        if (RedisUtils.hasKey(getCacheKey(loginName))) {
            RedisUtils.deleteObject(getCacheKey(loginName));
        }
    }
}
