package com.ruoyi.common.translation.core.impl;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.translation.core.TranslationInterface;
import com.ruoyi.common.translation.annotation.TranslationType;
import com.ruoyi.common.translation.constant.TransConstant;
import com.ruoyi.common.translation.feign.RemoteTranslationService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 部门翻译实现
 *
 * @author Lion Li
 */
@AllArgsConstructor
@TranslationType(type = TransConstant.DEPT_ID_TO_NAME)
public class DeptNameTranslationImpl implements TranslationInterface<String> {

    @Autowired
    private RemoteTranslationService remoteTranslationService;

    @Override
    public String translation(Object key, String other) {
        R<String> result = remoteTranslationService.selectDeptNameByIds(key.toString(), SecurityConstants.INNER);
        return result.getData();
    }
}
