package com.ruoyi.common.translation.feign.factory;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.translation.feign.RemoteTranslationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 翻译服务降级处理
 *
 * @author zly
 */
@Component
public class RemoteTranslationFallbackFactory implements FallbackFactory<RemoteTranslationService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteTranslationFallbackFactory.class);

    @Override
    public RemoteTranslationService create(Throwable throwable) {
        log.error("翻译服务调用失败:{}", throwable.getMessage());
        return new RemoteTranslationService() {
            @Override
            public R<String> selectUserNameById(Long userId, String source) {
                return R.fail("用户名称获取失败:" + throwable.getMessage());
            }

            @Override
            public R<String> selectDeptNameByIds(String deptIds, String source) {
                return R.fail("部门名称获取失败:" + throwable.getMessage());
            }
        };
    }
}
