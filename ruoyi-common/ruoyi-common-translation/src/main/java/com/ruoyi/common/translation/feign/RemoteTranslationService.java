package com.ruoyi.common.translation.feign;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.translation.feign.factory.RemoteTranslationFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 翻译服务远程调用
 *
 * @author zly
 */
@FeignClient(contextId = "remoteTranslationService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteTranslationFallbackFactory.class)
public interface RemoteTranslationService {

    /**
     * 根据用户id查询用户名称
     *
     * @param userId 用户id
     * @param source 请求来源
     * @return
     */
    @GetMapping("/user/getUserinfo/{userId}")
    public R<String> selectUserNameById(@PathVariable("userId") Long userId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过部门ID查询部门名称
     *
     * @param deptIds 部门ID串逗号分隔
     * @return 部门名称串逗号分隔
     */
    @GetMapping("/config/configKeyFeign/{deptIds}")
    public R<String> selectDeptNameByIds(@PathVariable("deptIds") String deptIds, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
