package com.ruoyi.common.translation.core.impl;

import com.ruoyi.common.core.utils.SpringUtils;
import com.ruoyi.common.dict.service.DictService;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.translation.core.TranslationInterface;
import com.ruoyi.common.translation.annotation.TranslationType;
import com.ruoyi.common.translation.constant.TransConstant;
import lombok.AllArgsConstructor;

/**
 * 字典翻译实现
 *
 * @author Lion Li
 */
@AllArgsConstructor
@TranslationType(type = TransConstant.DICT_TYPE_TO_LABEL)
public class DictTypeTranslationImpl implements TranslationInterface<String> {

    @Override
    public String translation(Object key, String other) {
        if (key instanceof String && StringUtils.isNotBlank(other)) {
            return SpringUtils.getBean(DictService.class).getDictLabel(other, key.toString());
        }
        return null;
    }
}
