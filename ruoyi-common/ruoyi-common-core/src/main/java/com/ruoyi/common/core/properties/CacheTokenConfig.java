package com.ruoyi.common.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * 登录缓存配置
 *
 * @author zly
 * @date 2023-07-30
 */
@Configuration
@RefreshScope
@ConfigurationProperties(prefix = "user-cache")
@Data
public class CacheTokenConfig {
    /**
     * 缓存有效期，默认720（分钟）
     */
    public long expiration = 720;

    /**
     * 缓存刷新时间，默认120（分钟）
     */
    public long refresh_time = 120;

    /**
     * 密码最大错误次数
     */
    public int password_max_retry_count = 5;

    /**
     * 密码锁定时间，默认10（分钟）
     */
    public long password_lock_time = 10;

    /**
     * 验证码有效期（分钟）
     */
    public long captcha_expiration = 2;
}

