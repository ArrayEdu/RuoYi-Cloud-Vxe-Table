package com.ruoyi.common.core.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum RoleAuthEnum {
    MULTIPLE("multiple"),
    SINGLE("single");
    private final String mode;

    public String getMode() {
        return mode;
    }
}
