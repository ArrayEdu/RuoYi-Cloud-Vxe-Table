package com.ruoyi.common.core.web.page;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderItems implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 需要进行排序的字段
     */
    private String column;
    /**
     * 是否正序排列，默认 true
     */
    private boolean asc = true;

    public static OrderItems asc(String column) {
        return build(column, true);
    }

    public static OrderItems desc(String column) {
        return build(column, false);
    }

    public static List<OrderItems> ascs(String... columns) {
        return Arrays.stream(columns).map(OrderItems::asc).collect(Collectors.toList());
    }

    public static List<OrderItems> descs(String... columns) {
        return Arrays.stream(columns).map(OrderItems::desc).collect(Collectors.toList());
    }

    private static OrderItems build(String column, boolean asc) {
        return new OrderItems(column, asc);
    }
}
