//package com.ruoyi.common.core.utils;
//
//import com.ruoyi.system.api.domain.ParamPower;
//import com.ruoyi.system.api.domain.SysUser;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ParamReplaceUtils {
//    /**
//     * SQL 参数替换方法
//     *
//     * @param sql 带有动态参数的SQL语句
//     * @return 处理过后的SQL语句
//     */
//    public static String sqlReplace(String sql, List<ParamPower> menuDictList) {
//        for (ParamPower paramPower : menuDictList) {
//            sql = sql.replace(paramPower.getRealName(), paramPower.getRealValue().toString());
//        }
//        return sql;
//    }
//
//    /**
//     * 处理角色功能授权所需固定值列表
//     *
//     * @param sysUser 需要处理的对象
//     * @return 处理过后的角色功能授权所需固定值列表
//     */
//    public static List<ParamPower> setTempValue(SysUser sysUser) {
//        List<ParamPower> menuDictList = new ArrayList<>();
//        ParamPower powerUserId = new ParamPower();
//        powerUserId.setRealName("${userId}");
//        powerUserId.setRealValue(sysUser.getUserId());
//        menuDictList.add(powerUserId);
//
//        ParamPower powerDeptId = new ParamPower();
//        powerDeptId.setRealName("${deptId}");
//        if (null == sysUser.getDeptId()) {
//            powerDeptId.setRealValue(0L);
//        } else {
//            powerDeptId.setRealValue(sysUser.getDeptId());
//        }
//        menuDictList.add(powerDeptId);
//
//        return menuDictList;
//    }
//}
