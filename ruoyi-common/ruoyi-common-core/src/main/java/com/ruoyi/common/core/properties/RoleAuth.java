package com.ruoyi.common.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * 角色模式(multiple：多角色模式，前端会弹出角色选择,single：单角色模式，把多个角色权限进行合并);默认为multiple
 *
 * @author zly
 * @date 2023-07-30
 */

@Configuration
@RefreshScope
@ConfigurationProperties(prefix = "role-pattern")
@Data
public class RoleAuth {
    private String mode = "multiple";
}
