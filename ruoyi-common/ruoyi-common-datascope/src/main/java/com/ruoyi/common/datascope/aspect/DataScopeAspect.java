package com.ruoyi.common.datascope.aspect;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.utils.ServletUtils;
import com.ruoyi.common.datascope.entity.ParamPower;
import com.ruoyi.common.datascope.utils.ParamReplaceUtils;
import com.ruoyi.common.core.web.domain.BaseEntity;
import com.ruoyi.system.api.RemoteRoleMenuPowerService;
import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.system.api.domain.SysRoleMenuPower;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.api.model.LoginUser;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ruoyi.common.core.context.SecurityContextHolder;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.datascope.annotation.DataScope;
import com.ruoyi.common.security.utils.SecurityUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 数据过滤处理
 *
 * @author ruoyi
 */
@Aspect
@Component
public class DataScopeAspect {
    /**
     * 全部数据权限
     */
    public static final String DATA_SCOPE_ALL = "1";

    /**
     * 自定数据权限
     */
    public static final String DATA_SCOPE_CUSTOM = "2";

    /**
     * 部门数据权限
     */
    public static final String DATA_SCOPE_DEPT = "3";

    /**
     * 部门及以下数据权限
     */
    public static final String DATA_SCOPE_DEPT_AND_CHILD = "4";

    /**
     * 仅本人数据权限
     */
    public static final String DATA_SCOPE_SELF = "5";
    /**
     * 自定义条件
     */
    public static final String DATA_SCOPE_CUSTOM_CONDITION = "6";

    /**
     * 数据权限过滤关键字
     */
    public static final String DATA_SCOPE = "dataScope";

    @Autowired
    private RemoteRoleMenuPowerService remoteRoleMenuPowerService;

    @Before("@annotation(controllerDataScope)")
    public void doBefore(JoinPoint point, DataScope controllerDataScope) throws Throwable {
        clearDataScope(point);
        handleDataScope(point, controllerDataScope);
    }

    protected void handleDataScope(final JoinPoint joinPoint, DataScope controllerDataScope) {
        // 获取当前的用户
        LoginUser loginUser = SecurityUtils.getLoginUser();
        if (StringUtils.isNotNull(loginUser)) {
            SysUser currentUser = loginUser.getSysUser();
            List<ParamPower> paramPowers = ParamReplaceUtils.setTempValue(currentUser);
            // 如果是超级管理员，则不过滤数据
            if (StringUtils.isNotNull(currentUser) && !currentUser.isAdmin()) {
                String permission = StringUtils.defaultIfEmpty(controllerDataScope.permission(), SecurityContextHolder.getPermission());
                dataScopeFilter(joinPoint, currentUser, controllerDataScope.deptAlias(),
                        controllerDataScope.userAlias(), permission, paramPowers);
            }
        }
    }

    /**
     * 数据范围过滤
     *
     * @param joinPoint  切点
     * @param user       用户
     * @param deptAlias  部门别名
     * @param userAlias  用户别名
     * @param permission 权限字符
     */
    public void dataScopeFilter(JoinPoint joinPoint, SysUser user, String deptAlias, String userAlias, String permission, List<ParamPower> paramPowers) {
        StringBuilder sqlString = new StringBuilder();
        List<String> conditions = new ArrayList<String>();

        //查询动态SQL，并替换值
        String newSql = "";
        SysRoleMenuPower sysRoleMenu = new SysRoleMenuPower();
        HttpServletRequest request = ServletUtils.getRequest();
        String tempMenuId = request.getParameter("tempMenuId");
        if (StringUtils.isNotBlank(tempMenuId)) {
            sysRoleMenu.setMenuId(Long.valueOf(tempMenuId));
        }
        String tempRoleId = request.getParameter("tempRoleId");
        if (StringUtils.isNotBlank(tempRoleId)) {
            sysRoleMenu.setRoleId(Long.valueOf(tempRoleId));
        }

        //查询出角色功能权限设置，并替换变量 ${userId} -- 当前用户  ${deptId} -- 当前部门
        R<SysRoleMenuPower> sysRoleMenuPower = null;
        if (sysRoleMenu.getRoleId() != null && sysRoleMenu.getMenuId() != null) {
            sysRoleMenuPower = remoteRoleMenuPowerService.getRoleMenuPower(sysRoleMenu, SecurityConstants.INNER);
        }

        if (null != sysRoleMenuPower && null != sysRoleMenuPower.getData()) {
            String sql = sysRoleMenuPower.getData().getPowerValue();
            newSql = ParamReplaceUtils.sqlReplace(sql, paramPowers);
        }

        for (SysRole role : user.getRoles()) {
            String dataScope = role.getDataScope();
            if (role.getRoleId() == Long.valueOf(tempRoleId)) {
                if (!DATA_SCOPE_CUSTOM.equals(dataScope) && conditions.contains(dataScope)) {
                    continue;
                }
                if (StringUtils.isNotEmpty(permission) && StringUtils.isNotEmpty(role.getPermissions())
                        && !StringUtils.containsAny(role.getPermissions(), Convert.toStrArray(permission))) {
                    continue;
                }
                // 全部数据权限
                if (DATA_SCOPE_ALL.equals(dataScope)) {
                    sqlString = new StringBuilder();
                    conditions.add(dataScope);
                    break;
                } else if (DATA_SCOPE_CUSTOM.equals(dataScope)) { //自定义数据权限
                    sqlString.append(StringUtils.format(
                            " OR {}.dept_id IN ( SELECT dept_id FROM sys_role_dept WHERE role_id = {} ) ", deptAlias,
                            role.getRoleId()));
                } else if (DATA_SCOPE_DEPT.equals(dataScope)) { //部门数据权限
                    sqlString.append(StringUtils.format(" OR {}.dept_id = {} ", deptAlias, user.getDeptId()));
                } else if (DATA_SCOPE_DEPT_AND_CHILD.equals(dataScope)) { //部门及以下数据权限
                    sqlString.append(StringUtils.format(
                            " OR {}.dept_id IN ( SELECT dept_id FROM sys_dept WHERE dept_id = {} or find_in_set( {} , ancestors ) )",
                            deptAlias, user.getDeptId(), user.getDeptId()));
                } else if (DATA_SCOPE_SELF.equals(dataScope)) { //仅本人数据权限
                    if (StringUtils.isNotBlank(userAlias)) {
                        sqlString.append(StringUtils.format(" OR {}.user_id = {} ", userAlias, user.getUserId()));
                    } else {
                        // 数据权限为仅本人且没有userAlias别名不查询任何数据
                        sqlString.append(StringUtils.format(" OR {}.dept_id = 0 ", deptAlias));
                    }
                } else if (DATA_SCOPE_CUSTOM_CONDITION.equals(dataScope)) {
                    sqlString.append(newSql);
                }
            }
            conditions.add(dataScope);
        }

        // 多角色情况下，所有角色都不包含传递过来的权限字符，这个时候sqlString也会为空，所以要限制一下,不查询任何数据
        if (StringUtils.isEmpty(conditions)) {
            sqlString.append(StringUtils.format(" OR {}.dept_id = 0 ", deptAlias));
        }

        if (StringUtils.isNotBlank(sqlString.toString())) {
            Object params = joinPoint.getArgs()[0];
            if (StringUtils.isNotNull(params) && params instanceof BaseEntity) {
                BaseEntity baseEntity = (BaseEntity) params;
                baseEntity.getParams().put(DATA_SCOPE, " AND (" + sqlString.substring(4) + ")");
            }
        }
    }

    /**
     * 拼接权限sql前先清空params.dataScope参数防止注入
     */
    private void clearDataScope(final JoinPoint joinPoint) {
        Object params = joinPoint.getArgs()[0];
        if (StringUtils.isNotNull(params) && params instanceof BaseEntity) {
            BaseEntity baseEntity = (BaseEntity) params;
            baseEntity.getParams().put(DATA_SCOPE, "");
        }
    }
}
