package com.ruoyi.common.datascope.entity;

import lombok.Data;

@Data
public class ParamPower {
    /**
     * 名称
     */
    private String realName;
    /**
     * 值
     */
    private Long realValue;

}
