package com.ruoyi.common.excel.entity;

import com.alibaba.fastjson2.JSONObject;
import lombok.Data;

import java.util.List;

@Data
public class Exceel {
    /**
     * 文件名
     */
    private String filename;

    /**
     * 表名（只对支持的文档类型有效）
     */
    private String sheetName;
    /**
     * 是否需要表头
     */
    private Boolean isHeader;
    /**
     * 输出数据的方式 current（当前页）, selected（选择导出）, all（所有）
     */
    private String mode;

    /**
     * 输出文件类型
     */
    private String type;

    /**
     * 选择导出时的主键
     */
    private List<Long> ids;

    /**
     * 导出的列
     */
    private List<ExceelFields> fields;

    /**
     * 查询条件
     */
    private JSONObject query;

}
