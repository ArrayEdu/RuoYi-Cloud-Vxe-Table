package com.ruoyi.common.excel.entity;

/**
 * 导出文件列
 *
 * @author zly
 */
public class ExceelFields {

    private String field;

    private String title;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }


}
