package com.ruoyi.common.excel.entity;

/**
 * 导出排序
 *
 * @author zly
 */
public class ExceelSort {
    /**
     * 排序字段
     */
    private String field;
    /**
     * 排序方式
     */
    private String order;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}
