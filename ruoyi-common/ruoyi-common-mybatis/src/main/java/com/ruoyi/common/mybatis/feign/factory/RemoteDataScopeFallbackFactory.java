package com.ruoyi.common.mybatis.feign.factory;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.mybatis.feign.RemoteDataScopeService;
import com.ruoyi.system.api.domain.SysRoleMenuPower;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 权限服务降级处理
 *
 * @author zly
 */
@Component
public class RemoteDataScopeFallbackFactory implements FallbackFactory<RemoteDataScopeService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteDataScopeFallbackFactory.class);

    @Override
    public RemoteDataScopeService create(Throwable throwable) {
        log.error("权限服务调用失败:{}", throwable.getMessage());
        return new RemoteDataScopeService() {
            @Override
            public R<String> getRoleCustom(Long roleId, String source) {
                return R.fail("获取角色自定义权限语句失败:" + throwable.getMessage());
            }

            @Override
            public R<String> getDeptAndChild(Long deptId, String source) {
                return R.fail("获取部门和下级权限语句失败:" + throwable.getMessage());
            }

            @Override
            public R<String> getRoleMenuPower(SysRoleMenuPower sysRoleMenuPower, String source) {
                return R.fail("获取角色菜单授权失败:" + throwable.getMessage());
            }
        };
    }
}
