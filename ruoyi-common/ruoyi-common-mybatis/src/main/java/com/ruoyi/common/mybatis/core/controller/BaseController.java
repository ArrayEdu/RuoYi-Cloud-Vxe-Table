package com.ruoyi.common.mybatis.core.controller;

import java.beans.PropertyEditorSupport;
import java.util.Date;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.PagePlus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import com.ruoyi.common.core.constant.HttpStatus;
import com.ruoyi.common.core.utils.DateUtils;

/**
 * web层通用数据处理
 *
 * @author ruoyi
 */
public class BaseController {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    protected PagePlus getDataTable(IPage<?> list) {
        PagePlus rspData = new PagePlus();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setRecords(list.getRecords());
        rspData.setRows(list.getRecords());
        rspData.setMsg("查询成功");
        rspData.setTotal(list.getTotal() <= 0 ? list.getRecords().size() > 0 ? list.getRecords().size() : 0 : list.getTotal());
        rspData.setCurrent(list.getCurrent());
        rspData.setSize(list.getSize());
        return rspData;
    }

    /**
     * 响应请求分页数据
     */
//    @SuppressWarnings({"rawtypes", "unchecked"})
//    protected TableDataInfo1 getDataTable(List<?> list) {
//        TableDataInfo1 rspData = new TableDataInfo1();
//        rspData.setCode(HttpStatus.SUCCESS);
//        rspData.setRows(list);
//        rspData.setMsg("查询成功");
//        rspData.setTotal(list.size());
//        return rspData;
//    }

    /**
     * 返回成功
     */
    public AjaxResult success() {
        return AjaxResult.success();
    }

    /**
     * 返回成功消息
     */
    public AjaxResult success(String message) {
        return AjaxResult.success(message);
    }

    /**
     * 返回成功消息
     */
    public AjaxResult success(Object data) {
        return AjaxResult.success(data);
    }

    /**
     * 返回失败消息
     */
    public AjaxResult error() {
        return AjaxResult.error();
    }

    /**
     * 返回失败消息
     */
    public AjaxResult error(String message) {
        return AjaxResult.error(message);
    }

    /**
     * 返回警告消息
     */
    public AjaxResult warn(String message) {
        return AjaxResult.warn(message);
    }

    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    protected AjaxResult toAjax(int rows) {
        return rows > 0 ? AjaxResult.success() : AjaxResult.error();
    }

    /**
     * 响应返回结果
     *
     * @param result 结果
     * @return 操作结果
     */
    protected AjaxResult toAjax(boolean result) {
        return result ? success() : error();
    }
}
