package com.ruoyi.common.mybatis.service;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.mybatis.feign.RemoteDataScopeService;
import com.ruoyi.system.api.domain.SysRoleMenuPower;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 数据权限 实现
 * <p>
 * 注意: 此Service内不允许调用标注`数据权限`注解的方法
 * 例如: deptMapper.selectList 此 selectList 方法标注了`数据权限`注解 会出现循环解析的问题
 *
 * @author Lion Li
 * @author zly
 */
@Service("sdss")
public class SysDataScopeService {

    @Autowired
    private RemoteDataScopeService remoteDataScopeService;

    public String getRoleCustom(Long roleId) {
        R<String> result = remoteDataScopeService.getRoleCustom(roleId, SecurityConstants.INNER);
        return result.getData();
    }

    public String getDeptAndChild(Long deptId) {
        R<String> result = remoteDataScopeService.getDeptAndChild(deptId, SecurityConstants.INNER);
        return result.getData();
    }

    /**
     * 自定义角色菜单条件
     *
     * @param tempRoleId
     * @param tempMenuId
     * @return
     */
    public String getCustomCondition(Long tempRoleId, Long tempMenuId) {
        SysRoleMenuPower sysRoleMenuPower = new SysRoleMenuPower();
        sysRoleMenuPower.setMenuId(tempMenuId);
        sysRoleMenuPower.setRoleId(tempRoleId);
        R<String> result = remoteDataScopeService.getRoleMenuPower(sysRoleMenuPower, SecurityConstants.INNER);
        return result.getData();
    }
}
