package com.ruoyi.common.mybatis.feign;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.mybatis.feign.factory.RemoteDataScopeFallbackFactory;
import com.ruoyi.system.api.domain.SysRoleMenuPower;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 数据权限 调用接口
 * <p>
 * 注意: 此Service内不允许调用标注`数据权限`注解的方法
 * 例如: deptMapper.selectList 此 selectList 方法标注了`数据权限`注解 会出现循环解析的问题
 *
 * @author zly
 */
@FeignClient(contextId = "remoteDataScopeService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteDataScopeFallbackFactory.class)
public interface RemoteDataScopeService {

    /**
     * 获取角色自定义权限语句
     */
    @GetMapping("/dataScope/roleCustom/{roleId}")
    R<String> getRoleCustom(@PathVariable("roleId") Long roleId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取部门和下级权限语句
     */
    @GetMapping("/dataScope/dept/{deptId}")
    public R<String> getDeptAndChild(@PathVariable("deptId") Long deptId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取角色菜单授权
     */
    @PostMapping("/dataScope/RoleMenuPower")
    public R<String> getRoleMenuPower(@RequestBody SysRoleMenuPower sysRoleMenuPower, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
