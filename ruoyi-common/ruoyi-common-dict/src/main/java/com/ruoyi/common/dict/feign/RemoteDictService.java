package com.ruoyi.common.dict.feign;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.dict.entity.SysDictDataVo;
import com.ruoyi.common.dict.feign.factory.RemoteDictFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

/**
 * 字典服务远程调用
 *
 * @author zly
 */
@FeignClient(contextId = "remoteDictService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteDictFallbackFactory.class)
public interface RemoteDictService {

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    @GetMapping("/dict/data/typeFeign/{dictType}")
    R<List<SysDictDataVo>> selectDictDataByType(@PathVariable String dictType, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
