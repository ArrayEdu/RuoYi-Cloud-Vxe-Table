package com.ruoyi.common.dict.feign.factory;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.dict.entity.SysDictDataVo;
import com.ruoyi.common.dict.feign.RemoteDictService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 字典配置降级处理
 *
 * @author zly
 */
@Component
public class RemoteDictFallbackFactory implements FallbackFactory<RemoteDictService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteDictFallbackFactory.class);

    @Override
    public RemoteDictService create(Throwable throwable) {
        log.error("字典配置服务调用失败:{}", throwable.getMessage());
        return new RemoteDictService() {
            @Override
            public R<List<SysDictDataVo>> selectDictDataByType(String dictType, String source) {
                return R.fail("获取字典失败:" + throwable.getMessage());
            }
        };
    }
}
