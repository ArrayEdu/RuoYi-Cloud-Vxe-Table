package com.ruoyi.common.dict.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.core.constant.CacheConstants;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.utils.SpringUtils;
import com.ruoyi.common.core.utils.StreamUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.dict.entity.SysDictDataVo;
import com.ruoyi.common.dict.feign.RemoteDictService;
import com.ruoyi.common.redis.utils.RedisUtils;

/**
 * 字典工具类
 *
 * @author zly
 */
public class DictUtils {

    /**
     * 设置字典缓存
     *
     * @param key       参数键
     * @param dictDatas 字典数据列表
     */
    public static void setDictCache(String key, List<SysDictDataVo> dictDatas) {
        RedisUtils.setCacheObject(getCacheKey(key), dictDatas);
    }

    /**
     * 获取字典缓存
     *
     * @param key 参数键
     * @return dictDatas 字典数据列表
     */
    public static List<SysDictDataVo> getDictCache(String key) {
        List<SysDictDataVo> sysDictDataVoList = RedisUtils.getCacheObject(getCacheKey(key));
        return sysDictDataVoList;
    }

    /**
     * 删除指定字典缓存
     *
     * @param key 字典键
     */
    public static void removeDictCache(String key) {
        RedisUtils.deleteObject(getCacheKey(key));
    }

    /**
     * 清空字典缓存
     */
    public static void clearDictCache() {
        Collection<String> keys = RedisUtils.keys(CacheConstants.SYS_DICT_KEY + "*");
        RedisUtils.deleteObject(keys);
    }

    /**
     * 设置cache key
     *
     * @param configKey 参数键
     * @return 缓存键key
     */
    public static String getCacheKey(String configKey) {
        return CacheConstants.SYS_DICT_KEY + configKey;
    }


    /**
     * 根据字典类型和字典值获取字典标签
     *
     * @param dictType  字典类型
     * @param dictValue 字典值
     * @param separator 分隔符
     * @return 字典标签
     */
    public static String getDictValue(String dictType, String dictValue, String separator) {
        // 优先从缓存获取
        List<SysDictDataVo> datas = RedisUtils.getCacheObject(getCacheKey(dictType));
        if (ObjectUtil.isNull(datas)) {
            datas = SpringUtils.getBean(RemoteDictService.class).selectDictDataByType(dictType, SecurityConstants.INNER).getData();
            DictUtils.setDictCache(dictType, datas);
        }
        Map<String, String> map = StreamUtils.toMap(datas, SysDictDataVo::getDictValue, SysDictDataVo::getDictLabel);
        if (StringUtils.containsAny(dictValue, separator)) {
            return Arrays.stream(dictValue.split(separator))
                    .map(v -> map.getOrDefault(v, StringUtils.EMPTY))
                    .collect(Collectors.joining(separator));
        } else {
            return map.getOrDefault(dictValue, StringUtils.EMPTY);
        }
    }

    /**
     * 根据字典类型和字典值获取字典标签
     *
     * @param dictType  字典类型
     * @param dictValue 字典值
     * @return 字典标签
     */
    public static String getDictValue(String dictType, String dictValue) {
        return getDictValue(dictType, dictValue, StringUtils.SEPARATOR);
    }

    /**
     * 根据字典类型和字典标签获取字典值
     *
     * @param dictType  字典类型
     * @param dictLabel 字典标签
     * @param separator 分隔符
     * @return 字典值
     */
    public static String getDictLabel(String dictType, String dictLabel, String separator) {
        // 优先从缓存获取
        List<SysDictDataVo> datas = RedisUtils.getCacheObject(getCacheKey(dictType));
        if (ObjectUtil.isNull(datas)) {
            datas = SpringUtils.getBean(RemoteDictService.class).selectDictDataByType(dictType, SecurityConstants.INNER).getData();
            DictUtils.setDictCache(dictType, datas);
        }

        Map<String, String> map = StreamUtils.toMap(datas, SysDictDataVo::getDictLabel, SysDictDataVo::getDictValue);
        if (StringUtils.containsAny(dictLabel, separator)) {
            return Arrays.stream(dictLabel.split(separator))
                    .map(l -> map.getOrDefault(l, StringUtils.EMPTY))
                    .collect(Collectors.joining(separator));
        } else {
            return map.getOrDefault(dictLabel, StringUtils.EMPTY);
        }
    }

    /**
     * 根据字典类型和字典标签获取字典值
     *
     * @param dictType  字典类型
     * @param dictLabel 字典标签
     * @return 字典值
     */
    public static String getDictLabel(String dictType, String dictLabel) {
        return getDictLabel(dictType, dictLabel, StringUtils.SEPARATOR);
    }
}
