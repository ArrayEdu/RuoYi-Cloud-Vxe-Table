
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">RuoYi-Cloud-Vxe-Table</h1>
<h4 align="center">基于Spring Boot/Spring Cloud & Alibaba 前后端分离的分布式微服务架构</h4>

## 平台简介
* Ruoyi-Vxe-Table后台，基于RuoYi-Cloud 并且添加了RuoYi-Cloud-plus中的一些功能。（不兼容RuoYi-Cloud和RuoYi-Cloud-plus）
* 后端采用Spring Boot、Spring Cloud & Alibaba。
* 注册中心、配置中心选型Nacos，权限认证使用Redis。
* 流量控制框架选型Sentinel，分布式事务选型Seata。
* 通过把vxe-table的配置信息保存到数据库，可通过角色和菜单授权字段。并把个人的表格列配置、查询条件保存在数据库实现表格的个人化。
* 表格字段选择导出

## 修改内容
* 引入flyway数据库版本管理
* 使用mybatis-plus
* 增加字典模块、数据加解密模块、excel模块、翻译模块、websocket模块
* 修改RuoYi-Cloud中AOP方式的数据权限，参与mybatis-plus中的数据权限插件
* 增加了多角色的授权方式（multiple：多角色模式，前端会弹出角色选择,single：单角色模式，把多个角色权限进行合并）
* 增加hiprint打印后端功能
* 增加vxe-table后端配置功能

## 系统模块

~~~
com.ruoyi     
├── ruoyi-gateway         // 网关模块 [8080]
├── ruoyi-auth            // 认证中心 [9200]
├── ruoyi-api             // 接口模块
│       └── ruoyi-api-system                          // 系统接口
├── ruoyi-common          // 通用模块
│       └── ruoyi-common-bom                          // 通用模块依赖管理
│       └── ruoyi-common-core                         // 核心模块
│       └── ruoyi-common-datascope                    // 权限范围
│       └── ruoyi-common-dict                         // 字典模块
│       └── ruoyi-common-encrypt                      // 数据加解密模块
│       └── ruoyi-common-excel                        // excel模块
│       └── ruoyi-common-mybatis                      // 数据库服务
│       └── ruoyi-common-log                          // 日志记录
│       └── ruoyi-common-redis                        // 缓存服务
│       └── ruoyi-common-seata                        // 分布式事务
│       └── ruoyi-common-security                     // 安全模块
│       └── ruoyi-common-swagger                      // 系统接口
│       └── ruoyi-common-translation                  // 翻译模块
│       └── ruoyi-common-websocket                    // websocket模块
├── ruoyi-modules         // 业务模块
│       └── ruoyi-system                              // 系统模块 [9201]
│       └── ruoyi-gen                                 // 代码生成 [9202]
│       └── ruoyi-job                                 // 定时任务 [9203]
│       └── ruoyi-file                                // 文件服务 [9300]
│       └── ruoyi-print                               // 打印服务 [9204]
│       └── ruoyi-tableConfig-open                    // vxe-table表格配置服务 [9205]
├── ruoyi-visual          // 图形化管理模块
│       └── ruoyi-visual-monitor                      // 监控中心 [9100]
├──pom.xml                // 公共依赖
~~~
## 内置功能
1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，表格配置，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分、设置表格配置。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 在线构建器：拖动表单元素生成相应的HTML代码。
17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
18. 打印管理：配置打印模板。

## 快速开始
1. 前往Gitee下载页面(https://gitee.com/zlyKent/RuoYi-Cloud-Vxe-Table.git)
2. 导入项目。
4. 创建数据库ry-config并导入数据脚本ry_config.sql（必须）
5. 配置nacos持久化，修改conf/application.properties文件，增加支持mysql数据源配置

## 后期工作
1. 完善数据权限
2. 完善导出、打印功能

## 版权说明
- 代码中使用了第三方开源的代码，应第三方要求，禁止修改代码中的版权和作者信息。

## 参考
- RuoYi-Cloud-plus(https://plus-doc.dromara.org/#/ruoyi-cloud-plus/home)