package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.domain.SysRoleMenuPower;
import com.ruoyi.system.api.factory.RemoteRoleMenuPowerFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 数据角色功能授权服务
 *
 * @author zly
 */
@FeignClient(contextId = "remoteRoleMenuPowerService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteRoleMenuPowerFallbackFactory.class)
public interface RemoteRoleMenuPowerService {

    /**
     * @param sysRoleMenuPower 数据角色功能授权
     * @param source           请求来源
     * @return 结果
     */

    @PostMapping("/power/listFeign")
    public R<SysRoleMenuPower> getRoleMenuPower(@RequestBody SysRoleMenuPower sysRoleMenuPower, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
