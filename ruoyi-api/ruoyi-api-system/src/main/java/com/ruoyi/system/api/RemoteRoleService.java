package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.system.api.domain.SysRoleMenu;
import com.ruoyi.system.api.factory.RemoteRoleFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色服务
 *
 * @author zly
 */
@FeignClient(contextId = "remoteRoleService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteRoleFallbackFactory.class)
public interface RemoteRoleService {
    /**
     * 通过角色权限查询角色信息
     *
     * @param roleKey 角色权限
     * @param source  请求来源
     * @return 结果
     */
    @GetMapping("/role/getRoleKey/{roleKey}")
    public R<SysRole> getRoleKey(@PathVariable("roleKey") String roleKey, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过角色id查询角色和菜单关联
     *
     * @param roleId 角色id
     * @param source
     * @return
     */
    @GetMapping("/roleMenu/getByRoleId/{roleId}")
    public R<List<SysRoleMenu>> getInfoByRoleId(@PathVariable("roleId") Long roleId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过菜单id查询角色和菜单关联
     *
     * @param menuId 菜单id
     * @param source
     * @return
     */
    @GetMapping("/roleMenu/getByMenuId/{menuId}")
    public R<List<SysRoleMenu>> getInfoByMenuId(@PathVariable("menuId") Long menuId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
