package com.ruoyi.system.api.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.excel.annotation.ExcelDictFormat;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * 数据角色功能授权对象 sys_role_menu_power
 *
 * @author zly
 * @date 2023-05-23
 */
@Data
@TableName("sys_role_menu_power")
public class SysRoleMenuPower extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 数据角色功能授权ID
     */
    @TableId(value = "rmp_id", type = IdType.AUTO)
    private Long rmpId;

    /**
     * 角色ID
     */
    @ExcelProperty(value = "角色ID")
    private Long roleId;

    /**
     * 菜单id
     */
    @ExcelProperty(value = "菜单id")
    private Long menuId;

    /**
     * 授权值
     */
    @ExcelProperty(value = "授权值")
    private String powerValue;

    /**
     * 排序
     */
    @ExcelProperty(value = "排序")
    private Integer sort;

    /**
     * 状态（1启用
     */
    @ExcelProperty(value = "状态")
    @ExcelDictFormat(readConverterExp = "1=启用,0=停用")
    private Integer status;

    /**
     * 角色名称
     */
    @ExcelProperty(value = "角色名称")
    @TableField(exist = false)
    private String roleName;

    /**
     * 菜单名称
     */
    @ExcelProperty(value = "菜单名称")
    @TableField(exist = false)
    private String menuName;

    public void setRmpId(Long rmpId) {
        this.rmpId = rmpId;
    }

    public Long getRmpId() {
        return rmpId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setPowerValue(String powerValue) {
        this.powerValue = powerValue;
    }

    public String getPowerValue() {
        return powerValue;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getSort() {
        return sort;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuName() {
        return menuName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("rmpId", getRmpId())
                .append("roleId", getRoleId())
                .append("menuId", getMenuId())
                .append("powerValue", getPowerValue())
                .append("sort", getSort())
                .append("status", getStatus())
                .append("roleName", getRoleName())
                .append("menuName", getMenuName())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
