package com.ruoyi.system.api.factory;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.RemoteRoleService;
import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.system.api.domain.SysRoleMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户服务降级处理
 *
 * @author ruoyi
 */
@Component
public class RemoteRoleFallbackFactory implements FallbackFactory<RemoteRoleService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteRoleFallbackFactory.class);

    @Override
    public RemoteRoleService create(Throwable throwable) {
        log.error("角色服务调用失败:{}", throwable.getMessage());
        return new RemoteRoleService() {
            @Override
            public R<SysRole> getRoleKey(String roleKey, String source) {
                return R.fail("获取角色失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysRoleMenu>> getInfoByRoleId(Long roleId, String source) {
                return R.fail("获取角色和菜单失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysRoleMenu>> getInfoByMenuId(Long menuId, String source) {
                return R.fail("获取角色和菜单失败:" + throwable.getMessage());
            }
        };
    }
}
