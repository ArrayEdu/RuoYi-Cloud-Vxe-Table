package com.ruoyi.system.api.factory;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.RemoteRoleMenuPowerService;
import com.ruoyi.system.api.domain.SysRoleMenuPower;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 数据角色功能授权服务降级处理
 *
 * @author zly
 */
@Component
public class RemoteRoleMenuPowerFallbackFactory implements FallbackFactory<RemoteRoleMenuPowerService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteRoleMenuPowerFallbackFactory.class);

    @Override
    public RemoteRoleMenuPowerService create(Throwable throwable) {
        log.error("数据角色功能授权服务调用失败:{}", throwable.getMessage());
        return new RemoteRoleMenuPowerService() {
            @Override
            public R<SysRoleMenuPower> getRoleMenuPower(SysRoleMenuPower sysRoleMenuPower, String source) {
                return R.fail("获取数据角色功能授权失败:" + throwable.getMessage());
            }
        };
    }
}
