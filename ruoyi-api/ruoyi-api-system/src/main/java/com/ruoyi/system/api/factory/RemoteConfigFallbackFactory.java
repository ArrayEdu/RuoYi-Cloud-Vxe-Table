package com.ruoyi.system.api.factory;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.RemoteConfigService;
import com.ruoyi.system.api.domain.SysConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 系统配置降级处理
 *
 * @author zly
 */
@Component
public class RemoteConfigFallbackFactory implements FallbackFactory<RemoteConfigService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteConfigFallbackFactory.class);

    @Override
    public RemoteConfigService create(Throwable throwable) {
        log.error("系统配置服务调用失败:{}", throwable.getMessage());
        return new RemoteConfigService() {
            @Override
            public R<SysConfig> getConfigKey(String configKey, String source) {
                return R.fail("获取用户失败:" + throwable.getMessage());
            }
        };
    }
}
